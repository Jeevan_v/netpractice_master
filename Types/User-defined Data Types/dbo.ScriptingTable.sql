CREATE TYPE [dbo].[ScriptingTable] AS TABLE
(
[VisitID] [int] NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ItemNo] [int] NULL,
[DosageDetails] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
