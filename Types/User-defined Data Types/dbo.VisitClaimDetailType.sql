CREATE TYPE [dbo].[VisitClaimDetailType] AS TABLE
(
[ClaimID] [int] NULL,
[VisitID] [int] NULL,
[Quantity] [int] NULL,
[Amount] [decimal] (18, 2) NULL,
[LineDefination] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DispFee] [decimal] (18, 2) NULL,
[NoDays] [int] NULL,
[IsChronic] [bit] NULL,
[Code] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Diagnosis] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[DosageID] [int] NULL,
[DailyDosage] [decimal] (18, 2) NULL,
[Starttime] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Endtime] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Memo] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[Size_Desc] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Dose_Desc] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DoseForm] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DoseFreq_Desc] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RouteLocation_Desc] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OtherInst_Desc] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TimePeriod_Desc] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
