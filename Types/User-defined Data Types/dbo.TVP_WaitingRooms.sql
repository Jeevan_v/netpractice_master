CREATE TYPE [dbo].[TVP_WaitingRooms] AS TABLE
(
[WaitingRoomID] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([WaitingRoomID])
)
GO
