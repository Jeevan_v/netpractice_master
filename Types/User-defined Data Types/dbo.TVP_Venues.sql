CREATE TYPE [dbo].[TVP_Venues] AS TABLE
(
[VenueID] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([VenueID])
)
GO
