CREATE TYPE [dbo].[TVP_Privileges] AS TABLE
(
[PrivilegeID] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([PrivilegeID])
)
GO
