CREATE TYPE [dbo].[VisitClaimDetailType1] AS TABLE
(
[Quantity] [int] NULL,
[Amount] [numeric] (13, 4) NULL,
[LineDefination] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DispFee] [numeric] (13, 4) NULL,
[NoDays] [int] NULL,
[IsChronic] [bit] NULL,
[Code] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Diagnosis] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[DosageID] [int] NULL,
[DailyDosage] [numeric] (6, 2) NULL,
[Starttime] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Endtime] [varchar] (10) COLLATE Latin1_General_CI_AS NULL
)
GO
