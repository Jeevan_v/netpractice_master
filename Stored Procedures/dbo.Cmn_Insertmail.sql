SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Proc [dbo].[Cmn_Insertmail]
@ToEmailID varchar(250),
@FromEmailID varchar(250),
@Subject varchar(250),
@Body nvarchar(max),
@EmailType int,
@IsSent bit
As
Begin
Insert into Mail(ToEmailId,
FromEmailID ,
[Subject],
[Body],
EmailType,
DateLogged,
IsSent)values(@ToEmailID,
@FromEmailID,
@Subject,
@Body,
@EmailType,
GETDATE(),
@IsSent)
End
GO
