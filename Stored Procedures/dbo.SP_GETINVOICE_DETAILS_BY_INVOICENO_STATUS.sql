SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_GETINVOICE_DETAILS_BY_INVOICENO_STATUS] 
	-- Add the parameters for the stored procedure here
	@InvoiceNo int , 
	@Status Varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	select  distinct cc.InvoiceNo , cc.claimid , vv.VisitID , TotalAmount, vv.VisitDate , vv.VenueID, uv.PhysicalAddressID, uv.PostalAddressID , acc.PatientID,acc.PracticeID,  prac.BankID  
		from claim cc with (nolock) 
		inner join ClaimDetail cd with(nolock) on cd.ClaimID = cc.ClaimID
		inner join Visit vv with(nolock) on vv.VisitID = cc.VisitID 
		inner join Account acc with(nolock) on acc.AccountID = vv.AccountID 
		inner join Practice prac with (nolock) on acc.PracticeID = prac.PracticeID
		inner join Venue uv with(nolock ) on vv.VenueID = uv.VenueID
		where 
		upper(cc.status) like upper(@STATUS)
		AND
		 CC.InvoiceNo = @INVOICENO  order by 1 desc

END


GO
