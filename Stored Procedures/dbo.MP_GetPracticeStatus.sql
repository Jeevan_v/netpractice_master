SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetPracticeStatus]
	@PracticeID int
AS

SELECT
	p.IsActive,
	ir.ReasonID,
	ir.Reason
FROM 
Practice p 

INNER JOIN 
InActiveReason ir

ON

p.ReasonID = ir.ReasonID

where 
p.PracticeID=@PracticeID

GO
