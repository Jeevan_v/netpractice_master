SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Sameer>
-- Create date: <4/12/2012>
-- Description:	<Get Citites by Province>
-- =============================================
CREATE PROCEDURE [dbo].[CHE_GetCitiesByProvince] 
AS
BEGIN
SELECT 
p.ProvinceID,cty.CityID,cty.City
FROM 
City cty
INNER JOIN 
ProvinceCityRelationship pcr
ON
cty.CityID=pcr.CityID
INNER JOIN Province p
ON
p.ProvinceID=pcr.ProvinceID

END

GO
