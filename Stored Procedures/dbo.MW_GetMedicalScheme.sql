SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetMedicalScheme]
AS
SELECT
	MedicalSchemeID,
	SchemeName
	FROM
	MedicalScheme ms
	JOIN 
	T_MFAdmin ma
	ON ms.MFAdminID=ma.MFAdmin_ID
	WHERE
	ma.Discontinued = 0
	AND
	ms.Discontinued = 0	
Order By SchemeName

GO
