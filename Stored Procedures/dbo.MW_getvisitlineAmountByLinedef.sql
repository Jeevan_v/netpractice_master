SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_getvisitlineAmountByLinedef]    

 @Type varchar(2),    

 @VisitID int    

AS    

BEGIN    

  if(@type='P')    

   Begin    

 select SUM(Amount)as TotalAmount     

    from VisitLine    

 where LineDefinition in('procedure','Material','Consultation','Modifier')and  VisitID=@VisitID    

 End    

 Else if(@type='MC')  

 Begin    

    Select SUM(Amount)as Totalmed    

       from VisitLine where LineDefinition like 'Medicine' and  VisitID=@VisitID   and IsChronic=1 

    END  
	
	else
	begin
	 Select SUM(Amount)as Totalmed    

       from VisitLine where LineDefinition like 'Medicine' and  VisitID=@VisitID and IsChronic=0
	end  

    

    

END 
GO
