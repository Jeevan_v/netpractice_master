SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[CheckIfReportsSelected]     

 @PracticeID int    

AS    

BEGIN  
SET NOCOUNT ON;    
SELECT ISNULL(Approved,'') from PracticePackageRelationship where PracticeId = @PracticeID 
and PackageID = (select PackageId from Package where Package ='FINANCIAL REPORTS') AND Subscribed =1   

END 
GO
