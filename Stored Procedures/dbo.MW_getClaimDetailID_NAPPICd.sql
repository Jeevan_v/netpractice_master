SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 18/12/2014
-- Description:	This procedure will be userd to fetch claimdetaild id based on climd id and NAPPI Cd
-- =============================================
CREATE PROCEDURE [dbo].[MW_getClaimDetailID_NAPPICd] 
	-- Add the parameters for the stored procedure here
	 @claimID int,
	@chargeCode nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT VisitLineID from ClaimDetail where
	ClaimID= @claimID and NappiCode =@chargeCode

END
GO
