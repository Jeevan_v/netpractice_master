SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[CMN_GetMailsforActivatedPractice]

as

begin

select m.ToEmailId, m.FromEmailID, m.[Subject],m.Body ,m.id

from mail m 

where IsSent=0 and m.EmailType=1

end
GO
