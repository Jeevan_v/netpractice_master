SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_UpdateCountofProcessedFile]
		@ClaimFileName varchar(Max) = NULL
AS
Begin
	
	/* Increment File counter */
	 update cx
	 set [Counter] =ISNULL(Counter,0) +1
	from claimxml cx
	where   cx.XMLFileName = @ClaimFileName
 END
GO
