SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sameer>
-- Create date: <18/12/2012>
-- Description:	<Get Patient Details By PatientID>
-- =============================================
CREATE PROCEDURE [dbo].[MW_GetPatientDetailForConsultation] 
	@PatientID int
   ,@PracticeID int
AS
IF @PracticeID NOT IN (0)
BEGIN
	SELECT
	   p.[PatientID]
	  ,a.[AccountID]
	  ,a.[PracticeID] 
	  ,p.[MembershipID]
      ,p.[IDNo]
      ,p.[TitleID]
      ,UPPER(LEFT(p.[FirstName],1))+LOWER(SUBSTRING(p.[FirstName],2,LEN(p.[FirstName]))) as [FirstName]
      ,UPPER(LEFT(p.[LastName],1))+LOWER(SUBSTRING(p.[LastName],2,LEN(p.[LastName]))) as [LastName]
      ,p.[Initials]
      ,Replace(convert(Date,P.DOB),'-','')as  DOB
      ,p.[PhysicalAddressID]
      ,p.[PostalAddressID]
      ,p.[TelNo]
      ,p.[CellNo]
      ,p.[FaxNo]
      ,p.[GenderID]
      ,p.[RaceID]
      ,p.[LanguageID]
      ,p.[DependendNo]
      ,p.[IsDependant]
      ,p.[MainMemberID]
      ,p.[RelationShipID]
      ,p.[ChargeCategoryID]
      ,p.[ChargeTypeID]
      ,p.[PlanID]
      ,p.[SchemeID]
      ,p.[PatientHeight]
      ,p.[PatientWeight]
      ,p.[EmergencyContName]
      ,p.[EmergencyTelNo]
      ,p.[PassportNo]
      ,p.[AllergyTypeID]
      ,p.[MedicalAidNo]
      ,p.[FMCheckDate]
      ,p.[AllergyTypeOther]
      ,p.countryID
	  ,p.companyaddressID
	  ,p.companyname
	  ,p.companyvatno
	  ,p.patienttype
	  ,vf.[FileNo] 
      ,vf.VenueFileID
      ,mp.FirstName as MainMemberName  
	  ,  t.Title as TitleName
   FROM Patient p
   INNER JOIN
   Account a
   ON p.PatientID=a.PatientID
   INNER JOIN
   VenueFile vf
   ON a.AccountID=vf.AccountID
    INNER JOIN
   Patient mp
   ON mp.PatientID=p.MainMemberID
   INNER JOIN
   [Title] t
   ON t.TitleID = p.TitleID
   WHERE P.PatientID=@PatientID 
   AND
   a.PracticeID=@PracticeID     
END
ELSE
BEGIN
SELECT
       TOP 1
	   p.[PatientID]
	  ,a.[AccountID]
	  ,a.[PracticeID] 
	  ,p.[MembershipID]
      ,p.[IDNo]
      ,p.[TitleID]
      ,UPPER(LEFT(p.[FirstName],1))+LOWER(SUBSTRING(p.[FirstName],2,LEN(p.[FirstName]))) as [FirstName]
      ,UPPER(LEFT(p.[LastName],1))+LOWER(SUBSTRING(p.[LastName],2,LEN(p.[LastName]))) as [LastName]
      ,p.[Initials]
      ,Replace(convert(Date,p.DOB),'-','')as  DOB
      ,p.[PhysicalAddressID]
      ,p.[PostalAddressID]
      ,p.[TelNo]
      ,p.[CellNo]
      ,p.[FaxNo]
      ,p.[GenderID]
      ,p.[RaceID]
      ,p.[LanguageID]
      ,p.[DependendNo]
      ,p.[IsDependant]
      ,p.[MainMemberID]
      ,p.[RelationShipID]
      ,p.[ChargeCategoryID]
      ,p.[ChargeTypeID]
      ,p.[PlanID]
      ,p.[SchemeID]
      ,p.[PatientHeight]
      ,p.[PatientWeight]
      ,p.[EmergencyContName]
      ,p.[EmergencyTelNo]
      ,p.[PassportNo]
      ,p.[AllergyTypeID]
      ,p.[MedicalAidNo]
      ,p.[FMCheckDate]
      ,p.[AllergyTypeOther]
	  ,p.countryID
	  ,p.companyaddressID
	  ,p.companyname
	  ,p.companyvatno
	  ,p.patienttype
      ,vf.[FileNo] 
      ,vf.VenueFileID
      ,mp.FirstName as MainMemberName    
	  ,     t.Title  as TitleName
   FROM Patient p
   INNER JOIN
   Account a
   ON p.PatientID=a.PatientID
   INNER JOIN
   VenueFile vf
   ON a.AccountID=vf.AccountID
    INNER JOIN
   Patient mp
   ON mp.PatientID=p.MainMemberID
     INNER JOIN
   [Title] t
   ON t.TitleID = p.TitleID
   WHERE P.PatientID=@PatientID 
END
GO
