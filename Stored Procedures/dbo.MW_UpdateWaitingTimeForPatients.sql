SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[MW_UpdateWaitingTimeForPatients]
(
@DoctorID INT,@Doctor VARCHAR(60),
@VenueID INT,@waitingRoomID INT,
@PracticeID INT,@PatientID INT
)
--@WaitingRoomIDs TVP_WaitingRooms READONLY)
AS
BEGIN
-- Get Average consultation time for doctor 
DECLARE @AverageTime VARCHAR(20)
DECLARE @Temp TABLE(AverageTime VARCHAR(20))
INSERT @Temp EXEC dbo.MW_GetAverageAppointmentTimeForDoctor @DoctorID
SELECT @AverageTime= AverageTime FROM @Temp

-- Get Waiting Room ID for the Patient 
IF @waitingRoomID = 0
BEGIN 
SELECT @waitingRoomID=WaitingRoomID FROM WaitingRoom 
WHERE  
AccountID=(SELECT AccountID FROM Account WHERE PracticeID=@PracticeID AND PatientID=@PatientID)
AND
VenueID= @VenueID
AND
CONVERT(DATE,DateLogged)= CONVERT(DATE,GETDATE())
END

-- Update waiting time for other patient in queue
UPDATE WaitingRoom SET [Time] 
= CONVERT(varchar(10),(CONVERT(DECIMAL(36,2),[Time]) - CONVERT(DECIMAL(36,2),@AverageTime)))
WHERE
WaitingRoomID
IN(
SELECT WaitingRoomID  
FROM WaitingRoom 
WHERE Doctor=@Doctor 
AND VenueID=@VenueID
AND CONVERT(DATE,DateLogged)= CONVERT(DATE,GETDATE())
AND	WaitingRoomID > @WaitingRoomID
)
END


GO
