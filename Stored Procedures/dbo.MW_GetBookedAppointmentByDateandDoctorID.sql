SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetBookedAppointmentByDateandDoctorID]
@Date date,
@DoctorID int
AS
BEGIN
SELECT
a.IsBooked,
'APPOINTMENT ID: ' + CAST(a.AppointmentID AS VARCHAR(100)) AS AppointmentID,
'PATIENT NAME: ' + p.FirstName +' '+ p.LastName AS PatientName,
'ADDRESS: ' + ad.AddressLine1 + ' ' +ad.AddressLine2 AS [Address],
'APPOINTMENT TIME: ' + a.StartTime + ' - ' + a.EndTime AS AppointmentTime,
'VENUE: '+v.VenueName AS VenueName,
(case when ar.RoleName <> 'Super Admin' then 'PRACTITIONER NAME: '+u.FirstName+' '+u.LastName 
else 'ADMIN NAME: '+u.FirstName+' '+u.LastName end) AS DoctorName,
(CASE WHEN p.TelNo <> '' THEN ('TELEPHONE-NO: ' + p.TelNo) ELSE p.TelNo  END) as TelNo,
p.PatientID
FROM Appointment a INNER JOIN Patient p ON a.PatientID=p.PatientID 
INNER JOIN [Address] ad ON p.PhysicalAddressID=ad.AddressID 
INNER JOIN Venue v ON v.VenueID=a.VenueID
INNER JOIN [User] u ON U.UserID=a.DoctorID
INNER JOIN [aspnet_UsersInRoles] ur on ur.UserId=u.MembershipID
INNER JOIN [aspnet_Roles] ar on ur.RoleId=ar.RoleId
WHERE [date]=@Date AND DoctorID=@DoctorID AND a.IsBooked=1
ORDER BY StartTime

SELECT COUNT(AppointmentID) AS DueToday
FROM Appointment 
WHERE ([Date]= CONVERT(DATE,GETDATE()) AND IsBooked=1 AND StartTime>=CONVERT(TIME,GETDATE()) AND DoctorID=@DoctorID  ) 

SELECT COUNT(AppointmentID) AS DueTomorrow
FROM Appointment 
WHERE [Date]= CONVERT(DATE,GETDATE()+1) AND IsBooked=1  AND DoctorID=@DoctorID

select FirstName + ' '+LastName as Name from [User] where UserID=@DoctorID

END
GO
