SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
CREATE PROCEDURE [dbo].[MW_GetConsultationHistory]  
  
@PracticeID int,  
@PatientID int,  
@StartDate DATE,  
@EndDate DATE    
AS  
  
BEGIN  
  
	SELECT   
			 v.[VisitID] ,
			 convert(date,[VisitDate])as VisitDate,     
			 Sum(vl.[Amount]) AS Amount,   
			 c.[Status],
			  CONVERT(Decimal(30,2),ROUND((SELECT ISNULL(SUM([WriteOffAmount]),0) FROM WS_WriteOff AS AP2 WHERE AP2.VisitID = v.VisitID),2)) AS WriteOff 
	FROM  
			Visit v  
	JOIN  
			VisitLine vl   ON  v.VisitID=vl.VisitID
	JOIN   
			Claim c  ON  v.VisitID =  c.VisitID  
	WHERE    
			 v.AccountID in ( SELECT   AccountID   FROM   Account   WHERE   PracticeID =  @PracticeID  AND   PatientID = @PatientID)  
			 AND CONVERT(date,v.VisitDate) BETWEEN  CONVERT(DATE,@StartDate) AND CONVERT(DATE,@EndDate) 
	GROUP BY    
			convert(date,[VisitDate]), v.[VisitID], c.[Status]
	ORDER BY 
			convert(date,VisitDate) desc, VisitID desc  
   
END  
GO
