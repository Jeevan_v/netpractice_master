SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_PAYMENTSRECEIVED] @DATE DATETIME,@PRACTICEID INT, @DOCTORID INT, @RPTFREQUENCY INT                       
AS     
    
declare @CURRMONTH_STDATE DATETIME      
declare @CURRMONTH_ENDDATE DATETIME      
declare @STDATE_FREQUENCY DATETIME      
declare @ENDDATE_FREQUENCY DATETIME        
declare @ENDDATE_TEMP_FREQUENCY DATETIME          
-- declare @PRACTICEID numeric     
--declare @DOCTORID numeric
--declare @DATE DATETIME
--declare @RPTFREQUENCY numeric

--SET @PRACTICEID=0
--set @DOCTORID=0
--SET @DATE='06/03/2013' 

--set @RPTFREQUENCY=3  
----CALCULATE CURRERNT MONTH DATE      
SET @CURRMONTH_STDATE = CONVERT(DATETIME,CONVERT(VARCHAR(20),CONVERT(VARCHAR(20),MONTH(@DATE))+'/01/'+CONVERT(VARCHAR(20),YEAR(@DATE))))      
SET @CURRMONTH_ENDDATE = @DATE      
----------------      
      
----------CALCULATE STDATE AND END DATE BASED ON FREQUENCY      
IF @RPTFREQUENCY = 1      
BEGIN      
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-3,@DATE)      
END      
ELSE IF @RPTFREQUENCY = 2      
BEGIN      
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-6,@DATE)      
END      
ELSE      
BEGIN      
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-12,@DATE)      
END      
      
      
SET @STDATE_FREQUENCY = CONVERT(DATETIME,CONVERT(VARCHAR(20),CONVERT(VARCHAR(20),MONTH(@ENDDATE_TEMP_FREQUENCY)) + '/01/' + CONVERT(VARCHAR(20),YEAR(@ENDDATE_TEMP_FREQUENCY))))      
      
SET @ENDDATE_FREQUENCY = DATEADD(DD,-1,@CURRMONTH_STDATE)      
      
      
-------------------------------      
        
BEGIN     
 SELECT DISTINCT 'A_SCHEME' AS ITEM_TYPE,    
 MS.MedicalSchemeID  AS ItemID,        
 MS.SchemeName AS ItemName,                 
 PR.PRACTICEID ,         
 VS.DOCTORID ,                        
 PR.PRACTICENAME PRACTICE,                
 (USR.FIRSTNAME+ ' '+USR.LASTNAME) AS DOCTOR,        
        
 SUM(CASE WHEN P.[DATE] >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE          
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  CURRENTAMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-1,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-1,@DATE)) AND @RPTFREQUENCY >=1 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON1_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-2,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-2,@DATE)) AND @RPTFREQUENCY >=1   THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON2_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-3,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-3,@DATE)) AND @RPTFREQUENCY >=1   THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON3_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-4,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-4,@DATE))  AND @RPTFREQUENCY >=2  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON4_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-5,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-5,@DATE)) AND @RPTFREQUENCY >=2  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON5_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-6,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-6,@DATE)) AND @RPTFREQUENCY >=2  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON6_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-7,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-7,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON7_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-8,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-8,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON8_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-9,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-9,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON9_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-10,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-10,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON10_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-11,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-11,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON11_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-12,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-12,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON12_AMT,   
   
 SUM(CASE WHEN P.[DATE] < @ENDDATE_FREQUENCY       
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END) OLDER,        
         
 SUM(CASE WHEN P.[DATE] >= @CURRMONTH_STDATE and P.[DATE] <=  @CURRMONTH_ENDDATE          
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)      
 +      
 SUM(CASE WHEN P.[DATE] >= @STDATE_FREQUENCY and P.[DATE] <=  @ENDDATE_FREQUENCY          
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END) TOTAL_AMT        
         
 FROM        
 VISIT VS      
 INNER JOIN Account A ON VS.AccountID = A.AccountID          
 INNER JOIN Patient  PT ON A.PatientID = PT.PatientID     
 INNER JOIN PAYMENT P ON VS.VisitID = P.VisitID     
 INNER JOIN VENUE VEN ON VEN.VENUEID = VS.VENUEID                    
 INNER JOIN PRACTICE PR ON PR.PRACTICEID = VEN.PRACTICEID                  
 INNER JOIN [USER] USR ON USR.USERID = VS.DOCTORID 
    --Changed for Checking Doctor roles only
 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID       
 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID  
  -- Changed  For Incorrect MApping of User and Practice
 INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID 
                   
 INNER JOIN MEDICALSCHEME MS ON MS.MEDICALSCHEMEID = VS.SCHEMEID           
WHERE  
 --VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE       
       
 --AND  
 CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 end =                   
      CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 end              
                     
 AND  CASE WHEN @DOCTORID <> 0 THEN VS.DOCTORID ELSE 1 end =                   
      CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 end          
      --Changes done for incorrect mapping         
  AND  UVR.VenueID= VS.VenueID     
   AND
    --Checking doctor (gowsi)
  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor') 
  AND
 --changes done to get the income by only scheme wise
    P.DESCRIPTION='Medical Aid'  
     GROUP BY                   
 VS.DOCTORID ,PR.PRACTICENAME,                   
 USR.FIRSTNAME,USR.LASTNAME,MS.SchemeName,MS.MedicalSchemeID,        
 PR.PRACTICEID,VS.DOCTORID,USR.FIRSTNAME,USR.LASTNAME  
    

UNION       
    
 SELECT DISTINCT 'B_PATIENT' AS ITEM_TYPE,    
 PT.PatientID  AS ItemID,        
 PT.FirstName+ ' ' + PT.LastName AS ItemName,    
 PR.PRACTICEID ,         
 VS.DOCTORID ,                        
 PR.PRACTICENAME PRACTICE,                
 (USR.FIRSTNAME+ ' '+USR.LASTNAME) AS DOCTOR,        
        
 SUM(CASE WHEN P.[DATE] >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE          
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  CURRENTAMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-1,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-1,@DATE)) AND @RPTFREQUENCY >=1 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON1_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-2,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-2,@DATE)) AND @RPTFREQUENCY >=1   THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON2_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-3,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-3,@DATE)) AND @RPTFREQUENCY >=1   THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON3_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-4,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-4,@DATE))  AND @RPTFREQUENCY >=2  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON4_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-5,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-5,@DATE)) AND @RPTFREQUENCY >=2  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON5_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-6,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-6,@DATE)) AND @RPTFREQUENCY >=2  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON6_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-7,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-7,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON7_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-8,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-8,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON8_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-9,@DATE))         AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-9,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON9_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-10,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-10,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON10_AMT,        
        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-11,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-11,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON11_AMT,        
        
 SUM(CASE WHEN MONTH(P.[DATE]) = MONTH(DATEADD(MM,-12,@DATE))          
 AND         
 YEAR(P.[DATE])  = YEAR(DATEADD(MM,-12,@DATE)) AND @RPTFREQUENCY =3  THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)  MON12_AMT,  
   
 SUM(CASE WHEN P.[DATE] < @ENDDATE_FREQUENCY       
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END) OLDER,          
         
 SUM(CASE WHEN P.[DATE] >= @CURRMONTH_STDATE and P.[DATE] <=  @CURRMONTH_ENDDATE          
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END)      
 +      
 SUM(CASE WHEN P.[DATE] >= @STDATE_FREQUENCY and P.[DATE] <=  @ENDDATE_FREQUENCY          
 THEN         
 ISNULL(P.AMOUNTPAID,0) ELSE 0 END) TOTAL_AMT        
         
 FROM        
 VISIT VS      
 INNER JOIN Account A ON VS.AccountID = A.AccountID          
 INNER JOIN Patient  PT ON A.PatientID = PT.PatientID     
 INNER JOIN PAYMENT P ON VS.VisitID = P.VisitID     
 INNER JOIN VENUE VEN ON VEN.VENUEID = VS.VENUEID                    
 INNER JOIN PRACTICE PR ON PR.PRACTICEID = VEN.PRACTICEID                  
 INNER JOIN [USER] USR ON USR.USERID = VS.DOCTORID   
     --Changed for Checking Doctor roles only
 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID       
 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID
  -- Changed  For Incorrect MApping of User and Practice
 INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID 
                 
 INNER JOIN MEDICALSCHEME MS ON MS.MEDICALSCHEMEID = VS.SCHEMEID           
 WHERE     
 --VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE       
       
 --AND 
  CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 end =                   
      CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 end              
                     
 AND  CASE WHEN @DOCTORID <> 0 THEN VS.DOCTORID ELSE 1 end =                   
      CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 end          
         --Changes done for incorrect mapping         
 AND  UVR.VenueID= VS.VenueID
 AND
    --Checking doctor (gowsi)
  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor')   
   AND
   --changes done for getting income by only patient
    P.DESCRIPTION='Patient payable'       
 GROUP BY                   
 VS.DOCTORID ,PR.PRACTICENAME,                   
 USR.FIRSTNAME,USR.LASTNAME,    
 PR.PRACTICEID ,VS.DOCTORID,USR.FIRSTNAME,USR.LASTNAME,    
 PT.PatientID,    
 PT.FirstName,PT.LastName          
 order by ITEM_TYPE,ItemName,PRACTICENAME,DOCTOR    
end
GO
