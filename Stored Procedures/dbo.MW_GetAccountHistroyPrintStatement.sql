SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetAccountHistroyPrintStatement]
@PatientID INT , @PracticeID INT,  @FromDate NVARCHAR(30), @ToDate NVARCHAR(30)  
  
AS  
BEGIN  

DECLARE @FDate NVARCHAR(30)
IF (@FromDate = '')
	BEGIN
		SELECT @FromDate =   CONVERT(DATETIME,MIN(VisitDate),103) FROM(
						SELECT TOP 5 (v.[VisitDate]) 
						FROM Visit v   
						JOIN  Account a ON  a.AccountID = v.AccountID 
						AND v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID )
						ORDER BY v.InvoiceNo DESC
						)AA		
		SET @FDate = CONVERT(VARCHAR(12),CONVERT(DATETIME, @FromDate),103)
	END	
ELSE
	BEGIN
		SET @FDate = @FromDate
	END

IF (@ToDate = '')
	BEGIN	
		SELECT @ToDate = GETDATE()	
		--SELECT @ToDate =  CONVERT(DATETIME,MAX(VisitDate),103) FROM(
		--				SELECT TOP 5 (v.[VisitDate]) 
		--				FROM Visit v   
		--				JOIN  Account a ON  a.AccountID = v.AccountID 
		--				AND v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID )
		--				ORDER BY v.InvoiceNo DESC
		--				)AA
	END
 SELECT   
  'Opening Balance' AS [Activity],  
  NULL AS [InvoiceNo],   
  @FDate AS VisitDate,   
  NULL AS [TOTALAMOUNT],  
  NULL  AS [PAYMENT],
  
  --******** Write off is not needed to calculate the opening balance ****
  --SUM(ISNULL([TotalAmount],0) -  ISNULL([Payment],0) - ISNULL([WriteOff],0)) as [Balance], 
  SUM(ISNULL([TotalAmount],0) -  ISNULL([Payment],0)) as [Balance],
  --******** Below line is commented and as write off is not needed to calculate the opening balance ****
  
  NULL AS [Duedate],  
  NULL AS [VisitID],  
  NULL AS [VDate],
  NULL AS [PaymentDate], 
  1 AS [Pref]  
 FROM  
   (  
  SELECT  
	v.InvoiceNo,
	SUM(ISNULL(c.[TotalAmount],0)) AS [TotalAmount],
	(SELECT SUM(ISNULL(AmountPaid,0)) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID AND CONVERT(DATETIME,Pa.[Date],103) < CONVERT(DATETIME,@FromDate,103)) AS [Payment]
	
	--******** Write off is not needed to calculate the opening balance ****
	--(SELECT SUM(ISNULL(WriteOffAmount,0)) FROM WS_WriteOff AS AP2 WHERE AP2.VisitID = v.VisitID AND CONVERT(DATETIME,AP2.[Date],103) < CONVERT(DATETIME,@FromDate,103)) AS [WriteOff]
	--******** Write off is not needed to calculate the opening balance ****
 FROM     
	  Visit v    
	  JOIN  Claim c  ON c.VisitID = v.VisitID    
	  JOIN  Account a ON  a.AccountID = v.AccountID  
 WHERE     
	v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID )   
	AND  CONVERT(DATETIME,v.VisitDate,103)  < CONVERT(DATETIME,@FromDate,103)  
 GROUP BY 
	v.[VisitID],c.[AmountPaid],v.[InvoiceNo]  
 )AA
   
 UNION ALL  
   
 SELECT [INVOICE] AS [Activity],InvoiceNo AS [InvoiceNo],[VISITDATE],[TOTALAMOUNT] AS [TOTALAMOUNT],[PAYMENT] AS [PAYMENT],[BALANCE] AS [Balance],[Duedate],[VisitID],[VDate],[PaymentDate],[Pref] FROM  
 (  
 SELECT  
	  'Invoice #INV- ' + CONVERT(VARCHAR,v.InvoiceNo) AS [INVOICE],  
	  v.InvoiceNo,
	  CONVERT(VARCHAR(12),v.[VisitDate],103) AS [VISITDATE],   
	  CONVERT(Decimal(30,2),ROUND(c.[TotalAmount],2) - ISNULL(Wf.WriteOffAmount,0)) AS [TOTALAMOUNT],  
	  NULL AS [PAYMENT],  	   
	  NULL AS [BALANCE],  
	  CONVERT(VARCHAR(20),DATEADD(DAY,   
		(  
		SELECT   
		CASE [PaymentDuration]  
		 WHEN '30 Days' THEN 30  
		 WHEN '60 Days' THEN 60  
		 WHEN '90 Days' THEN 90  
		END   
	   FROM   
		[PaymentDuration]   
	   WHERE   
		[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]),103)  
	  AS [Duedate],  
	  v.[VisitID],  
	  v.[VisitDate] AS [VDate],
	  NULL AS PaymentDate,
	  2 AS [Pref]  
 FROM  
	  Visit v    
	  JOIN  Claim c  ON c.VisitID = v.VisitID    
	  JOIN  Account a ON  a.AccountID = v.AccountID  
	  LEFT JOIN WS_WriteOff Wf ON Wf.VisitID = v.[VisitID] AND ISNULL(Wf.WriteOffAmount,0) > 0
 WHERE     
	   v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID )   
	   AND   CONVERT(DATETIME,v.VisitDate,103) BETWEEN   CONVERT(DATETIME,@FromDate,103) AND CONVERT(DATETIME,@ToDate,103)  
   
 UNION ALL  
   
 SELECT  
	  'Payment on Invoice #INV- ' + CONVERT(VARCHAR,v.InvoiceNo) AS [INVOICE],  
	  v.InvoiceNo,
	  CONVERT(VARCHAR(12),v.[VisitDate],103) AS [VISITDATE],  
	  NULL AS [TOTALAMOUNT],
	  CONVERT(Decimal(30,2),ISNULL(Pa.AmountPaid,0)) AS [PAYMENT],	  
	  NULL AS [BALANCE],  
	  CONVERT(VARCHAR(20),DATEADD(DAY,   
		(  
		SELECT   
		CASE [PaymentDuration]  
		 WHEN '30 Days' THEN 30  
		 WHEN '60 Days' THEN 60  
		 WHEN '90 Days' THEN 90  
		END   
	   FROM   
		[PaymentDuration]   
	   WHERE   
		[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]),103) 
	  AS [Duedate],  
	  v.[VisitID],  
	  v.[VisitDate] AS [VDate],  
	  Pa.[Date] AS PaymentDate,
	  3 AS [Pref]  
 FROM  
	  Visit v    
	  JOIN  Claim c  ON c.VisitID = v.VisitID    
	  JOIN  Account a ON  a.AccountID = v.AccountID 
	  LEFT JOIN Payment Pa ON Pa.VisitID = v.[VisitID] 
 WHERE     
	   v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID )   
	   --AND   CONVERT(DATETIME,v.VisitDate,103) BETWEEN   CONVERT(DATETIME,@FromDate,103) AND CONVERT(DATETIME,@ToDate,103)	   
	   AND   CONVERT(DATETIME,Pa.Date,103) BETWEEN   CONVERT(DATETIME,@FromDate,103) AND CONVERT(DATETIME,@ToDate,103)	   
	   AND ISNULL(Pa.AmountPaid,0) > 0

/******** Write off is not needed to calculate the opening balance ****	   
UNION ALL  
   
SELECT  
	  'Write off on Invoice #INV- ' + CONVERT(VARCHAR,v.InvoiceNo) AS [INVOICE],  
	  v.InvoiceNo,
	  CONVERT(VARCHAR(12),v.[VisitDate],103) AS [VISITDATE],  
	  NULL AS [TOTALAMOUNT],  	  
	  CONVERT(Decimal(30,2),ISNULL(Wf.WriteOffAmount,0)) AS [PAYMENT],	  
	  NULL AS [BALANCE],  
	  CONVERT(VARCHAR(20),DATEADD(DAY,   
		(  
		SELECT   
		CASE [PaymentDuration]  
		 WHEN '30 Days' THEN 30  
		 WHEN '60 Days' THEN 60  
		 WHEN '90 Days' THEN 90  
		END   
	   FROM   
		[PaymentDuration]   
	   WHERE   
		[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]),103) 
	  AS [Duedate],  
	  v.[VisitID],  
	  v.[VisitDate] AS [VDate],  
	  Wf.[Date] AS PaymentDate,
	  4 AS [Pref]  
 FROM  
	  Visit v    
	  JOIN  Claim c  ON c.VisitID = v.VisitID    
	  JOIN  Account a ON  a.AccountID = v.AccountID 
	  LEFT JOIN WS_WriteOff Wf ON Wf.VisitID = v.[VisitID] 
 WHERE     
	   v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID )   
	   AND   CONVERT(DATETIME,v.VisitDate,103) BETWEEN   CONVERT(DATETIME,@FromDate,103) AND CONVERT(DATETIME,@ToDate,103)  
	   AND ISNULL(Wf.WriteOffAmount,0) > 0
******** Write off is not needed to calculate the opening balance ****/
 
 ) AA  
   
 UNION ALL  
   
 SELECT  
	  'Over Payment on Invoice #INV- '  + CONVERT(VARCHAR,v.InvoiceNo) AS [INVOICE],  
	  v.InvoiceNo,
	  CONVERT(VARCHAR(12),v.[VisitDate],103) AS [VISITDATE],  
	  NULL AS [TOTALAMOUNT],  
	  --CONVERT(Decimal(30,2),ROUND(ISNULL(c.[AmountPaid],0) + (SELECT SUM(ISNULL(AmountPaid,0)) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) - (CASE WHEN c.[TotalAmount] IS NULL THEN 0 ELSE ISNULL(c.[TotalAmount],0)  END) ,2)) AS [PAYMENT] ,  
	  CONVERT(Decimal(30,2),ROUND((SELECT SUM(ISNULL(AmountPaid,0)) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) - (CASE WHEN c.[TotalAmount] IS NULL THEN 0 ELSE ISNULL(c.[TotalAmount],0)  END) ,2)) AS [PAYMENT] ,  
	  NULL AS [BALANCE],  
	  CONVERT(VARCHAR(20),DATEADD(DAY,   
		(  
		SELECT   
		CASE [PaymentDuration]  
		 WHEN '30 Days' THEN 30  
		 WHEN '60 Days' THEN 60  
		 WHEN '90 Days' THEN 90  
		END   
	   FROM   
		[PaymentDuration]   
	   WHERE   
		[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]),103) 
	  AS [Duedate],  
	  v.[VisitID],  
	  v.[VisitDate] AS [VDate],
	  (SELECT MAX(Date) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) AS PaymentDate,
	  5 AS [Pref]  
 FROM  
	  Visit v    
	  JOIN  Claim c  ON c.VisitID = v.VisitID    
	  JOIN  Account a ON  a.AccountID = v.AccountID
	  LEFT JOIN Payment Pa ON Pa.VisitID = v.[VisitID]   
 WHERE     
	   v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID )   
	   --AND   CONVERT(DATETIME,v.VisitDate) BETWEEN   CONVERT(DATETIME,@FromDate,103) AND CONVERT(DATETIME,@ToDate,103)  
	   AND   CONVERT(DATETIME,Pa.Date) BETWEEN   CONVERT(DATETIME,@FromDate,103) AND CONVERT(DATETIME,@ToDate,103)  
	   --AND  CONVERT(Decimal(30,2),ROUND((CASE WHEN c.[AmountPaid] IS NULL THEN 0 ELSE ISNULL(c.[AmountPaid],0)  END) + (SELECT SUM(ISNULL(AmountPaid,0)) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) - (CASE WHEN c.[TotalAmount] IS NULL THEN 0 ELSE ISNULL(c.[TotalAmount],0)  END) ,2)) >CONVERT(Decimal(30,2),0)
	   AND  CONVERT(Decimal(30,2),ROUND((SELECT SUM(ISNULL(AmountPaid,0)) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) - (CASE WHEN c.[TotalAmount] IS NULL THEN 0 ELSE ISNULL(c.[TotalAmount],0)  END) ,2)) >CONVERT(Decimal(30,2),0)
     
 ORDER BY VDate,[Pref] ASC  

   
END
GO
