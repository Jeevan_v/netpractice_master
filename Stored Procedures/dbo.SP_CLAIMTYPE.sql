SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*====================== DEVELOPER COMMENTS===========================    
DEVELOPER NAME : PRASANNAVENGADESAN    
PROCEDURE NAME : SP_CLAIMTYPE  
DATE           : FEB 21ST 2013    
PURPOSE        : THIS IS FOR FETCHING ALL CLAIM TYPES  
======================DEVELOPER COMMENTS===========================*/    
  
CREATE PROCEDURE [dbo].[SP_CLAIMTYPE]      
AS      
BEGIN      
 SELECT 0 As ClaimTypeID,'ALL' as ClaimType  FROM CLAIMTYPE    
 UNION       
 SELECT ClaimTypeID,ClaimType FROM CLAIMTYPE     
 ORDER BY 1    
END

GO
