SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[SP_PRACTICE_LIST_WAITINGFORAPPROVAL]    

AS    

BEGIN    

    

 SELECT DISTINCT P.PracticeID,p.PracticeNo,USR.FIRSTNAME+ ' '+USR.LASTNAME AS "User Name",USR.MPNo As "HPC Number",  

 USR.DocPracticeNo as "Doctor Practice Number"  

 FROM [USER] USR           

 INNER JOIN USERVENUERELATIONSHIP UVR ON UVR.USERID = USR.USERID            

 INNER JOIN VENUE V ON V.VENUEID = UVR.VENUEID          

 INNER JOIN PRACTICE P ON P.PRACTICEID  = V.PRACTICEID           

 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID           

 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID 


 WHERE ROLES.ROLENAME in ('Practitioner','Practitioner Admin')   

 and P.ExtraInfo = 'Waiting Approval' and P.PracticeID not in (select PracticeID from MailToMedikreditStatus where EmailStatus ='Y')  

 order by "User Name"  

END     
GO
