SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_SaveRolePrivileges]
	@RoleID uniqueidentifier ,
	@PrivilegeID int 
	
AS

BEGIN

INSERT INTO PrivilegeRoleRelationship 
(
	PrivilegeID,
	RoleID
)
VALUES 
(
	@PrivilegeID,
	@RoleID
)

END

GO
