SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetPaymentDurationByPaymentDurationID]
@PaymentDurationID int
AS
BEGIN
SELECT
	PaymentDuration
FROM
	PaymentDuration
WHERE
	PaymentDurationID=@PaymentDurationID
END

GO
