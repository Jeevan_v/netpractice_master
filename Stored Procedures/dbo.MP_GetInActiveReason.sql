SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[MP_GetInActiveReason] 
AS

SELECT
	ReasonID,
	ReasonCategory,
	Reason
FROM
	InActiveReason






GO
