SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[GetPackagesForPractice]     
 -- Add the parameters for the stored procedure here    
 @PracticeId int    
AS    
BEGIN    
 SET NOCOUNT ON;    
     
 If exists(Select PracticeId from PracticePackageRelationship where PracticeID = @PracticeId)    
 Begin    
 Select pr.PracticeID,    
 Package, PR.Approved, PR.Subscribed  
 from PracticePackageRelationship PR    
 Join Package P on PR.PackageID = P.PackageID    
 Where PracticeID = @PracticeId    
 End    
END 
GO
