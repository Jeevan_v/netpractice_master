SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 18th Oct 2016
-- Description:	Fetch Claim Details and visitline details from claim id
-- =============================================
CREATE PROCEDURE [dbo].[SP_FETCHCLAIMVISITLINE_DETAILS] 
	-- Add the parameters for the stored procedure here
	@ClaimID int  
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT CD.CLAIMDETAILID,CD.CLAIMID, VL.VISITLINEID, VL.VISITID, CD.Auth_HNET,Cd.NappiCode,CD.Status , VL.LineDefinition
	 FROM CLAIMDETAIL CD with(NOLOCK)
	 INNER JOIN VISITLINE VL ON VL.VISITLINEID = CD.VISITLINEID
	 WHERE CD.CLAIMID = @ClaimID
END
GO
