SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[MW_AddNotes] --43444,'dewew','gsdf',' ',' '
@VisitID int ,
@ClinicalNotes nvarchar(2000),
@MedicalReport nvarchar(2000),
@ReferralLetter nvarchar(2000),
@SickNotes nvarchar(2000)
AS  
BEGIN 

INSERT into AddNotes(VisitID,ClinicalNotes,MedicalReport,ReferralLetter,SickNotes)
values (@VisitID,@ClinicalNotes,@MedicalReport,@ReferralLetter,@SickNotes)

END
GO
