SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetScriptingByVisitID](
@intVisitId INT
)
AS 
BEGIN
SELECT [Description],ItemNo,DosageDetails FROM Scripting
WHERE
VisitID=@intVisitId
END
GO
