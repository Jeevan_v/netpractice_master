SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DB_GetPerformanceChartDataByPracticeID] --42



@practiceid int		



AS



BEGIN







;with Months(MonthNo) as



(



  select CAST (1/1/YEAR(GETDATE()) as DATETIME)



  union all



  select DATEADD(MM,1,MonthNo)



  from Months



  where DATEPART(MM,MonthNo) <12



)







SELECT ISNULL(SUM(TotalAmount),0) AS Amount , 



UPPER(SUBSTRING ( DATENAME(MONTH,MonthNo),1 , 3 )) AS Months 



FROM Claim p 



JOIN Visit v  ON p.VisitID = v.VisitID 



join Account a on v.AccountID =a.AccountID



and PracticeID=@practiceid 



AND DATEPART(yyyy,v.DateLogged)= DATEPART(yyyy,getdate())



RIGHT OUTER JOIN Months M on DATEPART(MM,v.DateLogged) =DATEPART(MM,MonthNo)



GROUP BY DATENAME(MONTH,MonthNo), DATEPART(MM,MonthNo)



ORDER by  DATEPART(MM,MonthNo)



END
GO
