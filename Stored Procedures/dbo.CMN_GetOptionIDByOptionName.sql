SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CMN_GetOptionIDByOptionName]
	@OptionName varchar(200)
AS
BEGIN
	SELECT
	MFOptionID
	FROM MFOption
	WHERE
	MFOptionName=@OptionName
END


GO
