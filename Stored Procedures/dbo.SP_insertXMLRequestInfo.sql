SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 12/4/2014
-- Description:	Insert xml request details in the table
-- =============================================
CREATE PROCEDURE [dbo].[SP_insertXMLRequestInfo] 
	-- Add the parameters for the stored procedure here
			@CLAIMID int =0
			,@PATIENTID int =0  
           ,@PRACTICEID int= 0 
           ,@VISITID int =0 
           ,@ACCOUNTID int =0
           ,@TYPEOFREQUEST varchar(50) = null --NEW , REVERESE, FMC etc
           ,@STATUSOFREQUEST int = 1 -- Status request 1. In-Process,2. Success, 3. Failure
		   ,@CLAIMSWITCH varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @PATIENTID = 0 
	BEGIN
	SELECT top 1 @PATIENTID = PATIENTID from Account WHERE AccountID= @ACCOUNTID  
	END
	 
	 DECLARE @RECCNT INT = 0 

	 SELECT @RECCNT = count(*) from  [dbo].[XmlRequest] where [CLAIMID] =@CLAIMID and [TYPEOFREQUEST] = @TYPEOFREQUEST and 
	 [PATIENTID] =@PATIENTID and 
          [PRACTICEID] =@PRACTICEID and 
           [VISITID] = @VISITID and 
		  [ACCOUNTID] = @ACCOUNTID
    -- Insert statements for procedure here
	--IF @RECCNT = 0 
	--BEGIN
INSERT INTO [dbo].[XmlRequest]
           ([CLAIMID]
           ,[PATIENTID]
           ,[PRACTICEID]
           ,[VISITID]
           ,[ACCOUNTID]
           ,[TYPEOFREQUEST]
           ,[STATUSOFREQUEST]
           ,[CLAIMSWITCH] 
        )
     VALUES
           (@CLAIMID
           ,@PATIENTID
           ,@PRACTICEID
           ,@VISITID
           ,@ACCOUNTID
		   ,@TYPEOFREQUEST
		    ,@STATUSOFREQUEST
			,@CLAIMSWITCH
		   )

		   ---select identity

 
		  --END 
		 --ELSE 
		 --BEGIN 
		 --INSERT INTO [dbo].[XmlRequest]
   --        ([CLAIMID]
   --        ,[PATIENTID]
   --        ,[PRACTICEID]
   --        ,[VISITID]
   --        ,[ACCOUNTID]
   --        ,[TYPEOFREQUEST]
   --        ,[STATUSOFREQUEST]
		 --  ,[REMARKS]
           
   --     )
   --  VALUES
   --        (@CLAIMID
   --        ,@PATIENTID
   --        ,@PRACTICEID
   --        ,@VISITID
   --        ,@ACCOUNTID
		 --  ,@TYPEOFREQUEST
		 --   ,3
			--,'DUPLICATE REQUEST'
		 --  )
 
		  
		 -- END


		  SELECT SCOPE_IDENTITY()
END



GO
