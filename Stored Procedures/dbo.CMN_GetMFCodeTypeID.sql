SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<sameer>
-- Create date: <16/01/2013>
-- Description:	<Get MF Code Type Id>
-- =============================================
CREATE PROCEDURE  [dbo].[CMN_GetMFCodeTypeID]
	@MFCodeTypeDefinition varchar(200)
AS
BEGIN
	SELECT 
	MFCodeTypeID
	FROM
	MFCodeType
	WHERE
	CodeTypeDefinition=@MFCodeTypeDefinition
END

GO
