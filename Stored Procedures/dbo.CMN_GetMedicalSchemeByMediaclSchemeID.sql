SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetMedicalSchemeByMediaclSchemeID]  
	@MainMemberID int,  
	@SchemeID int  
AS  
BEGIN  
	SELECT  
		 p.PatientID,  
		 ms.SchemeName,  
		 ms.POPostal,  
		 ms.POPostalNo,  
		 ms.POCity,  
		 ms.POPostalCode  
	FROM  
		 MedicalScheme ms JOIN Patient p  
	ON  
		 ms.MedicalSchemeID=p.SchemeID  
	WHERE   
		 p.MainMemberID=PatientID AND p.MainMemberID=@MainMemberID AND p.SchemeID=@SchemeID  
END
GO
