SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetRolePrivileges]
	@RoleID uniqueidentifier
AS
BEGIN
	SELECT	RoleName,
			RoleID
	FROM	aspnet_Roles 
	WHERE	RoleId=@RoleID;
END

GO
