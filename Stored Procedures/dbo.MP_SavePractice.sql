SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_SavePractice]
	@PracticeID int,
	@PracticeName varchar(50),
	@PracticeNo varchar(30),
	@VatNo varchar(30),
	@Email varchar(50),
	@TypeID int,
	@AddVat bit,
	@IsActive bit,
	@ExtraInfo varchar(100),
	@PackageID int,
	@BankID int,
	@BankNameOther varchar(100),
	@BranchCode varchar(100),
	@BranchName varchar(100),
	@AccountNumber varchar(100),
	@PaymentDurationID int,
	@ReasonID int,
	@IsAurum bit,
	@HasAurumNurse bit
AS

IF @PracticeID IS NULL
BEGIN

INSERT INTO Practice 
(
	PracticeName,
	PracticeNo,
	VatNo,
	Email,
	PracticeTypeID,
	AddVat,
	IsActive,
	ExtraInfo,
	PackageID,
	BankID,
	BankNameOther,
	BranchCode,
	BranchName,
	AccountNumber,
	PaymentDurationID,
	ReasonID,
	IsAurum,
	HasAurumNurse
)
VALUES 
(
	@PracticeName,
	@PracticeNo,
	@VatNo,
	@Email,
	@TypeID,
	@AddVat,
	@IsActive,
	@ExtraInfo,
	@PackageID,
	@BankID,
	@BankNameOther,
	@BranchCode,
	@BranchName,
	@AccountNumber,
	@PaymentDurationID,
	@ReasonID,
	@IsAurum,
	@HasAurumNurse
)

SELECT SCOPE_IDENTITY()

END

ELSE
BEGIN

UPDATE Practice
SET
    
	PracticeName = @PracticeName,
	PracticeNo = @PracticeNo,
	VatNo = @VatNo,
	Email = @Email,
	PracticeTypeID = @TypeID,
	AddVat = @AddVat,
	IsActive = @IsActive,                                                                                        
	ExtraInfo = @ExtraInfo,
	PackageID = @PackageID,
	BankID=@BankID,
	BankNameOther=@BankNameOther,
	BranchCode=	@BranchCode,
	BranchName=	@BranchName,
	AccountNumber=@AccountNumber,
	PaymentDurationID=@PaymentDurationID,
	ReasonID = @ReasonID,
	IsAurum = @IsAurum,
	HasAurumNurse = @HasAurumNurse
WHERE
	PracticeID = @PracticeID
	
SELECT 1
END

GO
