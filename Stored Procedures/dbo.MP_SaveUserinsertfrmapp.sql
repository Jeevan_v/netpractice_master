SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_SaveUserinsertfrmapp]  --15,'0782df6d-ab53-4451-b10c-e8f8f4ca8243',1,'Olebogeng','Mosala','15','','',1,1,''
 @UserID int,  
 @MembershipID uniqueidentifier,  
 @TitleID int,  
 @FirstName varchar(30),  
 @LastName varchar(30),  
 @IDNo varchar(20),  
 @MPNo varchar(20),  
 @DocPracticeNo varchar(20),  
    
 @ReasonID int,  
 @DispenseLicenseNo varchar(30)  
AS  
  
 
  
INSERT INTO [User]  
(  
 UserID,
 MembershipID,  
 TitleID,  
 FirstName,  
 LastName,  
 IDNo,  
 MPNo,  
 DocPracticeNo,  
  
 ReasonID,  
 DispenseLicenseNo   
)  
VALUES  
(  
 @UserID,
 @MembershipID,  
 @TitleID,  
 @FirstName,  
 @LastName,  
 @IDNo,  
 @MPNo,  
 @DocPracticeNo,  
 @ReasonID,   
 @DispenseLicenseNo  
)  
GO
