SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetInvoiceNo]  
@VisitID int  
AS  
BEGIN  
SELECT  
   InvoiceNo,  
   VisitDate  
     
   FROM  
   Visit  
WHERE  
    VisitID=@VisitID  
  
END

GO
