SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[MW_SelectStatusOfClaim]
	@ClaimID int
AS
BEGIN
	SELECT COUNT(*) as acceptedClaim ,

(select COUNT(*)  from CLAIMDETAIL where ClaimID= @ClaimID and status = 'duplicate') as duplicate
  FROM CLAIMDETAIL
  WHERE ClaimID= @ClaimID
  AND (status  = 'accepted' or status = 'warning')
END


GO
