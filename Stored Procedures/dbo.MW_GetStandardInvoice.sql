SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetStandardInvoice]
@InvoiceNo int
AS
BEGIN
SELECT
    vl.Diagnosis,
    vl.Code,
	vl.[Description],
	vl.LineDefinition,
	cast(vl.Amount/vl.Quantity AS DECIMAL(12,2))As 'UnitPrice',
	cast(vl.Amount AS Decimal(12,2))As Amount , 
	
	v.InvoiceNo 
	

	FROM
	VisitLine vl
	 inner join 
	Visit v on
	 v.VisitID=vl.VisitID

WHERE
    v.InvoiceNo=@InvoiceNo

END

GO
