SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SE_ApprovePackages]
	-- Add the parameters for the stored procedure here
	@PackageName varchar(max), 
	@PracticeName varchar(max),
	@checked bit,
	@RejectedReason varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @PackageId int
	Declare @PracticeId int
	
	Set @PackageId =  (SELECT PackageID from Package where Package =@PackageName) 
	Set @PracticeId = (SELECT top 1 PracticeID from Practice where PracticeName =@PracticeName and IsActive = 1)
	   
	if exists(select practiceId from PracticePackageRelationship where PracticeID =@PracticeId and packageID = @PackageId)
	Begin
	update PracticePackageRelationship 
	set Approved = @checked,
	Rejected_Reason = @RejectedReason,
	Date_approved = GETDATE() 
	Where  PracticeID =@PracticeId 
	and 
	packageID = @PackageId
	End 
	else
	Begin
    INSERT INTO [PracticePackageRelationship]
	(
		PracticeId,
		PackageId,
		Approved,
		Rejected_Reason,
		Date_approved
	)
	VALUES
	(
		@PracticeId,
		@PackageId,
		@checked,
		@RejectedReason,
		GETDATE()
	)
	End
END
GO
