SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create proc [dbo].[cmn_UpdateMailStatus]
(@MailId int,@MailStatus bit)
as
begin
update Mail  set IsSent=@MailStatus where ID=@MailId
end
GO
