SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetVenueStatus] 
	@VenueID int
AS
IF (Select v.IsActive from Venue v where v.VenueID=@VenueID)='true'
Begin
SELECT
vn.IsActive,
	ReasonID=null,
    Reason=null
FROM 
Venue vn 
where vn.VenueID=@VenueID
END

ELSE

BEGIN
SELECT
	v.IsActive,
	ir.ReasonID,
	ir.Reason
FROM 
Venue v 

INNER JOIN 
InActiveReason ir
ON
v.ReasonID = ir.ReasonID

where 
v.VenueID=@VenueID
END


select * from Venue

GO
