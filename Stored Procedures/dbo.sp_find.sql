SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[sp_find]  
  @findcolumn varchar(50)  
as  
begin  
  set nocount on  
  select 
    sysobjects.name as TableFound,  
    syscolumns.name as ColumnFound  
  from sysobjects  
    inner join syscolumns on sysobjects.id=syscolumns.id  
    where syscolumns.name like '%' + @findcolumn +'%'  
       or sysobjects.name like '%' + @findcolumn +'%'  
  order by sysobjects.name  
end  
GO
GRANT EXECUTE ON  [dbo].[sp_find] TO [public]
GO
