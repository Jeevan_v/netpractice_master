SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetPostalCodeByCityId] 
      @CityId int
AS
BEGIN

SELECT

	pc.PostalCodeID,
	pc.PostalCode
	
	
FROM

dbo.PostalCode pc

Inner Join 

dbo.CityPostalCodeRelationship cpr

ON

pc.PostalCodeID= cpr.PostalCodeID

WHERE

cpr.CityID=@CityId 

END	

GO
