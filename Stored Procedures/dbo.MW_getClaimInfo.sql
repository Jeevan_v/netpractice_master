SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MW_getClaimInfo]
	@ClaimID int
AS
BEGIN
	select * from Claim
	where ClaimID=@ClaimID;
	
	select * from ClaimDetail
	where ClaimID=@ClaimID
END


GO
