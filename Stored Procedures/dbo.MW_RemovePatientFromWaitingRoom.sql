SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_RemovePatientFromWaitingRoom]
@waitingRoomID INT,
@PatientID INT,
@PracticeID INT,
@VenueID INT
AS
IF @waitingRoomID IS NOT NULL AND @VenueID IS NOT NULL
BEGIN
-- Delete patient from waiting room without doing consultation
DELETE 
FROM WaitingRoom
WHERE
WaitingRoomID=@waitingRoomID
AND
VenueID=@venueID	
END
ELSE
BEGIN
-- Delete patient from waiting room for consultation
-- Get accountid for patient that is in the waiting room
DECLARE @AccountID INT
SET @AccountID =(SELECT AccountID FROM Account WHERE PracticeID=@PracticeID AND PatientID=@PatientID)
IF @AccountID IS NOT NULL
BEGIN
DELETE 
FROM
WaitingRoom
WHERE
AccountID=@AccountID
AND
VenueID=@VenueID
END
END


GO
