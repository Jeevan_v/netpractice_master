SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Shainy>
-- Create date: <29/01/2013>
-- Description:	<Get the details of recently rejected claims >
-- =============================================
CREATE procedure [dbo].[DB_Diary](@VenueID int,@UserID int,@ClaimStatus Varchar(30),@Count int)
As
Begin
SELECT Top(@Count)
    convert(date,Vs.VisitDate), 
    Vs.DoctorID, 
	Pt.FirstName + ' '+  Pt.LastName AS [PatientName],
	Clm.AmountPaid,
	Clm.TotalAmount,
    (Clm.AmountPaid - Clm.TotalAmount) AS [Balance]	
FROM 
	Visit Vs

	JOIN Account Acc ON Acc.AccountID = Vs.AccountID
	JOIN Patient Pt ON Pt.PatientID = Acc.PatientID
	JOIN VisitLine Vl ON Vl.VisitID = Vs.VisitID
	JOIN ClaimDetail Cd ON Cd.VisitLineID = Vl.VisitLineID
	JOIN Claim Clm ON Clm.ClaimID = Cd.ClaimID
	JOIN Practice p ON Acc.PracticeID=p.PracticeID
	
WHERE
	p.PracticeID = @VenueID
	AND
	cd.status=@ClaimStatus
	
END


select * from Visit




GO
