SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetRegisteredPractices]
@Key varchar(30)
AS
SELECT
	DISTINCT
	p.PracticeID,
	p.PracticeName,
	p.PracticeNo,
	p.VatNo,
	p.Email,
	p.PracticeTypeID,
	p.AddVat,
	p.IsActive,
	p.ExtraInfo,
	p.PackageID,
	p.ReasonID,
	p.IsAurum,
	p.HasAurumNurse,
	p.BankID,
	p.PaymentDurationID,
	p.BankNameOther,
	p.BranchCode,
	p.BranchName,
	p.AccountNumber,
	u.FirstName
	
FROM 

Practice p 

JOIN 

Venue v 

ON

p.PracticeID = v.PracticeID

JOIN 

UserVenueRelationship uvr

ON

v.VenueID = uvr.VenueID

JOIN

[User] u 

ON

u.UserID = uvr.UserID

where 

u.UserID 

IN  

(

SELECT

u.UserID 

FROM

[User] u 

JOIN

aspnet_Users au

ON

u.MembershipID = au.UserId

JOIN

aspnet_UsersInRoles aur

ON

au.UserId = aur.UserId

JOIN

aspnet_Roles ar 

ON 

ar.RoleId = aur.RoleId

WHERE

ar.RoleName in('Practice Admin','Practitioner Admin')
AND
p.ExtraInfo in(@Key)
)
ORDER BY PracticeName

GO
