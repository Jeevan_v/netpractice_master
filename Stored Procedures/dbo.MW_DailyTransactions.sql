SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[MW_DailyTransactions] (@intPractice int)    

As    

Begin    

SELECT    

 lower(pat.FirstName + ' ' + pat.LastName) as PatientName,  

 CONVERT(Decimal(30,2),ROUND(cl.[TotalAmount],2)) as AmountPaid,  

 CONVERT(Decimal(30,2),ROUND(cl.[TotalAmount] - (CASE WHEN cl.AmountPaid IS NULL THEN 0 ELSE cl.AmountPaid  END) - (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID),2)) AS [Balance],    

 pat.[PatientID],    

 v.[VisitID],    

 [ClaimID],    

 [PracticeID],    

 UPPER(LEFT(cl.[Status],1))+LOWER(SUBSTRING(cl.[Status],2,LEN(cl.[Status]))) as [Status],    

 ms.[SchemeName],    

 ac.[AccountNumber]    

FROM    

Visit v    

INNER JOIN    

Claim cl    

on     

v.VisitID=cl.VisitID    

INNER JOIN    

Account ac    

on    

v.AccountID=ac.AccountID    

INNER JOIN    

Patient pat    

on    

ac.PatientID=pat.PatientID    

INNER JOIN    

MedicalScheme ms    

on    

pat.SchemeID=ms.MedicalSchemeID    

where    

PracticeID=@intPractice   

AND     

 CONVERT(date,V.DateLogged) = CONVERT(date,GETDATE())    

 Order By v.VisitID desc    

     

END    

    

    

    

    

    
GO
