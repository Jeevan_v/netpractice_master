SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**********************************************************
***Change By : Sachin Gandhi
***Change Date : 19th Nov 2014
***Description : Removing distinct clause from the query to resolve the bug#22
***Change Date : 8th Jul 2015
***Descritpion : added extra fields for invoice details
***********************************************************/

CREATE PROCEDURE [dbo].[MW_GetPrintInvoice]   --689286--25134--6796
@InvoiceNo int
AS
BEGIN



SELECT
 
	vl.visitlineid,--DISTINCT
    v.DoctorID,
    v.VisitDate,
	v.RefPracticeNo as RefPracticeName,
	v.RefPracticeNo,
	v.DateLogged,
	vl.Quantity,
    vl.Diagnosis,
    vl.Code,
	vl.[Description],
	vl.LineDefinition,	
	vl.Starttime,
	vl.Endtime,
	(cast((vl.Amount/1.14)/vl.Quantity as  Decimal(36,2)))As 'UnitPrice',
	cast((vl.Amount/1.14) as  Decimal(36,2)) As 'Amount',
	CASE 
	  WHEN acc.PracticeID = 180 and cc.Status='Cash Patient' THEN 0
	  ELSE 
	   vl.Amount-cast((vl.Amount/1.14) AS Decimal(36,2))
	END 
	-- vl.Amount-cast((vl.Amount/1.14) AS Decimal(36,2)) 
	 AS 'Tax',
	vl.Amount As 'TotalZAR',	 
	v.InvoiceNo,
	v.AuthorisationCode ,
	cc.[Status],
	cc.ClaimID,
	acc.AccountNumber

	FROM
	VisitLine vl
	inner join ClaimDetail	cd with(nolock)  on cd.VisitLineID = vl.VisitLineID
	 inner join Visit v with(nolock) on v.VisitID=vl.VisitID --and v.SchemeID = 77
	 inner join Account acc with(nolock) on acc.AccountID = v.AccountID
	 inner join Claim cc with(nolock) on cc.VisitID = v.VisitID
	-- left outer join Practice prac with(nolock) on v.RefPracticeNo = prac.PracticeNo and ISNULL(v.RefPracticeNo,'') <> ''
WHERE
    v.InvoiceNo=@InvoiceNo
END



 

  




GO
