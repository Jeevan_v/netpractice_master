SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[USP_ClaimProcessappool] 
as
begin



select distinct  cx.XMLFileName from claimxml cx with(nolock)
inner join claim cc with(nolock) on cc.ClaimID = cx.ClaimID --and isnull(cc.status,'') =''
inner join claimdetail cd with (nolock) on cd.ClaimID= cc.ClaimId and     ( isnull(cd.status,'')= '' and cc.status <> 'REVERSED')    
inner join visit v with(nolock) on v.VisitID = cc.VisitID  and (v.DateLogged >= getdate()-120 and v.DateLogged < DATEADD(day,1,getdate()))
 where  ( cx.IsReversed <>'P'  and cx.IsReversed <> 'NP') or (cx.isreversed='R')
 union 
 
select distinct  cx.XMLFileName from claimxml cx with(nolock)
inner join claim cc with(nolock) on cc.ClaimID = cx.ClaimID --and isnull(cc.status,'') =''
inner join claimdetail cd with (nolock) on cd.ClaimID= cc.ClaimID and     (isnull(cc.status,'') ='' and isnull(cd.status,'')= '')  
inner join visit v with(nolock) on v.VisitID = cc.VisitID  and (v.DateLogged >= getdate()-120 and v.DateLogged < DATEADD(day,1,getdate()))
 where  ( cx.IsReversed <>'P'   and cx.IsReversed <> 'NP') or (cx.isreversed='R')

union

select distinct  cx.XMLFileName from claimxml cx with(nolock)
inner join claim cc with(nolock) on cc.ClaimID = cx.ClaimID --and isnull(cc.status,'') =''
inner join claimdetail cd with (nolock) on cd.ClaimID= cc.ClaimID and     (cx.isreversed='R' and isnull(cc.status,'') <>'' and cc.StatusReason not like 'Reverse%'  and isnull(cd.status,'')<> '') 
inner join visit v with(nolock) on v.VisitID = cc.VisitID  and (v.DateLogged >= getdate()-120 and v.DateLogged < DATEADD(day,1,getdate()))
 where  ( cx.IsReversed <>'P' and cx.IsReversed <> 'NP') or (cx.isreversed='R')
   
end
GO
