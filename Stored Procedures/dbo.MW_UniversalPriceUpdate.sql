SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_UniversalPriceUpdate] 
	@claimID int,
	@universalPrice decimal(36,2),
	@chargeCode nvarchar(20)
AS
BEGIN
	Declare @chargeType_ID int ,@scheme_ID int, @plan_ID int, @practitionerGroup_ID int, @practiceType varchar(75),
	@PractitionerType_ID int, @ChargeCode_ID int, @ChargeType varchar(50), @PaymentArrangementDetail_ID int,
	@Price decimal(18,2)= 0.00, @price_NO_VAT decimal(18,2)= 0.00, @idScope int, @idCC int, 
	@oldPrice decimal(18,2)= 0.00
	
	SELECT @chargeType_ID = V.ChargeTypeID, @ChargeType = CT.ChargeType, @scheme_ID = V.SchemeID, @plan_ID = V.PlanID,
	@practiceType = PT.PracticeType, @practitionerGroup_ID = PT.PractitionerGroup_ID, 
	@PractitionerType_ID = PT.PracticeTypeID 
	FROM Claim C JOIN Visit V ON C.VisitID = V.VisitID JOIN 
	UserVenueRelationship R on R.UserID = V.DoctorID JOIN Venue VE on VE.VenueID = R.VenueID JOIN
	Practice P on P.PracticeID = VE.PracticeID JOIN PracticeType PT on PT.PracticeTypeID = P.PracticeTypeID JOIN
	ChargeType CT on CT.ChargeTypeID = V.ChargeTypeID
	WHERE C.ClaimID = @claimID 
		
	set @ChargeCode_ID = (select Top 1 ChargeCodeID  from ChargeCode where CodeGroupID in(4,3)     
						  and CodeGroupdetail_ID = 1 and TYPEID = @practitionerGroup_ID and ChargeCode = @chargeCode)
	
	set @PaymentArrangementDetail_ID = (select  distinct PAD.PaymentArrangementDetail_ID from ChargeType CT join 
										T_PaymentArrangement PA  on CT.ChargeType=PA.DetailCode     
										join T_PaymentArrangementDetail PAD on 
										PAD.PaymentArrangement_ID = PA.PaymentArrangement_ID    
										where CT.ChargeType = 'Universal')
							
	set @Price = (select distinct ValueInclRounded from T_ChargeCodeStraightCharge where GETDATE() between 
				 DateValidFrom and DateValidTo and PractitionerType_ID = @PractitionerType_ID and
				 ChargeCode_ID=@ChargeCode_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID)
	
	SET @price_NO_VAT = @universalPrice/1.14
	
	
	  --Change by Sachin Gandhi
	--					  --23rd Dec 2014
	IF (isnull(@ChargeCode_ID  ,0) <> 0)  
	BEGIN			  			 
	IF(@Price is not Null or @Price <> 0.00)  
		BEGIN	
			IF(@Price <> @universalPrice) 
				BEGIN			
					SELECT @idCC = ChargeCodeStraightCharge_ID, @oldPrice = ValueInclRounded FROM 
					T_ChargeCodeStraightCharge WHERE GETDATE() between DateValidFrom and DateValidTo and 
					PractitionerType_ID = @PractitionerType_ID and ChargeCode_ID=@ChargeCode_ID and 
					PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID
					
					print 	@idCC
					print 	@oldPrice
					
					UPDATE T_ChargeCodeStraightCharge SET ValueInclRounded = @universalPrice, 
					ValueInclUnrounded = @universalPrice, ValueExclUnrounded = ROUND(@price_NO_VAT,4),
					ValueExclRounded = ROUND(@price_NO_VAT,4)
					WHERE GETDATE() between DateValidFrom and DateValidTo 
					and PractitionerType_ID = @PractitionerType_ID and ChargeCode_ID=@ChargeCode_ID and 
					PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID										
					
					print @universalPrice
					
					INSERT INTO Track_UniversalPriceUpdate (ChargeCodeStraightCharge_ID,Old_Price,New_Price,
					DateLogged,is_New) VALUES (@idCC,@oldPrice,@universalPrice,GETDATE(),0)
					
					print SCOPE_IDENTITY()
				END			
		END	
	ELSE
		BEGIN
			INSERT INTO T_ChargeCodeStraightCharge(ChargeCode_ID,PractitionerType_ID,
			PaymentArrangementDetail_ID,ValueExclUnrounded,ValueExclRounded,VatPerc_ID,
			ValueInclUnrounded,ValueInclRounded,DateValidFrom,DateValidTo,RoundingRule_ID,LocationType_ID)
			VALUES (@ChargeCode_ID,@PractitionerType_ID,@PaymentArrangementDetail_ID,ROUND(@price_NO_VAT,4),
			ROUND(@price_NO_VAT,4),1,@universalPrice,@universalPrice,GETDATE(),DATEADD(YEAR,1,GETDATE()),1,0)
			
			set @idScope = SCOPE_IDENTITY()
			
			INSERT INTO Track_UniversalPriceUpdate (ChargeCodeStraightCharge_ID,Old_Price,New_Price,
			DateLogged,is_New) VALUES (@idScope,@universalPrice,@universalPrice,GETDATE(),1)
		END			 
	END

END

GO
