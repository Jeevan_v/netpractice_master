SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_DeleteRolePrivileges] 
	@RoleID uniqueidentifier ,
	@PrivilegeID int
	
AS

BEGIN

DELETE 
FROM 
	PrivilegeRoleRelationship 
WHERE 
	RoleID = @RoleID AND PrivilegeID = @PrivilegeID

END 

GO
