SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetWaitingRoomList]
@VenueID INT
AS
BEGIN
SELECT 
 WR.WaitingRoomID,
 WR.AccountID,
 WR.VenueID,
 P.PatientID,
 (P.FirstName + ' ' + P.LastName) AS PatientName,
 RIGHT('00'+ CONVERT(VARCHAR(10),FLOOR(CONVERT(INT,WR.[Time])/ 60.0)),2)
 +':'+
 RIGHT('00'+CONVERT(VARCHAR(10),FLOOR(CONVERT(INT,WR.[Time]) % 60.0)),2) AS [Time],
 U.FirstName + ' '+ u.LastName AS [Doctor],
 WR.DoctorID,
 WR.IsWalkIn,
 WR.WaitStartTime
FROM 
WaitingRoom WR
JOIN
Account A
ON WR.AccountID=A.AccountID
JOIN
Patient P
ON A.PatientID=P.PatientID	
JOIN --Added for Doctor ID Change
[User] U
ON WR.DoctorID=U.UserID 
WHERE
WR.VenueID=@VenueID
AND
CONVERT(DATE,WR.DateLogged)=CONVERT(DATE,GETDATE()) 
END






GO
