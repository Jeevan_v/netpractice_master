SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetPlanCodeByPlanIDOptionID]   
 @OptionID int,  
 @MedicalPlanID int,  
 @MFCodeTypeID int  
AS  
BEGIN  
DECLARE @PlanCode varchar(20)  
SET @PlanCode=  
(SELECT   
 DISTINCT  
 MFOLC.MFOptionLinkCode  
 FROM  
 MFOption MFO  
 JOIN  
 MFOptionLink MFOL  
 ON  
 MFO.MFOptionID=MFOL.MFOptionID  
 JOIN  
 MedicalPlan MP  
 ON  
 MP.MedicalPlanID=MFOL.PlanID  
 JOIN  
 MFOptionLinkCode MFOLC  
 ON  
 MFOLC.MFOptionLinkID=MFOL.MFOptionLinkID  
 JOIN  
 MFCodeType MFCT  
 ON  
 MFCT.MFCodeTypeID=MFOLC.MFCodeTypeID  
 WHERE  
 MP.MedicalPlanID=@MedicalPlanID  
 AND  
 MFO.MFOptionID=@OptionID  
 AND  
 MFOLC.MFCodeTypeID=@MFCodeTypeID  
 AND  -- added and condition for cheking where plan is Fm Check enabled or not
 MFOLC.IsFMcheckEnabled = 1
)  
IF @PlanCode is null  
BEGIN  
SELECT ''  
END  
ELSE  
BEGIN  
SELECT @PlanCode  
END  
END
GO
