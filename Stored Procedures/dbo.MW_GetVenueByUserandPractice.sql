SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetVenueByUserandPractice]  
	-- Add the parameters for the stored procedure here
(

@PracticeID INT,
@UserID INT,
@RoleName varchar(50)
)
AS
BEGIN
IF(@RoleName = 'Patient')
BEGIN
SELECT DISTINCT	
	v.VenueName,
	v.VenueID
FROM
Venue v
INNER JOIN
Account A
ON 
A.PracticeID=v.PracticeID
Inner Join 
Patient Pat
ON pat.PatientID=A.PatientID
WHERE
pat.PatientID=@UserID
AND
V.PracticeID=@PracticeID
ORDER BY 
v.VenueName
END
ELSE IF(@RoleName='Super Admin')
BEGIN
SELECT
	DISTINCT	
	v.VenueName,
	v.VenueID
FROM
Venue v
WHERE
V.PracticeID=@PracticeID
ORDER BY 
v.VenueName
END
ELSE
BEGIN
SELECT
	DISTINCT	
	v.VenueName,
	v.VenueID
FROM
Venue v
Inner Join 
UserVenueRelationship uv
ON uv.VenueID=v.VenueID
INNER JOIN 
[User] u
ON u.UserID=uv.UserID
WHERE
u.UserID=@UserID
AND
V.PracticeID=@PracticeID
ORDER BY 
v.VenueName
END
	
END
GO
