SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetTariffCode] --1,'SOB','0191','New and established patient: Consultation/visit of'







(







   @PracticeID int,







   @ChargeType varchar(50),







   @chargeCode nvarchar(20),







    @ShortDescription nvarchar(500)







)







AS















BEGIN







        --variable declaration --







        Declare @practitionerGroup_id int ,@PaymentArrangementDetail_ID int,@PractitionerType_ID int,@ChargeCode_ID varchar(10),@Price decimal(18,2)=0.00















        --Get and Set practitionerGroup_Id based on Logged in practice




		--Modified By and Date: Rajib 26/7/2013 
         --Comments: PracticeTypeID is send as a parameter instead of Practice ID
         --Start


        --set @practitionerGroup_id=(select Top 1 pt.practitionerGroup_id from Practice p inner join practicetype pt on p.PracticeTypeID=pt.PracticeTypeID







        --                                                   where p.PracticeID=@PracticeID)


		 
		set @practitionerGroup_id = (select top 1 practitionerGroup_id from practicetype where PracticeTypeID = @PracticeID)
		














        --Get and Set PractitionerType_ID based on Logged in practice







        --set @PractitionerType_ID=(select Top 1 p.PracticeTypeID from Practice p where p.PracticeID=@PracticeID)

		set @PractitionerType_ID=@PracticeID

		-- END













        --GET and SET ChargeCode_ID based on ChargeCode







        set @ChargeCode_ID=(select Top 1 ChargeCodeID  from ChargeCode where CodeGroupID in(4,3)







                                                and CodeGroupdetail_ID=1  and TYPEID=@practitionerGroup_id and ChargeCode=@chargeCode)















        --Get and Set @PaymentArrangementDetail_ID based on Patient Charge Type







        set @PaymentArrangementDetail_ID=(select  Top 1 PAD.PaymentArrangementDetail_ID from ChargeType CT inner join T_PaymentArrangement PA  on CT.ChargeType=PA.DetailCode







                                                                          inner join T_PaymentArrangementDetail PAD on PAD.PaymentArrangement_ID=PA.PaymentArrangement_ID







                                                                          where CT.ChargeType=@ChargeType)















        --check for PaymentArrangementDetail_ID is there for give charge Type







        if (@PaymentArrangementDetail_ID <>'' or @PaymentArrangementDetail_ID<>NULL)







                BEGIN







                        --Get and set the current year price based on user selected Chargecode.







                        set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge where GETDATE() between DateValidFrom and DateValidTo and







                                                PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID







                                                and ChargeCode_ID=@ChargeCode_ID)















                        --Check  price available for the current year







                        if(@Price<>0.00 or @Price<>NULL)







                                BEGIN







                                        --Retrieving charge code,short description,long description,price







                                        Select  Top 1 CC.Chargecode, left(CC.ShortDescription,50)as ShortDescription ,CC.LongDescription,@Price as Price,CG.CodeGroupDescr from ChargeCode cc







                                        inner join CodeGroup CG on cc.CodeGroupID = CG.CodeGroupID where cc.CodeGroupID in(4,3)and CC.CodeGroupdetail_ID=1







                                        and CC.TYPEID=@practitionerGroup_id and CC.ChargeCode=@chargeCode and







                                        CC.ShortDescription like @ShortDescription +'%'







                                END







                        ELSE







                                BEGIN







                                        --Get PaymentArrangementDetail_ID  for Universal charge Type







                                        set @PaymentArrangementDetail_ID=(select Top 1 PAD.PaymentArrangementDetail_ID from ChargeType CT inner join T_PaymentArrangement PA  on CT.ChargeType=PA.DetailCode







                                                                                                          inner join T_PaymentArrangementDetail PAD on PAD.PaymentArrangement_ID=PA.PaymentArrangement_ID







where CT.ChargeType='Universal')







                                        --Get and set the current year price based on Universal Chargecode.







                                        set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge where GETDATE() between DateValidFrom and DateValidTo and







                                                                PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID







                                                                and ChargeCode_ID=@ChargeCode_ID)







                                        IF(@Price<>0.00 or @Price<>NULL)







                                                BEGIN







                                                        --Get and Set @PaymentArrangementDetail_ID based on Patient Charge Type







                                                        set @PaymentArrangementDetail_ID=(select  Top 1 PAD.PaymentArrangementDetail_ID from ChargeType CT inner join T_PaymentArrangement PA  on CT.ChargeType=PA.DetailCode







                                                        inner join T_PaymentArrangementDetail PAD on PAD.PaymentArrangement_ID=PA.PaymentArrangement_ID







                                                        where CT.ChargeType='Universal')







                                                        --Get and set the previous year price based on user selected Chargecode.







                                                        set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge  where PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID







                                                                                and ChargeCode_ID=@ChargeCode_ID order by DateValidTo desc )







                                                        Select  Top 1 CC.Chargecode, left(CC.ShortDescription,50)as ShortDescription ,CC.LongDescription,@Price as Price,CG.CodeGroupDescr from ChargeCode cc







                                                        inner join CodeGroup CG on cc.CodeGroupID = CG.CodeGroupID where cc.CodeGroupID in(4,3)and CC.CodeGroupdetail_ID=1







                                                        and CC.TYPEID=@practitionerGroup_id and CC.ChargeCode=@chargeCode and







                                                        CC.ShortDescription like @ShortDescription +'%'







                                                END







                                        ELSE







                                                BEGIN







                                                        set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge where PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID







                                                                                and ChargeCode_ID=@ChargeCode_ID order by DateValidTo desc)







                                                        Select  CC.Chargecode, left(CC.ShortDescription,50)as ShortDescription ,CC.LongDescription,@Price as Price,CG.CodeGroupDescr from ChargeCode cc







                                                        inner join CodeGroup CG on cc.CodeGroupID = CG.CodeGroupID where cc.CodeGroupID in(4,3)







                                                        and CC.CodeGroupdetail_ID=1  and CC.TYPEID=@practitionerGroup_id and







               CC.ChargeCode=@chargeCode and  CC.ShortDescription like @ShortDescription +'%'







                                                END







                                END















                END







        ELSE







                BEGIN







                        --Get PaymentArrangementDetail_ID  for Universal charge Type







                        set @PaymentArrangementDetail_ID=(select Top 1 PAD.PaymentArrangementDetail_ID from ChargeType CT inner join T_PaymentArrangement PA  on CT.ChargeType=PA.DetailCode







                                                                                       inner join T_PaymentArrangementDetail PAD on PAD.PaymentArrangement_ID=PA.PaymentArrangement_ID







                                                                                          where CT.ChargeType='Universal')















                        --Get and set the current year price based on Universal Chargecode.







                        set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge where GETDATE() between DateValidFrom and DateValidTo and







                                                PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID







                                                and ChargeCode_ID=@ChargeCode_ID)















                        if(@Price<>0.00 or @Price<>NULL)







                                BEGIN







                                        --Retrieving charge code,short description,long description,price







                                        Select Top 1 CC.Chargecode, left(CC.ShortDescription,50)as ShortDescription ,CC.LongDescription,@Price as Price,CG.CodeGroupDescr from ChargeCode cc







                                        inner join CodeGroup CG on cc.CodeGroupID = CG.CodeGroupID where cc.CodeGroupID in(4,3)







                                        and CC.CodeGroupdetail_ID=1  and CC.TYPEID=@practitionerGroup_id and CC.ChargeCode=@chargeCode and  CC.ShortDescription like @ShortDescription +'%'







                                END







                        ELSE







                                BEGIN







                                        set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge where PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID







                                                                and ChargeCode_ID=@ChargeCode_ID order by DateValidTo desc)







                                        Select Top 1  CC.Chargecode, left(CC.ShortDescription,50)as ShortDescription ,CC.LongDescription,@Price as Price,CG.CodeGroupDescr from ChargeCode cc







                                        inner join CodeGroup CG on cc.CodeGroupID = CG.CodeGroupID where cc.CodeGroupID in(4,3)







                                        and CC.CodeGroupdetail_ID=1  and CC.TYPEID=@practitionerGroup_id and CC.ChargeCode=@chargeCode and  CC.ShortDescription like @ShortDescription +'%'







                                END







                END







END
















GO
