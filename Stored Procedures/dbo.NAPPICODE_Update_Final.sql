SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create Procedure [dbo].[NAPPICODE_Update_Final]
AS
BEGIN
Declare 	@Nappi nvarchar(20),
		@IsMedMat nvarchar(20),
		@Desc nvarchar(3000),
		@ProductStrength nvarchar(50),
		@DosageFormCode nvarchar(50),
	@NappiSuffix nvarchar(50),
	 @NappiDesc nvarchar(150),
	 @PackSize nvarchar(500),
	 @PackPrice nvarchar(800),
	@Schedule nvarchar(5)
 


 Declare @Count int,@processed_rows int 

 select @Count= count(*) from TempTableNappi as N
 inner join  TempTableNappiCode NC on N.Nappi = NC.NappiCode


 Print ('Total Rows tobe processed: ' );
 print (@Count);
set @processed_rows = 0
DECLARE db_cursor CURSOR FOR  
select N.Nappi, N.Description as NDescrption, N.DosageFormCode, N.IsMedMat, N.ProductStrength, NC.NappiCodeWithSuffix, NC.Description NCodeDesc,NC.PackSize
, NC.PackPrice, NC.Schedule from TempTableNappi as N
 inner join  TempTableNappiCode NC on N.Nappi = NC.NappiCode

oPEN db_cursor
 
 Declare  @medMat int, @isExist int,@itemID int,
	@stockCodeID int, @isNappiLike int,  @StockIDCode int, @StockIDItem int,
	 @first varchar(250), @last varchar(250),@isPack int, @idSCode int, @idSItem int, @isNCLike int, @isChk int,
	@firstPack varchar(250), @lastPack varchar(250),@RetailPrice nvarchar(800),@WholeSalePrice nvarchar(800), @ProductDesc nvarchar(4000),
	@Description nvarchar(4000), @TempDesc nvarchar(150), @TempSTG nvarchar(50), @TempDSG nvarchar(50),
	@stkCodeID int, @StkitemID int,@id int,@chargeType_ID int

FETCH NEXT FROM db_cursor INTO
@Nappi, @Desc,@DosageFormCode,@IsMedMat,@ProductStrength,@NappiSuffix,@NappiDesc,@PackSize, @PackPrice,@Schedule;

WHILE @@FETCH_STATUS = 0
BEGIN
			 set @processed_rows= @processed_rows +1
			 
			 print ('Row Counter :');
			 print (@processed_rows);
			SET @Description = UPPER(LTRIM(RTRIM(@Desc)) + ' ' + @DosageFormCode + LTRIM(RTRIM(@ProductStrength)))

			IF(@IsMedMat = 'E')
				BEGIN
					set @medMat = 1
				END
			ELSE IF(@IsMedMat = 'S')
				BEGIN
					set @medMat = 2
				END	
			ELSE IF(@IsMedMat = 'R')
				BEGIN
					set @medMat = 7
				END	
	
			--SET @first = Substring(@PackPrice,1,(len(@PackPrice)-3))
			--SET @last = Substring(@PackPrice,(len(@PackPrice)-2),3)
			SET @WholeSalePrice =@PackPrice -- @first+'.'+@last
			print(@WholeSalePrice)
			SET @RetailPrice = CONVERT(varchar(250),(CONVERT(decimal(9,3), (CONVERT(decimal(9,3),@WholeSalePrice)/CONVERT(decimal(9,2),@PackSize)))))

		select @isExist = COUNT(*) from StockCode c join StockItem i on c.StockItemID = i.StockItemID 
			join CodeGroup g on g.CodeGroupID = i.CodeGroupID where c.StockCodeTypeID =1 and 
			c.Code = @NappiSuffix --and c.Discontinued = 0
	
			IF(@isExist <> 0)
				BEGIN
					IF(@isExist = 1) 
						BEGIN
							select @stockCodeID = c.StockCodeID,@itemID = c.StockItemID from StockCode c join
							StockItem i on c.StockItemID = i.StockItemID join 
							CodeGroup g on g.CodeGroupID = i.CodeGroupID where c.StockCodeTypeID =1 and 
							c.Code = @NappiSuffix --and c.Discontinued = 0
							
							UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize) 
							,PackPrice = convert(decimal(9,3),@WholeSalePrice),
							UnitPrice = convert(decimal(9,3),@RetailPrice), Discontinued = 0
							WHERE StockCodeID = @stockCodeID
								    
							UPDATE StockItem SET Description = @Description,CodeGroupID = @medMat,
							Schedule = @Schedule, PackSize = convert(decimal(9,2),@PackSize) 
							WHERE StockItemID = @itemID
						END
					ELSE IF(@isExist <> 1 and @isExist <> 0) 
						BEGIN
							/*update StockCode set Discontinued = 1 where Code in (select code from StockCode
							where StockCodeTypeID =1 and Discontinued = 0 group by Code having (COUNT(Code) > 1)) 
							and StockCodeTypeID =1 and StockCodeID not in(select max(StockCodeID) from StockCode 
							where StockCodeTypeID =1 and Discontinued = 0 group by Code having (COUNT(Code) > 1))*/
							
							update StockCode set Discontinued = 1 where Code = @NappiSuffix and Discontinued = 0
							and StockCodeTypeID =1 and StockCodeID not in(select max(StockCodeID) from StockCode 
							where StockCodeTypeID =1 and Discontinued = 0 and Code = @NappiSuffix)
							
							select @stockCodeID = c.StockCodeID,@itemID = c.StockItemID from StockCode c join
							StockItem i on c.StockItemID = i.StockItemID join 
							CodeGroup g on g.CodeGroupID = i.CodeGroupID where c.StockCodeTypeID =1 and 
							c.Code = @NappiSuffix and c.Discontinued = 0
							
							UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize)
							,PackPrice = convert(decimal(9,3),@WholeSalePrice),
							UnitPrice = convert(decimal(9,3),@RetailPrice), Discontinued = 0  
							WHERE StockCodeID = @stockCodeID						  
														    
							UPDATE StockItem SET Description = @Description,CodeGroupID = @medMat,
							Schedule = @Schedule, PackSize = convert(decimal(9,2),@PackSize) 
							WHERE StockItemID = @itemID
						END
				END
			ELSE IF(@isExist = 0)
				BEGIN
					select @isNappiLike = COUNT(*) from StockCode c where --c.Discontinued = 0 and 
					c.StockCodeTypeID = 1 and c.Code like @Nappi+'%' 
					
					IF(@isNappiLike = 0) 
						BEGIN
							select @isChk = COUNT(*) from StockCode where Code = @NappiSuffix
							IF(@isChk <> 0)
								BEGIN
									update StockCode set Discontinued = 1 where Code = @NappiSuffix
								END
								
							INSERT into StockItem (CodeGroupID,Description,Status,PrefStockLevel,
							MinStockLevel,PackSize,Schedule) values (@medMat,@Description,'List Item',0,0,
							convert(decimal(9,2),@PackSize),@Schedule)
							
							set @id = SCOPE_IDENTITY()
							
							INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,PackPrice,
							UnitPrice,Discontinued)
							Values (@id,1,@NappiSuffix,convert(decimal(9,2),@PackSize),
							convert(decimal(9,3),@WholeSalePrice),convert(decimal(9,3),@RetailPrice),0)
							
							/*INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,Discontinued)
							Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),0)*/
						END
					ELSE IF(@isNappiLike = 1) 
						BEGIN
							select @isNCLike = COUNT(*) from StockCode c where --c.Discontinued = 0 and
							c.StockCodeTypeID = 1 and c.Code like @Nappi+'%' and 
							c.PackSize = convert(decimal(9,2),@PackSize)
							
							IF(@isNCLike = 1)
								BEGIN
									select @StockIDCode = c.StockCodeID, @StockIDItem = c.StockItemID from 
									StockCode c where --c.Discontinued = 0 and 
									c.StockCodeTypeID = 1 and c.Code like @Nappi+'%'
									
									UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize),
									Code = @NappiSuffix, Discontinued = 0
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice) 
									WHERE StockCodeID = @StockIDCode						  
																    
									UPDATE StockItem SET Description = @Description,Schedule = @Schedule, 
									PackSize = convert(decimal(9,2),@PackSize) 
									WHERE StockItemID = @StockIDItem 
								END
							ELSE IF(@isNCLike = 0)
								BEGIN
									select @isChk = COUNT(*) from StockCode where Code = @NappiSuffix
									IF(@isChk <> 0)
										BEGIN
											update StockCode set Discontinued = 1 where Code = @NappiSuffix
										END
									INSERT into StockItem (CodeGroupID,Description,Status,PrefStockLevel,
									MinStockLevel,PackSize,Schedule) values (@medMat,@Description,'List Item',0,0,
									convert(decimal(9,2),@PackSize),@Schedule)
									
									set @id = SCOPE_IDENTITY()
									
									INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,PackPrice,
									UnitPrice,Discontinued)
									Values (@id,1,@NappiSuffix,convert(decimal(9,2),@PackSize),
									convert(decimal(9,3),@WholeSalePrice),convert(decimal(9,3),@RetailPrice),0)
									
									/*INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,Discontinued)
									Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),0)	*/
								END					
							ELSE IF(@isNCLike <> 1 and @isNCLike <> 0)
								BEGIN
									UPDATE StockCode SET Discontinued = 1 WHERE Code like @Nappi+'%' and 
									Discontinued = 0 and StockCodeTypeID = 1 and 
									PackSize = convert(decimal(9,2),@PackSize) and
									StockCodeID not in(select max(StockCodeID) FROM StockCode 
									WHERE StockCodeTypeID =1 and Discontinued = 0 and Code like @Nappi+'%'
									and PackSize = convert(decimal(9,2),@PackSize))
							
									SELECT @stkCodeID = c.StockCodeID,@StkitemID = c.StockItemID 
									FROM StockCode c WHERE c.Discontinued = 0 and
									c.StockCodeTypeID = 1 and c.Code like @Nappi+'%' and 
									c.PackSize = convert(decimal(9,2),@PackSize)
							
									UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize),
									Code = @NappiSuffix, Discontinued = 0
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice) 
									WHERE StockCodeID = @stkCodeID						  
																	    
									UPDATE StockItem SET Description = @Description,Schedule = @Schedule, 
									PackSize = convert(decimal(9,2),@PackSize) 
									WHERE StockItemID = @StkitemID
								END
						END
					ELSE IF(@isNappiLike <> 0 and @isNappiLike <> 1)
						BEGIN						
							select @isPack = COUNT(*) from StockCode c where c.Code like @Nappi+'%' and
							c.PackSize = convert(decimal(9,2),@PackSize) and c.StockCodeTypeID = 1  
							--and c.Discontinued = 0 
							
							IF(@isPack = 1)
								BEGIN
									select @idSCode = StockCodeID, @idSItem = StockItemID from StockCode c 
									where c.Code like @Nappi+'%' and --c.Discontinued = 0 and
									c.PackSize = convert(decimal(9,2),@PackSize) and c.StockCodeTypeID = 1  								 
								
									update StockCode set Code = @NappiSuffix
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice), Discontinued = 0
									where  StockCodeID = @idSCode
									
									update StockItem set Description = @Description,CodeGroupID = @medMat,
									Schedule = @Schedule WHERE StockItemID = @idSItem  
								END
							ELSE IF(@isPack = 0)
								BEGIN
									select @isChk = COUNT(*) from StockCode where Code = @NappiSuffix
									IF(@isChk <> 0)
										BEGIN
											update StockCode set Discontinued = 1 where Code = @NappiSuffix
										END
									INSERT into StockItem (CodeGroupID,Description,Status,PrefStockLevel,
									MinStockLevel,PackSize,Schedule) values (@medMat,@Description,'List Item',0,0,
									convert(decimal(9,2),@PackSize),@Schedule)
									
									set @id = SCOPE_IDENTITY()
									
									INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,PackPrice,
									UnitPrice,Discontinued)
									Values (@id,1,@NappiSuffix,convert(decimal(9,2),@PackSize),
									convert(decimal(9,3),@WholeSalePrice),convert(decimal(9,3),@RetailPrice),0)
									
									/*INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,Discontinued)
									Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),0)*/
								END
							ELSE IF(@isPack <> 0 and @isPack <> 1)
								BEGIN
									update StockCode set Discontinued = 1 where Code like @Nappi+'%' 
									and Discontinued = 0 
									and StockCodeTypeID =1 and PackSize = convert(decimal(9,2),@PackSize) and
									StockCodeID not in(select max(StockCodeID) from StockCode 
									where StockCodeTypeID =1 and Discontinued = 0 and Code like @Nappi+'%'
									and PackSize = convert(decimal(9,2),@PackSize))
									
									select @idSCode = StockCodeID, @idSItem = StockItemID from StockCode 
									where StockCodeTypeID =1 and Discontinued = 0 and Code like @Nappi+'%'
									and PackSize = convert(decimal(9,2),@PackSize)
									
									update StockCode set Code = @NappiSuffix
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice)
									where  StockCodeID = @idSCode
									
									update StockItem set Description = @Description,CodeGroupID = @medMat,
									Schedule = @Schedule WHERE StockItemID = @idSItem
								END						
						END				
				END

FETCH NEXT FROM db_cursor INTO
@Nappi, @Desc,@DosageFormCode,@IsMedMat,@ProductStrength,@NappiSuffix,@NappiDesc,@PackSize, @PackPrice,@Schedule;

END

CLOSE db_cursor
DEALLOCATE db_cursor

END
GO
