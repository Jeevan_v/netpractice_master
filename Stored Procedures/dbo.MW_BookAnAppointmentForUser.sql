SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_BookAnAppointmentForUser]  
(
@BookedAppointmentID varchar(50),
@PatientID INT,
@PracticeID INT,
@VenueID INT,
@DoctorID INT,
@CreatedBy VARCHAR(50),
@CreatorID INT,
@StartTime VARCHAR(50),
@EndTime VARCHAR(50),
@AppointmentDate DATE,
@AppointmentStatus VARCHAR(30),
@IsBooked bit,
@RescheduleUserID INT,
@ReschedulePatientID INT,
@IsCancel BIT
--@BookingResult Varchar(200) OUTPUT
)
AS
DECLARE @AppointmentID varchar(50)
DECLARE @PatientAppointment INT
DECLARE @BookAppointment INT
--Scheduling for New Appointment
IF @BookedAppointmentID IS NULL
BEGIN
--Check patient already has appointment
SET @PatientAppointment = (select  count(A.AppointmentID) from Appointment A
where (
@StartTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or
@EndTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or 
DATEADD(minute,1,convert(time,StartTime)) between @StartTime  and @EndTime 
or 
DATEADD(minute,-1,convert(time,EndTime)) between @StartTime  and @EndTime
)
AND
A.[Date]= @AppointmentDate AND A.PatientID=@PatientID AND A.IsBooked=1 )
IF(@PatientAppointment=0)
BEGIN
--Check doctor already has appointment
SET @BookAppointment = (select  count(A.AppointmentID) from Appointment A
where (
@StartTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or
@EndTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or 
DATEADD(minute,1,convert(time,StartTime)) between @StartTime  and @EndTime 
or 
DATEADD(minute,-1,convert(time,EndTime)) between @StartTime  and @EndTime
)
AND
A.[Date]= @AppointmentDate
AND
A.DoctorID=@DoctorID
AND
A.IsBooked=1
)
 IF (@PatientAppointment = 0 AND @BookAppointment = 0)
BEGIN
 -- Create Appointment ID For Appointment 
 DECLARE @Count VARCHAR(10)  
-- SET @Count = (SELECT RIGHT('000'+CONVERT(VARCHAR(3),COUNT(*)+1),3) FROM Appointment WHERE [DATE]=@AppointmentDate AND VenueID=@VenueID)
SET @Count = (SELECT RIGHT('000'+CONVERT(VARCHAR(3),COUNT(*)+1),3) FROM Appointment 
where AppointmentID like 
(convert(varchar(10),@PracticeID)+Convert(varchar(10),@VenueID) +
right('00'+CONVERT (VARCHAR(4),DATEPART(year,@AppointmentDate)),2)+
right('00' + convert(varchar(2),DATEPART(month,@AppointmentDate)),2))
+right('00' + convert(varchar(2),DATEPART(DD,@AppointmentDate)),2)+'%')
SET @AppointmentID = (CONVERT(VARCHAR(10),@PracticeID)+CONVERT(VARCHAR(10),@VenueID)+ right('00'+CONVERT (VARCHAR(4),DATEPART(year,@AppointmentDate)),2) +  
right('00' + convert(varchar(2),DATEPART(month,@AppointmentDate)),2)
+right('00' + convert(varchar(2),DATEPART(DD,@AppointmentDate)),2)
+@Count)
 DECLARE @CreatedTime DATETIME
 SELECT @CreatedTime= GETDATE()
 INSERT INTO Appointment(
	   [AppointmentID]	   
      ,[Date]
      ,[StartTime]
      ,[EndTime]
      ,[CreatedBy]
      ,[CreatorID]
      ,[AppointmentStatus]
      ,[IsBooked]
      ,[CreatedTime]
      ,[VenueID]
      ,[DoctorID]
      ,[PatientID]
      ,[RescheduleUserID]
      ,[RescheduledTime]
      ,[ReschedulePatientID]
      )
      VALUES
      ( @AppointmentID,
		@AppointmentDate,
		@StartTime,
        @EndTime,
        @CreatedBy,
        @CreatorID,
        @AppointmentStatus,
        @IsBooked,
        @CreatedTime,
        @VenueID,
        @DoctorID,
        @PatientID,
        @RescheduleUserID,
        NULL,
        @ReschedulePatientID
      )
--SET @BookingResult = 'Your Booking ID: ' + @AppointmentID
SELECT 'Your Appointment ID: ' + @AppointmentID
END	
ELSE
BEGIN
--SET @BookingResult = 'Appointment Already Exist at this Time. Please select Different Timing'
SELECT 'Doctor has an appointment at this time. Please select Different Timing.'
END
END
ELSE
BEGIN
SELECT 'This patient have an appointment at this time. Please select different Timing.'
END
END

--Reschudling excisting appointment
ELSE if @IsCancel IS NULL 
BEGIN
DECLARE @IsAppointmentCancelled BIT
SET @IsAppointmentCancelled = (SELECT IsBooked FROM Appointment WHERE AppointmentID = @BookedAppointmentID)
IF(@IsAppointmentCancelled = 1)
BEGIN
--check patient already has appointment
SET @PatientAppointment = (select  count(A.AppointmentID) from Appointment A
where (
@StartTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or
@EndTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or 
DATEADD(minute,1,convert(time,StartTime)) between @StartTime  and @EndTime 
or 
DATEADD(minute,-1,convert(time,EndTime)) between @StartTime  and @EndTime
)
AND
A.[Date]= @AppointmentDate
AND
A.PatientID=@PatientID
AND
A.IsBooked=1
AND
A.AppointmentID != @BookedAppointmentID
)
IF(@PatientAppointment=0)
BEGIN
--Check doctor already has appointment
SET @BookAppointment = (select  count(A.AppointmentID) from Appointment A
where (
@StartTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or
@EndTime  BETWEEN DATEADD(minute,1,convert(time,StartTime)) and DATEADD(minute,-1,convert(time,EndTime))
or 
DATEADD(minute,1,convert(time,StartTime)) between @StartTime  and @EndTime 
or 
DATEADD(minute,-1,convert(time,EndTime)) between @StartTime  and @EndTime
)
AND
A.[Date]= @AppointmentDate
AND
A.DoctorID=@DoctorID
AND
A.IsBooked=1
AND
A.AppointmentID != @BookedAppointmentID
)
IF (@BookAppointment = 0)
Begin
---INSERTING RECORD
UPDATE Appointment SET
      [Date] = @AppointmentDate,
      [StartTime]=@StartTime,
      [EndTime]= @EndTime,
      [AppointmentStatus]= @AppointmentStatus,
      [VenueID]=  @VenueID,
      [RescheduleUserID] = @RescheduleUserID,
      [RescheduledTime]=GETDATE(),
      [ReschedulePatientID]=@ReschedulePatientID
      
      where AppointmentID=@BookedAppointmentID

SELECT 'Appointment has been Rescheduled Successfully.'+','
+ISNULL((select Email from aspnet_Membership  am inner join Patient p on p.MembershipID = am.UserId 
where p.PatientID=@PatientID),' ')
END
---INSERTING RECORD
ELSE
BEGIN
--SET @BookingResult = 'Appointment Already Exist at this Time. Please select Different Timing'
SELECT 'Doctor has an appointment at this time. Please select Different Timing.'
END
END
ELSE
BEGIN
SELECT 'This patient have an appointment at this time. Please select different Timing.'
END
END
ELSE
BEGIN
SELECT 'Your Appointment has been Cancelled by Another User.'
END
END
---Cancellling of appointment
ELSE
BEGIN
UPDATE Appointment set IsBooked=0
WHERE AppointmentID=@BookedAppointmentID
SELECT 'Your Appointment has been cancelled successfully'
END

GO
