SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  /****************************************************************
  ***** Change by: Sachin Gandhi
  ***** Change Date : 19 Nov 2014
  ***** Description: Removing distinct clause and putting order by visitline id(bug#22)
  ****************************************************************/
CREATE PROCEDURE [dbo].[MW_GetConsultationDetailByVisistID]   
  
@VisitID int  
    
AS  
  
BEGIN  
  
SELECT  
  
vl.VisitLineID,
   
 vl.Diagnosis,   
   
 vl.Description,   
   
 vl.Code ,   
   
 vl.Quantity ,   
 vl.DosageID,  
 ISNULL(d.Size,'--NA--') AS [Size],  
 ISNULL(d.Dose,'--NA--') AS [Dose],  
 ISNULL(d.DoseForm,'--NA--') AS [DoseForm],  
 ISNULL(d.DoseFrequency,'--NA--') AS [DoseFrequency],  
 ISNULL(d.[Route/Location],'--NA--') AS [Route/Location] ,  
 ISNULL(d.OtherInstruction,'--NA--') AS [OtherInstruction],  
 ISNULL(d.[Time/Period],'--NA--') AS [Time/Period],  
 vl.Amount  
  
FROM  
  
 VisitLine vl   
  LEFT join VisitLineDosage d  
 on vl.DosageID=d.VisitLineDosageID  
   
WHERE  
  
 vl.VisitID = @VisitID 
 order by vl.VisitLineID 
   
END  
  
GO
