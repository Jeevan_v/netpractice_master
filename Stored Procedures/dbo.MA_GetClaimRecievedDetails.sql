SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MA_GetClaimRecievedDetails] 
@PracticeID INT,
@ClaimStatus VARCHAR(100),
@StartDate DATE,
@EndDate DATE,
@UserID INT,
@MedicalScheme Varchar(100)
AS
BEGIN
IF (@PracticeID != 0 ) 
BEGIN
SELECT
	LOWER(pat.FirstName + ' ' + pat.LastName) AS PatientName,
	(ISNULL(cl.[TotalAmount],0)) AS AmountPaid,
	((ISNULL(cl.[TotalAmount],0))  - 
	((ISNULL(cl.AmountPaid,0))+ SUM(ISNULL(p.AmountPaid,0)))) 	
	AS [Balance],
	pat.[PatientID],
	v.[VisitID],
	[ClaimID],
	[PracticeID],
	LOWER([Status]) as [Status],
	[SchemeName],
	[AccountNumber],
	v.[DateLogged] AS VisitDate,
	v.[DoctorID] as [UserID],
	V.InvoiceNo
FROM

Visit v 

INNER JOIN Claim cl ON v.VisitID=cl.VisitID
INNER JOIN Account ac ON v.AccountID=ac.AccountID
INNER JOIN Patient pat ON ac.PatientID=pat.PatientID
INNER JOIN MedicalScheme ms ON pat.SchemeID=ms.MedicalSchemeID
LEFT JOIN Payment P ON V.VisitID=P.VisitID
WHERE	
	PracticeID=@PracticeID	
	And
	ms.SchemeName = @MedicalScheme
AND 
	CONVERT(date,v.[DateLogged]) BETWEEN 
	CONVERT(DATE,@StartDate) AND CONVERT(DATE,@EndDate)
AND 
	LOWER((CASE WHEN @ClaimStatus <> 'Show All' THEN cl.[Status] ELSE '1' END) )
	=LOWER((CASE WHEN @ClaimStatus <> 'Show All' THEN @ClaimStatus ELSE '1' END))

GROUP BY
pat.FirstName,pat.LastName,pat.[PatientID],
v.[VisitID],[ClaimID],[PracticeID],
[Status],[SchemeName],[AccountNumber],
v.[DateLogged],v.[DoctorID],cl.[TotalAmount],cl.AmountPaid,V.InvoiceNo
ORDER BY v.VisitID DESC
END
else
BEGIN
SELECT
	LOWER(pat.FirstName + ' ' + pat.LastName) AS PatientName,
	(ISNULL(cl.[TotalAmount],0)) AS AmountPaid,
	((ISNULL(cl.[TotalAmount],0))  - 
	((ISNULL(cl.AmountPaid,0))+ SUM(ISNULL(p.AmountPaid,0)))) 	
	AS [Balance],
	pat.[PatientID],
	v.[VisitID],
	[ClaimID],
	[PracticeID],
	LOWER([Status]) as [Status],
	[SchemeName],
	[AccountNumber],
	v.[DateLogged] AS VisitDate,
	v.[DoctorID] as [UserID],
	V.InvoiceNo
FROM

Visit v 

INNER JOIN Claim cl ON v.VisitID=cl.VisitID
INNER JOIN Account ac ON v.AccountID=ac.AccountID
INNER JOIN Patient pat ON ac.PatientID=pat.PatientID
INNER JOIN MedicalScheme ms ON pat.SchemeID=ms.MedicalSchemeID
LEFT JOIN Payment P ON V.VisitID=P.VisitID
WHERE	
	ms.SchemeName = @MedicalScheme
AND 
	CONVERT(date,v.[DateLogged]) BETWEEN 
	CONVERT(DATE,@StartDate) AND CONVERT(DATE,@EndDate)
AND 
	LOWER((CASE WHEN @ClaimStatus <> 'Show All' THEN cl.[Status] ELSE '1' END) )
	=LOWER((CASE WHEN @ClaimStatus <> 'Show All' THEN @ClaimStatus ELSE '1' END))

GROUP BY
pat.FirstName,pat.LastName,pat.[PatientID],
v.[VisitID],[ClaimID],[PracticeID],
[Status],[SchemeName],[AccountNumber],
v.[DateLogged],v.[DoctorID],cl.[TotalAmount],cl.AmountPaid,V.InvoiceNo
ORDER BY v.VisitID DESC
END
END
GO
