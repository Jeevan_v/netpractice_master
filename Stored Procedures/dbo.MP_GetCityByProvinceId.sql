SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetCityByProvinceId] 
      @ProvinceId int
AS
BEGIN

SELECT

	cty.CityID,
	cty.City
	
	
FROM

dbo.City cty

Inner Join 

dbo.ProvinceCityRelationship pcr

ON

cty.CityID= pcr.CityID

WHERE

pcr.ProvinceID=@ProvinceId 

END	

GO
