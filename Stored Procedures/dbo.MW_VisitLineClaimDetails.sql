SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_VisitLineClaimDetails]      
@visitclaimdtl [VisitClaimDetailTypeNew] READONLY      
AS      
BEGIN      
  DECLARE @visitlineID int, @claimDetailID int,@MemoID int , @TaxAmount Decimal(36,2),@VisitLineDosageID int    
    
  --Declare for cursor--    
    
  Declare @ClaimID varchar(50),@VisitID varchar(50),@Quantity varchar(50),@Amount varchar(50),@LineDefination varchar(50),    
  @DispFee varchar(50),@NoDays varchar(50),@IsChronic varchar(50),    
  @Code varchar(50),@Diagnosis varchar(8000),@Description varchar(8000),    
  @DosageID varchar(50),@DailyDosage varchar(50),@Starttime varchar(50),@EndTime varchar(50),    
  @Memo varchar(50),    
  @Size varchar(50),    
  @Dose varchar(50),    
  @DoseForm varchar(50),    
  @DoseFrequency varchar(50),    
  @RouteLocation varchar(50),    
  @OtherInstruction varchar(50),    
  @TimePeriod varchar(50),    
  @IsModifier varchar(2),    
  @ModifierType varchar(200),@FuntionType varchar(200),@FunctionAmount varchar(50),@AssistantPracticeNo varchar(50) ,@AssistantMpNo varchar(20),@ModifierAppliedCode varchar(20)  
    
  Declare @VisitlineIDforloop varchar(50)     
    
    BEGIN       
    
  Delete from VisitLineDosage where VisitLineDosageID in(select DosageID  from VisitLine where VisitID in(select VisitID from @visitclaimdtl))     
  --Delete from ClaimError  where ClaimID in(select ClaimID from @visitclaimdtl)    
  Delete from Memo where VisitLineID in(select VisitLineID from VisitLine where VisitID in(select VisitID from @visitclaimdtl))     
  --Change for modifier By priyanka    
  delete from ModifierTransaction where VisitLineID in(select VisitLineID from VisitLine where VisitID in(select VisitID from @visitclaimdtl))     
  Delete from ClaimDetail where ClaimID in(select ClaimID from @visitclaimdtl)    
    
        
    
  END      
    
  BEGIN        
    
  Delete from  VisitLine where VisitID in(select VisitID from @visitclaimdtl)     
    
  END      
      
     
    
  DECLARE VisitlineID_cursor CURSOR FOR      
    
    Select * from @visitclaimdtl    
    insert into Module (DisplayName)     
    select Amount from @visitclaimdtl    
             
    
    OPEN VisitlineID_cursor      
    
 FETCH NEXT FROM VisitlineID_cursor INTO @ClaimID,@VisitID,@Quantity,@Amount,@LineDefination,@DispFee,@NoDays,@IsChronic,@Code,@Diagnosis,@Description,@DosageID,@DailyDosage,@Starttime,@EndTime,@Memo,    
  @Size ,@Dose ,    
  @DoseForm ,    
  @DoseFrequency ,    
  @RouteLocation ,    
  @OtherInstruction,    
  @TimePeriod    
  ,@IsModifier,@ModifierType,@FuntionType,@FunctionAmount ,@AssistantPracticeNo ,@AssistantMpNo,@ModifierAppliedCode  
    
 WHILE @@FETCH_STATUS = 0    
    
 BEGIN      
       insert into Module (DisplayName) values ('variable asign'+@Amount);    
       if(UPPER(@LineDefination)='PROCEDURE'or UPPER(@LineDefination) ='CONSULTATION')    
       Begin    
           set @TaxAmount=Convert(Decimal(36,2),@Amount)+(Convert(Decimal(36,2),@Amount)*0.14);    
            
       end    
       else    
       Begin    
     set  @TaxAmount=(Convert(Decimal(36,2),@Amount)-@DispFee)+(Convert(Decimal(36,2),@Amount)-@DispFee)*0.14;    
    end    
           
     
    
 --Inserting into VisitLine Table    
 insert into  VisitLineDosage(Size,Dose,DoseForm,DoseFrequency,[Route/Location],[OtherInstruction],[Time/period])    
 values( @Size,@Dose ,@DoseForm ,@DoseFrequency ,@RouteLocation ,@OtherInstruction ,@TimePeriod)     
 set @VisitLineDosageID=(select SCOPE_IDENTITY())    
    
    
 insert into VisitLine(VisitID,Quantity,Amount,LineDefinition,DispFee,NoDays,IsChronic,Code,Diagnosis,Description,DosageID,DailyDosage,Starttime,Endtime,TaxAmount,IsModifier,AssistantPracticeNo,AssistantMpNo, ModifierAppliedCode)    
    
 --Values (@VisitID,@Quantity,@Amount,@LineDefination,@DispFee,@NoDays,@IsChronic,@Code,@Diagnosis,@Description,@VisitLineDosageID,@DailyDosage,@Starttime,convert(varchar(8),getdate(),108),@TaxAmount)    
 Values (@VisitID,@Quantity,@Amount,@LineDefination,@DispFee,@NoDays,@IsChronic,@Code,@Diagnosis,@Description,@VisitLineDosageID,@DailyDosage,@Starttime,@EndTime,@TaxAmount,@IsModifier,@AssistantPracticeNo,@AssistantMpNo,@ModifierAppliedCode)    
    
     
    
 SET @visitlineID = (select SCOPE_IDENTITY())    
    
     
    
 insert into ClaimDetail(ClaimID,VisitLineID,NappiCode) values (@ClaimID,@visitlineID,@Code)    
    
     
    insert into ModifierTransaction(VisitID,VisitlineID,ModifierType,FunctionType,FunctionAmount)values(@VisitID,@visitlineID,@ModifierType,@FuntionType,@FunctionAmount)    
 insert into Memo(VisitLineID,Memo) values (@visitlineID,@Memo)    
     
    
 FETCH NEXT FROM VisitlineID_cursor INTO @ClaimID,@VisitID,@Quantity,@Amount,@LineDefination,@DispFee,@NoDays,@IsChronic,@Code,@Diagnosis,@Description,@DosageID,@DailyDosage,@Starttime,@EndTime,@Memo,    
 @Size ,@Dose ,    
  @DoseForm ,    
  @DoseFrequency ,    
  @RouteLocation ,    
  @OtherInstruction,    
  @TimePeriod    
  ,@IsModifier,@ModifierType,@FuntionType,@FunctionAmount,@AssistantPracticeNo,@AssistantMpNo ,@ModifierAppliedCode   
 END    
    
   CLOSE VisitlineID_cursor    
    
   DEALLOCATE VisitlineID_cursor    
    
END 

GO
