SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_savexmlfilename]    
    @ClaimID int,    
 @xmlfile varchar(2000)  ,  
 @Isrev char(10),
 @ClaimDetailID  int
     
AS    
BEGIN    
 insert into ClaimXML     
 values(@ClaimID,@xmlfile,@Isrev ,@ClaimDetailID)    
END 

GO
