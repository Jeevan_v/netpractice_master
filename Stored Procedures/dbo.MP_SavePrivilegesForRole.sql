SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MP_SavePrivilegesForRole](
	@RoleID uniqueidentifier,
	@RolePrivileges TVP_Privileges READONLY)
AS
BEGIN
	DELETE FROM PrivilegeRoleRelationship
	WHERE RoleID =@RoleID;
					  
	INSERT INTO PrivilegeRoleRelationship ( RoleID, 
											PrivilegeID)
	SELECT @RoleID, PrivilegeID
	FROM @RolePrivileges;						  
					  
END

GO
