SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DisplayChargeType]  	
AS

BEGIN
select c.ChargeType from ChargeType c order by c.ChargeType asc
END
GO
