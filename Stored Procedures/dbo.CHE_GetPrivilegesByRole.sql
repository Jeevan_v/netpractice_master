SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Sameer>
-- Create date: <4/12/2012>
-- Description:	<Get Rolewise Privileges>
-- =============================================
CREATE PROCEDURE [dbo].[CHE_GetPrivilegesByRole]
AS
BEGIN
SELECT

	pri.Privilege,ar.RoleName,pri.PrivilegeID
	
FROM

	dbo.Privilege pri

INNER JOIN 

	dbo.PrivilegeRoleRelationship prr

ON

	pri.PrivilegeID=prr.PrivilegeID

INNER JOIN 

	dbo.aspnet_Roles ar

ON

	prr.RoleID=ar.RoleId

ORDER BY ar.RoleName
END


GO
