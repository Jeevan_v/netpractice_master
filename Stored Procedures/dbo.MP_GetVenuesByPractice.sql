SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetVenuesByPractice]
	@PracticeID int,@IsActive bit
	
AS
IF @IsActive IS Not Null
BEGIN
SELECT
	VenueID,
	PracticeID,
	VenueName,
	PhysicalAddressID,
	PostalAddressID,
	Telephone,
	ReasonID,
	Fax,
	IsActive
FROM
	Venue
WHERE
	PracticeID = @PracticeID and
	IsActive = @IsActive
	
	END

	ELSE

BEGIN
SELECT
	VenueID,
	PracticeID,
	VenueName,
	PhysicalAddressID,
	PostalAddressID,
	Telephone,
	ReasonID,
	Fax,
	IsActive
FROM
	Venue
WHERE
	PracticeID = @PracticeID
	
	END


GO
