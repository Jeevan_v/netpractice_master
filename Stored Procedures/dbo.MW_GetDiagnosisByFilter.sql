SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetDiagnosisByFilter] 
	@FilterType varchar(20)
	,@Key varchar(25)
	AS
IF @FilterType='Code'
BEGIN
	SELECT
		d.Code,
		d.[Description],
		dtl.DiagnosisTypeID,
		dt.TypeDefinition
		
	  FROM
		Diagnosis d		
	  INNER JOIN
		DiagnosisTypeLink dtl
	  ON
		d.DiagnosisID=dtl.DiagnosisID
		INNER JOIN
		DiagnosisType dt
		on
		dtl.DiagnosisTypeID=dt.DiagnosisTypeID
	  WHERE
		d.Code like @key + '%' 
END 
ELSE IF @FilterType='Description'
BEGIN
	SELECT
		d.Code,
		d.[Description],
		dtl.DiagnosisTypeID,
		dt.TypeDefinition
		
	  FROM
		Diagnosis d		
	  INNER JOIN
		DiagnosisTypeLink dtl
	  ON
		d.DiagnosisID=dtl.DiagnosisID
		INNER JOIN
		DiagnosisType dt
		on
		dtl.DiagnosisTypeID=dt.DiagnosisTypeID
	WHERE
		UPPER(d.[Description]) like '%'+ @key + '%'  
		order by dtl.DiagnosisTypeID asc
	END
	
	ELSE IF @FilterType='ALL'
BEGIN
	SELECT
		d.Code,
		d.[Description],
		dtl.DiagnosisTypeID,
		dt.TypeDefinition
		
	  FROM
		Diagnosis d	with(nolock)	
	  INNER JOIN
		DiagnosisTypeLink dtl with (nolock)
	  ON
		d.DiagnosisID=dtl.DiagnosisID
		INNER JOIN
		DiagnosisType dt
		on
		dtl.DiagnosisTypeID=dt.DiagnosisTypeID
	 
	END
GO
