SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetRoles] 
AS

SELECT
	RoleId,
	RoleName
	
FROM
	aspnet_Roles
	
WHERE	
RoleName != 'Patient'


GO
