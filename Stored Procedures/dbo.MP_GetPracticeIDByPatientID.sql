SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MP_GetPracticeIDByPatientID]
	@patientid  integer
	
AS
BEGIN
SELECT
--Temporary "top 1"
top 1
PracticeID 
FROM
dbo.[Account] 
WHERE
PatientID=@patientid
END

GO
