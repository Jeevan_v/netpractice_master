SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateMKOptionCode]  
       @admin nvarchar(100),
       @medical_scheme nvarchar(100),
       @medical_plan nvarchar(100),
       @claim_type nvarchar(10),
       @option_name nvarchar(50),
       @mk_option_code nvarchar(25),
       @mk_plan_code nvarchar(25),
       @fam_check nvarchar(10),
       @POPostal nvarchar(10),
       @POPostalNo nvarchar(10),
       @POPostOffice nvarchar(10),
       @POCity nvarchar(10),
       @POProvince nvarchar(10),
       @POCountry nvarchar(10),
       @POPostalCode nvarchar(10),
       @FaxNo nvarchar(10),
       @TelNo nvarchar(10),
       @Email nvarchar(10),
       @URL nvarchar(10)
AS
BEGIN
       DECLARE @optionID int, @countAdmin int, @countScheme int, @countPlan int, @newAdminID int, @newSchemeID int, 
       @newPlanID int, @newOptionLinkID int, @existAdminID int, @existNewSchemeID int, @new_PlanID int, @discPlan int, 
       @new_OptionLinkID int, @famFlag int, @mkCodeFlag int, @existPlanID int, @countOptionLinkID int, @discscheme int,
       @set_OptionLinkID int, @is_adminScheme int, @is_adminSchemePlan int, @OptionLinkID int, @LinkCodeCount int,
       @planID_New int, @optionLinkID_New int, @disc int, @adminIDExist int, @discAdm int, @aschemeIDExist int,
       @countoptionID int, @linkIDCount int, @count_adminScheme int ,@is_AdminIDExist int, @newadminIDExist int
       
       IF((@admin <> '' or @admin is not null) and 
       (@medical_scheme <> '' or @medical_scheme is not null) and
       (@medical_plan <> '' or @medical_plan is not null) and 
       (@claim_type <> '' or @claim_type is not null) and
       (@option_name <> '' or @option_name is not null))
       BEGIN
       
       IF(@fam_check <> '' or @fam_check is not null)
              BEGIN
                     SET @famFlag = 1
              END
       ELSE
              BEGIN
                     SET @famFlag = 0
              END
              
       SET @optionID = (select o.MFOptionID from MFOption o where o.MFOptionName like @option_name+'%' and 
       o.MFOptionID not in (17,18,19))
              
       IF(@mk_option_code <> '' or @mk_option_code is not null)
              BEGIN
                     SET @mkCodeFlag = 1
              END
       ELSE
              BEGIN
                     SET @mkCodeFlag = 0
              END
              
       SET @countAdmin = (select count(*) from T_MFAdmin a where a.MFAdminName = @admin)          
       IF(@countAdmin = 0)
              BEGIN
                     INSERT INTO T_MFAdmin (MFAdminName,Custom,Discontinued) VALUES (@admin,0,0)
                     SET @countAdmin = (select count(*) from T_MFAdmin a where a.MFAdminName = @admin)
              END
        
       SET @countScheme = (select count(*) from MedicalScheme s join T_MFAdmin a on a.MFAdmin_ID = s.MFAdminID
						  where s.SchemeName = @medical_scheme and a.MFAdminName = @admin)
       IF(@countScheme = 0)
              BEGIN
			         select @newadminIDExist = a.MFAdmin_ID from T_MFAdmin a where a.MFAdminName = @admin
			         
                     INSERT INTO MedicalScheme (MFAdminID,SchemeName,Discontinued,Custom,POPostal,
                     POPostalNo,POPostOffice,POCity,POProvince,POCountry,POPostalCode,FaxNo,TelNo,Email,URL) 
                     VALUES (@newadminIDExist,@medical_scheme,0,0,@POPostal,@POPostalNo,@POPostOffice,@POCity,
                     @POProvince,@POCountry,@POPostalCode,@FaxNo,@TelNo,@Email,@URL)
                     
                     SET @countScheme = (select count(*) from MedicalScheme s join T_MFAdmin a on 
										a.MFAdmin_ID = s.MFAdminID where s.SchemeName = @medical_scheme
										and a.MFAdminName = @admin)
              END
              
       SET @countPlan = (select count(*) from MedicalPlan p where p.PlanName = @medical_plan)
       
       --SET @is_adminScheme = (select count(*) from MedicalScheme s join T_MFAdmin a on a.MFAdmin_ID = s.MFAdminID  
       --where a.MFAdminName = @admin and s.SchemeName = @medical_scheme)
       
       SET @is_adminSchemePlan = (select count(*) from MedicalScheme s join T_MFAdmin a on a.MFAdmin_ID = s.MFAdminID  
       join MedicalPlan p on p.MedicalSchemeID = s.MedicalSchemeID where a.MFAdminName = @admin and 
       s.SchemeName = @medical_scheme and p.PlanName = @medical_plan)
           
       --Admin,Scheme,Plan doesn't exist in the database
       IF(@countAdmin = 0 and @countScheme = 0 and @countPlan = 0)
              BEGIN
              --print 'hi'
                     INSERT INTO T_MFAdmin (MFAdminName,Custom,Discontinued) VALUES (@admin,0,0)
                     SET @newAdminID = (select a.MFAdmin_ID from T_MFAdmin a where a.MFAdminName = @admin)
                     
                     INSERT INTO MedicalScheme (MFAdminID,SchemeName,Discontinued,Custom,
                     POPostal,POPostalNo,POPostOffice,POCity,POProvince,POCountry,POPostalCode,FaxNo,TelNo,Email,URL) 
                     VALUES (@newAdminID,@medical_scheme,0,0,@POPostal,@POPostalNo,@POPostOffice,@POCity,
                     @POProvince,@POCountry,@POPostalCode,@FaxNo,@TelNo,@Email,@URL)
                     
                     SET @newSchemeID = (select s.MedicalSchemeID from MedicalScheme s where s.SchemeName = @medical_scheme)
                     
                     INSERT INTO MedicalPlan (MedicalSchemeID,CategoryID,ChargeTypeID,PlanName,Discontinued,Custom) 
                     VALUES (@newSchemeID,1,NULL,@medical_plan,0,0)
                     SET @newPlanID = (select p.MedicalPlanID from MedicalPlan p where p.PlanName = @medical_plan)
                     
                     INSERT INTO MFOptionLink (MFOptionID,PlanID,Discontinued,Custom) VALUES (@optionID,@newPlanID,0,0)
                     SET @newOptionLinkID = (select distinct o.MFOptionLinkID from MFOptionLink o where 
                     o.MFOptionID = @optionID and o.PlanID = @newPlanID)
                     
                     IF(@mkCodeFlag <> 0)
                           BEGIN
                                  INSERT INTO MFOptionLinkCode (MFCodeTypeID,MFOptionLinkID,MFOptionLinkCode,Active,
                                  IsFMcheckEnabled)VALUES (5,@newOptionLinkID,@mk_option_code,1,@famFlag)
                           END
              END
       --Admin exist but Scheme isn't present in the database
       ELSE IF(@countAdmin <> 0 and @countScheme = 0)
              BEGIN                
                     select @existAdminID = a.MFAdmin_ID,@disc = a.Discontinued from T_MFAdmin a where a.MFAdminName = @admin                                 
                                         
                     IF(@disc = 1)
                           BEGIN
                                  UPDATE T_MFAdmin set Discontinued = 0 where MFAdmin_ID = @existAdminID
                           END
                           
                     INSERT INTO MedicalScheme (MFAdminID,SchemeName,Discontinued,Custom,
                     POPostal,POPostalNo,POPostOffice,POCity,POProvince,POCountry,POPostalCode,FaxNo,TelNo,Email,URL) 
                     VALUES (@existAdminID,@medical_scheme,0,0,@POPostal,@POPostalNo,@POPostOffice,@POCity,
                     @POProvince,@POCountry,@POPostalCode,@FaxNo,@TelNo,@Email,@URL)
                     
                     --SET @existNewSchemeID = (select s.MedicalSchemeID from MedicalScheme s where s.SchemeName = @medical_scheme)
                     SET @existNewSchemeID = SCOPE_IDENTITY()        
                     
                     INSERT INTO MedicalPlan (MedicalSchemeID,CategoryID,ChargeTypeID,PlanName,Discontinued,Custom) 
                     VALUES (@existNewSchemeID,1,NULL,@medical_plan,0,0)
                     
                     --SET @new_PlanID = (select p.MedicalPlanID from MedicalPlan p where p.PlanName = @medical_plan)
                     SET @new_PlanID = SCOPE_IDENTITY()       
                                  
                     INSERT INTO MFOptionLink (MFOptionID,PlanID,Discontinued,Custom) VALUES (@optionID,@new_PlanID,0,0)
                     
                     --SET @new_OptionLinkID = (select distinct o.MFOptionLinkID from MFOptionLink o where 
                     --o.MFOptionID = @optionID and o.PlanID = @new_PlanID)
                     
                     SET @new_OptionLinkID = SCOPE_IDENTITY()               
                                  
                     IF(@mkCodeFlag <> 0) --error
                     BEGIN
                           INSERT INTO MFOptionLinkCode (MFCodeTypeID,MFOptionLinkID,MFOptionLinkCode,Active,IsFMcheckEnabled) 
                           VALUES (5,@new_OptionLinkID,@mk_option_code,1,@famFlag)
                     END
              END           
       --Admin and Scheme exist in the database
       ELSE IF(@countAdmin <> 0 and @countScheme <> 0)                       
              BEGIN
                     SET @count_adminScheme = (select COUNT(*) from MedicalScheme s join T_MFAdmin a on 
                     a.MFAdmin_ID = s.MFAdminID where a.MFAdminName = @admin and s.SchemeName = @medical_scheme)
                     
                     print 'count_adminScheme'
                     print @count_adminScheme

                     IF(@count_adminScheme = 0)
                     BEGIN
                           select @is_AdminIDExist = a.MFAdmin_ID from T_MFAdmin a 
                           where a.MFAdminName = @admin
                           
                           INSERT INTO MedicalScheme (MFAdminID,SchemeName,Discontinued,Custom,POPostal,POPostalNo,
                           POPostOffice,POCity,POProvince,POCountry,POPostalCode,FaxNo,TelNo,Email,URL) 
                           VALUES (@is_AdminIDExist,@medical_scheme,0,0,@POPostal,@POPostalNo,@POPostOffice,@POCity,
                           @POProvince,@POCountry,@POPostalCode,@FaxNo,@TelNo,@Email,@URL)
                     END
                     
                     select @adminIDExist = a.MFAdmin_ID,@discAdm = a.Discontinued,@aschemeIDExist = s.MedicalSchemeID,
                     @discscheme = s.Discontinued from MedicalScheme s join T_MFAdmin a on a.MFAdmin_ID = s.MFAdminID 
                     where a.MFAdminName = @admin and s.SchemeName = @medical_scheme                   
                     
                     IF(@discAdm = 1 or @discscheme = 1)
                           BEGIN
                                  UPDATE T_MFAdmin set Discontinued = 0 where MFAdmin_ID = @adminIDExist
                                  UPDATE MedicalScheme set Discontinued = 0 where MedicalSchemeID = @aschemeIDExist
                           END
                     
                     print 'end'
                     print @adminIDExist
                     print @aschemeIDExist
                     print 'end'

                     --Admin,Scheme,plan exist in the database
                     IF(@is_adminSchemePlan <> 0)
                           BEGIN 
                                  select @existPlanID = p.MedicalPlanID, @discPlan = p.Discontinued from MedicalScheme s 
                                  join T_MFAdmin a on a.MFAdmin_ID = s.MFAdminID join MedicalPlan p on 
                                  p.MedicalSchemeID = s.MedicalSchemeID where a.MFAdminName = @admin and 
                                  s.SchemeName = @medical_scheme and p.PlanName = @medical_plan
                                  
                                  IF(@discPlan = 1)
                                         BEGIN
                                                UPDATE MedicalPlan set Discontinued = 0 where MedicalPlanID = @existPlanID
                                         END
                                  
                                  SET @countOptionLinkID = (select COUNT(*) from MFOptionLink o where o.MFOptionID = @optionID and 
                                  o.PlanID = @existPlanID)
                                                              
                                  IF(@countOptionLinkID = 0) 
                                         BEGIN
                                                INSERT INTO MFOptionLink (MFOptionID,PlanID,Discontinued,Custom) VALUES 
                                                (@optionID,@existPlanID,0,0)
                                                
                                                --SET @set_OptionLinkID = (select distinct o.MFOptionLinkID from MFOptionLink o where 
                                                --o.MFOptionID = @optionID and o.PlanID = @existPlanID)
                                                SET @set_OptionLinkID = SCOPE_IDENTITY()
                                                
                                                IF(@mkCodeFlag <> 0)
                                                       BEGIN
                                                              INSERT INTO MFOptionLinkCode (MFCodeTypeID,MFOptionLinkID,MFOptionLinkCode,
                                                              Active,IsFMcheckEnabled) VALUES (5,@set_OptionLinkID,@mk_option_code,1,@famFlag)
                                                       END    
                                         END
                                  ELSE IF(@countOptionLinkID <> 0) --CHECK
                                         BEGIN
                                                SET @countoptionID = (select COUNT(*) from MFOptionLink o where 
                                                o.MFOptionID = @optionID and o.PlanID = @existPlanID)
                                                
                                                IF(@countoptionID = 1)
                                                       BEGIN
                                                              SET @OptionLinkID = (select top 1 o.MFOptionLinkID from MFOptionLink o where 
                                                              o.MFOptionID = @optionID and o.PlanID = @existPlanID)
                                                       END
                                                ELSE IF(@countoptionID <> 0 and @countoptionID <> 1)
                                                       BEGIN
                                                              SET @linkIDCount = (select count(*) from MFOptionLink o 
                                                              join MFOptionLinkCode  olc on olc.MFOptionLinkID = o.MFOptionLinkID where 
                                                              o.MFOptionID = @optionID and o.PlanID = @existPlanID and 
                                                              olc.MFCodeTypeID = 5)
                                                              
                                                              IF(@linkIDCount = 1 or @linkIDCount <> 0)
                                                                     BEGIN
                                                                           SET @OptionLinkID = (select top 1 o.MFOptionLinkID from MFOptionLink o 
                                                                           join MFOptionLinkCode  olc on olc.MFOptionLinkID = o.MFOptionLinkID where 
                                                                           o.MFOptionID = @optionID and o.PlanID = @existPlanID and 
                                                                           olc.MFCodeTypeID = 5)
                                                                     END
                                                              ELSE IF(@linkIDCount = 0) 
                                                                     BEGIN
                                                                           SET @OptionLinkID = (select top 1 o.MFOptionLinkID from MFOptionLink o 
                                                                           where o.MFOptionID = @optionID and o.PlanID = @existPlanID)      
                                                                     END
                                                       END
                                                
                                                SET @LinkCodeCount = (select COUNT(*) from MFOptionLinkCode mfo where 
                                                mfo.MFOptionLinkID = @OptionLinkID and mfo.MFCodeTypeID = 5)
                                                
                                                IF(@LinkCodeCount = 0)
                                                       BEGIN
                                                              IF(@mkCodeFlag <> 0)
                                                                     BEGIN
                                                                           INSERT INTO MFOptionLinkCode (MFCodeTypeID,MFOptionLinkID,
                                                                            MFOptionLinkCode,Active,IsFMcheckEnabled) VALUES 
                                                                            (5,@OptionLinkID,@mk_option_code,1,@famFlag)
                                                                     END
                                                       END
                                                ELSE IF(@LinkCodeCount <> 0)
                                                       BEGIN
                                                              UPDATE MFOptionLinkCode set MFOptionLinkCode = @mk_option_code, Active = 1,
                                                              IsFMcheckEnabled = @famFlag where MFOptionLinkID = @OptionLinkID and 
                                                              MFCodeTypeID = 5
                                                       END
                                         END
                           END
                     ELSE IF(@is_adminSchemePlan = 0)
                           BEGIN
                                  --query for existing admin and scheme. but for new plan                                   
                                  INSERT INTO MedicalPlan (MedicalSchemeID,CategoryID,ChargeTypeID,PlanName,Discontinued,Custom) 
                                  VALUES (@aschemeIDExist,1,NULL,@medical_plan,0,0)
                                  
                                  --SET @planID_New = (select p.MedicalPlanID from MedicalScheme s join T_MFAdmin a on 
                                  --a.MFAdmin_ID = s.MFAdminID join MedicalPlan p on p.MedicalSchemeID = s.MedicalSchemeID 
                                  --where a.MFAdminName = @admin and s.SchemeName = @medical_scheme and p.PlanName = @medical_plan)
                                  SET @planID_New = SCOPE_IDENTITY()
                                               
                                  INSERT INTO MFOptionLink (MFOptionID,PlanID,Discontinued,Custom) VALUES 
                                  (@optionID,@planID_New,0,0)
                                  
                                  --SET @optionLinkID_New = (select distinct o.MFOptionLinkID from MFOptionLink o where 
                                  --o.MFOptionID = @optionID and o.PlanID = @planID_New)                                 
                                  SET @optionLinkID_New = SCOPE_IDENTITY()
                                                
                                  IF(@mkCodeFlag <> 0)
                                         BEGIN
                                                INSERT INTO MFOptionLinkCode (MFCodeTypeID,MFOptionLinkID,MFOptionLinkCode,Active,
                                                IsFMcheckEnabled)VALUES (5,@optionLinkID_New,@mk_option_code,1,@famFlag)
                                         END
                           END
              END                  
       END
       
       delete from MedicalPlan where MedicalSchemeID is null and PlanName is null        
     
END



GO
