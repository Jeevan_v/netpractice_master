SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[MW_PaymentReceipt]      
  @intVisitId INT,      
  @AmountPaid DECIMAL(30,2),      
  @Method VARCHAR(50),  
  @ReceiptNo VARCHAR(50),  
  @Description VARCHAR(50),  
  @UserID INT,
  @PaymentDate Date     
AS      
BEGIN  
  INSERT INTO PAYMENT ([VisitID],[AmountPaid],[Method],[ReceiptNo],[Description],[UserID],[Date])   
  VALUES (@intVisitId,@AmountPaid,@Method,@ReceiptNo,@Description,@UserID, ISNULL(@PaymentDate,getdate()));  
END      
GO
