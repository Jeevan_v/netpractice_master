SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_updateClaimtableSurch] 
@status varchar(50), 
@statusregion varchar(150),  
@spbhf varchar(20),  
@AmountPaid numeric(32,2), 
 @amountovercharged numeric(32,2) ,
 @ClaimID int,  
 @schmSurch  numeric(32,2),
 @refSurch  numeric(32,2),
 @saveSurch  numeric(32,2)

AS  

BEGIN 

Declare @Amtpaid numeric(32,2),@Totalamt numeric(32,2)

set @AmtPaid= (select CASE WHEN AmountPaid IS NULL THEN 0 ELSE AmountPaid END from Claim with (nolock) where ClaimID=@claimID)

set @Totalamt=(select TotalAmount from Claim with (nolock) where ClaimID=@claimID)

set @AmtPaid=@AmtPaid+@AmountPaid -- + @schmSurch+@refSurch + @saveSurch
set @amountovercharged = @amountovercharged +@schmSurch+@refSurch + @saveSurch
if(@AmtPaid <= @Totalamt)

Begin  

 update Claim 

  set AmountPaid=@AmtPaid,  

  SP_BHF=@spbhf, Status=@status,StatusReason=@statusregion,

AmountOverCharged=@amountovercharged  

 where ClaimID=@claimID  

 End

END 
GO
