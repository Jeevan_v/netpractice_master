SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****************************************************/
/**Procedure Name- MW_DetailTRansaction**/
/**Creatred By - Amarnath Bhattacharya**/
/**Created ON - 08/02/2013**/
/**Function- To get claim details for a procatice ID**/
/*****************************************************/
CREATE procedure [dbo].[MW_DetailTRansaction](@intPractice int)
As
Begin
SELECT 
	CASE
	WHEN UPPER(VL.[LineDefinition])=UPPER('Medicine') AND VL.IsChronic=1 THEN CONVERT(VARCHAR,CD.[ClaimID])+ 'MC'
	WHEN UPPER(VL.[LineDefinition])=UPPER('Medicine') AND VL.IsChronic=0 THEN CONVERT(VARCHAR,CD.[ClaimID])+ 'MA'
	ELSE CONVERT(VARCHAR,CD.[ClaimID])+ 'P'
	END AS [ClaimID],		
	Vs.[AuthorisationCode],
	VL.[Description],
	CD.[Status],
	VL.[Amount] AS AmountDue,
	CD.[AmountPaid],
	CE.[ErrorDesc] AS Warnings,
	Clm.[StatusReason] AS Details
FROM
	ClaimDetail CD
	JOIN VisitLine VL ON VL.[VisitLineID] = CD.[VisitLineID]
	LEFT JOIN ClaimError CE ON CE.ClaimDetailID = CD.[ClaimDetailID]
	JOIN Visit Vs ON Vs.[VisitID]=VL.[VisitID]
	JOIN Venue Ven ON Ven.[VenueID] = Vs.[VenueID]
	JOIN Practice Pr ON Pr.[PracticeID] = Ven.[PracticeID]
	JOIN Claim Clm ON Clm.[ClaimID]=CD.[ClaimID]
WHERE
	Pr.PracticeID = @intPractice
End

GO
