SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_SaveVenue]
	@VenueID int,
	@PracticeID int,
	@VenueName varchar(50),
	@ReasonID int,
	@PhysicalAddressID int,
	@PostalAddressID int,
	@Telephone varchar(30),
	@Fax varchar(30),
	@IsActive bit
AS

IF @VenueID IS NULL
BEGIN

INSERT INTO Venue 
(
	PracticeID,
	VenueName,
	ReasonID,
	PhysicalAddressID,
	PostalAddressID,
	Telephone,
	Fax,
	IsActive
)
VALUES
(
	@PracticeID,
	@VenueName,
	@ReasonID,
	@PhysicalAddressID,
	@PostalAddressID,
	@Telephone,
	@Fax,
	@IsActive
)

SELECT SCOPE_IDENTITY()

END

ELSE
BEGIN

UPDATE Venue
SET
	
	VenueName = @VenueName,
	Telephone = @Telephone,
	ReasonID=@ReasonID,
	Fax = @Fax,
	IsActive = @IsActive
WHERE
	VenueID = @VenueID
	
SELECT 1

END

GO
