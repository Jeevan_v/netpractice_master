SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MW_GetClaimStatusByVisitID] 

@VisitID int
		
AS

BEGIN
SELECT 
	CASE
	WHEN UPPER(VL.[LineDefinition])=UPPER('Medicine') AND VL.IsChronic=1 THEN CONVERT(VARCHAR(20),CD.[ClaimID])+ 'MC'
	WHEN UPPER(VL.[LineDefinition])=UPPER('Medicine') AND VL.IsChronic=0 THEN CONVERT(VARCHAR(20),CD.[ClaimID])+ 'MA'
	ELSE CONVERT(VARCHAR(20),CD.[ClaimID])+ 'P'
	END AS ClaimID,		
	Vs.[AuthorisationCode],
	VL.[Description],
	UPPER(LEFT(CD.[Status],1))+LOWER(SUBSTRING(CD.[Status],2,LEN(CD.[Status]))) as [Status],
	CONVERT(Decimal(30,2),ROUND(VL.[Amount],2)) AS AmountDue,
	CONVERT(Decimal(30,2),ROUND(CD.[AmountPaid],2))AS AmountPaid ,
	CE.[ErrorDesc] AS Warnings,
	Clm.[StatusReason] AS Details
FROM
	ClaimDetail CD
	JOIN VisitLine VL ON VL.[VisitLineID] = CD.[VisitLineID]
	LEFT JOIN ClaimError CE ON CE.ClaimDetailID = CD.[ClaimDetailID]
	JOIN Visit Vs ON Vs.[VisitID]=VL.[VisitID]
	JOIN Venue Ven ON Ven.[VenueID] = Vs.[VenueID]
	JOIN Practice Pr ON Pr.[PracticeID] = Ven.[PracticeID]
	JOIN Claim Clm ON Clm.[ClaimID]=CD.[ClaimID]
WHERE
	Clm.ClaimID IN
	
	(
		SELECT ClaimID FROM Claim WHERE VisitID = @VisitID
	)
	
END

GO
