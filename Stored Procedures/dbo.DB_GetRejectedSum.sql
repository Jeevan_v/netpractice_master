SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[DB_GetRejectedSum] 
@PracticeID INT,
@ClaimStatus VARCHAR(100)
AS
BEGIN
SELECT 
SUM(cl.TotalAmount) AS TotalAmount FROM 
Visit v 

INNER JOIN Claim cl ON v.VisitID=cl.VisitID
INNER JOIN Account ac ON v.AccountID=ac.AccountID
INNER JOIN Patient pat ON ac.PatientID=pat.PatientID
INNER JOIN MedicalScheme ms ON pat.SchemeID=ms.MedicalSchemeID
LEFT JOIN Payment P ON V.VisitID=P.VisitID
WHERE 
Status='Rejected'
AND 
PracticeID=@PracticeID	

END
GO
