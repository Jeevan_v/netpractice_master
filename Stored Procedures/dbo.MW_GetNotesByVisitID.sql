SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetNotesByVisitID](
@intVisitId INT
)
AS 
BEGIN
SELECT ClinicalNotes,MedicalReport,ReferralLetter,SickNotes FROM AddNotes
WHERE
VisitID=@intVisitId
END
GO
