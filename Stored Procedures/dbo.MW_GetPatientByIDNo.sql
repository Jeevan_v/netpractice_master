SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sameer>
-- Create date: <18/12/2012>
-- Description:	<Get Patient By PatientID>
-- =============================================
CREATE PROCEDURE [dbo].[MW_GetPatientByIDNo]
       @IDNo varchar(13)
AS
BEGIN
	SELECT 
	   [PatientID]
    FROM
      Patient
    WHERE 
      IDNo=@IDNo  
END


GO
