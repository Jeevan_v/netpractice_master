SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_MEDICALAIDS] @DATE DATETIME,@PRACTICEID INT, @DOCTORID INT, @RPTFREQUENCY INT                     
AS        
declare @CURRMONTH_STDATE DATETIME    
declare @CURRMONTH_ENDDATE DATETIME    
declare @STDATE_FREQUENCY DATETIME    
declare @ENDDATE_FREQUENCY DATETIME      
declare @ENDDATE_TEMP_FREQUENCY DATETIME      
--declare @PRACTICEID int     
--declare @DOCTORID int
--declare @DATE DATETIME
--declare @RPTFREQUENCY int

--SET @PRACTICEID=2383--44
--set @DOCTORID=2516--85
--SET @DATE='2015-09-03 00:00:00'

--set @RPTFREQUENCY=3  
             
----CALCULATE CURRERNT MONTH DATES 

--Change By Sachin Gandhi
--Change Date 27th August 2015
--Description : Replace CLM.AmountPaid with PP.AmouuntPaid
SET @CURRMONTH_STDATE = CONVERT(DATETIME,CONVERT(VARCHAR(20),CONVERT(VARCHAR(20),MONTH(@DATE))+'/01/'+CONVERT(VARCHAR(20),YEAR(@DATE))))    
SET @CURRMONTH_ENDDATE = @DATE    
----------------    
    
----------CALCULATE STDATE AND END DATE BASED ON FREQUENCY    
   
IF @RPTFREQUENCY = 1    
BEGIN    
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-3,@DATE)    
END    
ELSE IF @RPTFREQUENCY = 2    
BEGIN    
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-6,@DATE)    
END    
ELSE    
BEGIN    
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-12,@DATE)    
END    
    
    
SET @STDATE_FREQUENCY = CONVERT(DATETIME,CONVERT(VARCHAR(20),CONVERT(VARCHAR(20),MONTH(@ENDDATE_TEMP_FREQUENCY)) + '/01/' + CONVERT(VARCHAR(20),YEAR(DATEADD(MM,1,@ENDDATE_TEMP_FREQUENCY)))))    
    
SET @ENDDATE_FREQUENCY = DATEADD(DD,-1,@CURRMONTH_STDATE)    
    
    print @STDATE_FREQUENCY
	print @ENDDATE_FREQUENCY
-------------------------------  
DECLARE @PAYMENTREC_COUNT INT  
      select @PAYMENTREC_COUNT =count(*) from PAYMENT pp with(nolock)
	  INNER JOIN VISIT vv with(nolock) on vv.VisitID = pp.VisitID
	  INNER JOIN CLAIM CLM ON vv.VISITID = CLM.VISITID                           
			 INNER JOIN VENUE VEN ON VEN.VENUEID = vv.VENUEID                  
			 INNER JOIN PRACTICE PR ON PR.PRACTICEID = VEN.PRACTICEID                
			 INNER JOIN [USER] USR ON USR.USERID = vv.DOCTORID  
			  --Changed for Checking Doctor roles only (gowsi)
			 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID       
			 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID 
			 -- Changed  For Incorrect MApping of User and Practice
			 INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID  
                
			 INNER JOIN MEDICALSCHEME MS ON MS.MEDICALSCHEMEID = vv.SCHEMEID 
 
	  WHERE vv.VisitDate >= @STDATE_FREQUENCY and vv.VisitDate <=  @ENDDATE_FREQUENCY 
	  AND  
		CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 end =                 
      CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 end            
                   
	 AND  CASE WHEN @DOCTORID <> 0 THEN vv.DOCTORID ELSE 1 end =                 
		  CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 end        
		  --Changes done for incorrect mapping         
	  AND  UVR.VenueID= vv.VenueID          
		--Changed for Checking Doctor roles only (gowsi)
	 AND
	  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor') 
	  --changes starts in this report these two scheme should not displayed done by gowsalya   
	AND Ms.SchemeName not in('PRIVATE PATIENT','CASH PATIENT')

PRINT 'PAYMENT RECEIVED RECORDS: '
PRINT @PAYMENTREC_COUNT
BEGIN
IF (@PAYMENTREC_COUNT > 0 ) 
BEGIN
 SELECT DISTINCT              
 MS.MedicalSchemeID  AS SchemeID,      
 MS.SchemeName AS SchemeName,      
 PR.PRACTICEID ,       
 VS.DOCTORID ,                      
 PR.PRACTICENAME PRACTICE,              
 (USR.FIRSTNAME+ ' '+USR.LASTNAME) AS DOCTOR,      
      
 SUM(CASE WHEN VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE        
 THEN       
 ISNULL(PP.AmountPaid,0)
ELSE 0
 END)  CURRENTAMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-1,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-1,@DATE)) AND @RPTFREQUENCY >=1 THEN       
  ISNULL(PP.AmountPaid,0)
 ELSE 0
  END)  MON1_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-2,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-2,@DATE)) AND @RPTFREQUENCY >=1   THEN       
  ISNULL(PP.AmountPaid,0)
 ELSE 0
  END)  MON2_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-3,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-3,@DATE)) AND @RPTFREQUENCY >=1   THEN       
  ISNULL(PP.AmountPaid,0)
	  ELSE 0
 
  END)  MON3_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-4,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-4,@DATE))  AND @RPTFREQUENCY >=2  THEN       
ISNULL(PP.AmountPaid,0)
 ELSE 0
 
 END)  MON4_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-5,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-5,@DATE)) AND @RPTFREQUENCY >=2  THEN       
 ISNULL(PP.AmountPaid,0)
 ELSE 0
  END)  MON5_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-6,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-6,@DATE)) AND @RPTFREQUENCY >=2  
 THEN  
ISNULL(PP.AmountPaid,0)
 ELSE 0
 END)  MON6_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-7,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-7,@DATE)) AND @RPTFREQUENCY =3  THEN       
 ISNULL(PP.AmountPaid,0)
 ELSE 0
  END)  MON7_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-8,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-8,@DATE)) AND @RPTFREQUENCY =3  THEN       
 ISNULL(PP.AmountPaid,0)
 ELSE 0
  END)  MON8_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-9,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-9,@DATE)) AND @RPTFREQUENCY =3  THEN       
ISNULL(PP.AmountPaid,0)
 ELSE 0
 END)  MON9_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-10,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-10,@DATE)) AND @RPTFREQUENCY =3  THEN       
ISNULL(PP.AmountPaid,0)
 ELSE 0
  END) MON10_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-11,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-11,@DATE)) AND @RPTFREQUENCY =3  THEN       
ISNULL(PP.AmountPaid,0)
 ELSE 0
 END)  MON11_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-12,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-12,@DATE)) AND @RPTFREQUENCY =3  THEN       
  ISNULL(PP.AmountPaid,0)
 ELSE 0
 END)  MON12_AMT,      
       
 SUM(CASE WHEN VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE        
 THEN       
  ISNULL(PP.AmountPaid,0)
 ELSE 0
  END)    
 +    
 SUM(CASE WHEN VS.VisitDate >= @STDATE_FREQUENCY and VS.VisitDate <=  @ENDDATE_FREQUENCY        
 THEN       
 ISNULL(PP.AmountPaid,0)
 ELSE 0
 END) TOTAL_AMT      
       
 FROM      
 VISIT VS          
 INNER JOIN CLAIM CLM ON VS.VISITID = CLM.VISITID                           
 INNER JOIN VENUE VEN ON VEN.VENUEID = VS.VENUEID                  
 INNER JOIN PRACTICE PR ON PR.PRACTICEID = VEN.PRACTICEID                
 INNER JOIN [USER] USR ON USR.USERID = VS.DOCTORID  
  --Changed for Checking Doctor roles only (gowsi)
 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID       
 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID 
 -- Changed  For Incorrect MApping of User and Practice
 INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID  
                
 INNER JOIN MEDICALSCHEME MS ON MS.MEDICALSCHEMEID = VS.SCHEMEID 
  -- Change By Sachin Gandhi - 27th August 2015
  INNER JOIN Payment PP on PP.VisitID = VS.VisitID                 

 WHERE                  
 --VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE     
     
 --AND  
 CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 end =                 
      CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 end            
                   
 AND  CASE WHEN @DOCTORID <> 0 THEN VS.DOCTORID ELSE 1 end =                 
      CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 end        
      --Changes done for incorrect mapping         
  AND  UVR.VenueID= VS.VenueID          
    --Changed for Checking Doctor roles only (gowsi)
 AND
  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor') 
  --changes starts in this report these two scheme should not displayed done by gowsalya   
AND Ms.SchemeName not in('PRIVATE PATIENT','CASH PATIENT')

--change ends
 GROUP BY                 
 VS.DOCTORID ,PR.PRACTICENAME,                 
 USR.FIRSTNAME,USR.LASTNAME,MS.SchemeName,MS.MedicalSchemeID,      
 PR.PRACTICEID ,VS.DOCTORID,USR.FIRSTNAME,USR.LASTNAME        
 order by 3,4,1       
       
END 
ELSE 
BEGIN

 SELECT DISTINCT              
 MS.MedicalSchemeID  AS SchemeID,      
 MS.SchemeName AS SchemeName,      
 PR.PRACTICEID ,       
 VS.DOCTORID ,                      
 PR.PRACTICENAME PRACTICE,              
 (USR.FIRSTNAME+ ' '+USR.LASTNAME) AS DOCTOR,      
      
 SUM(CASE WHEN VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE        
 THEN       
 ISNULL(CLM.AmountPaid,0)
ELSE 0
 END)  CURRENTAMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-1,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-1,@DATE)) AND @RPTFREQUENCY >=1 THEN       
  ISNULL(CLM.AmountPaid,0)
 ELSE 0
  END)  MON1_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-2,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-2,@DATE)) AND @RPTFREQUENCY >=1   THEN       
  ISNULL(CLM.AmountPaid,0)
 ELSE 0
  END)  MON2_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-3,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-3,@DATE)) AND @RPTFREQUENCY >=1   THEN       
  ISNULL(CLM.AmountPaid,0)
	  ELSE 0
 
  END)  MON3_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-4,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-4,@DATE))  AND @RPTFREQUENCY >=2  THEN       
ISNULL(CLM.AmountPaid,0)
 ELSE 0
 
 END)  MON4_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-5,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-5,@DATE)) AND @RPTFREQUENCY >=2  THEN       
 ISNULL(CLM.AmountPaid,0)
 ELSE 0
  END)  MON5_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-6,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-6,@DATE)) AND @RPTFREQUENCY >=2  
 THEN  
ISNULL(CLM.AmountPaid,0)
 ELSE 0
 END)  MON6_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-7,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-7,@DATE)) AND @RPTFREQUENCY =3  THEN       
 ISNULL(CLM.AmountPaid,0)
 ELSE 0
  END)  MON7_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-8,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-8,@DATE)) AND @RPTFREQUENCY =3  THEN       
 ISNULL(CLM.AmountPaid,0)
 ELSE 0
  END)  MON8_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-9,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-9,@DATE)) AND @RPTFREQUENCY =3  THEN       
ISNULL(CLM.AmountPaid,0)
 ELSE 0
 END)  MON9_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-10,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-10,@DATE)) AND @RPTFREQUENCY =3  THEN       
ISNULL(CLM.AmountPaid,0)
 ELSE 0
  END) MON10_AMT,      
      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-11,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-11,@DATE)) AND @RPTFREQUENCY =3  THEN       
ISNULL(CLM.AmountPaid,0)
 ELSE 0
 END)  MON11_AMT,      
      
 SUM(CASE WHEN MONTH(VS.VisitDate) = MONTH(DATEADD(MM,-12,@DATE))        
 AND       
 YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-12,@DATE)) AND @RPTFREQUENCY =3  THEN       
  ISNULL(CLM.AmountPaid,0)
 ELSE 0
 END)  MON12_AMT,      
       
 SUM(CASE WHEN VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE        
 THEN       
  ISNULL(CLM.AmountPaid,0)
 ELSE 0
  END)    
 +    
 SUM(CASE WHEN VS.VisitDate >= @STDATE_FREQUENCY and VS.VisitDate <=  @ENDDATE_FREQUENCY        
 THEN       
 ISNULL(CLM.AmountPaid,0)
 ELSE 0
 END) TOTAL_AMT      
       
 FROM      
 VISIT VS          
 INNER JOIN CLAIM CLM ON VS.VISITID = CLM.VISITID                           
 INNER JOIN VENUE VEN ON VEN.VENUEID = VS.VENUEID                  
 INNER JOIN PRACTICE PR ON PR.PRACTICEID = VEN.PRACTICEID                
 INNER JOIN [USER] USR ON USR.USERID = VS.DOCTORID  
  --Changed for Checking Doctor roles only (gowsi)
 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID       
 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID 
 -- Changed  For Incorrect MApping of User and Practice
 INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID  
                
 INNER JOIN MEDICALSCHEME MS ON MS.MEDICALSCHEMEID = VS.SCHEMEID 
  -- Change By Sachin Gandhi - 27th August 2015

 WHERE                  
 --VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate <=  @CURRMONTH_ENDDATE     
     
 --AND  
 CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 end =                 
      CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 end            
                   
 AND  CASE WHEN @DOCTORID <> 0 THEN VS.DOCTORID ELSE 1 end =                 
      CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 end        
      --Changes done for incorrect mapping         
  AND  UVR.VenueID= VS.VenueID          
    --Changed for Checking Doctor roles only (gowsi)
 AND
  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor') 
  --changes starts in this report these two scheme should not displayed done by gowsalya   
AND Ms.SchemeName not in('PRIVATE PATIENT','CASH PATIENT')

--change ends
 GROUP BY                 
 VS.DOCTORID ,PR.PRACTICENAME,                 
 USR.FIRSTNAME,USR.LASTNAME,MS.SchemeName,MS.MedicalSchemeID,      
 PR.PRACTICEID ,VS.DOCTORID,USR.FIRSTNAME,USR.LASTNAME        
 order by 3,4,1 
END

END
GO
