SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MW_UpdateClaimErrorDetail] 
	@ClaimDetailID int
	
AS
BEGIN
	DELETE CLAIMERROR  
FROM CLAIMDETAIL CD INNER JOIN CLAIMERROR CE 
ON CE.ClaimDetailID= CD.ClaimDetailID 
WHERE  CD.ClaimDetailID =@ClaimDetailID
	
   UPDATE CLAIMDETAIL
    SET  status = NULL,
    AmountPaid= NULL,
   AmountOverCharged= NULL , 
   auth_hnet= NULL  
    WHERE  ClaimDetailID =@ClaimDetailID
END


GO
