SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MP_SaveVenueStatusByVenueID]
	@VenueID int,
	@IsActive bit,
	@ReasonID int
AS
BEGIN
UPDATE Venue
SET
	IsActive = @IsActive,
	ReasonID=@ReasonID
WHERE
	VenueID = @VenueID
	SELECT @VenueID
END


GO
