SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetPracticeName] 
@RoleName VARCHAR(50),
@UserID INT
AS
BEGIN 
IF(@RoleName = 'Patient')
BEGIN
SELECT
	DISTINCT	
	p.PracticeName,
	p.PracticeID
FROM
Practice p
INNER JOIN
Account A
ON 
A.PracticeID=P.PracticeID
Inner Join 
Patient Pat
ON pat.PatientID=A.PatientID
WHERE
pat.PatientID=@UserID
ORDER BY 
p.PracticeName
END
ELSE IF(@RoleName='Super Admin')
BEGIN
SELECT
	DISTINCT	
	p.PracticeName,
	p.PracticeID
FROM
Practice p
ORDER BY 
p.PracticeName
END
ELSE
BEGIN
SELECT
	DISTINCT	
	p.PracticeName,
	p.PracticeID
FROM
Practice p
INNER JOIN
Venue v
ON 
v.PracticeID=P.PracticeID
Inner Join 
UserVenueRelationship uv
ON uv.VenueID=v.VenueID
INNER JOIN 
[User] u
ON u.UserID=uv.UserID
WHERE
u.UserID=@UserID
ORDER BY 
p.PracticeName
END
END

GO
