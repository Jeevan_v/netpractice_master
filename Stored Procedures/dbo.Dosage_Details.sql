SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Dosage_Details]
AS
SET NOCOUNT ON;
BEGIN
	SELECT [Dosage_Id],[Description] FROM [DOSAGE] WHERE [Language_ID] = 1 AND [DescriptionLevel]=1 ORDER BY [Dosage_Id];
	SELECT [Dosage_Id],[Description] FROM [DOSAGE] WHERE [Language_ID] = 1 AND [DescriptionLevel]=2 ORDER BY [Dosage_Id];
	SELECT [Dosage_Id],[Description] FROM [DOSAGE] WHERE [Language_ID] = 1 AND [DescriptionLevel]=3 ORDER BY [Dosage_Id];
	SELECT [Dosage_Id],[Description] FROM [DOSAGE] WHERE [Language_ID] = 1 AND [DescriptionLevel]=4 ORDER BY [Dosage_Id];
	SELECT [Dosage_Id],[Description] FROM [DOSAGE] WHERE [Language_ID] = 1 AND [DescriptionLevel]=5 ORDER BY [Dosage_Id];
	SELECT [Dosage_Id],[Description] FROM [DOSAGE] WHERE [Language_ID] = 1 AND [DescriptionLevel]=6 ORDER BY [Dosage_Id];
	SELECT [Dosage_Id],[Description] FROM [DOSAGE] WHERE [Language_ID] = 1 AND [DescriptionLevel]=7 ORDER BY [Dosage_Id];
END

GO
