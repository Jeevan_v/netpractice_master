SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateNappiCode] 
(  
	@FileNo nvarchar(20),
	@Nappi nvarchar(20),
	@NappiSuffix nvarchar(10),
	@PackSize nvarchar(500),
	@WholeSalePrice nvarchar(800),
	@DescFull nvarchar(150),
	@ProductStg nvarchar(50),
	@DosageForm nvarchar(50),
	@Schedule nvarchar(5),
	@isMedMat nvarchar(10),
	@Desc nvarchar(150) 	
	--@ManufactureCode nvarchar(10),
)    
AS    
BEGIN   
	DECLARE @chargeType_ID int, @id int, @medMat int, @nappiCode varchar(50), @isExist int, @itemID int,
	@stockCodeID int, @isNappiLike int,  @StockIDCode int, @StockIDItem int, @isMedMatCHK varchar(20),
	@isPack int, @idSCode int, @idSItem int, @isNCLike int, @isChk int, @first varchar(250), @last varchar(250),
	@firstPack varchar(250), @lastPack varchar(250),@RetailPrice nvarchar(800), @ProductDesc nvarchar(150),
	@Description nvarchar(150), @TempDesc nvarchar(150), @TempSTG nvarchar(50), @TempDSG nvarchar(50),
	@stkCodeID int, @StkitemID int

	IF(@FileNo = '201')
		BEGIN
			SET @ProductDesc = @NappiSuffix +  @PackSize + @WholeSalePrice + @DescFull
			INSERT INTO TempTableNappi(Nappi,IsMedMat,Description,ProductStrength,DosageFormCode) 
			VALUES (@Nappi,@isMedMat,@ProductDesc,@ProductStg,@DosageForm)		
		END	
	ELSE IF(@FileNo = '209')
		BEGIN
			IF(@PackSize is null or @PackSize = '' or @PackSize = '  ')
				BEGIN
					SET @PackSize = '0'
				END
			ELSE IF(@PackSize is not null or len(@PackSize) <> 0)
				BEGIN
					SET @firstPack = Substring(@PackSize,1,(len(@PackSize)-2))
					SET @lastPack = Substring(@PackSize,(len(@PackSize)-1),2)
					SET @PackSize = @firstPack+'.'+@lastPack
				END

			SET @nappiCode = @Nappi+@NappiSuffix				
			SELECT @isMedMatCHK = IsMedMat FROM TempTableNappi WHERE Nappi = @Nappi
			SELECT @TempDesc = Description, @TempSTG = ProductStrength, @TempDSG = DosageFormCode
			FROM TempTableNappi WHERE Nappi = @Nappi
			SET @Description = UPPER(LTRIM(RTRIM(@TempDesc)) + ' ' + @TempDSG + LTRIM(RTRIM(@TempSTG)))
			
			SET @first = Substring(@WholeSalePrice,1,(len(@WholeSalePrice)-3))
			SET @last = Substring(@WholeSalePrice,(len(@WholeSalePrice)-2),3)
			SET @WholeSalePrice = @first+'.'+@last
			SET @RetailPrice = CONVERT(varchar(250),(CONVERT(decimal(9,3),
							   (CONVERT(decimal(9,3),@WholeSalePrice)/CONVERT(decimal(9,2),@PackSize)))))
			
			INSERT INTO TempTableNappiCode (NappiCode,Description,PackSize,PackPrice,Schedule) 
			VALUES (@nappiCode,@Desc,@PackSize,@WholeSalePrice,@Schedule)
			
			IF(@isMedMatCHK = 'E')
				BEGIN
					set @medMat = 1
				END
			ELSE IF(@isMedMatCHK = 'S')
				BEGIN
					set @medMat = 2
				END	
			ELSE IF(@isMedMatCHK = 'R')
				BEGIN
					set @medMat = 7
				END	
	
			select @isExist = COUNT(*) from StockCode c join StockItem i on c.StockItemID = i.StockItemID 
			join CodeGroup g on g.CodeGroupID = i.CodeGroupID where c.StockCodeTypeID =1 and 
			c.Code = @nappiCode --and c.Discontinued = 0
	
			IF(@isExist <> 0)
				BEGIN
					IF(@isExist = 1) 
						BEGIN
							select @stockCodeID = c.StockCodeID,@itemID = c.StockItemID from StockCode c join
							StockItem i on c.StockItemID = i.StockItemID join 
							CodeGroup g on g.CodeGroupID = i.CodeGroupID where c.StockCodeTypeID =1 and 
							c.Code = @nappiCode --and c.Discontinued = 0
							
							UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize) 
							,PackPrice = convert(decimal(9,3),@WholeSalePrice),
							UnitPrice = convert(decimal(9,3),@RetailPrice), Discontinued = 0
							WHERE StockCodeID = @stockCodeID
								    
							UPDATE StockItem SET Description = @Description,CodeGroupID = @medMat,
							Schedule = @Schedule, PackSize = convert(decimal(9,2),@PackSize) 
							WHERE StockItemID = @itemID
						END
					ELSE IF(@isExist <> 1 and @isExist <> 0) 
						BEGIN
							/*update StockCode set Discontinued = 1 where Code in (select code from StockCode
							where StockCodeTypeID =1 and Discontinued = 0 group by Code having (COUNT(Code) > 1)) 
							and StockCodeTypeID =1 and StockCodeID not in(select max(StockCodeID) from StockCode 
							where StockCodeTypeID =1 and Discontinued = 0 group by Code having (COUNT(Code) > 1))*/
							
							update StockCode set Discontinued = 1 where Code = @nappiCode and Discontinued = 0
							and StockCodeTypeID =1 and StockCodeID not in(select max(StockCodeID) from StockCode 
							where StockCodeTypeID =1 and Discontinued = 0 and Code = @nappiCode)
							
							select @stockCodeID = c.StockCodeID,@itemID = c.StockItemID from StockCode c join
							StockItem i on c.StockItemID = i.StockItemID join 
							CodeGroup g on g.CodeGroupID = i.CodeGroupID where c.StockCodeTypeID =1 and 
							c.Code = @nappiCode and c.Discontinued = 0
							
							UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize)
							,PackPrice = convert(decimal(9,3),@WholeSalePrice),
							UnitPrice = convert(decimal(9,3),@RetailPrice), Discontinued = 0  
							WHERE StockCodeID = @stockCodeID						  
														    
							UPDATE StockItem SET Description = @Description,CodeGroupID = @medMat,
							Schedule = @Schedule, PackSize = convert(decimal(9,2),@PackSize) 
							WHERE StockItemID = @itemID
						END
				END
			ELSE IF(@isExist = 0)
				BEGIN
					select @isNappiLike = COUNT(*) from StockCode c where --c.Discontinued = 0 and 
					c.StockCodeTypeID = 1 and c.Code like @Nappi+'%' 
					
					IF(@isNappiLike = 0) 
						BEGIN
							select @isChk = COUNT(*) from StockCode where Code = @nappiCode
							IF(@isChk <> 0)
								BEGIN
									update StockCode set Discontinued = 1 where Code = @nappiCode
								END
								
							INSERT into StockItem (CodeGroupID,Description,Status,PrefStockLevel,
							MinStockLevel,PackSize,Schedule) values (@medMat,@Description,'List Item',0,0,
							convert(decimal(9,2),@PackSize),@Schedule)
							
							set @id = SCOPE_IDENTITY()
							
							INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,PackPrice,
							UnitPrice,Discontinued)
							Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),
							convert(decimal(9,3),@WholeSalePrice),convert(decimal(9,3),@RetailPrice),0)
							
							/*INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,Discontinued)
							Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),0)*/
						END
					ELSE IF(@isNappiLike = 1) 
						BEGIN
							select @isNCLike = COUNT(*) from StockCode c where --c.Discontinued = 0 and
							c.StockCodeTypeID = 1 and c.Code like @Nappi+'%' and 
							c.PackSize = convert(decimal(9,2),@PackSize)
							
							IF(@isNCLike = 1)
								BEGIN
									select @StockIDCode = c.StockCodeID, @StockIDItem = c.StockItemID from 
									StockCode c where --c.Discontinued = 0 and 
									c.StockCodeTypeID = 1 and c.Code like @Nappi+'%'
									
									UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize),
									Code = @nappiCode, Discontinued = 0
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice) 
									WHERE StockCodeID = @StockIDCode						  
																    
									UPDATE StockItem SET Description = @Description,Schedule = @Schedule, 
									PackSize = convert(decimal(9,2),@PackSize) 
									WHERE StockItemID = @StockIDItem 
								END
							ELSE IF(@isNCLike = 0)
								BEGIN
									select @isChk = COUNT(*) from StockCode where Code = @nappiCode
									IF(@isChk <> 0)
										BEGIN
											update StockCode set Discontinued = 1 where Code = @nappiCode
										END
									INSERT into StockItem (CodeGroupID,Description,Status,PrefStockLevel,
									MinStockLevel,PackSize,Schedule) values (@medMat,@Description,'List Item',0,0,
									convert(decimal(9,2),@PackSize),@Schedule)
									
									set @id = SCOPE_IDENTITY()
									
									INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,PackPrice,
									UnitPrice,Discontinued)
									Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),
									convert(decimal(9,3),@WholeSalePrice),convert(decimal(9,3),@RetailPrice),0)
									
									/*INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,Discontinued)
									Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),0)	*/
								END					
							ELSE IF(@isNCLike <> 1 and @isNCLike <> 0)
								BEGIN
									UPDATE StockCode SET Discontinued = 1 WHERE Code like @Nappi+'%' and 
									Discontinued = 0 and StockCodeTypeID = 1 and 
									PackSize = convert(decimal(9,2),@PackSize) and
									StockCodeID not in(select max(StockCodeID) FROM StockCode 
									WHERE StockCodeTypeID =1 and Discontinued = 0 and Code like @Nappi+'%'
									and PackSize = convert(decimal(9,2),@PackSize))
							
									SELECT @stkCodeID = c.StockCodeID,@StkitemID = c.StockItemID 
									FROM StockCode c WHERE c.Discontinued = 0 and
									c.StockCodeTypeID = 1 and c.Code like @Nappi+'%' and 
									c.PackSize = convert(decimal(9,2),@PackSize)
							
									UPDATE stockcode SET PackSize = convert(decimal(9,2),@PackSize),
									Code = @nappiCode, Discontinued = 0
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice) 
									WHERE StockCodeID = @stkCodeID						  
																	    
									UPDATE StockItem SET Description = @Description,Schedule = @Schedule, 
									PackSize = convert(decimal(9,2),@PackSize) 
									WHERE StockItemID = @StkitemID
								END
						END
					ELSE IF(@isNappiLike <> 0 and @isNappiLike <> 1)
						BEGIN						
							select @isPack = COUNT(*) from StockCode c where c.Code like @Nappi+'%' and
							c.PackSize = convert(decimal(9,2),@PackSize) and c.StockCodeTypeID = 1  
							--and c.Discontinued = 0 
							
							IF(@isPack = 1)
								BEGIN
									select @idSCode = StockCodeID, @idSItem = StockItemID from StockCode c 
									where c.Code like @Nappi+'%' and --c.Discontinued = 0 and
									c.PackSize = convert(decimal(9,2),@PackSize) and c.StockCodeTypeID = 1  								 
								
									update StockCode set Code = @nappiCode
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice), Discontinued = 0
									where  StockCodeID = @idSCode
									
									update StockItem set Description = @Description,CodeGroupID = @medMat,
									Schedule = @Schedule WHERE StockItemID = @idSItem  
								END
							ELSE IF(@isPack = 0)
								BEGIN
									select @isChk = COUNT(*) from StockCode where Code = @nappiCode
									IF(@isChk <> 0)
										BEGIN
											update StockCode set Discontinued = 1 where Code = @nappiCode
										END
									INSERT into StockItem (CodeGroupID,Description,Status,PrefStockLevel,
									MinStockLevel,PackSize,Schedule) values (@medMat,@Description,'List Item',0,0,
									convert(decimal(9,2),@PackSize),@Schedule)
									
									set @id = SCOPE_IDENTITY()
									
									INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,PackPrice,
									UnitPrice,Discontinued)
									Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),
									convert(decimal(9,3),@WholeSalePrice),convert(decimal(9,3),@RetailPrice),0)
									
									/*INSERT into StockCode (StockItemID,StockCodeTypeID,Code,PackSize,Discontinued)
									Values (@id,1,@nappiCode,convert(decimal(9,2),@PackSize),0)*/
								END
							ELSE IF(@isPack <> 0 and @isPack <> 1)
								BEGIN
									update StockCode set Discontinued = 1 where Code like @Nappi+'%' 
									and Discontinued = 0 
									and StockCodeTypeID =1 and PackSize = convert(decimal(9,2),@PackSize) and
									StockCodeID not in(select max(StockCodeID) from StockCode 
									where StockCodeTypeID =1 and Discontinued = 0 and Code like @Nappi+'%'
									and PackSize = convert(decimal(9,2),@PackSize))
									
									select @idSCode = StockCodeID, @idSItem = StockItemID from StockCode 
									where StockCodeTypeID =1 and Discontinued = 0 and Code like @Nappi+'%'
									and PackSize = convert(decimal(9,2),@PackSize)
									
									update StockCode set Code = @nappiCode
									,PackPrice = convert(decimal(9,3),@WholeSalePrice),
									UnitPrice = convert(decimal(9,3),@RetailPrice)
									where  StockCodeID = @idSCode
									
									update StockItem set Description = @Description,CodeGroupID = @medMat,
									Schedule = @Schedule WHERE StockItemID = @idSItem
								END						
						END				
				END
		END         
	ELSE IF(@FileNo = '299')
		BEGIN
			DELETE FROM TempTableNappi
			UPDATE StockCode SET Discontinued = 1 WHERE Code NOT IN (SELECT NappiCode FROM TempTableNappiCode)
			DELETE FROM TempTableNappiCode
		END
END  

/*INSERT INTO CodeGroup (CodeGroupID,CodeGroupDescr,CodeGroupDefinition,CodeType,ChargeVAT,Scriptable)
values (7,'Reference','Reference','S',1,0)

CREATE TABLE TempTableNappi(ID int IDENTITY(1,1) NOT NULL,Nappi varchar(20) NULL,
IsMedMat varchar(20) NULL)

Create a new column in StockCode Table - "Discontinued"
Datatype is int and make it as "not null" field

set Discontinued = 0 for all the existing records

check for the duplicate (9digit nappicode) in StockCode table. IF exist then set Discontinued = 1 for those records

Inform AMAR/Mages to add the "Discontinued = 0" check while searching the dispense.

change the datatype of packsize to decimal(9,2) in StockItem and StockCode table
change the datatype of packprice,unitprice to decimal(9,3) in StockCode table

Add extra columns - "Description" varchar(150), "ProductStrength" varchar(50), "DosageFormCode" varchar(50) in 
TempTableNappi table

Create New Table TempTableNappiCode

Add extra columns-"PackSize" varchar(500),"PackPrice" varchar(800),"Schedule" varchar(5) in TempTableNappiCode table
*/

		
		


GO
