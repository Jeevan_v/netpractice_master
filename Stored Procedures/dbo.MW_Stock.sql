SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_Stock] (@SearchValue VARCHAR(8000),@IsDescription BIT)     
AS    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
SET NOCOUNT ON;    
    
IF @SearchValue =''    
 BEGIN    
  SELECT top 2500 sc.[Code],sc.[PackPrice],si.[Description],Convert(int,si.[Packsize])as PackSize,sc.[UnitPrice],cg.[CodeGroupID],cg.[CodeGroupDescr],mo.[MFOptionID],mo.[MFOptionName]     
  FROM dbo.StockCode sc     
  INNER JOIN dbo.StockItem si ON sc.StockItemID=si.StockItemID    
  INNER JOIN dbo.StockCodeType sct on sct.StockCodeTypeID=sc.StockCodeTypeID and sct.StockCodeType='NAPPI'    
  INNER JOIN dbo.CodeGroup cg ON cg.CodeGroupID = si.CodeGroupID    
  INNER JOIN dbo.[MFOption] mo ON mo.MFOptionID = cg.MFOptionID  WHERE sc.Discontinued = 0  
  Order by si.[Description];    
 END    
ELSE     
 IF @IsDescription = 1         
  BEGIN    
   SELECT top 2500 sc.[Code],sc.[PackPrice],si.[Description],Convert(int,si.[PackSize])as PackSize,sc.[UnitPrice],cg.[CodeGroupID],cg.[CodeGroupDescr],mo.[MFOptionID],mo.[MFOptionName]    
   FROM dbo.StockCode sc     
   INNER JOIN dbo.StockItem si ON sc.StockItemID=si.StockItemID    
   INNER JOIN dbo.StockCodeType sct on sct.StockCodeTypeID=sc.StockCodeTypeID and sct.StockCodeType='NAPPI'    
   INNER JOIN dbo.CodeGroup cg ON cg.CodeGroupID = si.CodeGroupID    
   INNER JOIN dbo.[MFOption] mo ON mo.MFOptionID = cg.MFOptionID    
   WHERE si.[Description] LIKE '%' + @SearchValue + '%' AND sc.Discontinued = 0  
   Order by si.[Description];    
  END    
 ELSE    
  BEGIN    
   SELECT top 2500 sc.[Code],sc.[PackPrice],si.[Description],Convert(int,si.[Packsize])as PackSize,sc.[UnitPrice],cg.[CodeGroupID],cg.[CodeGroupDescr],mo.[MFOptionID],mo.[MFOptionName]    
   FROM dbo.StockCode sc     
   INNER JOIN dbo.StockItem si ON sc.StockItemID=si.StockItemID    
   INNER JOIN dbo.StockCodeType sct on sct.StockCodeTypeID=sc.StockCodeTypeID and sct.StockCodeType='NAPPI'    
   INNER JOIN dbo.CodeGroup cg ON cg.CodeGroupID = si.CodeGroupID    
   INNER JOIN dbo.[MFOption] mo ON mo.MFOptionID = cg.MFOptionID    
   WHERE sc.[Code] LIKE '%' + @SearchValue + '%' AND sc.Discontinued = 0    
   Order by si.[Description];    
  END 
GO
