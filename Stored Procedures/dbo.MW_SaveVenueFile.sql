SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_SaveVenueFile]
	   @VenueFileID int
	  ,@VenueID int
      ,@AccountID int
      ,@FileNo varchar(30)
AS

IF @VenueFileID IS NULL
BEGIN
	INSERT INTO VenueFile
	(
	   VenueID
      ,AccountID
      ,FileNo
	)
	VALUES
	(
	   @VenueID
      ,@AccountID
      ,@FileNo
	)
SELECT SCOPE_IDENTITY()
END
ELSE 
BEGIN	
	UPDATE VenueFile SET
	   	   FileNo=@FileNo
    WHERE
    VenueFileID=@VenueFileID  
    AND
    AccountID=@AccountID 
    SELECT  @VenueFileID    
END

GO
