SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 8th Dec 2014
-- Description:	This procedure will update status of XML Request info with other fields such as file name, date etc
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateXMLRequestInfo] 
	-- Add the parameters for the stored procedure here
	@XMLRequestId int = 0,
	@StatusofRequest int= 1,
	@ClaimXMLFileId int = 1,
	@XMLFileName Varchar(Max) = null,
	@Remarks varchar(Max) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[XmlRequest]
   SET 
      [STATUSOFREQUEST] =@StatusofRequest
      ,[CLAIMXMLFILEID] = @ClaimXMLFileId
      ,[XMLFILENAME] = @XMLFileName
      ,[UPDATEDDATE] = getdate()
      ,[REMARKS] =@Remarks
 WHERE [XMLREQUESTID] =@XMLRequestId


END

GO
