SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetUsersByPracticeForAttendingDoc]  
 @PracticeID int,@IsApproved bit  
AS  
IF @IsApproved IS Not Null  
BEGIN  
  
SELECT DISTINCT   
  
 u.UserID,  
 u.MembershipID,  
   
 u.FirstName +' '+ u.LastName as Name,  
   
 u.MPNo,  
 u.DocPracticeNo,  
 p.PracticeNo,  
  p.PracticeTypeID,
 ar.RoleName,  
 ams.IsApproved  
  
FROM  
  
dbo.[User] u  
  
JOIN  
  
dbo.aspnet_Users au  
  
ON   
  
u.MembershipID = au.UserId  
  
JOIN   
  
aspnet_Membership ams  
  
ON  
  
au.UserId = ams.UserId  
  
JOIN   
  
dbo.UserVenueRelationship uvr  
  
ON   
  
uvr.UserID = u.UserID  
  
JOIN   
  
dbo.Venue v  
  
ON   
  
uvr.VenueID = v.VenueID  
  
JOIN   
  
dbo.Practice p  
  
ON   
  
p.PracticeID = v.PracticeID  
  
JOIN  
  
aspnet_UsersInRoles uir  
  
ON  
  
 au.UserId = uir.UserId  
  
JOIN  
  
 aspnet_Roles ar  
ON  
  
 uir.RoleId = ar.RoleId  
  
WHERE  
  
 p.PracticeID= @PracticeID  
 AND ams.IsApproved=@IsApproved 
 AND ar.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor')   
 --AND MPNo !=''  --SMM : 1Apr14
order by Name asc  
END  
  
ELSE  
  
BEGIN  
  
SELECT DISTINCT   
  
 u.UserID,  
 u.MembershipID,  
 u.TitleID,  
 u.FirstName,  
 u.LastName,  
 u.IDNo,  
 u.MPNo,  
 u.DocPracticeNo,  
 u.JobTitleID,  
 u.ReasonID,  
 u.DispenseLicenseNo,  
 au.UserName,  
 ams.[Password],  
 ams.Email,  
 ar.RoleName,  
 ams.IsApproved  
  
FROM  
  
dbo.[User] u  
  
JOIN  
  
dbo.aspnet_Users au  
  
ON   
  
u.MembershipID = au.UserId  
  
JOIN   
  
aspnet_Membership ams  
  
ON  
  
au.UserId = ams.UserId  
  
JOIN   
  
dbo.UserVenueRelationship uvr  
  
ON   
  
uvr.UserID = u.UserID  
  
JOIN   
  
dbo.Venue v  
  
ON   
  
uvr.VenueID = v.VenueID  
  
JOIN   
  
dbo.Practice p  
  
ON   
  
p.PracticeID = v.PracticeID  
  
JOIN  
  
aspnet_UsersInRoles uir  
  
ON  
  
 au.UserId = uir.UserId  
  
JOIN  
  
 aspnet_Roles ar  
ON  
  
 uir.RoleId = ar.RoleId  
  
WHERE  
  
 p.PracticeID= @PracticeID  
 AND ar.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor')   
 --AND MPNo !=''  --SMM : 1Apr14
 order by u.FirstName asc  
  
END  
  
  

GO
