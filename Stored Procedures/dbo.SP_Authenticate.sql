SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SP_Authenticate] 
@UserName varchar(200)
AS
BEGIN	
	SET NOCOUNT ON;
	SELECT  aspnet_Membership.UserId,[Password],PasswordSalt,RoleName
	FROM    aspnet_Membership 
			INNER JOIN aspnet_Users on aspnet_Membership.UserID = aspnet_Users.UserID
			INNER JOIN aspnet_UsersInRoles on aspnet_UsersInRoles.UserId = aspnet_Users.UserID
			INNER JOIN aspnet_Roles on aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId
	WHERE    aspnet_Users.UserName = @UserName 
END

GO
