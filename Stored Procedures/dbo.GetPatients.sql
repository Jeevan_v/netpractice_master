SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[GetPatients]
As
SELECT  [PatientID]
      ,[AllergyTypeOther]
  FROM [dbo].[Patient] 
  Where [MembershipID] is null
GO
