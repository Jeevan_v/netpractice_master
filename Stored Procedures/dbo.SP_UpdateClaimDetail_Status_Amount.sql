SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 20/10/2015
-- Description:	Update claim status and amount for discovery delayed response related claims
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateClaimDetail_Status_Amount] 
	-- Add the parameters for the stored procedure here
	@ClaimID int, 
	@NappiCode nvarchar(50),
	@ClaimLineStatus nvarchar(50),
	@ErrorCode	nvarchar(50),
	@ErrorDesc	nvarchar(500),
	@ApprovedAmount	decimal(18,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  UPDATE ClaimDetail
  set [Status] = @ClaimLineStatus,  AmountPaid =  @ApprovedAmount
  WHERE ClaimID = @ClaimID and NappiCode like @NappiCode+'%'


END

GO
