SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 08-05-2015
-- Description:	This method will returns all invoices  based on status
-- =============================================
--select * from EmailInvoiceGeneration with(nolock) order by 1 desc 
CREATE PROCEDURE [dbo].[SP_GetAllInvoices_ByStatus]  ---'Claim Received'
	-- Add the parameters for the stored procedure here
	@Status  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT distinct  cc.claimid, cc.invoiceno, cc.status , vv.VisitDate, vv.DateLogged, ms.SchemeName ,ms.MedicalSchemeID,ml.Email, 
	pra.PracticeName
	from claim cc with(nolock) 
	inner join Visit vv with(nolock) on vv.VisitID = cc.VisitID
	inner join MedicalScheme ms with(nolock) on ms.MedicalSchemeID = vv.SchemeID
	inner join MedicalPlan mp with(nolock) on mp.MedicalSchemeID = ms.MedicalSchemeID and vv.PlanID = mp.MedicalPlanID
	inner  join MFOptionLink ml with(nolock) on ml.PlanID = mp.MedicalPlanID
	inner join Account acc with(nolock) on acc.AccountID	 = vv.AccountID
	inner join Practice pra with(nolock) on pra.PracticeID = acc.PracticeID 
	where  
	(upper(cc.status) =upper(@Status) or cc.StatusReason like '%Submit to Umvuzo%' ) and 
	vv.VisitDate >= (getdate()-150) and 
	--ms.MedicalSchemeID in ( 261) --and
	--ms.MedicalSchemeID not in (342)  --and ms.MedicalSchemeID = 520 
	--and pra.PracticeID = 2408 and cc.Status='rejected' 
	--and 
	cc.ClaimID not in (select claimid from EmailInvoiceGeneration with(nolock) where IsSuccess = 1 )
	and isnull(ms.Email,'') <>'' 
	--and 
--	ms.SchemeName not in
--	(
--	'BANKMED CARECROSS',
----'BUILD IND EAST CAPE',
----'DISCOVERY',
----'ESSENTIALMED CARECROSS',
--'HOSMED',
----'LA HEALTH',
--'LIBERTY BLUE',
----'LIBERTY MEDICAL',
--'MALCOR',
--'MEDIHELP PRIMECURE',
--'MEDSHIELD',
----'MOMENTUM HEALTH',
--'NEDGROUP',
----'NETCARE CSR FUND',
--'ONEPLAN',
--'PRIMECURE',
--'RAND MUTUAL',
--'RENAISSANCE HEALTH',
--'SPECTRAMED',
--'THEBEMED',
--'TIGER BRANDS',
--'TOPMED',
----'TRANSMED',
--'UMED',
----'UMVUZO HEALTH',
----'UMVUZO HEALTHPARTNERS',
--'UNIVERSITY OF NATAL'
--)
	order by vv.VisitDate desc
END
GO
