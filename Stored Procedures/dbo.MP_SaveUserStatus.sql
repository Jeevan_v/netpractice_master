SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[MP_SaveUserStatus]
	@UserID int,
	@IsActive bit,
	@ReasonID int,
	 @MembershipID uniqueidentifier
	 

AS
BEGIN
SET @MembershipID=(select MembershipID from [User] where UserID=@UserID)
UPDATE aspnet_Membership
SET
	IsApproved=@IsActive
	
WHERE
	UserId=@MembershipID
	
update [User] 
SET
ReasonID=@ReasonID
where

UserID=@UserID
SELECT @UserID

END



GO
