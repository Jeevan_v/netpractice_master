SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_SavePatient]
	   @PatientID int
      ,@MembershipID uniqueidentifier
      ,@IDNo varchar(25)
      ,@TitleID int
      ,@FirstName varchar(100)
      ,@LastName varchar(50)
      ,@Initials varchar(5)
      ,@DOB date
      ,@PhysicalAddressID int
      ,@PostalAddressID int
      ,@TelNo varchar(30)
      ,@CellNo varchar(30)
      ,@FaxNo varchar(30)
      ,@GenderID int
      ,@RaceID int
      ,@LanguageID int
      ,@DependendNo varchar(3)
      ,@IsDependant bit
      ,@MainMemberID bigint
      ,@RelationShipID int
      ,@ChargeCategoryID int
      ,@ChargeTypeID int
      ,@PlanID int
      ,@SchemeID int
      ,@PatientHeight varchar(5)
      ,@PatientWeight varchar(5)
      ,@EmergencyContName varchar(100)
      ,@EmergencyTelNo varchar(30)
      ,@PassportNo varchar(25)
      ,@AllergyTypeID int
      ,@MedicalAidNo varchar(50)     
      ,@FMCheckDate date
      ,@AllergyTypeOther varchar(100)
      ,@IsSuspended bit -- For FM Check Suspended
      ,@ReasonID int
	  ,@CountryID int
	  ,@PatientType varchar(50)
	  ,@CompanyAddressID int
	  ,@CompanyName varchar(100)
	  ,@CompanyVATNo varchar(50)
	
AS
if(@LanguageID=0)

            begin

            set @LanguageID=1

            end

if(@RelationShipID =0)
set @RelationShipID = NULL


IF @PatientID IS NULL   
BEGIN
	INSERT INTO Patient
	(
	   [MembershipID]
      ,[IDNo]
      ,[TitleID]
      ,[FirstName]
      ,[LastName]
      ,[Initials]
      ,[DOB]
      ,[PhysicalAddressID]
      ,[PostalAddressID]
      ,[TelNo]
      ,[CellNo]
      ,[FaxNo]
      ,[GenderID]
      ,[RaceID]
      ,[LanguageID]
      ,[DependendNo]
      ,[IsDependant]
      ,[MainMemberID]
      ,[RelationShipID]
      ,[ChargeCategoryID]
      ,[ChargeTypeID]
      ,[PlanID]
      ,[SchemeID]
      ,[PatientHeight]
      ,[PatientWeight]
      ,[EmergencyContName]
      ,[EmergencyTelNo]
      ,[PassportNo]
      ,[AllergyTypeID]
      ,[MedicalAidNo]
      ,[FMCheckDate]
      ,[AllergyTypeOther]
      ,[IsSuspended] -- For FM Check Suspended
      ,[ReasonID]
	  ,[CountryID]
	  ,[CompanyName]
	  ,[CompanyAddressID]
	  ,[PatientType]
	  ,[CompanyVATNo]
	)
	VALUES
	(
	   @MembershipID 
      ,@IDNo 
      ,@TitleID
      ,@FirstName 
      ,@LastName 
      ,@Initials 
      ,@DOB 
      ,@PhysicalAddressID 
      ,@PostalAddressID 
      ,@TelNo
      ,@CellNo
      ,@FaxNo 
      ,@GenderID 
      ,@RaceID 
      ,@LanguageID 
      ,@DependendNo 
      ,@IsDependant 
      ,@MainMemberID 
      ,@RelationShipID 
      ,@ChargeCategoryID
      ,@ChargeTypeID
      ,@PlanID 
      ,@SchemeID 
      ,@PatientHeight 
      ,@PatientWeight 
      ,@EmergencyContName 
      ,@EmergencyTelNo 
      ,@PassportNo 
      ,@AllergyTypeID 
      ,@MedicalAidNo 
      ,@FMCheckDate 
     ,@AllergyTypeOther 
     ,@IsSuspended -- For FM Check Suspended
     ,@ReasonID
	 ,@CountryID
	  ,@CompanyName
	  ,@CompanyAddressID
	  ,@PatientType
	  ,@CompanyVATNo
	 )
	 --IF THE PATIENT IS MAIN MEMBER PUT HIS/HER PATIENTID AS MAIN MEBERID
	 DECLARE @Patid INT
	 SET @Patid=(SELECT SCOPE_IDENTITY())	 	 
	 UPDATE Patient SET
	 MainMemberID=@Patid
	 WHERE
	 PatientID=@Patid
	 AND
	 MainMemberID IS NULL	 
	
	SELECT @Patid	 
END
ELSE
BEGIN
declare @oldReasonId int;
select @oldReasonId =reasonId from Patient where PatientID=@PatientID
UPDATE Patient SET	
	    
      IDNo=@IDNo 
	  ,TitleID=@TitleID
	  ,FirstName=@FirstName 
	  ,LastName=@LastName 
      ,Initials=@Initials 
      ,DOB=@DOB 
      ,PhysicalAddressID=@PhysicalAddressID 
      ,PostalAddressID=@PostalAddressID 
      ,TelNo=@TelNo
      ,CellNo=@CellNo
      ,FaxNo=@FaxNo 
      ,GenderID=@GenderID 
      ,RaceID=@RaceID 
      ,LanguageID=@LanguageID 
      ,DependendNo=@DependendNo 
      ,IsDependant=@IsDependant 
      ,RelationShipID=@RelationShipID 
      ,ChargeCategoryID=@ChargeCategoryID
      ,ChargeTypeID=@ChargeTypeID
      ,PlanID=@PlanID 
      ,MainMemberID=@MainMemberID
      ,SchemeID=@SchemeID 
      ,PatientHeight=@PatientHeight 
      ,PatientWeight=@PatientWeight 
      ,EmergencyContName=@EmergencyContName 
      ,EmergencyTelNo=@EmergencyTelNo 
      ,PassportNo=@PassportNo 
      ,AllergyTypeID=@AllergyTypeID 
      ,MedicalAidNo=@MedicalAidNo
      ,FMCheckDate=@FMCheckDate 
      ,AllergyTypeOther=@AllergyTypeOther 
      ,IsSuspended=@IsSuspended -- For FM Check Suspended
      ,ReasonID=@ReasonID
	  ,[CountryID]= @CountryID
	  ,[CompanyName]=@CompanyName
	  ,[CompanyAddressID] = @CompanyAddressID
	  ,[PatientType] = @PatientType
	  ,[CompanyVATNo] = @CompanyVATNo
  WHERE
	  PatientID=@PatientID	 
	  
	  if(@ReasonID is not null and @PatientID=@MainMemberID)
	  Begin
	  Update Patient set Reasonid=@ReasonID where PatientID in(Select Patientid from Patient where MainMemberID=@PatientID)
	  update aspm set aspm.IsApproved=0 from aspnet_Membership aspm inner join Patient p on aspm.UserId=p.MembershipID where MainMemberID=@PatientID
	  End  
	  else if(@ReasonID is null and @PatientID=@MainMemberID and @oldReasonId is not null)
	    begin
	    Update Patient set Reasonid=null where PatientID in(Select Patientid from Patient where MainMemberID=@PatientID)
	    update aspm set aspm.IsApproved=1 from aspnet_Membership aspm inner join Patient p on aspm.UserId=p.MembershipID where MainMemberID=@PatientID
	    end
SELECT @PatientID
     
END
GO
