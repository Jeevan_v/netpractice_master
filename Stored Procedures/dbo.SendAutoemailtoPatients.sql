SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SendAutoemailtoPatients]   

-- Add the parameters for the stored procedure here  

AS  

BEGIN  

SET NOCOUNT ON;  

   

SELECT M.EMAIL,v.VisitDate,  

CE.ErrorDesc ,Lower(P.FirstName +' '+ p.LastName) as Name  

FROM aspnet_Membership M   

INNER JOIN Patient P on M.UserId = P.MembershipID  

INNER JOIN Account A on P.PatientID = A.PatientID  

INNER JOIN Visit V on A.AccountID = V.AccountID  

INNER JOIN VisitLine VL on V.VisitID = VL.VisitID  

INNER JOIN ClaimDetail CD on VL.VisitLineID = CD.VisitLineID  

INNER JOIN ClaimError CE on CD.ClaimDetailID = CE.ClaimDetailID  

and (CD.Status = 'Rejected' or CD.Status = 'Partially Accepted') 

and convert(varchar(10),V.DateLogged,101) =convert(varchar(10),GETDATE(),101) and  (M.EMAIL<>'' or M.EMAIL is not null)

END
GO
