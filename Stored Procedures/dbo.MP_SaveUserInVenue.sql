SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sameer>
-- Create date: <5/11/2012>
-- Description:	<Allocate Venues>
-- =============================================
CREATE PROCEDURE [dbo].[MP_SaveUserInVenue]  
	@UserID int,
	@VenueID int	
AS

BEGIN
	
INSERT INTO UserVenueRelationship
VALUES(@UserID,@VenueID)	
END


GO
