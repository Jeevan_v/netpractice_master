SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 04 OCT 2016
-- Description:	Retrieving active token details for switch
-- =============================================
CREATE PROCEDURE [dbo].[SP_GET_SWITCH_TOKEN_DETAILS] 
	-- Add the parameters for the stored procedure here
@SwitchName  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT [SwitchTokenId]
      ,[SwitchName]
      ,[TokenValue]
      ,[TokenExpiresIn]
      ,[TokenType]
      ,[DtIssued]
      ,[DtExpire]
      ,[Sessionid]
      ,[UserId]
      ,[IsActive]
      ,[Remarks]
      ,[CreatedDate]
      ,[UpdatedDate]
  FROM [dbo].[SwitchTokenDetails]
  WHERE  SwitchName = @SwitchName AND IsActive =1
END

GO
