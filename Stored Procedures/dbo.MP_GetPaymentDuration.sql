SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetPaymentDuration] 
AS

SELECT
	PaymentDurationID,
	PaymentDuration
FROM
	PaymentDuration

GO
