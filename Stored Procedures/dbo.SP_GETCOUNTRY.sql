SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 18/06/2015
-- Description:	Get country list for country drop down
-- =============================================
CREATE PROCEDURE [dbo].[SP_GETCOUNTRY] 
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Select CountryID , CountryCode, CountryName from country 
   order by 1 ASC
END

GO
