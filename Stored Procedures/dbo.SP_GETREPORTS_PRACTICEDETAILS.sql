SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*====================== DEVELOPER COMMENTS===========================    



DEVELOPER NAME : PRASANNAVENGADESAN    



PROCEDURE NAME : SP_GETREPORTS_PRACTICEDETAILS  



DATE           : FEB 15TH 2013    



PURPOSE        : THIS IS FOR FETCHING PRACTICE DETAILS  



======================DEVELOPER COMMENTS===========================*/    



  



--SP_GETREPORTS_PRACTICEDETAILS 8    



CREATE PROCEDURE [dbo].[SP_GETREPORTS_PRACTICEDETAILS] @ID INT      



AS      



BEGIN      



IF @ID = 0      



 BEGIN      



  SELECT 0 AS PracticeID,'ALL' AS PracticeName,'A' AS OrderCol    



  UNION       



  SELECT PRACTICEID,PRACTICENAME,'B' AS ORDERCOL FROM PRACTICE ORDER BY 2       



 END      



ELSE      



 BEGIN      



  SELECT PracticeID,PracticeName,'A' AS OrderCol FROM PRACTICE WHERE PRACTICEID=@ID  ORDER BY 2       



 END      



      



END 




GO
