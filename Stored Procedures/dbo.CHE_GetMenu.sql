SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Sameer>
-- Create date: <2/12/2012>
-- Description:	<Getting Menus for Roles for Caching>
-- =============================================
CREATE PROCEDURE [dbo].[CHE_GetMenu]  
	-- Add the parameters for the stored procedure here
AS
BEGIN
SELECT DISTINCT r.RoleName, m.MenuID, 

m.DisplayName,m.ParentID,ParentM.DisplayName AS [ParentMenu]

FROM PrivilegeRoleRelationship p INNER JOIN aspnet_Roles r 

ON p.RoleID = r.RoleId 

INNER JOIN Privilege pr

ON pr.PrivilegeID=p.PrivilegeID

INNER JOIN PrivilegeMenuRelationship pm

ON pm.privilegeId=pr.PrivilegeID

INNER JOIN Menu m

ON m.MenuID=pm.menuId

INNER JOIN Menu ParentM

ON ParentM.MenuID=m.ParentID

END


GO
