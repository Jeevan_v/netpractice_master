SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CHE_GetAllMedicalPlans]
AS
BEGIN
	SELECT 
	MedicalPlanID,
	PlanName,
	MedicalSchemeID
	FROM
	MedicalPlan	
	ORDER BY PlanName
END


GO
