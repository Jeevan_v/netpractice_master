SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_SaveUser]
	@UserID int,
	@MembershipID uniqueidentifier,
	@TitleID int,
	@Initials varchar(10),
	@FirstName varchar(30),
	@LastName varchar(30),
	@IDNo varchar(20),
	@MPNo varchar(20),
	@DocPracticeNo varchar(20),
	 
	@ReasonID int,
	@DispenseLicenseNo varchar(30),
	@PracticeTypeID int
AS

IF @UserID IS NULL
BEGIN
IF    @TitleID <>0 
BEGIN 
		INSERT INTO [User]
		(
			MembershipID,
			TitleID,
			Initials,
			FirstName,
			LastName,
			IDNo,
			MPNo,
			DocPracticeNo,
			 
			ReasonID,
			DispenseLicenseNo,
			PracticeTypeID	
		)
		VALUES
		(
			@MembershipID,
			@TitleID,
			@Initials,
			@FirstName,
			@LastName,
			@IDNo,
			@MPNo,
			@DocPracticeNo,
			 
			@ReasonID,	
			@DispenseLicenseNo,
			@PracticeTypeID
		)
END 
ELSE 
BEGIN
INSERT INTO [User]
(
	MembershipID,
	
	Initials,
	FirstName,
	LastName,
	IDNo,
	MPNo,
	DocPracticeNo,
	ReasonID,
	DispenseLicenseNo,
	PracticeTypeID	
)
VALUES
(
	@MembershipID,
	 
	@Initials,
	@FirstName,
	@LastName,
	@IDNo,
	@MPNo,
	@DocPracticeNo,
	@ReasonID,	
	@DispenseLicenseNo,
	@PracticeTypeID
)
END 

SELECT SCOPE_IDENTITY()

END

ELSE

BEGIN
IF  @TitleID <>0 
BEGIN
UPDATE [User] SET
	
	TitleID=@TitleID,
	Initials = @Initials,
	FirstName=@FirstName,
	LastName=@LastName,
	IDNo=@IDNo,
	MPNo=@MPNo,
	DocPracticeNo=@DocPracticeNo,
	ReasonID=@ReasonID,
	DispenseLicenseNo=@DispenseLicenseNo,
	PracticeTypeID=@PracticeTypeID	
WHERE
	UserID=@UserID
END 

ELSE 
BEGIN 
UPDATE [User] SET
	
	 
	Initials = @Initials,
	FirstName=@FirstName,
	LastName=@LastName,
	IDNo=@IDNo,
	MPNo=@MPNo,
	DocPracticeNo=@DocPracticeNo,
	 
	ReasonID=@ReasonID,
	DispenseLicenseNo=@DispenseLicenseNo,
	PracticeTypeID=@PracticeTypeID	
WHERE
	UserID=@UserID
END

	SELECT @UserID	
END

GO
