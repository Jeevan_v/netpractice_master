SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetMainMemberStatus] 
 @patientID int  
AS  
select  
    aspm.IsApproved  
FROM   
[Patient] p   
INNER JOIN  
[aspnet_Membership] aspm  
ON  
aspm.UserId = p.MembershipID 
where 
p.PatientID=@patientID 
GO
