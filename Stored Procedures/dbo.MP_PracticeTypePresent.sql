SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create proc [dbo].[MP_PracticeTypePresent]
@practiceID int
AS
select COUNT(PracticeTypeID) from Practice p where p.PracticeTypeID in (select PracticeTypeID from Practicetype where PracticeType like 'group%') and p.PracticeID=@practiceID
GO
