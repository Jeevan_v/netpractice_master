SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[CMN_GetFullNameByUsername]  @username varchar(50),@IsUser bit
as
declare @UserFullname varchar(100), @PatientFullname varchar(100)
select @UserFullname= u.FirstName+' '+u.LastName from [aspnet_Users] asp inner join [User] u on asp.UserId=u.MembershipID where asp.UserName=@username
select @PatientFullname= u.FirstName+' '+u.LastName from [aspnet_Users] asp inner join [Patient] u on asp.UserId=u.MembershipID where asp.UserName=@username 
if(@UserFullname is NOT NULL and @PatientFullname is null)
begin 
 select @UserFullname
end
else
select @PatientFullname
GO
