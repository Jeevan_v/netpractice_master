SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetUserStatus] 
	@UserName varchar(100)
AS
select
	r.ReasonID,
    r.Reason
FROM 
[User] u 
INNER JOIN
[aspnet_Users] aspu
ON
aspu.UserId = u.MembershipID
INNER JOIN
[InActiveReason] r
ON
u.ReasonID = r.ReasonID
where 
aspu.UserName=@UserName


GO
