SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_updateClaimtable]  

 @ClaimID int,  

 @status varchar(50),  

 @statusregion varchar(150),  

 @spbhf varchar(20),  

 @amountpaid numeric(32,2),  

 @amountovercharged numeric(32,2)  

AS  

BEGIN 

Declare @Amtpaid numeric(32,2),@Totalamt numeric(32,2)

set @AmtPaid= (select CASE WHEN AmountPaid IS NULL THEN 0 ELSE AmountPaid END from Claim  with (nolock) where ClaimID=@ClaimID)

set @Totalamt=(select TotalAmount from Claim with(nolock) where ClaimID=@ClaimID)

set @AmtPaid=@AmtPaid+@amountpaid

if(@AmtPaid <= @Totalamt)

Begin  

 update Claim 

  set AmountPaid=@AmtPaid,  

  SP_BHF=@spbhf, Status=@status,StatusReason=@statusregion,

AmountOverCharged=@amountovercharged  

 where ClaimID=@ClaimID  

 End

		If(@status = 'REJECTED')
		Begin

		UPDATE ClaimDetail
		set Status=@status
		WHERE ClaimID =@ClaimID

		END

END 
GO
