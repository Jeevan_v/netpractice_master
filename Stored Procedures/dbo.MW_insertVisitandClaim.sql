SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_insertVisitandClaim]
	-- Add the parameters for the stored procedure here
 @accountid int,
 --@invoiceno int,
 @venueid int = 0,
 @chargetypeid int = 0,
 @chargecatid int = 0,
 @schemeid int = 0,
 @planid int = 0,
 @membershipno varchar(50),
 @authcode varchar(10),
 @visitdate varchar(20),
 @loginID int = 0,
 @userID int = 0,
 --@unique_key varchar(80),
 @total decimal(13,2)
AS
BEGIN
DECLARE @visitID int, @claimID int,@invoiceno int
  
   Delete from Visit where VisitID=@visitID
   Delete from Claim where ClaimID=@claimID

begin

   set @invoiceno=(select MAX(InvoiceNo)+1 from Visit)
   if(@invoiceno is null)
   set @invoiceno=1;
 end
      

INSERT into VISIT (AccountID, venueid, chargetypeid, ChargeCategoryID, schemeid, planid, 
 membershipno, AuthorisationCode, visitdate,datelogged, DoctorID, Invoiceno) VALUES 
(@accountid,@venueid,@chargetypeid,@chargecatid,@schemeid,@planid,@membershipno,
 @authcode,@visitdate,GetDate(),@userID,@invoiceno)
SET @visitID = (select SCOPE_IDENTITY())
INSERT into CLAIM (Visitid,switch,TotalAmount,invoiceno) VALUES 
(@visitID,'MEDIKREDIT',@total,@invoiceno)
SET @claimID =(select SCOPE_IDENTITY())
Select Visit.VisitID,Claim.ClaimID
    from Visit inner join Claim
    on Visit.VisitID=Claim.VisitID
    where Visit.VisitID=@visitID

END
GO
