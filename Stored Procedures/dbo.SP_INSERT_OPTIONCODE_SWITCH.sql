SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[SP_INSERT_OPTIONCODE_SWITCH]
	-- Add the parameters for the stored procedure here
	@Schemename varchar(50),
	 @SwitchName varchar(5000),
	  @optionCode varchar(5000)
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/* script for inserting option code for new switch */
Declare @MfcodeTypeid int
--Declare @Schemename varchar(50)
Declare @MedicalSchemeID	 int 
---Declare @SwitchName varchar(5000)
--set @SwitchName = 'MEDILINK'
--Declare @optionCode varchar(5000)

select @MfcodeTypeid= MFCodeTypeID from MFCodeType where CodeTypeDefinition like @SwitchName
print @MfcodeTypeid
if(isnull(@MfcodeTypeid,0)=0)
begin

INSERT INTO [dbo].[MFCodeType]
           ([AccountGroupID]
           ,[CodeTypeDefinition]
           ,[Description]
           ,[MFProviderCode]
           ,[Submit]
            )
     VALUES
           (null
           ,@SwitchName--'MEDILINK'
           ,@SwitchName--'MEDILINK'
           ,null--'ML'
           ,1
            )
end
--set @Schemename ='AECI'
print @Schemename
select @MedicalSchemeID = MedicalschemeID from MedicalScheme  where SchemeName like @Schemename 
print @MedicalSchemeID
if (isnull(@MedicalSchemeID, 0)<>0 )
Begin 
Declare @medicalplanid  int 
Declare @medicalplancnt int 
SElect @medicalplancnt = count(MedicalPlanid)  from  MedicalPlan where MedicalSchemeID=@MedicalSchemeID
print @medicalplancnt

		DECLARE cur_plan CURSOR FOR 
		SELECT  MedicalPlanid  from  MedicalPlan where MedicalSchemeID=@MedicalSchemeID
		OPEN cur_plan
		--print @@CURSOR_ROWS
		--IF @@CURSOR_ROWS > 0
		 
		 FETCH NEXT FROM cur_plan INTO @medicalplanid
		 WHILE @@Fetch_status = 0
		 BEGIN
		 PRINT 'ID : '+ convert(varchar(20),@medicalplanid)
		 ---mfoption 1 ,13, 5,6
		 declare @MfoptionLinkId int
		 declare @mfoptionLinkcodecnt int
		 select @MfoptionLinkId = MFOptionLinkID from  mfoptionlink where PlanID =@medicalplanid  and MFOptionid	= 1
		 select @mfoptionLinkcodecnt =count(*) from MFOptionLinkCode where MFOptionLinkID = @MfoptionLinkId and MFCodeTypeID = @MfcodeTypeid
		 if( @mfoptionLinkcodecnt =0)
		 begin

		 print 'here; inside code  1'
		 insert into MFOptionLinkCode
		 values
		 (@MfcodeTypeid,@MfoptionLinkId,@optionCode,null,1, null)
		 end
		 else 
		 print 'here; outside code 1 '

		 select @MfoptionLinkId = MFOptionLinkID from  mfoptionlink where PlanID =@medicalplanid  and MFOptionid	= 13
		 select @mfoptionLinkcodecnt =count(*) from MFOptionLinkCode where MFOptionLinkID = @MfoptionLinkId and MFCodeTypeID = @MfcodeTypeid
		 if( @mfoptionLinkcodecnt =0)
		 begin

		 print 'here; inside code 13'
		 insert into MFOptionLinkCode
		 values
		 (@MfcodeTypeid,@MfoptionLinkId,@optionCode,null,1, null)
		 end
		 else 
		 print 'here; outside code 13 '


		 select @MfoptionLinkId = MFOptionLinkID from  mfoptionlink where PlanID =@medicalplanid  and MFOptionid	= 5
		 select @mfoptionLinkcodecnt =count(*) from MFOptionLinkCode where MFOptionLinkID = @MfoptionLinkId and MFCodeTypeID = @MfcodeTypeid
		 if( @mfoptionLinkcodecnt =0)
		 begin

		 print 'here; inside code 5'
		 insert into MFOptionLinkCode
		 values
		 (@MfcodeTypeid,@MfoptionLinkId, @optionCode,null,1, null)
		 end
		 else 
		 print 'here; outside code 5'


		 select @MfoptionLinkId = MFOptionLinkID from  mfoptionlink where PlanID =@medicalplanid  and MFOptionid	= 6
		 select @mfoptionLinkcodecnt =count(*) from MFOptionLinkCode where MFOptionLinkID = @MfoptionLinkId and MFCodeTypeID = @MfcodeTypeid
		 if( @mfoptionLinkcodecnt =0)
		 begin

		 print 'here; inside code 6 '
		 insert into MFOptionLinkCode
		 values
		 (@MfcodeTypeid,@MfoptionLinkId, @optionCode,null,1, null)
		 end
		 else 
		 print 'here; outside code 6 '

		-- select * from MFOptionLinkCode order by 1 desc
		 FETCH NEXT FROM cur_plan INTO @medicalplanid
		 END
		 
		CLOSE cur_plan
		DEALLOCATE cur_plan

End
END
GO
