SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetPrivilegesForRole] 
	@RoleID uniqueidentifier 
AS

BEGIN

SELECT
	p.Privilege,p.PrivilegeID
FROM 
	PrivilegeRoleRelationship pr JOIN  Privilege p
ON 
	p.PrivilegeID = pr.PrivilegeID
WHERE 
	RoleID = @RoleID
	
END 

GO
