SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_UpdateClaimForReversal]--'Partially Accepted',598    
 -- Add the parameters for the stored procedure here    
 @Status varchar(50), 
 @StatusReason   varchar(150), 
 @ClaimID int,
  @strAuthhnet varchar(100)    
AS    
BEGIN 
   
	  if(UPPER(@Status) ='REVERSED' )  -- in case of reversal accepted by system
    Begin    
       Update Claim    
 set Status=@Status,SP_BHF=null,TRX_NBR=null,AmountPaid=null,StatusReason=@StatusReason,AmountOverCharged=null  
 where ClaimID=@ClaimID
  
   
   update ClaimXML
   set IsReversed='P'
   where ClaimID=@ClaimID   
    End    
    Else      if(UPPER(@Status) ='REJECTED' )
    Begin    --- in other cases such as normal response and in reversal rejected by the system
			 Update Claim    
			set  StatusReason = @StatusReason   ,Status=@Status 
			 where ClaimID=@ClaimID   
     
 End    
 ELSE
  Begin    --- in other cases such as normal response and in reversal rejected by the system
			 Update Claim    
			set Status=@Status , StatusReason = @StatusReason   
			 where ClaimID=@ClaimID   
     
 End    

 --update total amount paid and amoutn accepted from claimdetail 
 declare @TotalAmountPaid decimal(25,8) = 0.00

 Select @TotalAmountPaid=  sum(amountpaid) from ClaimDetail with(nolock) where ClaimID =@ClaimID
 
 update ClaimDetail
 set Auth_HNET = @strAuthhnet
 where ClaimID =@ClaimID

 update Claim
 set AmountPaid = @TotalAmountPaid
 where ClaimID = @ClaimID

END

GO
