SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_SavePracticeStatus]
	@PracticeID int,
	@IsActive bit,
	@ReasonID int,
	@ExtraInfo varchar(20)
AS
BEGIN
UPDATE Practice
SET
	IsActive = @IsActive,
	ReasonID=@ReasonID,
	ExtraInfo=@ExtraInfo
WHERE
	PracticeID = @PracticeID


	IF @IsActive = 1 
	BEGIN
	 INSERT INTO PracticePackageRelationship 
	 (PackageId, Subscribed, Approved, Date_Approved,PracticeId )
	 values 
	 (1,@IsActive,@IsActive,GETDATE(), @PracticeID)

 	 INSERT INTO PracticePackageRelationship 
	 (PackageId, Subscribed, Approved, Date_Approved,PracticeId )
	 values 
	 (2,@IsActive,@IsActive,GETDATE(), @PracticeID)

	 	 INSERT INTO PracticePackageRelationship 
	 (PackageId, Subscribed, Approved, Date_Approved,PracticeId )
	 values 
	 (3,@IsActive,@IsActive,GETDATE(), @PracticeID)

	 	 INSERT INTO PracticePackageRelationship 
	 (PackageId, Subscribed, Approved, Date_Approved,PracticeId )
	 values 
	 (4,@IsActive,@IsActive,GETDATE(), @PracticeID)

	 	 INSERT INTO PracticePackageRelationship 
	 (PackageId, Subscribed, Approved, Date_Approved,PracticeId )
	 values 
	 (5,@IsActive,@IsActive,GETDATE(), @PracticeID)

	 	 INSERT INTO PracticePackageRelationship 
	 (PackageId, Subscribed, Approved, Date_Approved,PracticeId )
	 values 
	 (6,@IsActive,@IsActive,GETDATE(), @PracticeID)

	 	 INSERT INTO PracticePackageRelationship 
	 (PackageId, Subscribed, Approved, Date_Approved,PracticeId )
	 values 
	 (7,@IsActive,@IsActive,GETDATE(), @PracticeID)

	END
	SELECT @PracticeID
END


GO
