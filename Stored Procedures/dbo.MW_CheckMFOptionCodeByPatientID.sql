SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MW_CheckMFOptionCodeByPatientID] 

@PatientID int
		
AS

BEGIN

SELECT

	mfo.MFOptionLinkCode 

FROM

	MFOptionLinkCode MFO 
	
JOIN 

	MFOptionLink OL 
	
ON

	OL.MFOptionLinkID = MFO.MFOptionLinkID 
JOIN 

	dbo.MedicalPlan P 

ON 

	P.MedicalPlanID = OL.PlanID

WHERE

	PlanID = 
	(
		
		SELECT  
			
			PlanID
		
		FROM
			
			Patient
		
		WHERE
		
			PatientID = @PatientID
	)
		
END

GO
