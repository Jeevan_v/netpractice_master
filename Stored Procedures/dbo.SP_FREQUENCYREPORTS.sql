SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_FREQUENCYREPORTS] @DATE DATETIME, @REPORTTYPE INT,@PRACTICEID INT, @DOCTORID INT                     
AS      
--declare @PRACTICEID numeric       
--declare @DOCTORID numeric  
--declare @DATE DATETIME  
--declare @REPORTTYPE numeric  
  
--SET @PRACTICEID=0  
--set @DOCTORID=0  
--SET @DATE='05/13/2013'   
  
--set @REPORTTYPE=1               
DECLARE @STARTDATE AS DATETIME                    
DECLARE @ENDDATE AS DATETIME                    
                    
IF @REPORTTYPE = 1              
BEGIN                    
 SET @STARTDATE = @DATE                    
 SET @ENDDATE = @DATE                    
END                    
ELSE IF  @REPORTTYPE = 2                 
BEGIN                    
 SET @STARTDATE = DATEADD(DD,-7,@DATE)                     
 SET @ENDDATE = @DATE                    
END                    
ELSE                     
BEGIN                    
 SET @STARTDATE = CONVERT(DATETIME,CONVERT(VARCHAR(20),CONVERT(VARCHAR(20),MONTH(@DATE))+'/01/'+CONVERT(VARCHAR(20),YEAR(@DATE)))) --DATEADD(DD,-30,@DATE)                     --SMM
 SET @ENDDATE = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@DATE))),DATEADD(mm,1,@DATE)),101) --@DATE      --SMM              
END                    
BEGIN                    
 SELECT DISTINCT V.VISITID,(P.FIRSTNAME+' '+P.LASTNAME) PATIENTNAME,VF.FILENO,MP.PLANNAME as [PLAN],MS.SCHEMENAME SCHEME,                    
 A.ACCOUNTID,                    
 CONVERT(VARCHAR(20),V.VISITDATE,103) VISITDATE,CONVERT(VARCHAR(20),V.DATELOGGED,103) DATELOGGED,          
 SUM(ISNULL(C.TOTALAMOUNT,0)) AMOUNT,                  
 (USR.FIRSTNAME+' '+USR.LASTNAME) AS DOCTOR,USR.USERID AS DOCTORID,                  
 PR.PRACTICEID,PR.PRACTICENAME PRACTICE                   
 FROM VISIT V                    
 INNER JOIN ACCOUNT A ON V.ACCOUNTID = A.ACCOUNTID                     
 INNER JOIN PATIENT P ON A.PATIENTID = P.PATIENTID                    
 INNER JOIN VENUEFILE VF ON VF.ACCOUNTID =A.ACCOUNTID                   
 INNER JOIN CLAIM C ON V.VISITID = C.VISITID                     
 INNER JOIN VENUE VEN ON VEN.VENUEID = V.VENUEID                    
 INNER JOIN PRACTICE PR ON PR.PRACTICEID = VEN.PRACTICEID                  
 INNER JOIN [USER] USR ON V.DOCTORID = USR.USERID   
 --Changed for Checking Doctor roles only  
 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID         
 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID   
 -- Changed  For Incorrect Mapping of User and Practice  
 INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID                  
 LEFT JOIN MEDICALPLAN MP ON MP.MEDICALPLANID = P.PLANID                     
 LEFT JOIN MEDICALSCHEME MS ON MS.MEDICALSCHEMEID = P.SCHEMEID     
 -- changes done for datelogged irrespective of visitdate (gowsi)                  
 WHERE PR.IsActive = 1 and CONVERT(DATE,V.DateLogged) >= @STARTDATE AND CONVERT(DATE,V.DateLogged) <= @ENDDATE 
               
 AND  CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 END =                   
      CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 END              
                    
 AND  CASE WHEN @DOCTORID <> 0 THEN V.DOCTORID ELSE 1 END =                   
      CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 END              
 -- Changed  For Incorrect MApping of User and Practice  
 AND  UVR.VenueID= V.VenueID   
 --Changed for Checking Doctor roles only  
 AND  
  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor')  
                 
 GROUP BY A.ACCOUNTID,V.DATELOGGED,P.FIRSTNAME,P.LASTNAME,VF.FILENO,MP.PLANNAME,MS.SCHEMENAME,                  
 USR.FIRSTNAME,USR.LASTNAME, PR.PRACTICEID,PR.PRACTICENAME,USR.USERID,V.VISITID,v.VisitDate                       
 ORDER BY 1,4,3                    
                    
END 
GO
