SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[insert_userfrmphp]
as
begin
select  * from (select k.id,k.username,k.password,k.email,k.idNo,k.mp_no,k.doc_practice_no,k.firstname,k.lastname,k.title_id,k.is_active,case when is_master=1 and is_doctor=0 and is_system_admin=1 and login_type_id=4 then 'Practice Admin' -- Practice Admin
		     when is_master=1 and is_doctor=1 and is_system_admin=1 and login_type_id=2 then 'Practitioner Admin' -- Practitioner Admin
		      when is_master=0 and is_doctor=1 and is_system_admin=0 and login_type_id=2 then 'Practitioner' -- Practitioner 
		      when is_master=0 and is_doctor=0 and is_system_admin=0 and login_type_id=4 then 'Receptionist' -- Receptionist
		      when is_master=1 and is_doctor=1 and is_system_admin=0 and login_type_id=2 then 'Practitioner' -- Practitioner 
		      when is_master=0 and is_doctor=0 and is_system_admin=1 and login_type_id=4 then 'Super Admin' -- super admin 
		       when is_master=0 and is_doctor=1 and is_system_admin=1 and login_type_id=4 then 'Practitioner Admin' -- Practitioner Admin 
		end as ROLEID from  [netpracticeoldNP].[dbo].[LOGIN] k ) a 
		
		end
GO
