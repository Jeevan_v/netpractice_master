SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MW_DoctorIDByVisistID] 

@intVisitId int
		
AS

BEGIN

SELECT

	DoctorID

FROM

	Visit
	
WHERE

	VisitID =@intVisitId
	
END

GO
