SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_updateClaimDetail] --10499,199.98,0,'accepted','78798798',1371,10510,319,'claimAccepted'
      @visitline int,
      @amountpaid decimal(18,2),
      @amountovercharged decimal(18,2),
      @status varchar(50),
      @strAuthhnet varchar(100),
      @ClaimID int,
      @ClaimDetailID int,
      @ErrorCode varchar(8),
      @ErrorDesc varchar(50)
AS
BEGIN
  Declare @cnterror  int,@modtype varchar(10),@NonModamt numeric(32,2),@chkvisitId int,@Amtpaid numeric(32,2),@Totalamt numeric(32,2),@maxvisitline int,
  @ReduceAmt numeric(32,2)
  
  set @chkvisitId=(select VisitID from claim  where claimID= @claimID )
  set @maxvisitline=(select MAX(VisitLineID) from  VisitLine where VisitID=@chkvisitId)
  
  
   
      update ClaimDetail
      set AmountPaid=@amountpaid,
      AmountOverCharged=@amountovercharged,
      Status=@status,
      Auth_HNET=@strAuthhnet
      where ClaimID=@claimID
      and VisitLineID=@visitline;
      
       
       
       if(@maxvisitline =@visitline)
      
   Begin
      set @Amtpaid=(select CASE WHEN AmountPaid IS NULL THEN 0 ELSE AmountPaid END from Claim where ClaimID=@claimID)
      Set @Totalamt=(select TotalAmount from Claim where ClaimID=@ClaimID)
      set @ReduceAmt = (select SUM(cd.AmountPaid) from VisitLine vl inner join ClaimDetail cd on vl.VisitLineID=cd.VisitLineID where
                           vl.VisitLineID  in  
                         (select  VisitLineID from ModifierTransaction where  ModifierType='Reduce' and VisitID=@chkvisitId)
                          and cd.ClaimID=@ClaimID)

      set @NonModamt=(select SUM(cd.AmountPaid) from VisitLine vl inner join ClaimDetail cd on vl.VisitLineID=cd.VisitLineID
                          where vl.VisitLineID not in 
                                    (select  VisitLineID from ModifierTransaction where  ModifierType='Reduce' and VisitID=@chkvisitId) 
                              and cd.ClaimID=@ClaimID)
      
         set @Amtpaid=@NonModamt-@ReduceAmt
      
        
         if(@AmtPaid <= @Totalamt)
          Begin
               update Claim
               set Amountpaid=@Amtpaid where ClaimID=@claimID
           End
        
   end
      
       
       
       
       
      select  @cnterror=COUNT(ClaimdetailID)  from ClaimError where ClaimDetailID=@ClaimDetailID
        
        if(@cnterror = 0)
      Begin
            Insert into ClaimError(ClaimDetailID,ErrorCode,ErrorDesc,ClaimID)
         Values(@ClaimDetailID,@ErrorCode,@ErrorDesc,@ClaimID)
    End
END
GO
