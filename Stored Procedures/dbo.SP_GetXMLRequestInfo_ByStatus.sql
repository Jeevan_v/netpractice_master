SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 8th Dec 2014
-- Description:	This procedure will select all the xml request info based on status passed.
-- =============================================
create PROCEDURE [dbo].[SP_GetXMLRequestInfo_ByStatus] 
@Status int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [XMLREQUESTID]
      ,[CLAIMID]
      ,[PATIENTID]
      ,[PRACTICEID]
      ,[VISITID]
      ,[ACCOUNTID]
      ,[TYPEOFREQUEST]
      ,[STATUSOFREQUEST]
      ,[CLAIMXMLFILEID]
      ,[XMLFILENAME]
      ,[CREATEDDATE]
      ,[UPDATEDDATE]
      ,[ISREPROCESSREQUIRED]
      ,[REMARKS]
      ,[REPROCESSINGDATE]
  FROM [dbo].[XmlRequest]
  WHERE STATUSOFREQUEST = @Status


END

GO
