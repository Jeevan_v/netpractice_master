SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CMN_GetPrivilegeByRoleName] 
      @RoleName nvarchar(256)
AS
BEGIN

SELECT

	pri.Privilege
	
FROM

dbo.Privilege pri

Inner Join 

dbo.PrivilegeRoleRelationship prr

ON

pri.PrivilegeID=prr.PrivilegeID

Inner Join 

dbo.aspnet_Roles ar

ON

prr.RoleID=ar.RoleId


WHERE

ar.RoleName=@RoleName 

END	


GO
