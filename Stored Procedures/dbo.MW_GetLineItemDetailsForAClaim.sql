SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[MW_GetLineItemDetailsForAClaim](@intClaimId int, @intVisitId int, @intPatientId int)
AS
BEGIN
SELECT  
	CASE
	WHEN  Vl.LineDefinition='Procedure' THEN cc.ChargeCode 	
	WHEN  Vl.LineDefinition='Medicine' THEN sc.Code
	WHEN  Vl.LineDefinition='Material' THEN sc.Code
	End AS [Code],
	Vl.[Description],
	Vl.[Diagnosis],
	dbo.[fn_Split](Vl.[Diagnosis]) As [Diagnosis_item],
	Vl.Amount,
	Vl.Quantity,
	sc.[UnitPrice] AS UnitPrice
FROM 
	Visit Vs 
	JOIN Account Acc ON Acc.AccountID = Vs.AccountID
	JOIN Patient Pt ON Pt.PatientID = Acc.PatientID
	JOIN VisitLine Vl ON Vl.VisitID = Vs.VisitID	
	JOIN ClaimDetail Cd ON Cd.VisitLineID = Vl.VisitLineID
	JOIN Claim Clm ON Clm.ClaimID = Cd.ClaimID
	JOIN Venue Ven ON Ven.VenueID = Vs.VenueID
	JOIN ChargeCode cc ON cc.ChargeCode = Vl.Code
	JOIN StockCode sc ON sc.Code = Vl.Code
WHERE 
	vs.VisitID = @intVisitId
	AND Pt.PatientID = @intPatientId
	AND Clm.ClaimID = @intClaimId
END

GO
