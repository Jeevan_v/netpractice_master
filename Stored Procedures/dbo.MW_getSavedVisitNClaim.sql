SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_getSavedVisitNClaim]    

 -- Add the parameters for the stored procedure here    

 @visitID int    

AS    

BEGIN    

 Select *,Replace(convert(Date,Visit.VisitDate),'-','')as Date     

    from Visit inner join Claim    

    on Visit.VisitID=Claim.VisitID    

    where Visit.VisitID=@visitID    

        

        

    select *,Replace(convert(varchar(8),Starttime,108),':','')as Time,Replace(convert(varchar(8),Endtime,108),':','')as Time

    -- chaged for calculating units for Modifiers with units

    ,CONVERT(DECIMAL(18,2),

    (CASE     

     WHEN  LOWER(RTRIM(LTRIM(mt.FunctionType))) = 'units'     

     THEN  mt.FunctionAmount /    

     (SELECT UnitPrice FROM ModifierRules WHERE ChargeCodeID=    

  (SELECT TOP 1  ChargeCodeID FROM ChargeCode WHERE InActive= 0 AND ChargeCode  = vl.Code AND CodeGroupID=5    

   AND TypeID=    

  (SELECT PractitionerGroup_ID FROM PracticeType WHERE PracticeTypeID=    

  (SELECT PracticeTypeID FROM Practice WHERE PracticeID=    

  (SELECT Practiceid FROM Account WHERE AccountID=(

  SELECT 

  AccountID from Visit where VisitID=@visitID))    

   )))) ELSE 0 END)) AS Units  

  -- chaged for calculating units for Modifiers with units

    from VisitLine    

    vl   

    inner join ModifierTransaction mt on vl.visitlineID=mt.visitlineID    

    where vl.VisitID=@visitID    

END 




GO
