SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_AddPatientToWaitingRoom]   
@PatientID INT,  
@PracticeID INT,  
@VenueID INT,  
@Time VARCHAR(10),  
--@Doctor VARCHAR(60), Removed Doctor Name 
@IsWalkIn BIT,  
@WaitStartTime VARCHAR(20) ,
@DoctorID INT 
AS  
BEGIN  
	--Get the account id based on patient id and practice id  
	DECLARE @accountID INT  
	SET @accountID=
	(
		SELECT TOP 1 AccountID   
		FROM Account   
		WHERE   
		PatientID=@PatientID AND PracticeID=@PracticeID
	)  
	
	IF @accountID IS NOT NULL  
	--IF account is already present then  insert into waiting room  
	BEGIN  
		-- If patient is already present in waiting room   
		DECLARE @waitingroomID INT  
		SET @waitingroomID=
		(
			SELECT WaitingRoomID   
			FROM WaitingRoom   
			WHERE AccountID=@accountID AND VenueID=@VenueID  
			AND  
			CONVERT(DATE,DateLogged)=CONVERT(DATE,GETDATE())
		)  
		
		IF @waitingroomID IS NULL  
			BEGIN  
				--DECLARE @LastPatientTime DECIMAL(36,2)  
				DECLARE @LastPatientTime INT  
				SELECT @LastPatientTime=MAX(CONVERT(INT, [Time]))  FROM WaitingRoom WHERE  Doctorid=@DoctorID AND VenueID=@VenueID AND CONVERT(DATE,DateLogged)=CONVERT(DATE,GETDATE())  
				INSERT INTO	WaitingRoom   
				(
				AccountID,
				VenueID,
				DateLogged,
				[Time],
				--Doctor Removed Doctor Name
				IsWalkIn,
				WaitStartTime,
				DoctorID
				)
				VALUES  
				(  
				 @accountID,  
				 @VenueID,  
				 GETDATE(),  
				 CASE WHEN @LastPatientTime IS NOT NULL THEN   
				 -- CONVERT(VARCHAR(10),@LastPatientTime + CONVERT(DECIMAL(36,2), @Time)) ELSE @Time END,  
				 CONVERT(VARCHAR(10),@LastPatientTime + CONVERT(INT, @Time)) ELSE @Time END,  
				 --@Doctor,  Removed Doctor Name
				 @IsWalkIn,  
				 @WaitStartTime,
				 @DoctorID 
				)  
				SELECT SCOPE_IDENTITY()  
			END  
		ELSE  
			BEGIN  
				SELECT @waitingroomID  
			END  
	END   
END  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
GO
