SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Mageswaran L
-- Create date: 01/02/2012
-- Description:	To fetch Charge code,Charge description and Price
-- =============================================
--[dbo].[MW_GetTariffCodeDetails] 1,'SOB','0190'
CREATE PROCEDURE [dbo].[MW_GetTariffCodeDetails] --1,'SOB','0190%'
(
   @PracticeID int,
   @ChargeType varchar(50),
   @chargeCode nvarchar(20)
)
AS
BEGIN

	--variable declaration --
	Declare @practitionerGroup_id int ,@PaymentArrangementDetail_ID int,@PractitionerType_ID int,@ChargeCode_ID varchar(10),@Price decimal(18,2)=0.00
    --Get and Set practitionerGroup_Id based on Logged in practice 
	set @practitionerGroup_id=(select distinct pt.practitionerGroup_id from Practice p 
							   inner join practicetype pt on p.PracticeTypeID=pt.PracticeTypeID 
							   where p.PracticeID=@PracticeID)
	--Get and Set PractitionerType_ID based on Logged in practice			
    set @PractitionerType_ID=(select distinct p.PracticeTypeID from Practice p where p.PracticeID=@PracticeID)
    --GET and SET ChargeCode_ID based on ChargeCode 
    set @ChargeCode_ID=(select Top 1 ChargeCodeID  from ChargeCode where CodeGroupID in(4,3) 
                        and CodeGroupdetail_ID=1  and TYPEID=@practitionerGroup_id and ChargeCode like @chargeCode+'%')
    --Get and Set @PaymentArrangementDetail_ID based on Patient Charge Type
    set @PaymentArrangementDetail_ID=(select  distinct PAD.PaymentArrangementDetail_ID from ChargeType CT 
									  inner join T_PaymentArrangement PA  on CT.ChargeType=PA.DetailCode 
                                      inner join T_PaymentArrangementDetail PAD 
                                      on PAD.PaymentArrangement_ID=PA.PaymentArrangement_ID
                                      where CT.ChargeType=@ChargeType)
    --check for PaymentArrangementDetail_ID is there for give charge Type
    if (@PaymentArrangementDetail_ID <>'' or @PaymentArrangementDetail_ID<>NULL)
		BEGIN
		    --Get and set the current year price based on user selected Chargecode.
			set @Price=(select distinct ValueInclRounded from T_ChargeCodeStraightCharge 
                        where GETDATE() between DateValidFrom and DateValidTo and 
                        PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID 
                        and ChargeCode_ID=@ChargeCode_ID)
            --Check  price available for the current year           
            if(@Price=0.00 or @Price=NULL)
				BEGIN
				    --Get and set the previous year price based on user selected Chargecode.
					set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge 
                            where PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID 
                            and ChargeCode_ID=@ChargeCode_ID order by DateValidTo asc ) 
                           
				END
			 ELSE
			        --Retrieving charge code,short description,long description,price
					Select Top 1 CC.Chargecode, left(CC.ShortDescription,50)as ShortDescription ,CC.LongDescription,@Price as Price,CG.CodeGroupDescr from ChargeCode cc 
					inner join CodeGroup CG on cc.CodeGroupID = CG.CodeGroupID where cc.CodeGroupID in(4,3) 
                    and CC.CodeGroupdetail_ID=1  and CC.TYPEID=@practitionerGroup_id and CC.ChargeCode like  @chargeCode +'%'
        
        END
    ELSE
        BEGIN
           --Get PaymentArrangementDetail_ID  for Universal charge Type
           set @PaymentArrangementDetail_ID=(select distinct PAD.PaymentArrangementDetail_ID from ChargeType CT 
									         inner join T_PaymentArrangement PA  on CT.ChargeType=PA.DetailCode 
                                             inner join T_PaymentArrangementDetail PAD 
                                             on PAD.PaymentArrangement_ID=PA.PaymentArrangement_ID
                                             where CT.ChargeType='Universal')
             --Get and set the current year price based on Universal Chargecode.
			set @Price=(select distinct ValueInclRounded from T_ChargeCodeStraightCharge 
                        where GETDATE() between DateValidFrom and DateValidTo and 
                        PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID 
                        and ChargeCode_ID=@ChargeCode_ID)
                        
            if(@Price=0.00 or @Price=NULL)
				BEGIN
					set @Price=(select Top 1 ValueInclRounded from T_ChargeCodeStraightCharge 
                            where PractitionerType_ID=@PractitionerType_ID and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID 
                            and ChargeCode_ID=@ChargeCode_ID order by DateValidTo asc) 
                             
				END
			 ELSE
			         --Retrieving charge code,short description,long description,price
					Select Top 1 CC.Chargecode, left(CC.ShortDescription,50)as ShortDescription ,CC.LongDescription,@Price as Price,CG.CodeGroupDescr from ChargeCode cc 
					inner join CodeGroup CG on cc.CodeGroupID = CG.CodeGroupID where cc.CodeGroupID in(4,3) 
                    and CC.CodeGroupdetail_ID=1  and CC.TYPEID=@practitionerGroup_id and CC.ChargeCode like @chargeCode + '%'
                     
       
        END
       
    
                               
   

END













GO
