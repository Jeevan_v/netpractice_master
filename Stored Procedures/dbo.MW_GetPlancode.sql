SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MW_GetPlancode]
	@planId int
AS
BEGIN
	SELECT MFOptionLinkCode, O.MFOptionName, O.MFOptionID, P.MedicalSchemeID, P.MedicalPlanID , MFC.CodeTypeDefinition
FROM dbo.MFOptionLinkCode MFO JOIN 

dbo.MFCodeType MFC 
ON MFC.MFCodeTypeID = MFO.MFCodeTypeID AND

(MFC.CodeTypeDefinition = 'Medikredit'  OR MFC.CodeTypeDefinition ='MediLink')
JOIN dbo.MFOptionLink OL on 
OL.MFOptionLinkID = MFO.MFOptionLinkID 
JOIN dbo.MedicalPlan P 
ON P.MedicalPlanID = OL.PlanID JOIN 
dbo.MFOption O on O.MFOptionID = OL.MFOptionID WHERE P.MedicalPlanID =@planId
END

GO
