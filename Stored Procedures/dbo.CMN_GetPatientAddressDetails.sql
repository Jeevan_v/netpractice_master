SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetPatientAddressDetails]  
	@patientid int  
AS  
BEGIN  
	SELECT  
		Pt.[PatientID],
		Pt.[FirstName] + ' ' + Pt.[LastName] AS [PatientName],
		ms.[SchemeName],
		Adrs.[AddressLine1],
		Adrs.[AddressLine2],
		Adrs.[SubUrb],
		Adrs.[City],
		Adrs.[Province],
		Adrs.[PostalCode]				
	FROM  
		[Patient] Pt 
		JOIN [MedicalScheme] ms ON Pt.SchemeID = ms.MedicalSchemeID	
		JOIN [address] Adrs ON Adrs.[AddressID] = Pt.[PhysicalAddressID]
	WHERE   
		Pt.[PatientID] =@patientid 
END
GO
