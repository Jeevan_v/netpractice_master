SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MW_GetScemeMedicalNoByPracticeID] 

@PatientID int
		
AS

BEGIN

SELECT 
	
	ms.SchemeName,
	
	p.MedicalAidNo,
	
	mp.PlanName
	
FROM

	Patient p 

JOIN

	MedicalScheme ms

ON 

	ms.MedicalSchemeID = p.SchemeID		
	
JOIN

	MedicalPlan mp

ON
	
	mp.MedicalPlanID = p.PlanID	

WHERE

	PatientID = @PatientID
		
END

GO
