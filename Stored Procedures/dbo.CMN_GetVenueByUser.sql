SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetVenueByUser]   
       
  @rolename nvarchar(256)= null,   
  @username nvarchar(256)=null  
   
AS  
BEGIN  
 declare @UserVId int  
 if(@rolename='Super Admin')  
 begin  
 select  convert(varchar,p.PracticeID) +'-'+ Convert(varchar,v.VenueID) as PracticeVenueID, p.PracticeName +' - '+UPPER(LEFT([VenueName],1))+LOWER(SUBSTRING([VenueName],2,LEN([VenueName])))  as VenuePracticeName,p.PracticeID,UserID=(Select u.UserID from aspnet_Users au inner join aspnet_Membership am  
           on au.UserId=am.UserId  
           inner join [User] u  
           on u.MembershipID=am.UserId  
            where au.UserName =@username) from Venue v inner join Practice p  
 on v.PracticeID=p.PracticeID  
 Where 
(p.ExtraInfo not in('Registered Practice' ,'Waiting Approval','Delete Permanently')
OR
p.ExtraInfo is null)
AND
v.IsActive=1
 ORDER BY VenuePracticeName  
 end  
 else  
 begin  
        Select @UserVId=u.UserID from aspnet_Users au inner join aspnet_Membership am  
           on au.UserId=am.UserId  
           inner join [User] u  
           on u.MembershipID=am.UserId  
             
          where au.UserName =@username  
            
          
          Select v.VenueID , p.PracticeID , p.PracticeName, UPPER(LEFT([VenueName],1))+LOWER(SUBSTRING([VenueName],2,LEN([VenueName]))) as VenueName ,UserID  from UserVenueRelationship uvr inner join Venue v  
            on uvr.VenueID=v.VenueID  
            inner join  Practice p  
             on p.PracticeID=v.PracticeID  
            where UserID= @UserVId 
            ORDER BY VenueName  
              
           -- Select v.VenueID,v.VenueID + p.PracticeID as VenuePracticeID,UPPER(LEFT([VenueName],1))+LOWER(SUBSTRING([VenueName],2,LEN([VenueName])))  as VenueName ,UPPER(LEFT([VenueName],1))+LOWER(SUBSTRING([VenueName],2,LEN([VenueName]))) +p.PracticeID as VenuePracticeName,
           -- p.PracticeName 
           -- from UserVenueRelationship uvr inner join Venue v  
           -- on uvr.VenueID=v.VenueID  
           -- inner join  Practice p  
           --  on p.PracticeID=v.PracticeID  
           -- where UserID= @UserVId and v.IsActive=0  
            -- ORDER BY VenueName  
          end  
END

GO
