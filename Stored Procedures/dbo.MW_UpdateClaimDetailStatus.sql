SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MW_UpdateClaimDetailStatus]
	@ClaimID int,
	@Status varchar(50)
AS
BEGIN
	Update ClaimDetail
	Set Status=@Status
	where ClaimID=@ClaimID
	
END


GO
