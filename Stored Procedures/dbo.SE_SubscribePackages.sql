SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[SE_SubscribePackages]  
 -- Add the parameters for the stored procedure here  
 @PackageName varchar(max),   
 @PracticeID int,  
 @checked bit  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 Declare @PackageId int 
 Set @PackageId =  (SELECT PackageID from Package where Package =@PackageName)   
 
 select practiceId from PracticePackageRelationship where PracticeID =@PracticeID and packageID = @PackageId and Subscribed = @checked 
      
 if exists(select practiceId from PracticePackageRelationship where PracticeID =@PracticeID and packageID = @PackageId)  
 Begin 
 
 update PracticePackageRelationship   
 set Subscribed = @checked, 
 Approved = case when @checked=0 then 0 else Approved end 
 Where  PracticeID =@PracticeID  
 and  packageID = @PackageId
End   
 else  
 Begin  
    INSERT INTO [PracticePackageRelationship]  
 (  
  PracticeId,  
  PackageId,  
  Subscribed  
 )  
 VALUES  
 (  
  @PracticeID,  
  @PackageId,  
  @checked  
 )  
 End  
END  
GO
