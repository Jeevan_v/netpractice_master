SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetAverageAppointmentTimeForDoctor] 
@DoctorID INT
AS
BEGIN
SELECT 
CASE 
WHEN 
SUM(DoctorsPastVisits.[Min])/COUNT(DoctorsPastVisits.VisitID)
IS NULL OR  SUM(DoctorsPastVisits.[Min])/COUNT(DoctorsPastVisits.VisitID) <= 0
THEN 
'30'
ELSE
CONVERT(VARCHAR(10),SUM(DoctorsPastVisits.[Min])/COUNT(DoctorsPastVisits.VisitID))
END
FROM Visit V 
INNER JOIN 
( 
SELECT 
VisitID,
--DATEDIFF(HH,CONVERT(TIME,MAX(Endtime)),CONVERT(TIME,MIN(Starttime))) AS [Hour],
DATEDIFF(MI,CONVERT(TIME,MAX(Starttime)),CONVERT(TIME,MIN(Endtime))) AS [Min] 
FROM VisitLine vl
WHERE vl.VisitID IN
(
SELECT 
TOP 10 VisitID 
FROM Visit 
WHERE DoctorID=@DoctorID
AND
CONVERT(DATE,DateLogged)<>CONVERT(DATE,GETDATE())
ORDER BY VisitID DESC
)
GROUP BY vl.VisitID
)AS DoctorsPastVisits ON V.VisitID = DoctorsPastVisits.VisitID
END
GO
