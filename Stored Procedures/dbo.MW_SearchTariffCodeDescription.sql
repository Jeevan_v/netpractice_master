SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_SearchTariffCodeDescription] --8,'SOB','%10%',1

(
   @PracticeID int,



   @ChargeType varchar(50),



   @SearchString nvarchar(20),



   @SearchFlag int



)



AS



BEGIN







	--variable declaration --



	Declare @practitionerGroup_id int ,@PaymentArrangementDetail_ID int,@PractitionerType_ID int,@ChargeCode_ID varchar(10),@Price numeric(18,2)=0.00



    --Get and Set practitionerGroup_Id based on Logged in practice 

	--Modified By and Date: Rajib 26/7/2013 

    --Comments: PracticeTypeID is send as a parameter instead of Practice ID

    --Start

	--set @practitionerGroup_id=(select distinct pt.practitionerGroup_id from Practice p 



	--						   inner join practicetype pt on p.PracticeTypeID=pt.PracticeTypeID 



	--						   where p.PracticeID=@PracticeID )



	set @practitionerGroup_id = (select distinct practitionerGroup_id from practicetype where PracticeTypeID=@PracticeID)



	--Get and Set PractitionerType_ID based on Logged in practice			



    --set @PractitionerType_ID=(select distinct p.PracticeTypeID from Practice p where p.PracticeID=@PracticeID)

	set @PractitionerType_ID=@PracticeID

	-- END

    if(@SearchFlag=1)



    BEGIN



        --Retrieve ChargeCode  based on ChargeCode 



         select Distinct ChargeCode,left(ShortDescription,50)as ShortDescription,LongDescription  from ChargeCode where CodeGroupID in(4,3) 



          and CodeGroupdetail_ID=1  and TYPEID=@practitionerGroup_id and ChargeCode like Lower(@SearchString)+'%'
		  --SMM : 21Jan2014 Hide the Deleted chargecodes
		  and ShortDescription not like 'Dele%'  order by ChargeCode asc



    END



    ELSE



    BEGIN



         --Retrieve ChargeCode  based on ChargeCode 



         select Distinct  ChargeCode,left(ShortDescription,50)as ShortDescription,LongDescription  from ChargeCode where CodeGroupID in(4,3) 



          and CodeGroupdetail_ID=1  and TYPEID=@practitionerGroup_id and Lower(LongDescription) like '%'+Lower(@SearchString)+'%'
		    --SMM : 21Jan2014 Hide the Deleted chargecodes
		  and ShortDescription not like 'Dele%'  order by ChargeCode asc



    END



    



 END
















GO
