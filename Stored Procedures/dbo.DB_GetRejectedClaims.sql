SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DB_GetRejectedClaims]-- 16,'Rejected',null



@PracticeID INT,



@ClaimStatus VARCHAR(100),



@UserID INT



AS



BEGIN

declare @RoleName VARCHAR(20)

set @RoleName=(SELECT DISTINCT ar.RoleName FROM  dbo.[User] u  JOIN dbo.aspnet_Users au  
ON u.MembershipID = au.UserId JOIN aspnet_UsersInRoles uir  
ON au.UserId = uir.UserId  JOIN aspnet_Roles ar ON uir.RoleId = ar.RoleId WHERE u.UserID= @UserID) 

if (@RoleName = 'Receptionist' or @RoleName = 'Receptionist Manager') 
	set @UserID = null


SELECT Top 5



     LOWER(pat.FirstName + ' ' + pat.LastName) AS PatientName,



	(ISNULL(cl.[TotalAmount],0)) AS AmountPaid,



	((ISNULL(cl.[TotalAmount],0))  - 



	((ISNULL(cl.AmountPaid,0))+ SUM(ISNULL(p.AmountPaid,0)))) 	



	AS [Balance],



--	(CASE WHEN cl.AmountPaid IS NULL THEN 0 ELSE cl.AmountPaid  END) +



	pat.[PatientID],



	v.[VisitID],



	[ClaimID],



	[PracticeID],



	LOWER([Status]) as [Status],



	[SchemeName],



	[AccountNumber],



	v.[VisitDate],



	v.[DoctorID] as [UserID]



FROM







Visit v 







INNER JOIN Claim cl ON v.VisitID=cl.VisitID



INNER JOIN Account ac ON v.AccountID=ac.AccountID



INNER JOIN Patient pat ON ac.PatientID=pat.PatientID



INNER JOIN MedicalScheme ms ON pat.SchemeID=ms.MedicalSchemeID



LEFT JOIN Payment P ON V.VisitID=P.VisitID



WHERE	



	PracticeID=@PracticeID	



AND 



	Status=@ClaimStatus



AND 



(CASE WHEN @UserID IS NOT NULL THEN v.DoctorID ELSE '1' END )



=(CASE WHEN @UserID IS NOT NULL THEN @UserID ELSE '1' END )



GROUP BY



pat.FirstName,pat.LastName,pat.[PatientID],



v.[VisitID],[ClaimID],[PracticeID],



[Status],[SchemeName],[AccountNumber],



v.[VisitDate],v.[DoctorID],cl.[TotalAmount],cl.AmountPaid



ORDER BY v.VisitID DESC	







END
GO
