SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetCountforProcessedFile] 
		@ClaimFileName varchar(Max) = NULL
AS
Begin
	
	/* Increment File counter */
	DECLARE @Cnt SMALLINT
 select @Cnt= Isnull(Counter,0) 
	from claimxml cx
	where   cx.XMLFileName = @ClaimFileName
return @Cnt

 END
GO
