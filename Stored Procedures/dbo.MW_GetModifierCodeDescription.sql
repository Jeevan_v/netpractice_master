SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================  
-- Author:  <Sameer>  
-- Create date: <17/05/2013>  
-- Description: <Get Modifier By Code or description>  
-- =============================================  
CREATE PROCEDURE [dbo].[MW_GetModifierCodeDescription]  --12, '0023', NULL
(  
@PracticeID INT,  
@ChargeCode NVARCHAR(20),  
@ShortDescription NVARCHAR(500)  
)  
AS  
BEGIN  
DECLARE @PractitionerGroupID INT  
-- Get Practitioner group id for the selected Practice
-- Modified by: Rajib on 7/30/2013
-- Commnets: Passing Pratice type id instead of practice id.  
--SELECT @PractitionerGroupID=PractitionerGroup_ID FROM PracticeType  
--       WHERE   
--       PracticeTypeID=(SELECT PracticeTypeID FROM Practice   
--                    WHERE PracticeID=@PracticeID)  
-- Get Modifier Code for search criteria  
  SELECT @PractitionerGroupID=PractitionerGroup_ID FROM PracticeType where PracticeTypeID = @PracticeID
-- End  
SELECT   
CC.ChargeCode,  
LEFT(CC.ShortDescription,50) AS ShortDescription,  
CC.LongDescription,  
ISNULL(MR.Percentage,0) AS Percentage,  
ISNULL(MR.PercentageBoundry,0) AS PercentageBoundry,  
ISNULL(MR.AfterBoundryExceed,0) AS AfterBoundryExceed,  
ISNULL(MR.Units,0) AS Units,  
ISNULL(MR.TimeIntervalStart,0) AS TimeIntervalStart,  
ISNULL(MR.TimeIntervalEnd,0) AS TimeIntervalEnd,  
ISNULL(MR.TimedUnitPrice,0) AS TimedUnitPrice,  
ISNULL(MR.TimedUnits,0) as TimedUnits,
ISNULL(MR.UnitPrice,0) AS UnitPrice,  
MT.ModifierType  
FROM ChargeCode CC  
INNER JOIN ModifierRules MR ON CC.ChargeCodeID=MR.ChargeCodeID  
INNER JOIN ModifierType MT ON MT.ModifierTypeID= MR.ModifierTypeID     
WHERE  
MR.InActive = 0  
AND  
-- Code Group ID is 5 for Modifiers   
CodeGroupID=5  
AND  
-- Select Modifier for PracticeGroup of the Practice   
-- TypeID in ChargeCode Table is PractitionerGroupID  
TypeID=@PractitionerGroupID  
AND  
CASE WHEN @ChargeCode IS NULL THEN '1' ELSE ChargeCode END like  
CASE WHEN @ChargeCode IS NULL THEN '1' ELSE @ChargeCode + '%' END  
AND  
CASE WHEN @ShortDescription IS NULL THEN '1' ELSE ShortDescription END like  
CASE WHEN @ShortDescription IS NULL THEN '1' ELSE '%' + @ShortDescription + '%' END   


select cc.ChargeCode , mc.TimedIntervalStart, mc.TimedIntervalEnd, mc.TimedFrequency, mc.TimedUnits, mc.TimedPrice, mc.ChargeCodeID
 from ModifierCondition MC with(nolock)
inner join ChargeCode cc with(nolock) on cc.chargecodeid = mc.chargecodeid  and ( cc.chargecode like  @ChargeCode + '%' or ShortDescription like  '%' + @ShortDescription + '%')



END 
 
GO
