SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ============================================================

-- Author:		SARANYA SRIRAMULU

-- Create date: 23-05-2013

-- Description:	TO INSERT AND UPDATE THE MEDICAL AID PAYMENTS

-- ============================================================



--exec [dbo].[UpdateMedicalAidPayments]  'LIN','0','MEDIHELP','','','EFT','2013-04-16','1418319','1093MA','268079523001','5584817','KHOZA','MM','','','RICKY','1','2013-03-18','0190','274.30','','243.30','','','','31.00','','900','MAX BENEFIT EXCEEDED (R31.00)'

--exec [dbo].[UpdateMedicalAidPayments] 'TRL','28','3150.85','','','','','','','','','','','','','','','','','','','','','','','','','',''

--exec [dbo].[UpdateMedicalAidPayments] 'LIN','1','SIZWE','','','','2013-07-08','1418319','1287P','','0425011856700','DIRE','','','','SOUNDY A I','','2013-06-29','140190','275.38','','275.38','','','','','0.00','',''

--exec [dbo].[UpdateMedicalAidPayments]  'LIN','0','MEDIHELP','','','EFT','2013-04-16','1418319','ACC1093','268079523001','5584817','KHOZA','MM','','','RICKY','1','2013-03-18','0190','274.30','','243.30','','','','31.00','','900','MAX BENEFIT EXCEEDED (R31.00)'





CREATE PROCEDURE [dbo].[UpdateMedicalAidPayments]  

@RecordType nvarchar(500), 

@BI nvarchar(50),  --for BISourceID / BatchLineNo / NumerOfEntries

@BI_Source_Code nvarchar(50), --for BICode / BI Source Code / TotalPaidToServiceProvider

@BatchNo_SchemeCode nvarchar(500), --for BatchNo / SchemeCode 

@BatchDate_AdminCode nvarchar(500), --for BatchDate / AdminCode 

@PaymentMethod nvarchar(500), --for RemittanceNo / PaymentMethod 

@PaymentDate nvarchar(500),

@BHFNo nvarchar(500),

@ServiceProviderRef nvarchar(500),

@SchemeRefNo nvarchar(500),

@MembershipNo nvarchar(500),

@MemberSurname nvarchar(500),

@MemberInitials nvarchar(50),

@PatientSurname nvarchar(500),

@PatientInitials nvarchar(50),

@PatientName nvarchar(500),

@DepNo nvarchar(500),

@ServiceDate nvarchar(500),

@ChargeCode nvarchar(500),

@ClaimAmount nvarchar(500),

@TariffAmount nvarchar(500),

@PaidToServiceProvider nvarchar(500),

@PaidToPatient nvarchar(500),

@PatientLevy nvarchar(500),

@ExceededBenefitAmount nvarchar(500),

@SurchargeAmounts nvarchar(500),

@RejectedAmount nvarchar(500),

@ReasonCode nvarchar(8),

@ReasonDescription nvarchar(300)



AS

BEGIN

	Declare @HeaderID int, @claimID int, @visitID int, @method varchar(15), @doctorID int, @count int,

	@claimDetailID int, @strClaim varchar(50), @isMed int, @subStrID int

	

	set @HeaderID = (select MAX(H.ID) from CSVHeader H)

	

	-- Added to check if the amount fileds are null or empty

	IF @ClaimAmount IS NULL OR @ClaimAmount='' OR @ClaimAmount=' '

		BEGIN

			SET @ClaimAmount = '0.00'

		END

	IF @TariffAmount IS NULL OR @TariffAmount='' OR @TariffAmount=' '

		BEGIN

			SET @TariffAmount = '0.00'

		END

	IF @PaidToServiceProvider IS NULL OR @PaidToServiceProvider='' OR @PaidToServiceProvider=' '

		BEGIN

			SET @PaidToServiceProvider = '0.00'

		END

	IF @PaidToPatient IS NULL OR @PaidToPatient='' OR @PaidToPatient=' '

		BEGIN

			SET @PaidToPatient = '0.00'

		END

	IF @PatientLevy IS NULL OR @PatientLevy='' OR @PatientLevy=' '

		BEGIN

			SET @PatientLevy = '0.00'

		END

	IF @ExceededBenefitAmount IS NULL OR @ExceededBenefitAmount='' OR @ExceededBenefitAmount=' '

		BEGIN

			SET @ExceededBenefitAmount = '0.00'

		END

	IF @SurchargeAmounts IS NULL OR @SurchargeAmounts='' OR @ExceededBenefitAmount=' '

		BEGIN

			SET @SurchargeAmounts = '0.00'

		END

	IF @RejectedAmount IS NULL OR @RejectedAmount='' OR @RejectedAmount=' '

		BEGIN

			SET @RejectedAmount = '0.00'

		END

	

	IF(@RecordType = 'HDR')

		BEGIN

			Insert into CSVHeader (RecordType,BISourceID,BICode,BatchNo,BatchDate,RemittanceNo) values 

			(@RecordType,@BI,@BI_Source_Code,@BatchNo_SchemeCode,@BatchDate_AdminCode,@PaymentMethod)

		END

	 ELSE IF(@RecordType = 'TRL')

		BEGIN

			Insert into CSVTail (CSVHeader_ID,RecordType,NumerOfEntries,TotalPaidToServiceProvider) values 

			(@HeaderID,@RecordType,Convert(int,@BI),Convert(decimal(36,2),@BI_Source_Code))		

		END

	ELSE IF(@RecordType = 'LIN')

		BEGIN

			print @RecordType

			Insert into CSVLines (CSVHeader_ID,RecordType,BatchLineNo,BI_Source_Code,SchemeCode,AdminCode,

			PaymentMethod,PaymentDate,BHFNo,ServiceProviderRef,SchemeRefNo,MembershipNo,MemberSurname,MemberInitials,

			PatientSurname,PatientInitials,PatientName,DepNo,ServiceDate,ChargeCode,ClaimAmount,TariffAmount,

			PaidToServiceProvider,PaidToPatient,PatientLevy,ExceededBenefitAmount,SurchargeAmounts,RejectedAmount,

			ReasonCode,ReasonDescription) values (@HeaderID,@RecordType,@BI,@BI_Source_Code,@BatchNo_SchemeCode,

			@BatchDate_AdminCode, @PaymentMethod,@PaymentDate,Convert(int,@BHFNo),@ServiceProviderRef,

			@SchemeRefNo,@MembershipNo,@MemberSurname,@MemberInitials,@PatientSurname,@PatientInitials,

			@PatientName,Convert(int,@DepNo),@ServiceDate,@ChargeCode,Convert(decimal(36,2),@ClaimAmount),

			Convert(decimal(36,2),@TariffAmount),Convert(decimal(36,2),@PaidToServiceProvider),

			Convert(decimal(36,2),@PaidToPatient),Convert(decimal(36,2),@PatientLevy),

			Convert(decimal(36,2),@ExceededBenefitAmount),Convert(decimal(36,2),@SurchargeAmounts),

			Convert(decimal(36,2),@RejectedAmount),@ReasonCode,@ReasonDescription)

			set @subStrID = scope_identity()

			

			IF(@PaymentMethod = 'CHQ')

				BEGIN

					set @method = 'Cheque'

				END

			ELSE

				BEGIN

					set @method = @PaymentMethod

				END

			

			IF(@PaidToServiceProvider <> '0.00')

			    -- Modified by: Rajib on 8/7/2013

			    -- Comments: Added code to handle ServiceProviderRef starts with 'ACC'

				BEGIN

					select @isMed = COUNT(*) from CSVLines c where c.CSVHeader_ID = @HeaderID and c.ID = @subStrID

					and ((c.ServiceProviderRef like 'ACC%'))

					IF(@isMed <> 0)

						BEGIN

							select @strClaim = c.ServiceProviderRef from CSVLines c where c.CSVHeader_ID = @HeaderID

							and c.ID = @subStrID

							SET @claimID = Substring(@strClaim,4,(len(@strClaim)-3))

						END

					ELSE

					-- End

						BEGIN					

							select @isMed = COUNT(*) from CSVLines c where c.CSVHeader_ID = @HeaderID and c.ID = @subStrID

							and ((c.ServiceProviderRef like '%MA') or (c.ServiceProviderRef like '%MC'))

							

							IF(@isMed <> 0)

								BEGIN

									select @strClaim = c.ServiceProviderRef from CSVLines c where c.CSVHeader_ID = @HeaderID

									and c.ID = @subStrID

									SET @claimID = Substring(@strClaim,1,(len(@strClaim)-2))

								END

							ELSE

								BEGIN

									select @strClaim = c.ServiceProviderRef from CSVLines c where c.CSVHeader_ID = @HeaderID

									and c.ID = @subStrID

									SET @claimID = Substring(@strClaim,1,(len(@strClaim)-1))

								END

						END

							/*select @strClaim = ServiceProviderRef from csvLines where ID=51 and 

							((ServiceProviderRef like '%P') or (ServiceProviderRef like '%B'))

							SET @claimID = Substring(@strClaim,1,(len(@strClaim)-1))*/

											

							select @visitID = c.VisitID from Claim c where c.ClaimID = @claimID

							select @count = count(c.VisitID) from Claim c where c.ClaimID = @claimID

							

							IF(@count <> 0)

								BEGIN

									select @doctorID = v.DoctorID from Visit v where v.VisitID = @visitID									

									INSERT INTO Payment (VisitID,AmountPaid,Method,Description,UserID,Date) 

									VALUES (@visitID,Convert(decimal(36,2),@PaidToServiceProvider),@method,'Medical Aid',

									@doctorID,@PaymentDate) 

									

									IF(LEN(@ReasonCode) <> 0)

									BEGIN

										SELECT @claimDetailID = ClaimDetailID FROM ClaimDetail WHERE ClaimID = @claimID and 

										NappiCode = @ChargeCode

										

										INSERT INTO ClaimError (ClaimDetailID,ErrorCode,ErrorDesc,ClaimID) VALUES

										(@claimDetailID,@ReasonCode,@ReasonDescription,@claimID)

									END

								END 

						 

				END	

		END

END
























GO
