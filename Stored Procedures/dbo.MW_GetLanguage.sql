SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MW_GetLanguage]
AS
SELECT

	LanguageID,
	[Language]

	FROM
	[Language]
	
	Order By Language asc
GO
