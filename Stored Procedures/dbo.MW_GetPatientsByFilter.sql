SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/********************************************************************
* Change By : Sachin Gandhi
* Change Date : 20th Nov 2014
* Description : Not allowing in-active patient except 'in-active surname' filter
* Change Date : 29th Jan 2015
* Description : Updated the logic for allowing option id for both category ( 5 and 6)
**********************************************************************/

CREATE PROCEDURE [dbo].[MW_GetPatientsByFilter]  
	@FilterType varchar(20)
	,@Key varchar(25)
	,@Initials varchar(20)
	,@PracticeID int
AS
IF @FilterType='Surname'
BEGIN
-- CHANGED FOR fM CHECK DATE 27/06/2013 
	SELECT  
    DISTINCT  
    p.[PatientID]  
   ,p.[IsSuspended]  
   ,[IDNo]     
   ,UPPER(LEFT([FirstName],1))+LOWER(SUBSTRING([FirstName],2,LEN([FirstName]))) as [FirstName]  
   ,UPPER(LEFT([LastName],1))+ LOWER(SUBSTRING([LastName],2,LEN([LastName]))) as [LastName]  
   ,[IsDependant]  
   ,DATEDIFF(MONTH,[FMCheckDate],GETDATE())+DATEDIFF(YEAR,[FMCheckDate],GETDATE()) as FMInterval  
   ,vf.FileNo  
   ,ad.AddressLine1     
   ,ad.AddressLine2  
   ,ad.SubUrb  
   ,ad.City        
   ,CASE WHEN 
   MFOLD.MFOPTIONLINKCODE IS NOT NULL 
   AND
   MFOLD.IsFMcheckEnabled = 1
   THEN 
   CONVERT(DATE,[FMCheckDate]) 
   ELSE
   NULL
   END   
   AS [FMCheckDate]
    -- For Fam check DAte in search grid                   
   FROM Patient p  
	   inner join  
	   [Address] ad  
	   ON ad.AddressID=p.PhysicalAddressID  
	   inner join  
	   [Account] ac  
	   ON ac.PatientID=p.PatientID  
	     inner join
	   [aspnet_Membership] aspm
	   on
	   p.MembershipID=aspm.UserId and aspm.IsApproved =1
	   inner join  
	   [VenueFile] vf  
	   on vf.AccountID=ac.AccountID 
	   LEFT JOIN
	   MFOPTIONLINK MFOL
	   ON p.planid= mfol.planid
	   AND mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures')))
	   LEFT JOIN  MFOPTIONLINKCODE MFOLD
	   ON MFOL.MFOPTIONLINKID = MFOLD.MFOPTIONLINKID
	   AND
	   MFOLD.Active = 1
	  
   WHERE  
   
   ac.PracticeID=@PracticeID   
   AND   
   UPPER(p.LastName) like @key + '%'     
   AND  
   UPPER(p.Initials) like @Initials + '%'    
   AND ( MFOL.MFOPTIONID IS NOT NULL and  mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))) OR ( 1=1) ) 
   -- CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN
   --MFOL.MFOPTIONID
   --ELSE
   --1
   --END
   --= -- TAKING MFOPTION ID FOR A PRACTICE
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN   
   --CASE 
   --WHEN   
   --((SELECT  PRACTICETYPEID  FROM PRACTICE
   --WHERE 
   --PRACTICEID=@PracticeID)
   --=
   --(SELECT TOP 1 PRACTICETYPEID 
   --FROM PRACTICETYPE 
   --WHERE UPPER(RTRIM(LTRIM(PRACTICETYPE)))
   --=UPPER('General Medical Practice')))
   --THEN
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures'))
   --ELSE
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))
   --END
   --ELSE
   --1
   --END 
     ORDER BY LastName 
-- CHANGED FOR fM CHECK DATE	27/06/2013 
END   
ELSE IF @FilterType='Full Name'
BEGIN
SELECT
       DISTINCT
	   p.[PatientID]
	  ,p.[IsSuspended]
	  ,[IDNo]
      ,UPPER(LEFT([FirstName],1))+LOWER(SUBSTRING([FirstName],2,LEN([FirstName]))) as [FirstName]
      ,UPPER(LEFT([LastName],1))+ LOWER(SUBSTRING([LastName],2,LEN([LastName]))) as [LastName]
      ,vf.FileNo
       ,DATEDIFF(MONTH,[FMCheckDate],GETDATE())+DATEDIFF(YEAR,[FMCheckDate],GETDATE()) as FMInterval
      ,[IsDependant]
      ,ad.AddressLine1
      ,ad.AddressLine2
      ,ad.SubUrb
      ,ad.City   
      ,CASE WHEN 
   MFOLD.MFOPTIONLINKCODE IS NOT NULL 
   AND
   MFOLD.IsFMcheckEnabled = 1
   THEN 
   CONVERT(DATE,[FMCheckDate]) 
   ELSE
   NULL
   END   
   AS [FMCheckDate] -- For Fam check DAte in search grid                 
   FROM Patient p
   inner join
   [Address] ad
   ON ad.AddressID=p.PhysicalAddressID
   inner join
   [Account] ac
   ON ac.PatientID=p.PatientID
   inner join
   [VenueFile] vf
   on vf.AccountID=ac.AccountID
    inner join
   [aspnet_Membership] aspm
   on
   p.MembershipID=aspm.UserId
 LEFT JOIN
   MFOPTIONLINK MFOL
   ON p.planid= mfol.planid
   AND mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures')))
   LEFT JOIN  MFOPTIONLINKCODE MFOLD
   ON MFOL.MFOPTIONLINKID = MFOLD.MFOPTIONLINKID
   AND
   MFOLD.Active = 1
   WHERE
	ac.PracticeID=@PracticeID 
   AND 
   UPPER(p.FirstName) like @key + '%'   
   AND
    UPPER(p.Initials) like @Initials + '%' 
    AND 
    aspm.IsApproved=1 
 AND
   CASE 
   WHEN  
   MFOL.MFOPTIONID IS NOT NULL THEN
   MFOL.MFOPTIONID
   ELSE
   1
   END
   = -- TAKING MFOPTION ID FOR A PRACTICE
   CASE 
   WHEN  
   MFOL.MFOPTIONID IS NOT NULL THEN   
   CASE 
   WHEN   
   ((SELECT  PRACTICETYPEID  FROM PRACTICE
   WHERE 
   PRACTICEID=@PracticeID)
   =
   (SELECT TOP 1 PRACTICETYPEID 
   FROM PRACTICETYPE 
   WHERE UPPER(RTRIM(LTRIM(PRACTICETYPE)))
   =UPPER('General Medical Practice')))
   THEN
   (SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures'))
   ELSE
   (SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))
   END
   ELSE
   1
   END  
     order by FirstName    	
END
ELSE IF @FilterType='Medical Aid No'
BEGIN
SELECT
       DISTINCT
	   p.[PatientID]
	  ,p.[IsSuspended]
	  ,[IDNo]
      ,UPPER(LEFT([FirstName],1))+LOWER(SUBSTRING([FirstName],2,LEN([FirstName]))) as [FirstName]
      ,UPPER(LEFT([LastName],1))+ LOWER(SUBSTRING([LastName],2,LEN([LastName]))) as [LastName]
      ,DATEDIFF(MONTH,[FMCheckDate],GETDATE())+DATEDIFF(YEAR,[FMCheckDate],GETDATE()) as FMInterval
      ,vf.FileNo
      ,[IsDependant]
      ,ad.AddressLine1
      ,ad.AddressLine2
      ,ad.SubUrb
      ,ad.City  
      ,CASE WHEN 
   MFOLD.MFOPTIONLINKCODE IS NOT NULL 
   AND
   MFOLD.IsFMcheckEnabled = 1
   THEN 
   CONVERT(DATE,[FMCheckDate]) 
   ELSE
   NULL
   END   
   AS [FMCheckDate] -- For Fam check DAte in search grid                 
   FROM Patient p
   inner join
   [Address] ad
   ON ad.AddressID=p.PhysicalAddressID
   inner join
   [Account] ac
   ON ac.PatientID=p.PatientID
   inner join
   [VenueFile] vf
   on vf.AccountID=ac.AccountID
   inner join
   [aspnet_Membership] aspm
   on
   p.MembershipID=aspm.UserId
 LEFT JOIN
   MFOPTIONLINK MFOL
   ON p.planid= mfol.planid
   AND mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures')))
   LEFT JOIN  MFOPTIONLINKCODE MFOLD
   ON MFOL.MFOPTIONLINKID = MFOLD.MFOPTIONLINKID
   AND
   MFOLD.Active = 1
   WHERE
	ac.PracticeID=@PracticeID 
   AND 
   UPPER(p.MedicalAidNo) like @key + '%'   
   AND
    UPPER(p.Initials) like @Initials + '%' 
    AND 
    aspm.IsApproved=1 
 AND
  ( MFOL.MFOPTIONID IS NOT NULL and  mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))) OR ( 1=1) ) 
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN
   --MFOL.MFOPTIONID
   --ELSE
   --1
   --END
   --= -- TAKING MFOPTION ID FOR A PRACTICE
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN   
   --CASE 
   --WHEN   
   --((SELECT  PRACTICETYPEID  FROM PRACTICE
   --WHERE 
   --PRACTICEID=@PracticeID)
   --=
   --(SELECT TOP 1 PRACTICETYPEID 
   --FROM PRACTICETYPE 
   --WHERE UPPER(RTRIM(LTRIM(PRACTICETYPE)))
   --=UPPER('General Medical Practice')))
   --THEN
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures'))
   --ELSE
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))
   --END
   --ELSE
   --1
   --END 
    
END
ELSE IF @FilterType='ID No'
BEGIN
SELECT
       DISTINCT
	   p.[PatientID]
	  ,p.[IsSuspended] 
	  ,[IDNo]
      ,UPPER(LEFT([FirstName],1))+LOWER(SUBSTRING([FirstName],2,LEN([FirstName]))) as [FirstName]
      ,UPPER(LEFT([LastName],1))+ LOWER(SUBSTRING([LastName],2,LEN([LastName]))) as [LastName]
      ,vf.FileNo
      ,DATEDIFF(MONTH,[FMCheckDate],GETDATE())+DATEDIFF(YEAR,[FMCheckDate],GETDATE()) as FMInterval
      ,[IsDependant]
      ,ad.AddressLine1
      ,ad.AddressLine2
      ,ad.SubUrb
      ,ad.City 
      ,CASE WHEN 
   MFOLD.MFOPTIONLINKCODE IS NOT NULL 
   AND
   MFOLD.IsFMcheckEnabled = 1
   THEN 
   CONVERT(DATE,[FMCheckDate]) 
   ELSE
   NULL
   END   
   AS [FMCheckDate] -- For Fam check DAte in search grid                 
   FROM Patient p
   inner join
   [Address] ad
   ON ad.AddressID=p.PhysicalAddressID
   inner join
   [Account] ac
   ON ac.PatientID=p.PatientID
   inner join
   [VenueFile] vf
   on vf.AccountID=ac.AccountID
    inner join
   [aspnet_Membership] aspm
   on
   p.MembershipID=aspm.UserId
 LEFT JOIN
   MFOPTIONLINK MFOL
   ON p.planid= mfol.planid
   AND mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures')))
   LEFT JOIN  MFOPTIONLINKCODE MFOLD
   ON MFOL.MFOPTIONLINKID = MFOLD.MFOPTIONLINKID
   AND
   MFOLD.Active = 1
   WHERE
	ac.PracticeID=@PracticeID 
   AND 
   UPPER(p.IDNo) like @key + '%'   
   AND
    UPPER(p.Initials) like @Initials + '%' 
    AND 
    aspm.IsApproved=1 
 AND  ( MFOL.MFOPTIONID IS NOT NULL and  mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))) OR ( 1=1) ) 
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN
   --MFOL.MFOPTIONID
   --ELSE
   --1
   --END
   --= -- TAKING MFOPTION ID FOR A PRACTICE
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN   
   --CASE 
   --WHEN   
   --((SELECT  PRACTICETYPEID  FROM PRACTICE
   --WHERE 
   --PRACTICEID=@PracticeID)
   --=
   --(SELECT TOP 1 PRACTICETYPEID 
   --FROM PRACTICETYPE 
   --WHERE UPPER(RTRIM(LTRIM(PRACTICETYPE)))
   --=UPPER('General Medical Practice')))
   --THEN
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures'))
   --ELSE
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))
   --END
   --ELSE
   --1
   --END  
    order by IDNo   	
END
ELSE IF @FilterType='File No'
BEGIN
SELECT
       DISTINCT
	   p.[PatientID]
	  ,p.[IsSuspended] 
	  ,[IDNo]
      ,UPPER(LEFT([FirstName],1))+LOWER(SUBSTRING([FirstName],2,LEN([FirstName]))) as [FirstName]
      ,UPPER(LEFT([LastName],1))+ LOWER(SUBSTRING([LastName],2,LEN([LastName]))) as [LastName]
      ,vf.FileNo
      ,DATEDIFF(MONTH,[FMCheckDate],GETDATE())+DATEDIFF(YEAR,[FMCheckDate],GETDATE()) as FMInterval
      ,[IsDependant]
      ,ad.AddressLine1
      ,ad.AddressLine2
      ,ad.SubUrb
      ,ad.City    
      ,CASE WHEN 
   MFOLD.MFOPTIONLINKCODE IS NOT NULL 
   AND
   MFOLD.IsFMcheckEnabled = 1
   THEN 
   CONVERT(DATE,[FMCheckDate]) 
   ELSE
   NULL
   END   
   AS [FMCheckDate] -- For Fam check DAte in search grid                 
   FROM Patient p
   inner join
   Account ac
   ON p.PatientID=ac.PatientID
   inner join
   VenueFile vf
   ON ac.AccountID=vf.AccountID
   inner join 
   [Address] ad
   ON ad.AddressID=p.PhysicalAddressID   
  inner join
   [aspnet_Membership] aspm
   on
   p.MembershipID=aspm.UserId
 LEFT JOIN
   MFOPTIONLINK MFOL
   ON p.planid= mfol.planid
   AND mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures')))
   LEFT JOIN  MFOPTIONLINKCODE MFOLD
   ON MFOL.MFOPTIONLINKID = MFOLD.MFOPTIONLINKID
   AND
   MFOLD.Active = 1
   WHERE
	ac.PracticeID=@PracticeID 
   AND 
   UPPER(vf.FileNo) like @key + '%'   
   AND
    UPPER(p.Initials) like @Initials + '%' 
    AND 
    aspm.IsApproved=1 
 AND  ( MFOL.MFOPTIONID IS NOT NULL and  mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))) OR ( 1=1) ) 
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN
   --MFOL.MFOPTIONID
   --ELSE
   --1
   --END
   --= -- TAKING MFOPTION ID FOR A PRACTICE
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN   
   --CASE 
   --WHEN   
   --((SELECT  PRACTICETYPEID  FROM PRACTICE
   --WHERE 
   --PRACTICEID=@PracticeID)
   --=
   --(SELECT TOP 1 PRACTICETYPEID 
   --FROM PRACTICETYPE 
   --WHERE UPPER(RTRIM(LTRIM(PRACTICETYPE)))
   --=UPPER('General Medical Practice')))
   --THEN
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures'))
   --ELSE
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))
   --END
   --ELSE
   --1
   --END  
    order by vf.FileNo   	
END
ELSE IF @FilterType='PatientID'
BEGIN
SELECT
       DISTINCT
	   p.[PatientID]
	  ,p.[IsSuspended] 
	  ,[IDNo]
      ,UPPER(LEFT([FirstName],1))+LOWER(SUBSTRING([FirstName],2,LEN([FirstName]))) as [FirstName]
      ,UPPER(LEFT([LastName],1))+ LOWER(SUBSTRING([LastName],2,LEN([LastName]))) as [LastName]
      ,vf.FileNo
      ,DATEDIFF(MONTH,[FMCheckDate],GETDATE())+DATEDIFF(YEAR,[FMCheckDate],GETDATE()) as FMInterval
      ,[IsDependant]
      ,ad.AddressLine1
      ,ad.AddressLine2
      ,ad.SubUrb
      ,ad.City     
      ,CASE WHEN 
   MFOLD.MFOPTIONLINKCODE IS NOT NULL 
   AND
   MFOLD.IsFMcheckEnabled = 1
   THEN 
   CONVERT(DATE,[FMCheckDate]) 
   ELSE
   NULL
   END   
   AS [FMCheckDate] -- For Fam check DAte in search grid                 
   FROM Patient p
   inner join
   Account ac
   ON p.PatientID=ac.PatientID
   inner join
   VenueFile vf
   ON ac.AccountID=vf.AccountID
   inner join 
   [Address] ad
   ON ad.AddressID=p.PhysicalAddressID   
  inner join
   [aspnet_Membership] aspm
   on
   p.MembershipID=aspm.UserId
 LEFT JOIN
   MFOPTIONLINK MFOL
   ON p.planid= mfol.planid
   AND mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures')))
   LEFT JOIN  MFOPTIONLINKCODE MFOLD
   ON MFOL.MFOPTIONLINKID = MFOLD.MFOPTIONLINKID
   AND
   MFOLD.Active = 1
   WHERE
	ac.PracticeID=@PracticeID 
   AND 
   p.PatientID=@key   
    AND 
    aspm.IsApproved=1 
 AND

  ( MFOL.MFOPTIONID IS NOT NULL and  mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))) OR ( 1=1) ) 
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN
   --MFOL.MFOPTIONID
   --ELSE
   --1
   --END
   --= -- TAKING MFOPTION ID FOR A PRACTICE
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN   
   --CASE 
   --WHEN   
   --((SELECT  PRACTICETYPEID  FROM PRACTICE
   --WHERE 
   --PRACTICEID=@PracticeID)
   --=
   --(SELECT TOP 1 PRACTICETYPEID 
   --FROM PRACTICETYPE 
   --WHERE UPPER(RTRIM(LTRIM(PRACTICETYPE)))
   --=UPPER('General Medical Practice')))
   --THEN
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures'))
   --ELSE
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))
   --END
   --ELSE
   --1
   --END   
   	
END

ELSE IF @FilterType='Inactive-Surname'
BEGIN
SELECT
       DISTINCT
	   p.[PatientID]
	  ,p.[IsSuspended] 
	  ,[IDNo]
      ,UPPER(LEFT([FirstName],1))+LOWER(SUBSTRING([FirstName],2,LEN([FirstName]))) as [FirstName]
      ,UPPER(LEFT([LastName],1))+ LOWER(SUBSTRING([LastName],2,LEN([LastName]))) as [LastName]
      ,vf.FileNo
      ,DATEDIFF(MONTH,[FMCheckDate],GETDATE())+DATEDIFF(YEAR,[FMCheckDate],GETDATE()) as FMInterval
      ,[IsDependant]
      ,ad.AddressLine1
      ,ad.AddressLine2
      ,ad.SubUrb
      ,ad.City    
      ,CASE WHEN 
   MFOLD.MFOPTIONLINKCODE IS NOT NULL 
   AND
   MFOLD.IsFMcheckEnabled = 1
   THEN 
   CONVERT(DATE,[FMCheckDate]) 
   ELSE
   NULL
   END   
   AS [FMCheckDate] -- For Fam check DAte in search grid                 
   FROM Patient p
   inner join
   Account ac
   ON p.PatientID=ac.PatientID
   inner join
   VenueFile vf
   ON ac.AccountID=vf.AccountID
   inner join 
   [Address] ad
   ON ad.AddressID=p.PhysicalAddressID   
  inner join
   [aspnet_Membership] aspm
   on
   p.MembershipID=aspm.UserId
 LEFT JOIN
   MFOPTIONLINK MFOL
   ON p.planid= mfol.planid
   AND mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
   WHERE
   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures')))
   LEFT JOIN  MFOPTIONLINKCODE MFOLD
   ON MFOL.MFOPTIONLINKID = MFOLD.MFOPTIONLINKID
   AND
   MFOLD.Active = 1
   WHERE
	ac.PracticeID=@PracticeID 
   AND 
   UPPER(p.LastName) like @key + '%'   
   AND
    UPPER(p.Initials) like @Initials + '%' 
    AND 
    aspm.IsApproved=0 
 AND
  ( MFOL.MFOPTIONID IS NOT NULL and  mfol.mfoptionid IN ((SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures')),(SELECT MFOPTIONID  FROM MFOPTION
	   WHERE
	   UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))) OR ( 1=1) ) 
   -- CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN
   --MFOL.MFOPTIONID
   --ELSE
   --1
   --END
   --= -- TAKING MFOPTION ID FOR A PRACTICE
   --CASE 
   --WHEN  
   --MFOL.MFOPTIONID IS NOT NULL THEN   
   --CASE 
   --WHEN   
   --((SELECT  PRACTICETYPEID  FROM PRACTICE
   --WHERE 
   --PRACTICEID=@PracticeID)
   --=
   --(SELECT TOP 1 PRACTICETYPEID 
   --FROM PRACTICETYPE 
   --WHERE UPPER(RTRIM(LTRIM(PRACTICETYPE)))
   --=UPPER('General Medical Practice')))
   --THEN
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('GP Consults and Procedures'))
   --ELSE
   --(SELECT MFOPTIONID  FROM MFOPTION
   --WHERE
   --UPPER(RTRIM(LTRIM(MFOPTIONNAME)))=UPPER('Specialist Consults and Procedures'))
   --END
   --ELSE
   --1
   --END   
   ORDER BY LastName
END
GO
