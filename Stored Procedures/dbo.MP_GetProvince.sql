SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [dbo].[MP_GetProvince]
	
AS
BEGIN
	SELECT	Province,
			ProvinceID
	FROM	dbo.Province
	
END

GO
