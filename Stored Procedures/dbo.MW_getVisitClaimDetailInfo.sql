SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_getVisitClaimDetailInfo]  
 @ClaimID int  
AS  
BEGIN  
 Select cd.ClaimDetailID,vl.LineDefinition,vl.IsChronic from  VisitLine vl inner join ClaimDetail cd  on vl.VisitLineID=cd.VisitLineID  
 where cd.ClaimID=@ClaimID  
END 
GO
