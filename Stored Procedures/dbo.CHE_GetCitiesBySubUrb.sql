SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Sameer>
-- Create date: <4/12/2012>
-- Description:	<Get Citites by SubUrb>
-- =============================================
CREATE PROCEDURE [dbo].[CHE_GetCitiesBySubUrb] 
AS
BEGIN
SELECT 
s.SubUrb,cty.CityID,cty.City
FROM 
City cty
INNER JOIN 
SubUrbCityPostalCodeRelationship scr
ON
cty.CityID=scr.CityID
INNER JOIN SubUrb s
ON
s.SubUrbID=scr.SubUrbID

END

GO
