SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sameer>
-- Create date: <18/12/2012>
-- Description:	<Insert Account Details >
-- =============================================
CREATE PROCEDURE [dbo].[MW_SaveAccount]
       @AccountNo VARCHAR(30)
      ,@PracticeID int
      ,@PatientID int
AS
BEGIN
	DECLARE @AccountID INT
	SELECT 
	@AccountID=AccountID
	FROM
	Account
	WHERE
	PracticeID=@PracticeID
	AND
	PatientID=@PatientID
	
	IF @AccountID IS NULL
	BEGIN
	INSERT INTO Account
	(
	   AccountNumber
      ,PracticeID
      ,PatientID
	)
	VALUES
	(
	   @AccountNo
      ,@PracticeID 
      ,@PatientID
	)	
	SET @AccountID=(SELECT SCOPE_IDENTITY())
	END
	
	SELECT @AccountID
END


GO
