SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetPracticeType]
AS

SELECT
	PracticeTypeID,
	PracticeType
	
	
FROM
	PracticeType
	--Excluded as right now we are not supporting in netpractice
	--Date : 9/9/2015
	--Sachin Gandhi
	where Practicetype not in ('Dental Therapy','Dental Technology','Optometry','Community Dentistry','General Dental Practice')
	
	ORDER BY PracticeType

GO
