SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetVenueByUserId] 
      @UserId int
AS
BEGIN

SELECT

	vne.VenueID,
	vne.VenueName,
	vne.PhysicalAddressID,
	vne.PostalAddressID,
	
	vne.Telephone,
	vne.Fax,
	vne.IsActive
	
FROM

dbo.Venue vne

Inner Join 

dbo.UserVenueRelationship uvr

ON

vne.VenueID= uvr.VenueID

WHERE

uvr.UserID=@UserId 

END	

GO
