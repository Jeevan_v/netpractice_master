SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[MW_GetPracticesForPatient]  
@Username varchar(50)
As
Select p.PracticeName,p.PracticeID from Practice p
Inner Join
Account A
On 
A.PracticeID=P.PracticeID
Inner Join 
Patient Pat
On pat.PatientID=A.PatientID
where
pat.PatientID=(select u.PatientID from aspnet_Users au inner join aspnet_Membership am  
           on au.UserId=am.UserId  
           inner join [Patient] u  
           on u.MembershipID=am.UserId  
            where au.UserName =@Username)
 
GO
