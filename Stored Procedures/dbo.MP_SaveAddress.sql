SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_SaveAddress]
	@AddressID int,
	@AddressLine1 varchar(200),
	@AddressLine2 varchar(200),
	@ProvinceID int,
	@SubUrb varchar(200),
	@City varchar(200),
	@PostalCode varchar(8)
AS

IF @AddressID IS NULL
BEGIN
IF @ProvinceID <>0
BEGiN 
INSERT INTO Address 
(
    AddressLine1,
	AddressLine2,
	ProvinceID,
	City,
	SubUrb,
	PostalCode
	
)
VALUES
(
    @AddressLine1,
	@AddressLine2,
	@ProvinceID,
	@City,
    @SubUrb,
	@PostalCode
)
END 
ELSE 
BEGIN 
INSERT INTO Address 
(
    AddressLine1,
	AddressLine2,
	City,
	SubUrb,
	PostalCode
	
)
VALUES
(
    @AddressLine1,
	@AddressLine2,
	@City,
    @SubUrb,
	@PostalCode
)
END 
SELECT SCOPE_IDENTITY()

END

ELSE
BEGIN
IF @ProvinceID <>0
BEGIN
UPDATE Address 
SET
	 AddressLine1=@AddressLine1,
	 AddressLine2=@AddressLine2,
	 ProvinceID=@ProvinceID,
	 SubUrb=@SubUrb,
	 City=@City,
	 PostalCode=@PostalCode
WHERE
	AddressID = @AddressID
	END 
	ELSE 
	BEGIN
	UPDATE Address 
		SET
	 AddressLine1=@AddressLine1,
	 AddressLine2=@AddressLine2,
	 
	 SubUrb=@SubUrb,
	 City=@City,
	 PostalCode=@PostalCode
WHERE
	AddressID = @AddressID
	END

SELECT @AddressID

END      
GO
