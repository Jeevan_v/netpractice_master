SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetMedicalPlanBySchemeId] 
      @MedicalSchemeID int
AS
BEGIN

SELECT

	MedicalPlanID,
	PlanName
	
	
FROM

dbo.MedicalPlan 

where

MedicalSchemeID=@MedicalSchemeID

END	

GO
