SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetCurrentOverDueBalance]
@PatientID INT , @PracticeID INT,  @FromDate NVARCHAR(30), @ToDate NVARCHAR(30)

AS
BEGIN
SELECT SUM(ISNULL(BBB.OVERDUE,0)) AS [OVERDUE], SUM(ISNULL(BBB.CurrentBalance,0)) AS [CurrentBalance]    FROM
(
SELECT AA.InvoiceNo,AA.[DueDate], (c.[TotalAmount] - SUM (ISNULL(AA.[OVERDUE],0))) AS [OVERDUE], (c.[TotalAmount] - SUM (ISNULL(AA.[CurrentBalance],0))) AS [CurrentBalance]  FROM
(
SELECT 
	v.[InvoiceNo],
	v.[VisitID],
	DATEADD(DAY, 
				(
				SELECT 
				CASE [PaymentDuration]
					WHEN	'30 Days' THEN 30
					WHEN	'60 Days' THEN 60
					WHEN	'90 Days' THEN 90
				END 
			FROM 
				[PaymentDuration] 
			WHERE 
				[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]) AS [DueDate],
	CASE 
		WHEN (DATEADD(DAY, 
				(
				SELECT 
				CASE [PaymentDuration]
					WHEN	'30 Days' THEN 30
					WHEN	'60 Days' THEN 60
					WHEN	'90 Days' THEN 90
				END 
			FROM 
				[PaymentDuration] 
			WHERE 
				[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]) < 	Pa.[Date])
			THEN CONVERT(Decimal(30,2),ROUND(SUM(ISNULL(Pa.[AmountPaid],0)),2)) 		
		END AS [OVERDUE],
		CASE 
			WHEN (DATEADD(DAY, 
						(
						SELECT 
						CASE [PaymentDuration]
							WHEN	'30 Days' THEN 30
							WHEN	'60 Days' THEN 60
							WHEN	'90 Days' THEN 90
						END 
					FROM 
						[PaymentDuration] 
					WHERE 
						[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]) > 	Pa.[Date])
				THEN CONVERT(Decimal(30,2),ROUND(SUM(ISNULL(Pa.[AmountPaid],0)),2)) 
				
			END AS [CurrentBalance]
		
FROM
		Visit v  		
		JOIN  Account a ON  a.AccountID = v.AccountID
		JOIN Payment Pa ON Pa.VisitID = v.[VisitID]
WHERE
		v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID ) 
		 AND   CONVERT(DATE,v.VisitDate) <= CONVERT(DATE,@ToDate,103)
GROUP BY v.[InvoiceNo],v.[VisitDate],Pa.[Date],v.[VisitID]
) AA
JOIN Claim c  ON c.VisitID = AA.VisitID  
GROUP BY AA.InvoiceNo,AA.[DueDate],c.[TotalAmount]

UNION ALL

SELECT [InvoiceNo],[DueDate],ISNULL([OVERDUE],0),ISNULL([CurrentBalance],0) FROM
(
SELECT 
	v.[InvoiceNo],
	DATEADD(DAY, 
				(
				SELECT 
				CASE [PaymentDuration]
					WHEN	'30 Days' THEN 30
					WHEN	'60 Days' THEN 60
					WHEN	'90 Days' THEN 90
				END 
			FROM 
				[PaymentDuration] 
			WHERE 
				[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]) AS [DueDate],
	CASE 
	WHEN ((SELECT COUNT([Date]) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) = 0) AND (DATEADD(DAY, 
				(
				SELECT 
				CASE [PaymentDuration]
					WHEN	'30 Days' THEN 30
					WHEN	'60 Days' THEN 60
					WHEN	'90 Days' THEN 90
				END 
			FROM 
				[PaymentDuration] 
			WHERE 
				[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]) < 	CONVERT(DATE,@ToDate,103))
			THEN ISNULL(c.[TotalAmount],0)
			END AS [OVERDUE],
			CASE
			WHEN ((SELECT COUNT([Date]) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) = 0) AND (DATEADD(DAY, 
				(
				SELECT 
				CASE [PaymentDuration]
					WHEN	'30 Days' THEN 30
					WHEN	'60 Days' THEN 60
					WHEN	'90 Days' THEN 90
				END 
			FROM 
				[PaymentDuration] 
			WHERE 
				[PaymentDurationID] = (SELECT [PaymentDurationID] FROM [Practice] WHERE [PracticeID] = @PracticeID)),v.[VisitDate]) > 	CONVERT(DATE,@ToDate,103))
			THEN ISNULL(c.[TotalAmount],0)
			END AS [CurrentBalance]
			
	FROM
		Visit v  
		JOIN  Claim c  ON c.VisitID = v.VisitID  
		JOIN  Account a ON  a.AccountID = v.AccountID
	WHERE   
		 v.AccountID IN  ( SELECT [AccountID] FROM Account WHERE [PracticeID] = @PracticeID AND [PatientID] = @PatientID ) 
		 AND   CONVERT(DATE,v.VisitDate) <= CONVERT(DATE,@ToDate,103)
	) AA 
	
	) BBB
END
GO
