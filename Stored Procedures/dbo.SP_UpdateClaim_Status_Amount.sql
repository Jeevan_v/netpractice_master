SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 20/10/2015
-- Description:	Update claim status and amount for discovery delayed response related claims
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateClaim_Status_Amount] 
	-- Add the parameters for the stored procedure here
	@ClaimID int, 
	@ClaimStatus nvarchar(50),
	@ErrorCode	nvarchar(50),
	@ErrorDesc	nvarchar(500),
	@AmountPaid	decimal(18,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
  UPDATE Claim
  set [Status] = @ClaimStatus, [StatusReason] = @ErrorDesc, AmountPaid = @AmountPaid 
  WHERE ClaimID = @ClaimID



  UPDATE ClaimError 
  SET ErrorCode =@ErrorCode , ErrorDesc = @ErrorDesc
  WHERE ClaimID = @ClaimID
END

GO
