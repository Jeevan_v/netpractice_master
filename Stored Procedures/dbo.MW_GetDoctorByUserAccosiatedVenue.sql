SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE  [dbo].[MW_GetDoctorByUserAccosiatedVenue] 
@UserID int
--@VenueID TVP_Venues RE
--
AS
BEGIN
--INSERT INTO @VenueID (VenueID)
--select VenueID from UserVenueRelationship where UserID=@UserID
SELECT DISTINCT
	u.UserID,
	u.FirstName+' '+u.LastName as DoctorName	
FROM
dbo.[User] u

JOIN dbo.aspnet_Users au
ON u.MembershipID = au.UserId

JOIN aspnet_Membership ams
ON au.UserId = ams.UserId

JOIN dbo.UserVenueRelationship uvr
ON uvr.UserID = u.UserID

JOIN dbo.Venue v
ON uvr.VenueID = v.VenueID

JOIN dbo.Practice p
ON p.PracticeID = v.PracticeID

JOIN aspnet_UsersInRoles uir
ON au.UserId = uir.UserId

JOIN aspnet_Roles ar
ON uir.RoleId = ar.RoleId

WHERE
	v.VenueID in (select VenueID from UserVenueRelationship where UserID=@UserID)
	AND
	--ar.RoleName in ('Locum Doctor','Practitioner','Practitioner Admin')
	ar.RoleName in ('Practitioner','Practitioner Admin','Locum Doctor')
	AND
	ams.IsApproved = 1
ORDER BY DoctorName
END

GO
