SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SP_CreateUser] 
@UserId nvarchar(max),
@RoleId nvarchar(max),
@UserName varchar(200),
@Password nvarchar(max),
@PasswordSalt nvarchar(max),
@Email nvarchar(200),
@RoleName varchar(200)
AS
BEGIN	
	SET NOCOUNT ON;
	insert into aspnet_Users
	(ApplicationId,UserId,UserName,LoweredUserName,IsAnonymous,LastActivityDate)
	values
	('89E91D34-14ED-4A21-AD0B-DABE160F20C9',convert(uniqueidentifier,@UserId),@UserName,@UserName,0,GETDATE())

	insert into aspnet_Membership
	(ApplicationId,UserId,Password,PasswordFormat,PasswordSalt,Email,IsApproved,IsLockedOut,CreateDate,LastLoginDate,LastPasswordChangedDate,LastLockoutDate,FailedPasswordAttemptCount,FailedPasswordAttemptWindowStart,FailedPasswordAnswerAttemptCount,FailedPasswordAnswerAttemptWindowStart)
	values
	('89E91D34-14ED-4A21-AD0B-DABE160F20C9',convert(uniqueidentifier,@UserId),@Password,1,@PasswordSalt,@Email,1,0,GETDATE(),GETDATE(),GETDATE(),GETDATE(),0,GETDATE(),0,GETDATE())
	
	insert into aspnet_Roles
	(ApplicationId,RoleId,RoleName,LoweredRoleName)
	values('89E91D34-14ED-4A21-AD0B-DABE160F20C9',convert(uniqueidentifier,@RoleId),@RoleName,@RoleName)

	insert into aspnet_UsersInRoles
	(UserId,RoleId)
	values
	(convert(uniqueidentifier,@UserId),convert(uniqueidentifier,@RoleId))

	 
END

GO
