SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetPrivilege] 

      

AS

BEGIN



SELECT



	Privilege,

	PrivilegeID

	

FROM



dbo.Privilege

where

Privilege Not IN('Assign privilege',
'View practice details-Super Admin',
'View Patient Profile',
'View Patient Details',
'Modify entire practice details')

	ORDER BY Privilege

END

----------------------------------------

GO
