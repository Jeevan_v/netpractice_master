SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetAmountSumStandardInvoice]-- 907  
@InvoiceNo int  
AS  
BEGIN  
  
SELECT     
	 SUM(CAST(vl.Amount AS Decimal(36,2)))As TotalZAR,     
	 --ISNULL(c.AmountPaid,0) + (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) AS AmountPaid,    
	 (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) AS AmountPaid,    
	 --(SUM(CAST(vl.Amount AS Decimal(36,2))) - ISNULL(c.AmountPaid,0) - (SELECT SUM(CAST(WriteOffAmount AS Decimal(36,2))) FROM WS_WriteOff AS AP2 WHERE AP2.VisitID = vl.VisitID)) as AmountDue,
	 --(SUM(CAST(vl.Amount AS Decimal(36,2))) - ISNULL(c.AmountPaid,0) - (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID)) as AmountDue,
	 (SUM(CAST(vl.Amount AS Decimal(36,2))) - (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID)) as AmountDue,
	 (SELECT SUM(CAST(WriteOffAmount AS Decimal(36,2))) FROM WS_WriteOff AS AP2 WHERE AP2.VisitID = vl.VisitID)   AS WriteOffAmount  
FROM   
		VisitLine vl   
		JOIN   Visit V   ON   v.VisitID = vl.VisitID     
		JOIN  Claim c  ON  c.VisitID = v.VisitID  			
WHERE  
		V.InvoiceNo = @InvoiceNo  
		GROUP BY  c.AmountPaid,V.VisitID,vl.VisitID
END  
  
GO
