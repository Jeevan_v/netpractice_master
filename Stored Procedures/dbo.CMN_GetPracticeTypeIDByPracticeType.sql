SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sameer>
-- Create date: <18/01/2013>
-- Description:	<Get Practice Type by PracticeTypeID>
-- =============================================
CREATE PROCEDURE [dbo].[CMN_GetPracticeTypeIDByPracticeType]
	@PracticeType varchar(50)
AS
BEGIN
	SELECT
	PracticeTypeID
	FROM
	PracticeType
	WHERE
	PracticeType=@PracticeType
END

GO
