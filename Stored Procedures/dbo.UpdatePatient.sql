SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 CREATE proc [dbo].[UpdatePatient]  @patientID int,@membershipID uniqueidentifier
  As
  Update [dbo].[Patient]
  Set [MembershipID]= @membershipID
  where PatientID =@patientID
GO
