SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[MW_GetVenueIdByInvoiceNumber]
@invoiceno INT , @VisitID INT

AS
BEGIN
IF @invoiceno !=  0 
	SELECT [VenueID] FROM Venue WHERE VenueID = (SELECT VenueID FROM Visit WHERE InvoiceNo=@invoiceno);
ELSE IF @VisitID != 0
	SELECT [VenueID] FROM Venue WHERE VenueID = (SELECT VenueID FROM Visit WHERE VisitID=@VisitID);
END
GO
