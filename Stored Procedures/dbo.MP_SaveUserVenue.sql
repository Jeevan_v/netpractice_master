SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [dbo].[MP_SaveUserVenue](
	@UserID int,
	@UserVenue TVP_Venues READONLY)
AS
BEGIN
	DELETE FROM UserVenueRelationship
	WHERE UserID =@UserID;
					  
	INSERT INTO UserVenueRelationship (UserID, 
											VenueID)
	SELECT @UserID , VenueID
	FROM @UserVenue;						  
					  
END

GO
