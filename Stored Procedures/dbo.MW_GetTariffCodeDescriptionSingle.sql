SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetTariffCodeDescriptionSingle] --257,'SOB','0',1  
(      
   @PracticeID int,      
   @ChargeType varchar(50),      
   @SearchString nvarchar(20),      
   @SearchFlag int      
)      
AS      
  
  
BEGIN  
declare @countTariff int  
  
declare @tmpTariffCodes table  
(  
   ChargeCode nvarchar(200),  
      ShortDescription nvarchar(4000),  
      LongDescription nvarchar(4000)  
)  
   
INSERT INTO @tmpTariffCodes  
Exec [MW_SearchTariffCodeDescription] @PracticeID ,@ChargeType,@SearchString,1  
  
Select @countTariff = COUNT(*) from @tmpTariffCodes  
  
  
IF @countTariff = 1   
BEGIN  
 DECLARE @desc VARCHAR(1500)  
 DECLARE @TariffCode nvarchar(200)  
  
 SELECT @desc = ShortDescription, @TariffCode=ChargeCode  FROM @tmpTariffCodes  
  
 Exec [MW_GetTariffCode] @PracticeID,@ChargeType,@TariffCode,@desc         
END  
ELSE  
 BEGIN  
  SELECT * FROM @tmpTariffCodes  
 END  
END  
  
  

GO
