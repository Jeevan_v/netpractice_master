SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

    
CREATE PROCEDURE [dbo].[MW_GetVisitLine]  
 @VisitID varchar(20)        
 AS      
        
SELECT vl.[VisitLineID]        
      ,vl.[VisitID]       
      ,vl.[Quantity] AS quantity        
      ,CONVERT(Decimal(30,2),ROUND([Amount]/Quantity,2)) AS UnitPrice        
      ,CONVERT(Decimal(30,2),ROUND([Amount],2)) AS amount        
      ,[LineDefinition]        
      ,[DispFee] AS dispfee        
      ,[NoDays] AS DaysSupply        
      ,[IsChronic]        
      ,[Code] as TariffCode        
      ,[Diagnosis]        
      ,[Description]        
      ,[DosageID]        
      ,[DailyDosage]        
      ,[Starttime]        
      ,[Endtime]      
       ,v.VisitDate     
       ,v.DoctorID     
      ,vld.Size,                
      vld.Dose,        
      vld.DoseForm,        
      vld.DoseFrequency,        
      vld.[Route/Location],        
      vld.[OtherInstruction],        
      vld.[Time/Period],        
      m.Memo,    
     -- commented for modifiers   
     vl.IsModifier,    
     RTRIM(LTRIM(mt.modifierType)) AS modifierType,    
     RTRIM(LTRIM(mt.FunctionType)) AS FunctionType,    
     CONVERT(DECIMAL(18,2),mt.FunctionAmount) AS FunctionAmount,  
     -- For Getting Units for the Line item having add units  
     CONVERT(DECIMAL(18,2),  
     CASE   
     WHEN  LOWER(RTRIM(LTRIM(mt.FunctionType))) = 'units'   
     THEN  mt.FunctionAmount /  
     (SELECT UnitPrice FROM ModifierRules WHERE ChargeCodeID=  
  (SELECT TOP 1  ChargeCodeID FROM ChargeCode WHERE ChargeCode  = vl.Code AND CodeGroupID=5  
   AND TypeID=  
  (SELECT PractitionerGroup_ID FROM PracticeType WHERE PracticeTypeID=  
  (SELECT PracticeTypeID FROM Practice WHERE PracticeID=  
  (SELECT Practiceid FROM Account WHERE AccountID=V.AccountID)  
   )))) ELSE 1 END)AS Units  ,
AssistantPracticeNo, 
AssistantMpNo,
ModifierAppliedCode
              
  FROM [VisitLine] vl   
-- commented for modifiers   
  LEFT join ModifierTransaction mt on vl.VisitLineID=mt.VisitLineID    
  INNER join VisitLineDosage vld        
  on vl.DosageID=vld.visitlineDosageID  inner join Memo m        
  on vl.VisitLineID=m.VisitLineID  inner join    
      
   Visit v      
  on v.VisitID=vl.VisitID      
  where         
  vl.VisitID=@VisitID  
  
  

    
    
  
    
    
  
  
  
  
   
GO
