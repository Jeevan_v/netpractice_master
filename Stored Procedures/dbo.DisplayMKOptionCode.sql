SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DisplayMKOptionCode]  	
AS

BEGIN
select a.MFAdminName,s.SchemeName,p.PlanName,o.MFOptionName,mfo.MFOptionLinkCode,mfo.IsFMcheckEnabled
from T_MFAdmin a join MedicalScheme s  on s.MFAdminID = a.MFAdmin_ID  
join MedicalPlan p on p.MedicalSchemeID = s.MedicalSchemeID join MFOptionLink ol on 
ol.PlanID = p.MedicalPlanID join MFOption o on o.MFOptionID = ol.MFOptionID join MFOptionLinkCode mfo on
mfo.MFOptionLinkID = ol.MFOptionLinkID join MFCodeType mfc on mfc.MFCodeTypeID = mfo.MFCodeTypeID 
and mfc.CodeTypeDefinition = 'Medikredit'
END
GO
