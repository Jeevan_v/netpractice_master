SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 22-08-2016
-- Description:	Fetch PracticeType and BHF no from PracticeTypeID
-- =============================================
create PROCEDURE [dbo].[SP_GETPRACTICETYPEDETAILSBYPRACTICETYPEID]
	-- Add the parameters for the stored procedure here
	@PracticeTypeID int
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select  [PracticeType]
      ,[BHFNo]
      ,[SubDiscipline]
      ,[PractitionerGroup_ID]
      ,[AddPrefix]

	FROM  [dbo].[PracticeType]
	WHERE PRACTICETYPEID = @PracticeTypeID


END
GO
