SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetPatientByPatientID]      
       @PatientID int      
AS      
BEGIN      
 SELECT       
   FirstName,      
   LastName,      
    CONVERT(varchar,[DOB],103) as DOB,      
   DependendNo,      
   MainMemberID,      
   SchemeID,  
   PlanID,    -- Added for FM check
   MedicalAidNo,      
   PassportNo,      
   IDNo,    
   FMCheckDate     
    FROM      
      Patient      
    WHERE       
      PatientID=@PatientID      
END
GO
