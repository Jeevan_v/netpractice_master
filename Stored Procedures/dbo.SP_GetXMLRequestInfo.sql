SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 8th Dec 2014
-- Description:	This procedure will select all the xml request info who's status is 1 (In proess). After completionof wor it will convert status in 2(Sucess ) or 3 (rejected) depeneds.
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetXMLRequestInfo] 
@CLAIMSWITCH varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT top 3 [XMLREQUESTID]
      ,[CLAIMID]
      ,[PATIENTID]
      ,[PRACTICEID]
      ,[VISITID]
      ,[ACCOUNTID]
      ,[TYPEOFREQUEST]
      ,[STATUSOFREQUEST]
      ,[CLAIMXMLFILEID]
      ,[XMLFILENAME]
      ,[CREATEDDATE]
      ,[UPDATEDDATE]
      ,[ISREPROCESSREQUIRED]
      ,[REMARKS]
      ,[REPROCESSINGDATE]
	  ,CLAIMSWITCH
  FROM [dbo].[XmlRequest]
  WHERE STATUSOFREQUEST = 1 AND upper(CLAIMSWITCH) =@CLAIMSWITCH


END

GO
