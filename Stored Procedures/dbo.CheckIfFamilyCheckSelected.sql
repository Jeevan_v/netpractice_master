SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CheckIfFamilyCheckSelected]  
 @PracticeID int  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 SELECT ISNULL(Approved,'') from PracticePackageRelationship 
 where PracticeId = @PracticeID and 
 PackageID =(select PackageID from Package where Package='FAMILY CHECK') AND 
 Subscribed =1  
END  
GO
