SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[GetRejectedReason]     
 -- Add the parameters for the stored procedure here    
 @PracticeId int    
AS    
BEGIN    
 SET NOCOUNT ON;    
 declare @reason int  
     
 If exists(Select PracticeId from PracticePackageRelationship where PracticeID = @PracticeId)    
 Begin    
 --Select distinct(    
 --ISNULL(Rejected_Reason,0)) as Rejected_Reason    
 --from PracticePackageRelationship PR    
 --Where PracticeID = @PracticeId     
    
set @reason = (Select distinct count(Rejected_Reason)  from PracticePackageRelationship PR    
Where PracticeID = @PracticeId  and Rejected_Reason is not null)  
  
if @reason >0   
begin  
Select distinct Rejected_Reason  from PracticePackageRelationship PR    
Where PracticeID = @PracticeId  and Rejected_Reason is not null  
end  
else  
begin  
select '0'  as Rejected_Reason
end  
   
 End    
END    

GO
