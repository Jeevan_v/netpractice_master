SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[MP_GetPatientDetailByMembershipID]
	@MembershipID uniqueidentifier
AS
BEGIN
SELECT
PatientID
FROM
dbo.[Patient] 
WHERE
MembershipID=@MembershipID
END


GO
