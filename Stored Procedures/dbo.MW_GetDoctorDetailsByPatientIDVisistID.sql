SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetDoctorDetailsByPatientIDVisistID]  

@PatientID int,
@VisitID int
		
AS

BEGIN
DECLARE @patientName VARCHAR(60),@DOB date,@MedicalAidNo varchar(50)
SET @patientName=(SELECT FirstName + ' '+ LastName FROM Patient where PatientID=@PatientID)
SET @DOB=(SELECT DOB FROM Patient where PatientID=@PatientID)
SET @MedicalAidNo=(SELECT MedicalAidNo FROM Patient where PatientID=@PatientID)
SELECT 
	
	DISTINCT

	u.FirstName+' '+u.LastName as DName ,
	
	u.UserID,
	
	u.DocPracticeNo ,
	
	u.MPNo,
	@MedicalAidNo as MedicalAidNo,
	@patientName as PName ,
	@DOB as PDob,
	
	ISNULL(v.AuthorisationCode,0) AS AuthorisationCode ,
	
	v.VisitDate,
	cd.Status
	
FROM 

	[User] u 
	
LEFT OUTER  JOIN 

	Visit v 

ON	

	 v.DoctorID = u.UserID 

LEFT OUTER JOIN
	 VisitLine vl 
	 
ON
	vl.VisitID = v.VisitID

LEFT OUTER JOIN

	ClaimDetail cd  
ON

	cd.VisitLineID = vl.VisitLineID 
WHERE
	u.UserID in 

	(

		select UserID from Visit where VisitID = @VisitID

	)
	
AND 

	v.VisitID = @VisitID
	
END
GO
