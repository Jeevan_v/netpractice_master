SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[SP_GETREPORTROLES](@RoleName varchar(100))
As
Begin
	if @RoleName <> 'Super Admin'
	Begin
		select 'A' OrderCol,0 ReportID,'--Select Report--' ReportName,'' RoleName 
		Union
		select 'B' OrderCol, p.privilegeid ReportID,P.Privilege ReportName,R.RoleName from 
		Privilege p
		inner join PrivilegeRoleRelationship p1 on p.PrivilegeID = p1.PrivilegeID
		inner join aspnet_Roles r on r.RoleId = p1.RoleID 
		where Privilege in ('Claim Status','Month End Report','Weekly Report','Day End Report','Income By Medical Aids','Income by Cash Patients','Payments Received','Aged Receivables')
		and RoleName = @RoleName
		order by OrderCol,rolename,ReportID
	end
	else
	Begin
		select 'A' OrderCol,0 ReportID,'--Select Report--' ReportName,'' RoleName 
		Union
		select 'B' OrderCol,p.privilegeid ReportID,P.Privilege ReportName,'Super Adminstrator' as RoleName
		from Privilege p where Privilege in ('Claim Status','Month End Report','Weekly Report','Day End Report','Income By Medical Aids','Income by Cash Patients','Payments Received','Aged Receivables')
		order by OrderCol,rolename,ReportID
		
	end
End

GO
