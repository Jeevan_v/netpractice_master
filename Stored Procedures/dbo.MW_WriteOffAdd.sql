SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[MW_WriteOffAdd]  
  @intVisitId INT,      
  @WriteOffAmount DECIMAL(36,2),    
  @ReceiptNo VARCHAR(50),    
  @UserID INT,
  @WriteOfDate Date    
AS      
BEGIN  
  INSERT INTO WS_WriteOff ([VisitID],[WriteOffAmount],[ReceiptNo],[UserID],[Date])   
  VALUES (@intVisitId,@WriteOffAmount,@ReceiptNo,@UserID, ISNULL(@WriteOfDate,GETDATE()));  
END
GO
