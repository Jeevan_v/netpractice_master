SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sameer>
-- Create date: <18/12/2012>
-- Description:	<Get Patient By PatientID>
-- =============================================
CREATE PROCEDURE [dbo].[MW_GetPatientByDOBInitialsAndName]
       @Initials VARCHAR(5),
       @FirstName VARCHAR(100),
       @LastName VARCHAR(50),
       @DOB DATE       
AS
BEGIN
	SELECT 
	   [PatientID]
    FROM
      Patient
    WHERE 
      Initials=@Initials
      AND
      FirstName=@FirstName
      AND
      LastName=@LastName
      AND
      DOB=@DOB  
END



GO
