SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 8 Jan 2015
-- Description:	This procedure will update the status of file whose response is processed successfully.
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateStatusOfProcessedFile] 
	-- Add the parameters for the stored procedure here
	@ClaimFileName varchar(Max) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update cx
	set cx.IsReversed = 'P'
	from claimxml cx
inner join claim cc with(nolock) on cc.ClaimID = cx.ClaimID 
inner join claimdetail cd with (nolock) on cd.ClaimID= cc.ClaimID and cd.ClaimDetailID = cx.ClaimDetailID
inner join visit v with(nolock) on v.VisitID = cc.VisitID-- and v.DateLogged > getdate()-1
 where   cx.XMLFileName = @ClaimFileName and  
 ( (cx.XMLFileName not  like '%REVERSE%' and isnull(cc.status,'')<> '' and isnull(cd.status,'')<> '') or 
	( isnull(cc.status,'')<> '' and (cc.status= 'REVERSED' or cc.StatusReason like '%Manually'))

  )


END

GO
