SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[MW_DailyTransaction](@intPractice int)
As
Begin
SELECT
	[PatientName],
	(cash_amount_paid + aid_amount_paid) AS [AmountPaid],
	(AmountDue - (cash_amount_paid + aid_amount_paid)) AS [Balance],
	[PatientID],
	[VisitID],
	[ClaimID],
	[UserID],
	[PracticeID],
	[Status],
	[SchemeName],
	[method],
	[AccountNumber]
FROM
(
SELECT 
	Pt.[FirstName] + ' '+  Pt.[LastName] AS [PatientName],	
	(SELECT SUM(amount) FROM VISITLINE VL WHERE VL.visitid = Vs.VisitID) as [AmountDue],
	(SELECT SUM(amountpaid) FROM PAYMENT WHERE PAYMENT.VisitID = Vs.VisitID) as [cash_amount_paid],
    (SELECT SUM(amountpaid) FROM CLAIM WHERE CLAIM.visitid = Vs.VisitID) as [aid_amount_paid], 
	pt.[PatientID],
	vs.[VisitID],
	Clm.[ClaimID],
	Vs.[DoctorID] AS [UserID],
	Acc.[PracticeID],
	Clm.[Status],
	MS.[SchemeName],
	PAY.[method],
	Acc.[AccountNumber]
FROM 
	Visit Vs 
	JOIN Account Acc ON Acc.AccountID = Vs.AccountID
	JOIN Patient Pt ON Pt.PatientID = Acc.PatientID
	--JOIN VisitLine Vl ON Vl.VisitID = Vs.VisitID	
	--JOIN ClaimDetail Cd ON Cd.VisitLineID = Vl.VisitLineID
	JOIN Claim Clm ON Clm.VisitID = Vs.VisitID
	JOIN Venue Ven ON Ven.VenueID = Vs.VenueID
	JOIN Practice Pr ON Pr.PracticeID = Ven.PracticeID
	LEFT JOIN MEDICALSCHEME MS ON MS.MedicalSchemeID = Vs.SchemeID
    LEFT JOIN PAYMENT PAY ON PAY.VisitID = Vs.VisitID
WHERE
	Pr.PracticeID = @intPractice 
	AND 
	CONVERT(date,Vs.VisitDate) = CONVERT(date,GETDATE())
)AA
End

GO
