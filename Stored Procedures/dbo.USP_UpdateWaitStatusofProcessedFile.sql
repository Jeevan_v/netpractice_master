SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_UpdateWaitStatusofProcessedFile]
	@ClaimFileName varchar(Max) = NULL,
	@Status varchar(2) = null
AS
	Begin
		update cx
	set cx.IsReversed = @Status
	from claimxml cx with(nolock)
	where cx.XMLFileName =@ClaimFileName


RETURN 0
END
GO
