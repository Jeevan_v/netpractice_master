SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetPrivileges] 
	
		
AS

BEGIN

SELECT
	Privilege , PrivilegeID

FROM 
	Privilege
	ORDER BY Privilege
	
END 

GO
