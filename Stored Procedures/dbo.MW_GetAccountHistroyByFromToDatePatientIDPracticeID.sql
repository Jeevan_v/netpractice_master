SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetAccountHistroyByFromToDatePatientIDPracticeID]   
  
@PatientID int , @PracticeID int,  @FromDate VARCHAR(30), @ToDate VARCHAR(30)  
    
AS  
  
BEGIN  
  
DECLARE @Sql nVARCHAR(4000)  
DECLARE @sqlTop5 nVARCHAR(30)  
DECLARE @sqlDate nVARCHAR(2000)  
DECLARE @sqlCommand nVARCHAR (4000)  
DECLARE @sqlGroupby nVARCHAR (500)  
  
SET @sqltop5 = ' TOP 5 '  
SET @Sql =  
'   v.InvoiceNo,   
 CONVERT(VARCHAR(12),VisitDate,103) AS VisitDate,    
 CONVERT(Decimal(30,2),ROUND(c.[TotalAmount],2)) AS TotalAmount ,   
 --CONVERT(Decimal(30,2),ROUND(c.[AmountPaid],2)) AS AmountPaid ,   
 (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) AS AmountPaid, 
 --CONVERT(Decimal(30,2),ROUND(c.[TotalAmount] - (CASE WHEN c.[AmountPaid] IS NULL THEN 0 ELSE c.[AmountPaid]  END) - (SELECT SUM(WriteOffAmount) FROM WS_WriteOff AS AP2 WHERE AP2.VisitID = v.VisitID),2)) AS Balance, 
 ISNULL(CONVERT(Decimal(30,2),ROUND((ISNULL(c.[TotalAmount],0)) - (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID) - (SELECT ISNULL(SUM(WriteOffAmount),0) FROM WS_WriteOff AS AP2 WHERE AP2.VisitID = v.VisitID),2)),0) AS Balance,   
 --ISNULL(CONVERT(Decimal(30,2),ROUND((ISNULL(c.[TotalAmount],0)) - (SELECT ISNULL(SUM(AmountPaid),0) FROM Payment AS Pa WHERE Pa.VisitID = v.VisitID),2)),0) AS Balance,   
 --ISNULL(SUM(Vl.TaxAmount),0) AS  TaxAmount 
 CONVERT(Decimal(30,2),ROUND((SELECT ISNULL(SUM(WriteOffAmount),0) FROM WS_WriteOff AS AP2 WHERE AP2.VisitID = v.VisitID),2)) AS WriteOff, 
 pat.[FirstName] + '' '' + pat.[LastName] AS PatientName,
 UPPER(LEFT(c.[Status],1))+LOWER(SUBSTRING(c.[Status],2,LEN(c.[Status]))) AS [Status],    
 ms.[SchemeName],
 v.[VisitID],
 c.[ClaimID],
 CASE 
	WHEN (SELECT COUNT(PaymentID) FROM Payment WHERE VisitID = v.VisitID) >=1 THEN 1
	WHEN (SELECT COUNT(WriteOffID) FROM WS_WriteOff WHERE VisitID = v.VisitID) >=1 THEN 2
	ELSE 0
	END AS [OPTIONSTATE]
 
FROM   
 Visit v  
JOIN  Claim c  ON c.VisitID = v.VisitID  
JOIN  Account a ON  a.AccountID = v.AccountID  
--JOIN VisitLine VL ON VL.VisitID = V.VisitID  
JOIN Patient pat on  a.PatientID = pat.PatientID
JOIN MedicalScheme ms  on  pat.SchemeID=ms.MedicalSchemeID

WHERE   
 v.AccountID IN  
 ( SELECT AccountID FROM Account WHERE PracticeID = @PracticeID AND PatientID = @PatientID ) '  
  
SET @sqlDate =   
      ' AND   
      CONVERT(DATE,v.VisitDate) BETWEEN   
      CONVERT(DATE,@FromDate,103) AND CONVERT(DATE,@ToDate,103) '  
        
SET @sqlGroupby =   
 'GROUP BY V.InvoiceNo,V.VisitDate,c.TotalAmount,c.AmountPaid,(c.TotalAmount-c.AmountPaid),pat.[FirstName] + '' '' + pat.[LastName],UPPER(LEFT(c.[Status],1))+LOWER(SUBSTRING(c.[Status],2,LEN(c.[Status]))),ms.[SchemeName], v.[VisitID], c.[ClaimID]'  
  
IF (@ToDate = '')   
SET @ToDate = getdate()  
  
  
IF (@FromDate <> '')  
 BEGIN  
  SET @sqlCommand = 'SELECT '  +  @Sql + @sqlDate + @sqlGroupby + ' ORDER BY v.InvoiceNo DESC'    
  EXECUTE sp_executesql @sqlCommand, N'@PatientID int,@PracticeID int,@FromDate VARCHAR(30),@ToDate VARCHAR(30)', @PatientID,@PracticeID,@FromDate,@ToDate    
 END  
 ELSE  
 BEGIN  
  SET @sqlCommand = 'SELECT '  + @sqltop5 +   @Sql + @sqlGroupby + ' ORDER BY v.InvoiceNo DESC'    
  EXECUTE sp_executesql @sqlCommand,  N'@PatientID int,@PracticeID int',  @PatientID,@PracticeID    
 END  
   
END  
  
  
GO
