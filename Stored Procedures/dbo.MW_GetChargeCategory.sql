SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetChargeCategory]
AS
SELECT

	ChargeCategoryID,
	ChargeCategory

	FROM
	dbo.ChargeCategory
	
	Order By ChargeCategory
GO
