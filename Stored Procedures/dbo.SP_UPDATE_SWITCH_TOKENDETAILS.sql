SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 04 OCT 2016
-- Description:	Updating Switch details. De Active the switch token value and set isActive false
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPDATE_SWITCH_TOKENDETAILS] 
	-- Add the parameters for the stored procedure here
	@Remarks varchar(8000),
	@IsActive bit, 
	 @TokenValue  varchar(8000) ,
	 @SwitchName varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[SwitchTokenDetails]
   SET  
       [Remarks] =@Remarks,
	 [IsActive] = @IsActive ,
       [UpdatedDate] =getdate()
 WHERE [IsActive] =1 and TokenValue =@TokenValue and SwitchName =@SwitchName

END

GO
