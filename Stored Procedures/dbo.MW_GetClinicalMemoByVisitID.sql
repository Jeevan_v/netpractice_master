SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
CREATE PROCEDURE [dbo].[MW_GetClinicalMemoByVisitID]   
  
@VisitID int  
    
AS  
  
BEGIN  
  
--SELECT 
--	Memo   
--FROM  
--	Memo  
--WHERE   
--	VisitLineID   IN  
--	(
--	SELECT
--		vl.VisitLineID   
--	FROM
--		VisitLine vl
--	WHERE
--		VisitID = @VisitID  
--	 )

SELECT 
		ClinicalNotes AS Memo,
		MedicalReport,
		ReferralLetter,
		SickNotes 
FROM 
		AddNotes  
WHERE  
		VisitID=@VisitID
END
GO
