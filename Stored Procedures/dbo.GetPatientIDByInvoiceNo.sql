SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetPatientIDByInvoiceNo]
	@InvoiceNo int
AS
BEGIN
	Select PatientId from Account where AccountId =(
	Select AccountId from Visit where InvoiceNo = @InvoiceNo)	
END
GO
