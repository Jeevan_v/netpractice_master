SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetMenuOnRole] 
	-- Add the parameters for the stored procedure here
   
   @rolename nvarchar(256)= null,
   @menuId int =null 
   
AS
BEGIN
   
   if(@menuId=0)
   begin
   Select MenuID,Displayname,NavigateURL from Menu where ParentID is null
   end
   else
   begin
	
	
	 Select distinct m.MenuID,m.DisplayName,m.NavigateURL from Menu m inner join PrivilegeMenuRelationship pmr
     on m.MenuID=pmr.MenuId
     inner join PrivilegeRoleRelationship prr
     on pmr.PrivilegeId=prr.PrivilegeID
     inner join aspnet_Roles ar
     on prr.RoleID=ar.RoleId
     where (ar.RoleName=@rolename and m.ParentID=@menuId) or m.MenuID=24
    end
	end
GO
