SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[SP_UpdateMailToMedikreditStatus]
(
@PracticeId int, @Status char(1)
)
as
BEGIN
Insert into MailToMedikreditStatus (PracticeID,EmailStatus) values (@PracticeId,@Status)
END
GO
