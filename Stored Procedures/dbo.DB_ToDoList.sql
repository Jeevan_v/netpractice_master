SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[DB_ToDoList] 

(@PracticeID int, @ClaimStatus Varchar(30))  

As  

Begin  

SELECT  Top 5 

 Lower(pat.FirstName + ' ' + pat.LastName) as PatientName,

 CONVERT(Decimal(30,2),ROUND(cl.[TotalAmount],2)) as AmountPaid,

 CONVERT(Decimal(30,2),ROUND(cl.[TotalAmount] - (CASE WHEN cl.AmountPaid IS NULL THEN 0 ELSE cl.AmountPaid  END),2)) AS [Balance],  

 pat.[PatientID],  

 v.[VisitID],  

 [ClaimID],  

 [PracticeID],  

 UPPER(LEFT([Status],1))+LOWER(SUBSTRING([Status],2,LEN([Status]))) as [Status],  

 [SchemeName], 

 [AccountNumber] ,

 v.VisitDate  

FROM  

Visit v  

INNER JOIN  

Claim cl 

on   

v.VisitID=cl.VisitID 

INNER JOIN  

Account ac 

on  

v.AccountID=ac.AccountID  

INNER JOIN  

Patient pat  

on  

ac.PatientID=pat.PatientID  

INNER JOIN  

MedicalScheme ms  

on  

pat.SchemeID=ms.MedicalSchemeID  

where  

cl.status='Rejected' 

AND

CONVERT(date,V.DateLogged) = CONVERT(date,GETDATE()) 

Order By v.VisitID desc 

 

END  









GO
