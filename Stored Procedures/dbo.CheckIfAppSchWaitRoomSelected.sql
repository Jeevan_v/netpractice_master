SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[CheckIfAppSchWaitRoomSelected] 
 @PracticeId int  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 SELECT ISNULL(Approved,'') FROM PracticePackageRelationship 
 WHERE PracticeId = @PracticeId 
 AND PackageId = (SELECT PackageId from Package where Package='APPOINTMENT SCHEDULER & WAITING ROOM')
 AND Subscribed = 1  
END  
GO
