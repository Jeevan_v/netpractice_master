SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CHE_GetAllSubUrbsCitiesAndPostalCodes]
AS
BEGIN
	SELECT 
		s.SubUrb,
		scpr.CityID,
	 Right(REPLICATE('0',4) +CAST(P.PostalCode AS VARCHAR(4)), 4) PostalCode,
	 Right(REPLICATE('0',4) + CAST(P.StreetCode AS VARCHAR(4)), 4) StreetCode
		
	FROM
	SubUrbCityPostalCodeRelationship scpr
	INNER JOIN 
	PostalCode P
	ON 	scpr.PostalCodeID=P.PostalCodeID
	INNER JOIN
	SubUrb s
	ON
	scpr.SubUrbID=s.SubUrbID
END

GO
