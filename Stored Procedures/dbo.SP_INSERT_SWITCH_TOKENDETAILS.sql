SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 04 OCT 2016
-- Description:	Insert Switch Token details.
-- =============================================
CREATE PROCEDURE [dbo].[SP_INSERT_SWITCH_TOKENDETAILS] 
	-- Add the parameters for the stored procedure here
@SwitchName varchar(50),   
@TokenValue varchar(8000) ,
--@TokenExpiresIn datetime,
@TokenType varchar(500),
@DtIssued datetime,
@DtExpire datetime, 
@Sessionid varchar(100) ,
@UserId int ,
 @IsActive bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- inactive existing token details
	update SwitchTokenDetails
	set IsActive = 0
	where IsActive = 1

    -- Insert statements for procedure here
	INSERT INTO [dbo].[SwitchTokenDetails]
           ([SwitchName]
           ,[TokenValue]
       ---    ,[TokenExpiresIn]
           ,[TokenType]
           ,[DtIssued]
           ,[DtExpire]
           ,[Sessionid]
           ,[UserId]
           ,[IsActive]
           ,[CreatedDate]
           )
     VALUES
           (@SwitchName
           ,@TokenValue
         --  ,@TokenExpiresIn
           ,@TokenType
           ,@DtIssued
           ,@DtExpire
           ,@Sessionid
           ,@UserId
           ,@IsActive
           ,GETDATE()
		   )
END
GO
