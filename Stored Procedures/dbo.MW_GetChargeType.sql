SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetChargeType]
AS
SELECT  
  
 ct.ChargeTypeID,  
 ChargeType,  
 mp.MedicalSchemeID  
 FROM  
 dbo.ChargeType ct  
 Inner JOIN  
 dbo.Chargetypeschemeplanrelationship mp  
 On  
 ct.ChargeTypeID=mp.ChargeTypeID   
 WHERE  
 LOWER(ct.ChargeType) not in(LOWER('UNIVERSAL'))  
 UNION  
 SELECT  
 TOP 1  
 ct.ChargeTypeID,  
 ChargeType,  
 mp.MedicalSchemeID  
 FROM  
 dbo.ChargeType ct  
 Inner JOIN  
 dbo.Chargetypeschemeplanrelationship mp  
 On  
 ct.ChargeTypeID=mp.ChargeTypeID   
 WHERE  
 LOWER(ct.ChargeType)=(LOWER('UNIVERSAL'))   
 order by ChargeType asc  
    
GO
