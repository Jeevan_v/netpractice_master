SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 20-05-2015
-- Description:	This procedure will help in inserting maual invoice details which 
--				sent from as email attachment to medical aid
-- =============================================
CREATE PROCEDURE [dbo].[SP_Insert_Update_ClaimReceivedInvoice_Details]
	-- Add the parameters for the stored procedure here
	 @InvoiceNo int,
	 @ClaimId int,
	 @VisitDate datetime,
	 @DateLogged Datetime,
	 @FromEmail varchar(1000),
	 @ToEmail Varchar(1000), 
	 @SchemeName varchar(1000),
	 @IsResend	 bit ,
	 @Status	varchar(100),	 
	 @IsSuccess bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (( SELECT COUNT(*) FROM EMAILINVOICEGENERATION WHERE InvoiceNo = @InvoiceNo and Claimid = @ClaimId  ) = 0)
	BEGIN
INSERT INTO [dbo].[EmailInvoiceGeneration]
           ([InvoiceNo]
           ,[ClaimId]
           ,[VisitDate]
		   ,[DateLogged]
           ,[MedicalAidEmail]
           ,[FromEmail]
           ,[GeneratedDate]
           ,[Status]
           ,[IsSuccess]
		   ,[IsResend]
		   ,[SchemeName])
     VALUES
           (@InvoiceNo
           ,@ClaimId
           ,@VisitDate
		   ,@DateLogged
           ,@ToEmail
           ,@FromEmail
           ,GETDATE()
           ,@Status
           ,@IsSuccess
		   ,@IsResend
		   ,@SchemeName
		   )
		   END 
    ELSE 
		   BEGIN
		   UPDATE EMAILINVOICEGENERATION
		   set isSuccess = @IsSuccess,
		   IsResend = @IsResend,
		   Status= @Status,
		   GeneratedDate= GETDATE()
		   WHERE ClaimId = @ClaimId and InvoiceNo = @InvoiceNo
		   END
END

GO
