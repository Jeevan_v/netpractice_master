SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetPatientByDependent]
@MainMemberID int
AS
BEGIN
SELECT
    PatientID,
	FirstName,
	LastName,
	CONVERT(varchar,[DOB],103) as DOB,
	IDNo,
	SchemeID,
	DependendNo,
	MedicalAidNo,
	PassportNo,
	TelNo,
	CellNo
	

	FROM
	Patient
	
	WHERE
	MainMemberID=PatientID and MainMemberID=@MainMemberID
	
	END


GO
