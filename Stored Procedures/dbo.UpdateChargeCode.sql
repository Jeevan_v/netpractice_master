SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateChargeCode] 
(  
   @practiceType nvarchar(50),
   @BHFNo nvarchar(3),
   @ChargeType nvarchar(100),
   @chargeCode nvarchar(20),       
   @ShortDescription nvarchar(4000) ,
   @LongDescription nvarchar(4000) ,
   @CodeGroupDescription  nvarchar(500) ,
   @Price nvarchar(50) 
)    
AS    
  
BEGIN       
	--variable declaration --    
    DECLARE @practitionerGroup_id int, @practicetype_id int, @PaymentArrangementDetail_ID int,
    @PaymentArrangementGroup_ID int, @ChargeCode_ID int, @price_NO_VAT decimal(18,2) = 0.00, 
    @CodeGroup_ID int, @insert_ChargeCode_ID varchar(10),@countDetailID int,@countPractGroup int,
    @countRec int, @ChargeCodeStraightCharge_ID int,@countRecInside int,@payDetailID int,@CodeGroupCount int
    
	IF(@ShortDescription is not null or @ShortDescription <> '')
	BEGIN
		IF(len(@ShortDescription) > 500)
		BEGIN
			set @ShortDescription = Substring(@ShortDescription,1,500)
		END		
	END

	IF(@ShortDescription is null or @ShortDescription = '')
    BEGIN
		IF(@LongDescription is not null or @LongDescription <> '')
		BEGIN
			set @ShortDescription = Substring(@LongDescription,1,500)
		END
    END
    
    IF(@LongDescription is null or @LongDescription = '')
    BEGIN
		IF(@ShortDescription is not null or @ShortDescription <> '')
		BEGIN
			set @LongDescription = @ShortDescription
		END
    END

    IF(len(@practiceType) <> 0 and len(@BHFNo) <> 0 and len(@ChargeType) <> 0 and len(@chargeCode) <> 0 
		and len(@CodeGroupDescription) <> 0)
    BEGIN
	IF(@Price is NULL or @Price = '')
		BEGIN
			set @Price = '0'
		END
    --Get and Set practitionerGroup_Id and  @practicetype_id
	/*
	IF(@BHFNo <> '' or @BHFNo is not null and @practiceType = '' or @practiceType is null)
		BEGIN
			SET @practitionerGroup_id = (select p.PractitionerGroup_ID from practicetype p where p.BHFNo = @BHFNo)
			SET @countPractGroup = (select count(p.PractitionerGroup_ID) from practicetype p where p.BHFNo = @BHFNo)
			SET @practicetype_id = (select p.PracticeTypeID from practicetype p where p.BHFNo = @BHFNo)
		END
	ELSE IF(@practiceType <> '' or @practiceType is not null and @BHFNo = '' or @BHFNo is null)
		BEGIN
			SET @practitionerGroup_id =(select p.PractitionerGroup_ID from practicetype p where 
									   p.PracticeType = @practiceType)
			SET @countPractGroup = (select count(p.PractitionerGroup_ID) from practicetype p where 
									   p.PracticeType = @practiceType)					
			SET @practicetype_id = (select p.PracticeTypeID from practicetype p where p.PracticeType = @practiceType)
		END
	ELSE */
	IF(len(@BHFNo) <> 0 and len(@practiceType) <> 0)
		BEGIN
			SET @practitionerGroup_id = (select p.PractitionerGroup_ID from practicetype p where p.BHFNo = @BHFNo
										and p.PracticeType = @practiceType)
			SET @countPractGroup = (select count(p.PractitionerGroup_ID) from practicetype p where p.BHFNo = @BHFNo
										and p.PracticeType = @practiceType)							
			SET @practicetype_id = (select p.PracticeTypeID from practicetype p where p.BHFNo = @BHFNo
									and p.PracticeType = @practiceType)
		END       
   
	--Get and Set @PaymentArrangementDetail_ID based on Patient Charge Type    
    SET @PaymentArrangementDetail_ID = (select pa.PaymentArrangementDetail_ID from T_PaymentArrangement p join  
										T_PaymentArrangementDetail pa on 
										pa.PaymentArrangement_ID = p.PaymentArrangement_ID 
										where p.DetailCode = @ChargeType) 

	SET @countDetailID = (select count(pa.PaymentArrangementDetail_ID) from T_PaymentArrangement p join  
						  T_PaymentArrangementDetail pa on pa.PaymentArrangement_ID = p.PaymentArrangement_ID 
						  where p.DetailCode = @ChargeType)					  
	IF(@countDetailID = 0 and len(@ChargeType) <> 0)
		BEGIN
			INSERT INTO ChargeType (CategoryID,ChargeType,StraightCharge) VALUES (1,@ChargeType,0)
			
			INSERT INTO T_PaymentArrangement (PaymentArrangementGroup_ID,DetailCode,LongDescription,DateValidFrom,
			DateValidTo,ElixirRateID,CCTGroupID,Iscontract,Isdefault)
			VALUES (68,@ChargeType,@ChargeType,'2000-01-01 00:00:00.000','3000-01-01 23:59:59.997',0,0,'F','F')
			
			SET @payDetailID = SCOPE_IDENTITY();
			
			INSERT INTO T_PaymentArrangementDetail(PaymentArrangement_ID,LocationType_ID,DetailCode,LongDescription,
			DateValidFrom,DateValidTo,IsStandardExcluded) VALUES (@payDetailID,0,@ChargeType,@ChargeType,
			'2000-01-01 00:00:00.000','3000-01-01 23:59:59.997',0)
			
			--Get and Set @PaymentArrangementDetail_ID based on Patient Charge Type    
			SET @PaymentArrangementDetail_ID = (select pa.PaymentArrangementDetail_ID from T_PaymentArrangement p 
			join T_PaymentArrangementDetail pa on pa.PaymentArrangement_ID = p.PaymentArrangement_ID 
			where p.DetailCode = @ChargeType)
		END
	
	--Get and Set @PaymentArrangementGroup_ID based on Patient Charge Type      
    SET @PaymentArrangementGroup_ID = (select p.PaymentArrangementGroup_ID from T_PaymentArrangement p join  
									   T_PaymentArrangementDetail pa on 
									   pa.PaymentArrangement_ID = p.PaymentArrangement_ID 
									   where p.DetailCode = @ChargeType) 
									   
	--Get and Set @CodeGroup_ID based on Patient Charge Type 
	IF(@CodeGroupDescription <> '' or @CodeGroupDescription is not NULL)
		BEGIN		
			SET @CodeGroup_ID = (select CodeGroupID from CodeGroup where CodeGroupDefinition = @CodeGroupDescription)
			SET @CodeGroupCount = (select COUNT(*) from CodeGroup where CodeGroupDefinition = @CodeGroupDescription)
		END
		
	--Set Price without VAT
	SET @price_NO_VAT = CONVERT(decimal(18,2),@Price)/1.14

    IF(@countPractGroup <> 0 and @CodeGroupCount <> 0)
		BEGIN
			--GET and SET ChargeCode_ID based on ChargeCode     
			SET @ChargeCode_ID = (select Top 1 ChargeCodeID  from ChargeCode where CodeGroupID in(4,3)     
								  and CodeGroupdetail_ID = 1  and TYPEID = @practitionerGroup_id and 
								  ChargeCode = @chargeCode)
                  
			--IF (@ChargeCode_ID <> '' or @ChargeCode_ID <> NULL) 
			IF(@ChargeCode_ID is not NULL)  
				BEGIN
					
					IF(len(@ShortDescription) <> 0 and  len(@LongDescription) <> 0)
					BEGIN
						UPDATE ChargeCode set ShortDescription = @ShortDescription, LongDescription = @LongDescription
						WHERE CodeGroupID in(4,3) and CodeGroupdetail_ID = 1  and TYPEID = @practitionerGroup_id and 
						ChargeCode = @chargeCode
					END

					SET @countRec = (select COUNT(*) as countREC from  T_ChargeCodeStraightCharge  where 
					ChargeCode_ID=@ChargeCode_ID and PractitionerType_ID=@practicetype_id
					and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID and GETDATE() between 
					DateValidFrom and DateValidTo)
  
					IF(@countRec <> 0)
						BEGIN
							UPDATE T_ChargeCodeStraightCharge SET ValueInclRounded = CONVERT(decimal(18,2),@Price), 
							ValueInclUnrounded = CONVERT(decimal(18,2),@Price), 
							ValueExclUnrounded = ROUND(@price_NO_VAT,4),ValueExclRounded = ROUND(@price_NO_VAT,4)
							WHERE ChargeCode_ID=@ChargeCode_ID and PractitionerType_ID=@practicetype_id and 
							PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID 
							and GETDATE() between DateValidFrom and DateValidTo 
						END	
					ELSE IF(@countRec = 0)
						BEGIN
							SET @countRecInside = (select COUNT(*) as countREC from  T_ChargeCodeStraightCharge where 
							ChargeCode_ID=@ChargeCode_ID and PractitionerType_ID=@practicetype_id
							and PaymentArrangementDetail_ID=@PaymentArrangementDetail_ID)

							IF(@countRecInside <> 0)
								BEGIN
									SET @ChargeCodeStraightCharge_ID = (select top 1 ChargeCodeStraightCharge_ID as 
									countREC from T_ChargeCodeStraightCharge where ChargeCode_ID = @ChargeCode_ID and 
									PractitionerType_ID = @practicetype_id and 
									PaymentArrangementDetail_ID = @PaymentArrangementDetail_ID order by 
									DateValidFrom desc)
									
									UPDATE T_ChargeCodeStraightCharge SET ValueInclRounded = CONVERT(decimal(18,2),@Price), 
									ValueInclUnrounded = CONVERT(decimal(18,2),@Price), 
									ValueExclUnrounded = ROUND(@price_NO_VAT,4),ValueExclRounded = ROUND(@price_NO_VAT,4),
									DateValidFrom = GETDATE(),DateValidTo=DATEADD(YEAR,1,GETDATE()) WHERE 
									ChargeCode_ID = @ChargeCode_ID and PractitionerType_ID = @practicetype_id and 
									PaymentArrangementDetail_ID = @PaymentArrangementDetail_ID 
									and ChargeCodeStraightCharge_ID = @ChargeCodeStraightCharge_ID 
								END
							ELSE
								BEGIN
									INSERT INTO T_ChargeCodeStraightCharge(ChargeCode_ID,PractitionerType_ID,
									PaymentArrangementDetail_ID,ValueExclUnrounded,ValueExclRounded,VatPerc_ID,
									ValueInclUnrounded,ValueInclRounded,DateValidFrom,DateValidTo,RoundingRule_ID,
									LocationType_ID)VALUES (@ChargeCode_ID,@practicetype_id,
									@PaymentArrangementDetail_ID,ROUND(@price_NO_VAT,4),ROUND(@price_NO_VAT,4),1,
									CONVERT(decimal(18,2),@Price),CONVERT(decimal(18,2),@Price),GETDATE(),
									DATEADD(YEAR,1,GETDATE()),1,0)
									print @practicetype_id
								END   
						END              
				END          
			--ELSE IF(@ChargeCode_ID = '' or @ChargeCode_ID = NULL)
			ELSE IF(@ChargeCode_ID is NULL)
				BEGIN
					INSERT INTO ChargeCode(CategoryID,TypeID,ChargeGroupID,CodeGroupID,ChargeCode,ShortDescription,
					LongDescription,PaymentArrangementGroup_ID,CodeType,CodeGroupDetail_ID,DateValidFrom,DateValidTo)
					VALUES (null,@practitionerGroup_id,null,@CodeGroup_ID,@chargeCode,@ShortDescription,
					@LongDescription,@PaymentArrangementGroup_ID,'I',1,GETDATE(),DATEADD(YEAR,1,GETDATE()))
				        
					SET @insert_ChargeCode_ID =(select ChargeCodeID  from ChargeCode where CodeGroupID = @CodeGroup_ID    
					and CodeGroupdetail_ID=1  and TYPEID=@practitionerGroup_id and ChargeCode=@chargeCode)   
				            
					INSERT INTO T_ChargeCodeStraightCharge(ChargeCode_ID,PractitionerType_ID,
					PaymentArrangementDetail_ID,ValueExclUnrounded,ValueExclRounded,VatPerc_ID,ValueInclUnrounded,
					ValueInclRounded,DateValidFrom,DateValidTo,RoundingRule_ID,LocationType_ID) VALUES 
				    (@insert_ChargeCode_ID,@practicetype_id,@PaymentArrangementDetail_ID,ROUND(@price_NO_VAT,4),
				    ROUND(@price_NO_VAT,4),1,CONVERT(decimal(18,2),@Price),CONVERT(decimal(18,2),@Price),GETDATE(),
				    DATEADD(YEAR,1,GETDATE()),1,0)		
				END
		END                           
	END
END
GO
