SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sameer>
-- Create date: <29-01-2013>
-- Description:	<Delete patient from waiting room list>
-- =============================================
CREATE PROCEDURE [dbo].[MW_DeletePatientFromWaitingRoomList] 
@waitingRoomID INT,
@accountID INT,
@venueID INT
AS
BEGIN
DELETE 
FROM WaitingRoom
WHERE
WaitingRoomID=@waitingRoomID
AND
AccountID=@accountID
AND
VenueID=@venueID	
END


GO
