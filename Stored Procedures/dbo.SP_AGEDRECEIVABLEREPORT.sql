SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[SP_AGEDRECEIVABLEREPORT] 
--'10/05/2013',0,0,1
--'10/05/2013',0,0,1 

-- '10/05/2013',0,0,1 

 @DATE DATETIME,@PRACTICEID INT, @DOCTORID INT, @RPTFREQUENCY INT                         
as

declare @CURRMONTH_STDATE DATETIME
declare @CURRMONTH_ENDDATE DATETIME
declare @STDATE_FREQUENCY DATETIME   
declare @ENDDATE_FREQUENCY DATETIME 
declare @ENDDATE_TEMP_FREQUENCY DATETIME   
--declare @PRACTICEID numeric     
--declare @DOCTORID numeric
--declare @DATE DATETIME
--declare @RPTFREQUENCY numeric

--SET @PRACTICEID=0
--set @DOCTORID=0
--SET @DATE='07/01/2012' 

--set @RPTFREQUENCY=1

SET @CURRMONTH_STDATE = CONVERT(DATETIME,CONVERT(VARCHAR(20),CONVERT(VARCHAR(20),MONTH(@DATE))+'/01/'+CONVERT(VARCHAR(20),YEAR(@DATE))))      
SET @CURRMONTH_ENDDATE = @DATE   
 
 
 ----------CALCULATE STDATE AND END DATE BASED ON FREQUENCY      
IF @RPTFREQUENCY = 1      
BEGIN      
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-3,@DATE)      
END      
ELSE IF @RPTFREQUENCY = 2      
BEGIN      
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-6,@DATE)      
END      
ELSE      
BEGIN      
 SET @ENDDATE_TEMP_FREQUENCY = DATEADD(MM,-12,@DATE)      
END      
SET @STDATE_FREQUENCY = CONVERT(DATETIME,CONVERT(VARCHAR(20),CONVERT(VARCHAR(20),MONTH(@ENDDATE_TEMP_FREQUENCY)) + '/01/' + CONVERT(VARCHAR(20),YEAR(@ENDDATE_TEMP_FREQUENCY))))      
SET @ENDDATE_FREQUENCY = DATEADD(DD,-1,@CURRMONTH_STDATE) 
BEGIN

select a.PracticeName,
a.DoctorName,
a.PatientName,
a.DoctorID,
a.PatientID,
a.PracticeID,
--a.VisitID,
--SUM(BalanceAmount) as BalanceAmount,
SUM(a.CURRENTAMT) as CURRENTAMT,
SUM(a.MON1_AMT) as MON1_AMT,
SUM(a.MON2_AMT) as MON2_AMT,
SUM(a.MON3_AMT) as MON3_AMT,
SUM(a.MON4_AMT) as MON4_AMT,
SUM(a.MON5_AMT) as MON5_AMT,
SUM(a.MON6_AMT) as MON6_AMT,
SUM(a.MON7_AMT) as MON7_AMT,
SUM(a.MON8_AMT) as MON8_AMT,
SUM(a.MON9_AMT) as MON9_AMT,
SUM(a.MON10_AMT) as MON10_AMT,
SUM(a.MON11_AMT) as MON11_AMT,
SUM(a.MON12_AMT) as MON12_AMT,
SUM(a.OLDER) as OLDER

--SUM(a.TOTAL_AMT) as TOTAL_AMT


 from  

(SELECT DISTINCT 
PR.PracticeName,
p.PatientID,
USR.FirstName+' '+USR.LastName as DoctorName,
p.FirstName + ' ' + P.LastName AS PatientName,
DoctorID,
vs.VisitDate,
pr.PracticeID,
--vs.VisitID,

--isnull(C.TotalAmount,0)-ISNULL(SUM(py.AmountPaid),0) - ISNULL(C.AmountPaid,0) as BalanceAmount,
--isnull(SUM(py.AmountPaid),0) as PatientAmountPaid,
--isnull(c.AmountPaid,0) as ClaimAmountPaid,
--isnull(c.TotalAmount,0) as TotalAmount,
--vs.VisitID,
--(C.TotalAmount)-(ISNULL(PY.AmountPaid,0) + ISNULL((C.AmountPaid),0)) as BalanceAmount,


 (CASE WHEN VS.VisitDate >= @CURRMONTH_STDATE and VS.VisitDate  <=  @CURRMONTH_ENDDATE THEN         
  (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS  CURRENTAMT,     

 (CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-1,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-1,@DATE))AND @RPTFREQUENCY >=1  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS MON1_AMT,
     
 (CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-2,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-2,@DATE))AND @RPTFREQUENCY >=1  
  THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS  MON2_AMT,        
     
(CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-3,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-3,@DATE))AND @RPTFREQUENCY >=1  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS MON3_AMT,
 
(CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-4,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-4,@DATE))AND @RPTFREQUENCY >=2  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS MON4_AMT, 
 
(CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-5,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-5,@DATE))AND @RPTFREQUENCY >=2  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS  MON5_AMT,        
        
(CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-6,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-6,@DATE))AND @RPTFREQUENCY >=2  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS MON6_AMT,        
 
 (CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-7,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-7,@DATE))AND @RPTFREQUENCY =3  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END)  AS MON7_AMT,
       
        
 (CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-8,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-8,@DATE))AND @RPTFREQUENCY =3  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS MON8_AMT, 
       
        
(CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-9,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-9,@DATE))AND @RPTFREQUENCY =3  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS  MON9_AMT,        
        
(CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-10,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-10,@DATE))AND @RPTFREQUENCY =3  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS  MON10_AMT,        
        
 (CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-11,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-11,@DATE))AND @RPTFREQUENCY =3  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS MON11_AMT,        
        
(CASE WHEN MONTH(VS.VisitDate)= MONTH(DATEADD(MM,-12,@DATE)) AND YEAR(VS.VisitDate)  = YEAR(DATEADD(MM,-12,@DATE))AND @RPTFREQUENCY =3  
 THEN (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS  MON12_AMT,   
   
--(CASE WHEN VS.VisitDate  < @ENDDATE_FREQUENCY THEN 
-- (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS OLDER  
--changes done for older column 
(CASE WHEN VS.VisitDate  < @STDATE_FREQUENCY  THEN 
 (ISNULL(C.TotalAmount,0))- ISNULL(SUM(PY.AmountPaid),0) ELSE 0 END) AS OLDER  

          
FROM Visit VS

INNER JOIN Account A ON vs.AccountID=a.AccountID
INNER JOIN Patient p ON p.PatientID=A.PatientID
INNER JOIN Claim C ON C.VisitID=vs.VisitID 
INNER JOIN Venue VE ON VE.VenueID=VS.VenueID 
INNER JOIN Practice PR ON PR.PracticeID=VE.PracticeID 
INNER JOIN [USER] USR ON USR.USERID = VS.DOCTORID
--Changed for Checking Doctor roles only
 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID       
 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID 

 -- Changed  For Incorrect MApping of User and Practice
 INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID   
LEFT JOIN Payment PY ON (py.VisitID=VS.VisitID AND py.Date BETWEEN VS.VisitDate  AND  @Date )
--where DoctorID=@DOCTORID and pr.PracticeID=@PRACTICEID 
 
 where  CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 end =                   
      CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 end              
                     
 AND  CASE WHEN @DOCTORID <> 0 THEN VS.DOCTORID ELSE 1 end =                   
      CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 end          
  -- Changed  For Incorrect MApping of User and Practice  
 AND  UVR.VenueID= Vs.VenueID        AND    
    --Checking doctor (gowsi)
  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor') 
--AND Vs.VisitDate<=@date
          
 GROUP BY 
 
 P.PatientID,DoctorID,PR.PracticeID,PR.PracticeName,USR.FirstName,USR.LastName,
c.TotalAmount,
c.AmountPaid,
--vs.VisitID,
pr.PracticeID,
vs.VisitDate,
P.FirstName,P.LastName
--vs.VisitID

 )a
  
 
 group by 

a.PracticeName,
a.DoctorName,
a.PatientName,
a.DoctorID,
a.PatientID,
a.PracticeID
--a.VisitID




 END
 
  
GO
