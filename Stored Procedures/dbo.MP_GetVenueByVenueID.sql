SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetVenueByVenueID]
	@VenueID int
	
AS

SELECT

	VenueID,
	VenueName,
	PracticeID,
	PhysicalAddressID,
	PostalAddressID,
	Telephone,
	ReasonID,
	Fax,
	IsActive
FROM
	Venue
WHERE
	VenueID = @VenueID 
	   

GO
