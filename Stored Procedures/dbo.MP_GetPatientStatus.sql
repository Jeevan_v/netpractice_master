SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [dbo].[MP_GetPatientStatus] 
 @UserName varchar(100)  
AS  
select  
 
    r.Reason  
FROM   
[Patient] p   
INNER JOIN  
[aspnet_Users] aspu  
ON  
aspu.UserId = p.MembershipID  
INNER JOIN  
[InActiveReason] r  
ON  
p.ReasonID = r.ReasonID  
where   
aspu.UserName=@UserName  
GO
