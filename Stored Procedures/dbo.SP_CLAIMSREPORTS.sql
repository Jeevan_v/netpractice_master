SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SP_CLAIMSREPORTS] @DATE DATETIME,@STATUS Varchar(50),@PRACTICEID INT, @DOCTORID INT                     
AS                  
BEGIN                  
 SELECT DISTINCT VS.VISITID,                   
  PT.FIRSTNAME + ' '+  PT.LASTNAME AS [PATIENTNAME], ACC.ACCOUNTID,
  '' FileNo,
  CLM."Status" ClaimStatus,                 
  MS.SchemeName, (ISNULL(CLM.TOTALAMOUNT,0)) AMOUNT, 
  --changes done to calculate balance               
  (ISNULL(CLM.TOTALAMOUNT,0)) - ((ISNULL(CLM.AMOUNTPAID,0))+SUM(ISNULL(PY.AmountPaid,0))) AS [BALANCE],                  
  VS.DOCTORID AS [DOCTORID],                   
  ACC.PRACTICEID ,  
  --changes made to display datelogged              
  CONVERT(varchar(20),VS.DateLogged,103) [DATE],                
  PR.PRACTICENAME PRACTICE,              
  1 as ClaimStatusID,                 
  (USR.FIRSTNAME+ ' '+USR.LASTNAME) AS DOCTOR                
 FROM                   
  VISIT VS                   
  INNER JOIN ACCOUNT ACC ON ACC.ACCOUNTID = VS.ACCOUNTID                  
  INNER JOIN PATIENT PT ON PT.PATIENTID = ACC.PATIENTID                 
  --INNER JOIN VENUEFILE VF ON VF.ACCOUNTID =ACC.ACCOUNTID                  
  INNER JOIN CLAIM CLM ON VS.VISITID = CLM.VISITID                  
  --INNER JOIN VENUE VEN ON VEN.VENUEID = VS.VENUEID                  
  INNER JOIN PRACTICE PR ON PR.PRACTICEID = ACC.PRACTICEID                
  INNER JOIN [USER] USR ON USR.USERID = VS.DOCTORID 
      --Changed for Checking Doctor roles only (gowsi)
 INNER JOIN ASPNET_USERSINROLES AUIR ON  USR.MEMBERSHIPID = AUIR.USERID       
 INNER JOIN ASPNET_ROLES ROLES ON ROLES.ROLEID = AUIR.ROLEID  
  -- Changed For Invalid incorrect user and practice mapping
  INNER JOIN UserVenueRelationship UVR ON uvr.USERID=USR.USERID          
  LEFT JOIN MEDICALSCHEME MS ON MS.MEDICALSCHEMEID = PT.SCHEMEID
  -- changes made for not updating in claim table(to calculate balance) 
  LEFT JOIN PAYMENT PY ON PY.VisitID=VS.VisitID                    
 WHERE PR.IsActive = 1 and                   
  VS.DateLogged >= DATEADD(DD,-30,@DATE) AND VS.DateLogged <= @DATE             
  AND  CASE WHEN @PRACTICEID <> 0 THEN  PR.PRACTICEID ELSE 1 end =                 
       CASE WHEN @PRACTICEID <> 0 THEN @PRACTICEID   ELSE 1 end            
                  
  AND  CASE WHEN @DOCTORID <> 0 THEN VS.DOCTORID ELSE 1 end =                 
       CASE WHEN @DOCTORID <> 0 THEN @DOCTORID  ELSE 1 end           
                 
  AND  CASE WHEN @STATUS <> 'ALL' THEN CLM."Status" ELSE 'A' end =                 
       CASE WHEN @STATUS <> 'ALL' THEN @STATUS  ELSE 'A' end
  -- Changed For Invalid incorrect user and practice mapping                
  --AND  UVR.VenueID= VS.VenueID 
  --Checking doctor (gowsi)
     AND
  ROLES.ROLENAME in ('Practitioner','Practitioner Admin','Locum Doctor')     
              
  GROUP BY                 
  PT.FIRSTNAME, PT.LASTNAME,PT.PATIENTID,ACC.ACCOUNTID,                 
  VS.DOCTORID , ACC.PRACTICEID ,VS.VISITDATE, PR.PRACTICENAME,                 
  USR.FIRSTNAME,USR.LASTNAME,
  --VF.FileNo,
  --FileNo,
  MS.SchemeName,VS.VISITID,CLM."Status",VS.DATELOGGED,
  CLM.TOTALAMOUNT,CLM.AMOUNTPAID                
 END 
GO
