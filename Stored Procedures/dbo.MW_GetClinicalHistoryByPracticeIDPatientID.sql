SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetClinicalHistoryByPracticeIDPatientID] 

@PracticeID int,@PatientID int, @FromDate VARCHAR(30), @ToDate VARCHAR(30)
		
AS

BEGIN

DECLARE @Sql nVARCHAR(4000)
DECLARE @sqlTop5 nVARCHAR(30)
DECLARE @sqlDate nVARCHAR(2000)
DECLARE @sqlCommand nVARCHAR (4000)

SET @sqltop5 = ' TOP 5 '
SET @Sql = 
	'v.VisitID,	
	CONVERT(VARCHAR(12),v.VisitDate, 103) as VisitDate,
	a.AccountNumber , 	
	u.FirstName+'' ''+ u.LastName as DName
FROM 
	Account a 
JOIN  
	Visit v 
ON
	a.AccountID=v.AccountID
JOIN 
	[User] u 
ON 
	u.UserID = V.DoctorID
WHERE
	a.AccountID IN
	(
		SELECT 		
			AccountID 		
		FROM 		
			Account		
		WHERE 			
			a.PatientID=@PatientID		
		AND
			a.PracticeID = @PracticeID
	)	
'

SET @sqlDate = 
      'AND 
      CONVERT(DATE,v.VisitDate) BETWEEN 
      CONVERT(DATE,@FromDate,103) AND CONVERT(DATE,@ToDate,103)'

IF (@ToDate = '') 
SET @ToDate = getdate()


IF (@FromDate <> '')
	BEGIN
		SET @sqlCommand = 'SELECT '  +  @Sql + @sqlDate + ' ORDER BY v.VisitID DESC'
		EXECUTE sp_executesql @sqlCommand, N'@PatientID int,@PracticeID int,@FromDate VARCHAR(30),@ToDate VARCHAR(30)', @PatientID,@PracticeID,@FromDate,@ToDate		
	END
 ELSE
	BEGIN
		SET @sqlCommand = 'SELECT '  + @sqltop5 +   @Sql + ' ORDER BY v.VisitID DESC'
		EXECUTE sp_executesql @sqlCommand, 	N'@PatientID int,@PracticeID int',  @PatientID,@PracticeID		
	END
END
GO
