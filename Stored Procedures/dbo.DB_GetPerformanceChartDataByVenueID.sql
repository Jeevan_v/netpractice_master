SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DB_GetPerformanceChartDataByVenueID] 

@VenueID int
		
AS

BEGIN

SELECT
	
	SUM(AmountPaid) AS Amount ,
	
	SUBSTRING ( DATENAME(MONTH, DateLogged) ,1 , 3 ) AS Months 
	
FROM 
	
	Payment p , 
	
	VISIT v 
	
WHERE 
	
	v.VisitID = p.VisitID 
	
AND 
	
	VenueID = @VenueID
	
--AND

--	v.VisitDate 
	
--BETWEEN
		
--	GETDATE()-120 
	
--AND 

--	GETDATE()	
	
GROUP BY 

	DATENAME(MONTH, DateLogged);
	
END
GO
