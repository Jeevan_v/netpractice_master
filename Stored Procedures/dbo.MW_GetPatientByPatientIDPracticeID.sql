SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[MW_GetPatientByPatientIDPracticeID] 

	@PatientID int

   ,@PracticeID int

AS

IF @PracticeID NOT IN (0)

BEGIN

	SELECT

	   p.[PatientID]

	  ,a.[AccountID]

	  ,a.[PracticeID] 

	  ,p.[MembershipID]

      ,p.[IDNo]

      ,p.[TitleID]

      ,UPPER(LEFT(p.[FirstName],1))+LOWER(SUBSTRING(p.[FirstName],2,LEN(p.[FirstName]))) as [FirstName]

      ,UPPER(LEFT(p.[LastName],1))+LOWER(SUBSTRING(p.[LastName],2,LEN(p.[LastName]))) as [LastName]

      ,p.[Initials]

      ,CONVERT(varchar,p.[DOB],103) as DOB

      ,p.[PhysicalAddressID]

      ,p.[PostalAddressID]

      ,p.[TelNo]

      ,p.[CellNo]

      ,p.[FaxNo]

      ,p.[GenderID]

      ,p.[RaceID]

      ,p.[LanguageID]

      ,p.[DependendNo]

      ,p.[IsDependant]

      ,p.[MainMemberID]

      ,p.[RelationShipID]

      ,p.[ChargeCategoryID]

      ,p.[ChargeTypeID]

      ,p.[PlanID]

      ,p.[SchemeID]

      ,p.[PatientHeight]

      ,p.[PatientWeight]

      ,p.[EmergencyContName]

      ,p.[EmergencyTelNo]

      ,p.[PassportNo]

      ,p.[AllergyTypeID]

      ,p.[MedicalAidNo]

      ,p.[FMCheckDate]

      ,p.[AllergyTypeOther]
	  ,p.patienttype
	  ,p.countryID
	  ,p.companyAddressID
	  ,p.companyName
	  ,p.companyvatno
      ,vf.[FileNo] 

      ,vf.VenueFileID

      ,vf.VenueID

      ,mp.FirstName as MainMemberName   

      ,p.ReasonID  

   FROM Patient p

   Left JOIN

   Account a

   ON p.PatientID=a.PatientID

   Left JOIN

   VenueFile vf

   ON a.AccountID=vf.AccountID

    Left JOIN

   Patient mp

   ON mp.PatientID=p.MainMemberID

   WHERE P.PatientID=@PatientID 

   AND

   a.PracticeID=@PracticeID     

END

ELSE

BEGIN

SELECT

       TOP 1

	   p.[PatientID]

	  ,a.[AccountID]

	  ,a.[PracticeID] 

	  ,p.[MembershipID]

      ,p.[IDNo]

      ,p.[TitleID]

      ,UPPER(LEFT(p.[FirstName],1))+LOWER(SUBSTRING(p.[FirstName],2,LEN(p.[FirstName]))) as [FirstName]

      ,UPPER(LEFT(p.[LastName],1))+LOWER(SUBSTRING(p.[LastName],2,LEN(p.[LastName]))) as [LastName]

      ,p.[Initials]

      ,CONVERT(varchar,p.[DOB],103) as DOB

      ,p.[PhysicalAddressID]

      ,p.[PostalAddressID]

      ,p.[TelNo]

      ,p.[CellNo]

      ,p.[FaxNo]

      ,p.[GenderID]

      ,p.[RaceID]

      ,p.[LanguageID]

      ,p.[DependendNo]

      ,p.[IsDependant]

      ,p.[MainMemberID]

      ,p.[RelationShipID]

      ,p.[ChargeCategoryID]

      ,p.[ChargeTypeID]

      ,p.[PlanID]

      ,p.[SchemeID]

      ,p.[PatientHeight]

      ,p.[PatientWeight]

      ,p.[EmergencyContName]

      ,p.[EmergencyTelNo]

      ,p.[PassportNo]

      ,p.[AllergyTypeID]

      ,p.[MedicalAidNo]

      ,p.[FMCheckDate]

      ,p.[AllergyTypeOther]

	  ,p.patienttype
	  ,p.countryID
	  ,p.companyAddressID
	  ,p.companyName
	  ,p.companyvatno

      ,vf.[FileNo] 

      ,vf.VenueFileID

      ,vf.VenueID

      ,mp.FirstName as MainMemberName     

      ,p.ReasonID  

   FROM Patient p

   Left JOIN

   Account a

   ON p.PatientID=a.PatientID

   Left JOIN

   VenueFile vf

   ON a.AccountID=vf.AccountID

    Left JOIN

   Patient mp

   ON mp.PatientID=p.MainMemberID

   WHERE P.PatientID=@PatientID 

END

GO
