SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[MW_InsertVisitsandClaims]  



 -- Add the parameters for the stored procedure here  



 @VisitID int,  



 @ClaimID int,  



 @accountid int,  



 --@invoiceno int,  



 @venueid int = 0,  



 @chargetypeid int = 0,  



 @chargecatid int = 0,  



 @schemeid int = 0,  



 @planid int = 0,  



 @membershipno varchar(50),  



 @authcode varchar(10),  



 @visitdate varchar(20),  



 @loginID int = 0,  



 @userID int = 0,  



 --@unique_key varchar(80),  



 @total decimal(36,2)  ,
 -- added field for referring doctor practiceno 
 @refDocPracticeNo varchar(50),
 @PlaceOfService int ,
 @BillTo varchar(20),
 @CLAIMSWITCH varchar(50)

AS  



BEGIN  



DECLARE @invoiceno int,@taxamt decimal(36,2)



 --set @taxamt=@total;   

 Declare @refPracticeId int 
 if (@refDocPracticeNo	is not null and @refDocPracticeNo <> '')
  select @refPracticeId = Practiceid from Practice with(nolock) where PracticeNo = @refDocPracticeNo 

if(@VisitID is null or @VisitID ='') and (@ClaimID is null or @ClaimID='' )   



BEGIN  



begin  



  



   set @invoiceno=(select MAX(InvoiceNo)+1 from Visit) 



  



   if(@invoiceno is null)  



   set @invoiceno=1;  



 end  



        



  insert into  Module (DisplayName) values(@total)



INSERT into VISIT (AccountID, venueid, chargetypeid, ChargeCategoryID, schemeid, planid,   



 membershipno, AuthorisationCode, visitdate,datelogged, DoctorID, Invoiceno,RefPracticeNo,ReferringDoctorID, PlaceOfService, BillTo) VALUES   



(@accountid,@venueid,@chargetypeid,@chargecatid,@schemeid,@planid,@membershipno,  



 @authcode,@visitdate,GetDate(),@userID,@invoiceno,@refDocPracticeNo,@refPracticeId,@PlaceOfService, @BillTo)  



SET @visitID = (select SCOPE_IDENTITY())  











INSERT into CLAIM (Visitid,switch,TotalAmount,invoiceno) VALUES   

---(@visitID,'MEDIKREDIT',@total,@invoiceno) 

(@visitID,@CLAIMSWITCH,@total,@invoiceno)  



SET @claimID =(select SCOPE_IDENTITY())  



  



  



END  



ELSE  



BEGIN  



 set  @invoiceno=(select InvoiceNo  from Visit where Visitid=@visitID)  



     



  UPDATE Visit  



  set AccountID=@accountid,  



  venueid=@venueid,  



  chargetypeid=@chargetypeid,  



   ChargeCategoryID=@chargecatid,  



  schemeid=@schemeid,  



  planid=@planid,   



  membershipno=@membershipno,  



  AuthorisationCode=@authcode,  



   visitdate=@visitdate,  



   datelogged=GetDate(),   



   DoctorID=@userID,   



   Invoiceno=@invoiceno  , 
   PlaceOfService = @PlaceOfService,
   RefPracticeNo=@refDocPracticeNo,
   ReferringDoctorID=@refPracticeId



   where VisitID=@VisitID  



     



     

	 

   Update Claim  



   set Visitid=@visitID,  

       

     Totalamount=@total ,

	 AmountPaid=null,

	 status=null,

	 StatusReason=null

     where ClaimID=@ClaimID  



END  



     Select Visit.VisitID,Claim.ClaimID  



    from Visit inner join Claim  



    on Visit.VisitID=Claim.VisitID  



    where Visit.VisitID=@visitID  



END  

GO
