SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CMN_GetAttendingDoctors] 
	@PracticeID int

AS
BEGIN

SELECT DISTINCT
	u.UserID,
	u.FirstName+' '+u.LastName as Name,
	v.Telephone

FROM

dbo.[User] u

JOIN

dbo.aspnet_Users au

ON 

u.MembershipID = au.UserId

JOIN 

aspnet_Membership ams

ON

au.UserId = ams.UserId

JOIN 

dbo.UserVenueRelationship uvr

ON 

uvr.UserID = u.UserID

JOIN 

dbo.Venue v

ON 

uvr.VenueID = v.VenueID

JOIN 

dbo.Practice p

ON 

p.PracticeID = v.PracticeID

JOIN

aspnet_UsersInRoles uir

ON

	au.UserId = uir.UserId

JOIN

	aspnet_Roles ar
ON

	uir.RoleId = ar.RoleId

WHERE

	p.PracticeID= @PracticeID and
	ar.RoleName<>'Finance'
	

END


GO
