SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,Priyanka>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CMN_GetMenu]
	-- Add the parameters for the stored procedure here
	@Rolename nvarchar(256)
AS
BEGIN
	select distinct r.RoleId, m.MenuID, 

m.DisplayName,m.ParentID,ParentM.DisplayName as [ParentMenu]

from PrivilegeRoleRelationship p inner join aspnet_Roles r 

on p.RoleID = r.RoleId 

inner join Privilege pr

on pr.PrivilegeID=p.PrivilegeID

inner join PrivilegeMenuRelationship pm

on pm.privilegeId=pr.PrivilegeID

inner join Menu m

on m.MenuID=pm.menuId

inner join Menu ParentM
on ParentM.MenuID=m.ParentID
where r.RoleName=@Rolename 
END

GO
