SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MP_GetAddressByAddressID]   
      @AddressID int  
AS  
BEGIN  
  
SELECT  
  
 adr.AddressID,  
 adr.[AddressLine1] ,  
 adr.[AddressLine2] ,  
 adr.[ProvinceID],
 pr.Province,
 adr.[City],  
 adr.SubUrb,  
adr.PostalCode
  
   
  
     
   
   
FROM  
  
dbo.Address adr  
  inner join 
  Province pr
  on adr.ProvinceID=pr.ProvinceID
  
WHERE  
  
adr.AddressID= @AddressID  
  
  
END   
GO
