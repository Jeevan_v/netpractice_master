SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sachin Gandhi
-- Create date: 20-01-2015
-- Description:	Check PracticeNo exists
-- =============================================
create PROCEDURE [dbo].[MW_CheckPraticeNo] 
	-- Add the parameters for the stored procedure here
	@PracticeNo varchar(50) = null 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 

    -- Insert statements for procedure here
	 SET NOCOUNT ON;
      IF NOT EXISTS
            (SELECT PracticeName, PracticeNo from Practice with (nolock) where practiceno = @PracticeNo and ReasonID <> 5 )
			SELECT 'true'
	  ELSE
			SELECT 'false'

END
GO
