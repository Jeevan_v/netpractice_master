SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MW_GetDependentsByPatientID] 
		@PatientID int
	   ,@PracticeID int
AS
IF @PracticeID NOT IN (0)
BEGIN
	SELECT
	    p.[PatientID]
	   ,p.IsSuspended 
       ,p.DependendNo+' '+p.[FirstName]+' '+p.LastName AS FirstName
       ,p.[DependendNo]
       ,mainPatient.FirstName+' '+mainPatient.LastName AS MainMemberName
       ,p.MainMemberID AS MainMemberID
   FROM Patient p 
   JOIN Account a
   ON
   p.PatientID=a.PatientID
    Join Patient mainPatient
    ON
    mainPatient.PatientID=p.MainMemberID
   WHERE
   p.MainMemberID =(SELECT MainMemberID FROM Patient WHERE PatientID=@PatientID)
   AND   
   A.PracticeID=@PracticeID   
END
ELSE
BEGIN
	SELECT
	    DISTINCT
	    p.[PatientID]
	   ,p.IsSuspended 
       ,p.DependendNo+' '+p.[FirstName]+' '+p.LastName AS FirstName
       ,p.[DependendNo]
       ,mainPatient.FirstName AS MainMemberName
       ,p.MainMemberID AS MainMemberID
   FROM Patient p 
   JOIN Account a
   ON
   p.PatientID=a.PatientID
   Join Patient mainPatient
    ON
    mainPatient.PatientID=p.MainMemberID
   WHERE
   p.MainMemberID =(SELECT MainMemberID FROM Patient WHERE PatientID=@PatientID)
END
GO
