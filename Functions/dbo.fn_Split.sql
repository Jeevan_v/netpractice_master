SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create  FUNCTION [dbo].[fn_Split](@text VARCHAR(8000))
RETURNS VARCHAR(8000)
AS
BEGIN
DECLARE @index INT 
DECLARE @RowCount INT
DECLARE @delimiter VARCHAR(20)
DECLARE @RowValue VARCHAR(5000)
Declare @CodetypeId VARCHAR(8000)
DECLARE @listStr VARCHAR(MAX)

SET @delimiter = ','
Declare @Strings TABLE
(    
  position int IDENTITY PRIMARY KEY,
  value varchar(8000)   
)


SET @text = LTRIM(RTRIM(@text))

SET @index = -1 
WHILE (LEN(@text) > 0) 
 BEGIN  
    SET @index = CHARINDEX(@delimiter , @text)  
    IF (@index = 0) AND (LEN(@text) > 0)  
      BEGIN   
        INSERT INTO @Strings VALUES (@text)
          BREAK  
      END  
    IF (@index > 1)  
      BEGIN   
        INSERT INTO @Strings VALUES (LEFT(@text, @index - 1))   
        SET @text = RIGHT(@text, (LEN(@text) - @index))  
      END  
    ELSE 
      SET @text = RIGHT(@text, (LEN(@text) - @index)) 
  END
  
SET @RowCount  = (SELECT COUNT(position) FROM   @Strings)
SET @delimiter = '-'
SET @index = -1 
WHILE(@RowCount > 0)  
	BEGIN
	SET @RowValue = (SELECT value FROM @Strings WHERE position = @RowCount)
	SET @index = CHARINDEX(@delimiter , @RowValue)
	
	 IF (@index = 0) AND (LEN(@RowValue) > 0)  
      BEGIN   
		  SELECT @listStr = COALESCE(@listStr+',' ,'') + CONVERT(varchar(50),DiagnosisTypeID) FROM DiagnosisTypeLink Dtl JOIN Diagnosis Dig ON Dig.DiagnosisID = Dtl.DiagnosisID WHERE Dig.[Code] = @RowValue	
          SET @CodetypeId = @listStr
          BREAK  
      END  
    
    IF (@index > 1)  
      BEGIN   
        SELECT @listStr = COALESCE(@listStr+',' ,'') + CONVERT(varchar(50),DiagnosisTypeID) FROM DiagnosisTypeLink Dtl JOIN Diagnosis Dig ON Dig.DiagnosisID = Dtl.DiagnosisID WHERE Dig.[Code] = (LEFT(@RowValue, @index - 1))
        SET @CodetypeId =  @listStr
        SET @RowValue = RIGHT(@text, (LEN(@RowValue) - @index))  
      END  
	 ELSE 
		SET @RowValue = RIGHT(@RowValue, (LEN(@RowValue) - @RowValue)) 	
      
	SET @RowCount = @RowCount -1 
	END
RETURN @CodetypeId
END

GO
