CREATE TABLE [dbo].[AddNotes]
(
[AddNotesID] [int] NOT NULL IDENTITY(1, 1),
[VisitID] [int] NULL,
[ClinicalNotes] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[MedicalReport] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[ReferralLetter] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[SickNotes] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AddNotes] ADD CONSTRAINT [PK_AddNotes] PRIMARY KEY CLUSTERED  ([AddNotesID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AddNotes] ADD CONSTRAINT [FK_AddNotes_Visit1] FOREIGN KEY ([VisitID]) REFERENCES [dbo].[Visit] ([VisitID])
GO
