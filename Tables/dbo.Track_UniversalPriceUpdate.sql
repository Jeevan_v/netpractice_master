CREATE TABLE [dbo].[Track_UniversalPriceUpdate]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ChargeCodeStraightCharge_ID] [int] NOT NULL,
[Old_Price] [decimal] (18, 4) NULL,
[New_Price] [decimal] (18, 4) NULL,
[DateLogged] [datetime] NOT NULL,
[is_New] [bit] NOT NULL
) ON [PRIMARY]
GO
