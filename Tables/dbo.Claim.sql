CREATE TABLE [dbo].[Claim]
(
[ClaimID] [int] NOT NULL IDENTITY(1, 1),
[VisitID] [int] NOT NULL,
[Switch] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SP_BHF] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[TRX_NBR] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[TotalAmount] [decimal] (36, 2) NULL,
[Status] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[StatusReason] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[AmountPaid] [decimal] (36, 2) NULL,
[AmountOverCharged] [decimal] (18, 2) NULL,
[Reference] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[InvoiceNo] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Claim] ADD CONSTRAINT [PK_Claim] PRIMARY KEY CLUSTERED  ([ClaimID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CLAIM_VISITID] ON [dbo].[Claim] ([VisitID]) INCLUDE ([ClaimID], [TotalAmount], [Status], [AmountPaid]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Claim] ADD CONSTRAINT [FK_Visit_Claim] FOREIGN KEY ([VisitID]) REFERENCES [dbo].[Visit] ([VisitID])
GO
