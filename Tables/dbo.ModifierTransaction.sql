CREATE TABLE [dbo].[ModifierTransaction]
(
[ModifiersTransactionID] [int] NULL,
[VisitID] [int] NULL,
[VisitLineID] [int] NULL,
[ModifierType] [nchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FunctionType] [nchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FunctionAmount] [nchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_VISIT_MODIFIERTYPE] ON [dbo].[ModifierTransaction] ([VisitID], [ModifierType]) ON [PRIMARY]
GO
