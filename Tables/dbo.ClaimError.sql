CREATE TABLE [dbo].[ClaimError]
(
[ClaimErrorID] [int] NOT NULL IDENTITY(1, 1),
[ClaimDetailID] [int] NOT NULL,
[ErrorCode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ErrorDesc] [nvarchar] (300) COLLATE Latin1_General_CI_AS NULL,
[ClaimID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimError] ADD CONSTRAINT [PK_ClaimError] PRIMARY KEY CLUSTERED  ([ClaimErrorID]) ON [PRIMARY]
GO
