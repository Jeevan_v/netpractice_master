CREATE TABLE [dbo].[SchemePricing]
(
[SchemePricingID] [int] NOT NULL,
[MedicalSchemeID] [int] NOT NULL,
[ChargeCode] [nchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ShortDescription] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Price] [numeric] (13, 2) NOT NULL,
[IsAurum] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchemePricing] ADD CONSTRAINT [PK_SchemePricing] PRIMARY KEY CLUSTERED  ([SchemePricingID]) ON [PRIMARY]
GO
