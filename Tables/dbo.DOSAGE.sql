CREATE TABLE [dbo].[DOSAGE]
(
[Dosage_ID] [int] NOT NULL,
[Language_ID] [int] NULL,
[Description] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[DosageCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[DescriptionLevel] [int] NULL,
[DosageDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
