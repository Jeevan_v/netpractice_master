CREATE TABLE [dbo].[ChargeCategory]
(
[ChargeCategoryID] [int] NOT NULL IDENTITY(1, 1),
[ChargeCategory] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeCategory] ADD CONSTRAINT [PK_ChargeCategory] PRIMARY KEY CLUSTERED  ([ChargeCategoryID]) ON [PRIMARY]
GO
