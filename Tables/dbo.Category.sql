CREATE TABLE [dbo].[Category]
(
[CategoryID] [int] NOT NULL IDENTITY(1, 1),
[ParentCategoryID] [int] NULL,
[CategoryTypeID] [int] NOT NULL,
[CategoryDesc] [varchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Category] ADD CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED  ([CategoryID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Category] ADD CONSTRAINT [FK_CategoryType_Category] FOREIGN KEY ([CategoryTypeID]) REFERENCES [dbo].[CategoryType] ([CategoryTypeID])
GO
