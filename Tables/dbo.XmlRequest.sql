CREATE TABLE [dbo].[XmlRequest]
(
[XMLREQUESTID] [int] NOT NULL IDENTITY(1, 1),
[CLAIMID] [int] NULL,
[PATIENTID] [int] NULL,
[PRACTICEID] [int] NULL,
[VISITID] [int] NULL,
[ACCOUNTID] [int] NULL,
[TYPEOFREQUEST] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[STATUSOFREQUEST] [int] NULL,
[CLAIMXMLFILEID] [int] NULL,
[XMLFILENAME] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CREATEDDATE] [datetime] NULL CONSTRAINT [DF_XmlRequest_CREATEDDATE] DEFAULT (getdate()),
[UPDATEDDATE] [datetime] NULL,
[ISREPROCESSREQUIRED] [bit] NULL CONSTRAINT [DF_XmlRequest_ISREPROCESSREQUIRED] DEFAULT ((0)),
[REMARKS] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[REPROCESSINGDATE] [datetime] NULL,
[CLAIMSWITCH] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[XmlRequest] ADD CONSTRAINT [PK_XmlRequest] PRIMARY KEY CLUSTERED  ([XMLREQUESTID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key from Account Table', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'ACCOUNTID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key from claim table.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'CLAIMID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is foreign key from claim xml table.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'CLAIMXMLFILEID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Created date of xml request', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'CREATEDDATE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'In case of xml generation failed then we can reprocess it by setting this flag to true for xml file.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'ISREPROCESSREQUIRED'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key from patient table.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'PATIENTID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This column will be used for setting any remarks for xml files status.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'REMARKS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of reprocessing.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'REPROCESSINGDATE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of xml request. It has folloing statuses : 1. In-Process, 2. Success, 3. Failure', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'STATUSOFREQUEST'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of request could be of following type : NEW ,REVERSAL, FMC etc.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'TYPEOFREQUEST'
GO
EXEC sp_addextendedproperty N'MS_Description', N'updated date of the request', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'UPDATEDDATE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key from VISIT table ', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'VISITID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This column will store the name of sml request file name.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'XMLFILENAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is primary key for xml request table. It will be auto increment column.', 'SCHEMA', N'dbo', 'TABLE', N'XmlRequest', 'COLUMN', N'XMLREQUESTID'
GO
