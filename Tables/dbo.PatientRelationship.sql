CREATE TABLE [dbo].[PatientRelationship]
(
[RelationshipID] [int] NOT NULL IDENTITY(1, 1),
[Relatiohship] [nchar] (25) COLLATE Latin1_General_CI_AS NULL,
[Order] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientRelationship] ADD CONSTRAINT [PK_PatientRelationship] PRIMARY KEY CLUSTERED  ([RelationshipID]) ON [PRIMARY]
GO
