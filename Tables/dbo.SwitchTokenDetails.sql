CREATE TABLE [dbo].[SwitchTokenDetails]
(
[SwitchTokenId] [int] NOT NULL IDENTITY(1, 1),
[SwitchName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TokenValue] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[TokenExpiresIn] [datetime] NULL,
[TokenType] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[DtIssued] [datetime] NULL,
[DtExpire] [datetime] NULL,
[Sessionid] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[UserId] [int] NULL,
[IsActive] [bit] NULL,
[Remarks] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_SwitchTokenDetails_CreatedDate] DEFAULT (getdate()),
[UpdatedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SwitchTokenDetails] ADD CONSTRAINT [PK_SwitchTokenDetails] PRIMARY KEY CLUSTERED  ([SwitchTokenId]) ON [PRIMARY]
GO
