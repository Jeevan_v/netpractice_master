CREATE TABLE [dbo].[T_PractitionerType]
(
[PractitionerType_ID] [int] NOT NULL IDENTITY(0, 1),
[PractitionerType] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[BHFNo] [nvarchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[SubDiscipline] [nvarchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[PractitionerGroup_ID] [int] NOT NULL,
[AddPrefix] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_PractitionerType] ADD CONSTRAINT [PK_MEPractitionerType] PRIMARY KEY CLUSTERED  ([PractitionerType_ID]) ON [PRIMARY]
GO
