CREATE TABLE [dbo].[Gender]
(
[GenderID] [int] NOT NULL IDENTITY(1, 1),
[Gender] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Gender] ADD CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED  ([GenderID]) ON [PRIMARY]
GO
