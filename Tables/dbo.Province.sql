CREATE TABLE [dbo].[Province]
(
[ProvinceID] [int] NOT NULL IDENTITY(1, 1),
[Province] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Province] ADD CONSTRAINT [PK_Province] PRIMARY KEY CLUSTERED  ([ProvinceID]) ON [PRIMARY]
GO
