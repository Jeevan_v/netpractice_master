CREATE TABLE [dbo].[PracticeType]
(
[PracticeTypeID] [int] NOT NULL IDENTITY(0, 1),
[PracticeType] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[BHFNo] [nvarchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[SubDiscipline] [nvarchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[PractitionerGroup_ID] [int] NOT NULL,
[AddPrefix] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PracticeType] ADD CONSTRAINT [PK_MEPracticeType] PRIMARY KEY CLUSTERED  ([PracticeTypeID]) ON [PRIMARY]
GO
