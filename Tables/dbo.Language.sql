CREATE TABLE [dbo].[Language]
(
[LanguageID] [int] NOT NULL IDENTITY(1, 1),
[Language] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Language] ADD CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED  ([LanguageID]) ON [PRIMARY]
GO
