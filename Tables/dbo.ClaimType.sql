CREATE TABLE [dbo].[ClaimType]
(
[ClaimTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClaimType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimType] ADD CONSTRAINT [PK_ClaimType] PRIMARY KEY CLUSTERED  ([ClaimTypeID]) ON [PRIMARY]
GO
