CREATE TABLE [dbo].[ModifierRules]
(
[ModifierRuleID] [int] NOT NULL IDENTITY(1, 1),
[ChargeCodeID] [int] NOT NULL,
[ModifierTypeID] [int] NOT NULL,
[Percentage] [decimal] (9, 2) NULL,
[PercentageBoundry] [decimal] (18, 2) NULL,
[AfterBoundryExceed] [decimal] (18, 2) NULL,
[Units] [decimal] (18, 2) NULL,
[TimeIntervalStart] [int] NULL,
[TimeIntervalEnd] [int] NULL,
[TimedUnits] [int] NULL,
[TimedUnitPrice] [decimal] (18, 2) NULL,
[UnitPrice] [decimal] (18, 2) NULL,
[InActive] [bit] NOT NULL,
[DateLogged] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModifierRules] ADD CONSTRAINT [PK_ModifierRules] PRIMARY KEY CLUSTERED  ([ModifierRuleID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModifierRules] ADD CONSTRAINT [FK_ModifierRules_ChargeCode] FOREIGN KEY ([ChargeCodeID]) REFERENCES [dbo].[ChargeCode] ([ChargeCodeID])
GO
ALTER TABLE [dbo].[ModifierRules] ADD CONSTRAINT [FK_ModifierRules_ModifierType] FOREIGN KEY ([ModifierTypeID]) REFERENCES [dbo].[ModifierType] ([ModifierTypeID])
GO
