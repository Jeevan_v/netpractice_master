CREATE TABLE [dbo].[SchemeCode]
(
[SchemeCodeID] [int] NOT NULL,
[MedicalSchemeID] [int] NULL,
[SchemeCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchemeCode] ADD CONSTRAINT [PK_SchemeCode] PRIMARY KEY CLUSTERED  ([SchemeCodeID]) ON [PRIMARY]
GO
