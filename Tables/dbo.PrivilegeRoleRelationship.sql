CREATE TABLE [dbo].[PrivilegeRoleRelationship]
(
[PrivilegeID] [int] NOT NULL,
[RoleID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PrivilegeRoleRelationship] ADD CONSTRAINT [PK_PrivilegeRoleRelationship] PRIMARY KEY CLUSTERED  ([PrivilegeID], [RoleID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PrivilegeRoleRelationship] ADD CONSTRAINT [FK_aspnetRoles_PrivilegeRoleRelationship] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[PrivilegeRoleRelationship] ADD CONSTRAINT [FK_Privilege_PrivilegeRoleRelationship] FOREIGN KEY ([PrivilegeID]) REFERENCES [dbo].[Privilege] ([PrivilegeID])
GO
