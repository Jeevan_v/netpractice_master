CREATE TABLE [dbo].[PracticePackageRelationship]
(
[PracticeId] [int] NOT NULL,
[PackageId] [int] NOT NULL,
[Subscribed] [bit] NULL,
[Approved] [bit] NULL,
[Rejected_Reason] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Date_Approved] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PracticePackageRelationship] ADD CONSTRAINT [FK_PracticePackageRelationship_Package] FOREIGN KEY ([PackageId]) REFERENCES [dbo].[Package] ([PackageID])
GO
ALTER TABLE [dbo].[PracticePackageRelationship] ADD CONSTRAINT [FK_PracticePackageRelationship_Practice] FOREIGN KEY ([PracticeId]) REFERENCES [dbo].[Practice] ([PracticeID])
GO
