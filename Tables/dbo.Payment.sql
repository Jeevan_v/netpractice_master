CREATE TABLE [dbo].[Payment]
(
[PaymentID] [int] NOT NULL IDENTITY(1, 1),
[VisitID] [int] NOT NULL,
[AmountPaid] [decimal] (18, 2) NULL,
[Method] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[ReceiptNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[UserID] [int] NULL,
[Date] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED  ([PaymentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_PAYMENT_VISITID] ON [dbo].[Payment] ([VisitID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_User_Payment] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_Visit_Payment] FOREIGN KEY ([VisitID]) REFERENCES [dbo].[Visit] ([VisitID])
GO
