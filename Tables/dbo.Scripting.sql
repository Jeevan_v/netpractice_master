CREATE TABLE [dbo].[Scripting]
(
[ScriptingID] [int] NOT NULL IDENTITY(1, 1),
[VisitID] [int] NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ItemNo] [int] NULL,
[DosageDetails] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Scripting] ADD CONSTRAINT [PK_Scripting] PRIMARY KEY CLUSTERED  ([ScriptingID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Scripting] ADD CONSTRAINT [FK_Scripting_Visit] FOREIGN KEY ([VisitID]) REFERENCES [dbo].[Visit] ([VisitID])
GO
