CREATE TABLE [dbo].[DiagnosisInVisitLine]
(
[VisitLineID] [int] NOT NULL,
[DiagnosisID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisInVisitLine] ADD CONSTRAINT [PK_DiagnosisInVisitLine] PRIMARY KEY CLUSTERED  ([VisitLineID], [DiagnosisID]) ON [PRIMARY]
GO
