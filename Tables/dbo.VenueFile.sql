CREATE TABLE [dbo].[VenueFile]
(
[VenueFileID] [int] NOT NULL IDENTITY(1, 1),
[VenueID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[FileNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VenueFile] ADD CONSTRAINT [PK_VenueFile] PRIMARY KEY CLUSTERED  ([VenueFileID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_VENUE_ACCOUNTID] ON [dbo].[VenueFile] ([AccountID]) INCLUDE ([FileNo]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VenueFile] ADD CONSTRAINT [FK_Account_VenueFile] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[VenueFile] ADD CONSTRAINT [FK_Venue_VenueFile] FOREIGN KEY ([VenueID]) REFERENCES [dbo].[Venue] ([VenueID])
GO
