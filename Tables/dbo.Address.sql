CREATE TABLE [dbo].[Address]
(
[AddressID] [int] NOT NULL IDENTITY(1, 1),
[AddressLine1] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AddressLine2] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[SubUrb] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[City] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Province] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PostalCode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ProvinceID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address] ADD CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED  ([AddressID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address] ADD CONSTRAINT [FK_Address_Province] FOREIGN KEY ([ProvinceID]) REFERENCES [dbo].[Province] ([ProvinceID])
GO
