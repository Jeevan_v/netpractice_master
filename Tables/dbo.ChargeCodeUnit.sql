CREATE TABLE [dbo].[ChargeCodeUnit]
(
[ChargeCodeUnitID] [int] NOT NULL IDENTITY(1, 1),
[ChargeCodeID] [int] NOT NULL,
[ChargeUnitTypeID] [int] NOT NULL,
[ChargeUnits] [numeric] (13, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeCodeUnit] ADD CONSTRAINT [PK_ChargeCodeUnit] PRIMARY KEY CLUSTERED  ([ChargeCodeUnitID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeCodeUnit] ADD CONSTRAINT [FK_ChargeCode_ChargeCodeUnit] FOREIGN KEY ([ChargeCodeID]) REFERENCES [dbo].[ChargeCode] ([ChargeCodeID])
GO
ALTER TABLE [dbo].[ChargeCodeUnit] ADD CONSTRAINT [FK_ChargeCodeUnit_ChargeCodeUnit] FOREIGN KEY ([ChargeCodeUnitID]) REFERENCES [dbo].[ChargeCodeUnit] ([ChargeCodeUnitID])
GO
