CREATE TABLE [dbo].[MailToMedikreditStatus]
(
[PracticeID] [int] NOT NULL,
[EmailStatus] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[DateLogged] [datetime] NULL CONSTRAINT [DF_MailSendStatus_DateLogged] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MailToMedikreditStatus] ADD CONSTRAINT [PK_MailSendStatus] PRIMARY KEY CLUSTERED  ([PracticeID]) ON [PRIMARY]
GO
