CREATE TABLE [dbo].[UserVenueRelationship]
(
[UserID] [int] NOT NULL,
[VenueID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserVenueRelationship] ADD CONSTRAINT [PK_UserVenueRelationship] PRIMARY KEY CLUSTERED  ([UserID], [VenueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserVenueRelationship] ADD CONSTRAINT [FK_User_UserVenueRelationship] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[UserVenueRelationship] ADD CONSTRAINT [FK_Venue_UserVenueRelationship] FOREIGN KEY ([VenueID]) REFERENCES [dbo].[Venue] ([VenueID])
GO
