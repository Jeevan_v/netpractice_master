CREATE TABLE [dbo].[Diagnosis]
(
[DiagnosisID] [int] NOT NULL,
[DiagnosisCategoryID] [int] NULL,
[Code] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[CodeDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LongDescription] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[MenuCategoryID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Diagnosis] ADD CONSTRAINT [PK_Diagnosis11] PRIMARY KEY CLUSTERED  ([DiagnosisID]) ON [PRIMARY]
GO
