CREATE TABLE [dbo].[MFOptionLinkCode]
(
[MFOptionLinkCodeID] [int] NOT NULL IDENTITY(1, 1),
[MFCodeTypeID] [int] NOT NULL,
[MFOptionLinkID] [int] NOT NULL,
[MFOptionLinkCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[IsDefault] [bit] NULL,
[Active] [bit] NULL,
[IsFMcheckEnabled] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MFOptionLinkCode] ADD CONSTRAINT [PK_MFOptionLinkCode] PRIMARY KEY CLUSTERED  ([MFOptionLinkCodeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MFOptionLinkCode] ADD CONSTRAINT [FK_MFCodeType_MFOptionLinkCode] FOREIGN KEY ([MFCodeTypeID]) REFERENCES [dbo].[MFCodeType] ([MFCodeTypeID])
GO
ALTER TABLE [dbo].[MFOptionLinkCode] ADD CONSTRAINT [FK_MFOptionLink_MFOptionLinkCode] FOREIGN KEY ([MFOptionLinkID]) REFERENCES [dbo].[MFOptionLink] ([MFOptionLinkID])
GO
