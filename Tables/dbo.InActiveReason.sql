CREATE TABLE [dbo].[InActiveReason]
(
[ReasonID] [int] NOT NULL IDENTITY(1, 1),
[Reason] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ReasonCategory] [varchar] (200) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InActiveReason] ADD CONSTRAINT [PK_InActiveReason] PRIMARY KEY CLUSTERED  ([ReasonID]) ON [PRIMARY]
GO
