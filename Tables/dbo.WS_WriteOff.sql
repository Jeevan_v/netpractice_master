CREATE TABLE [dbo].[WS_WriteOff]
(
[WriteOffID] [int] NOT NULL IDENTITY(1, 1),
[VisitID] [int] NOT NULL,
[WriteOffAmount] [decimal] (18, 2) NULL,
[ReceiptNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[UserID] [int] NULL,
[Date] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WS_WriteOff] ADD CONSTRAINT [PK_WriteOff] PRIMARY KEY CLUSTERED  ([WriteOffID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WS_WriteOff] WITH NOCHECK ADD CONSTRAINT [FK_User_WriteOff] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
GO
