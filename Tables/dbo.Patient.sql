CREATE TABLE [dbo].[Patient]
(
[PatientID] [int] NOT NULL IDENTITY(1, 1),
[MembershipID] [uniqueidentifier] NULL,
[IDNo] [varchar] (25) COLLATE Latin1_General_CI_AS NULL,
[TitleID] [int] NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Initials] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[DOB] [date] NULL,
[PhysicalAddressID] [int] NOT NULL,
[PostalAddressID] [int] NULL,
[TelNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[CellNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[FaxNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[GenderID] [int] NULL,
[RaceID] [int] NULL,
[LanguageID] [int] NULL,
[DependendNo] [varchar] (3) COLLATE Latin1_General_CI_AS NULL,
[IsDependant] [bit] NULL,
[MainMemberID] [int] NULL,
[RelationShipID] [int] NULL,
[ChargeCategoryID] [int] NULL,
[ChargeTypeID] [int] NULL,
[PlanID] [int] NULL,
[SchemeID] [int] NULL,
[PatientHeight] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[PatientWeight] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[EmergencyContName] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[EmergencyTelNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[PassportNo] [varchar] (25) COLLATE Latin1_General_CI_AS NULL,
[AllergyTypeID] [int] NULL,
[MedicalAidNo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FMCheckDate] [date] NULL,
[AllergyTypeOther] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[IsSuspended] [bit] NULL,
[ReasonID] [int] NULL,
[CountryID] [int] NULL,
[PatientType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CompanyAddressID] [int] NULL,
[CompanyVATNo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED  ([PatientID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_aspnetUsers_Patient] FOREIGN KEY ([MembershipID]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_ChargeCategory_Patient] FOREIGN KEY ([ChargeCategoryID]) REFERENCES [dbo].[ChargeCategory] ([ChargeCategoryID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Gender_Patient] FOREIGN KEY ([GenderID]) REFERENCES [dbo].[Gender] ([GenderID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Language_Patient] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Patient_Allergy] FOREIGN KEY ([AllergyTypeID]) REFERENCES [dbo].[AllergyType] ([AllergyTypeID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Patient_PhysicalAddress] FOREIGN KEY ([PhysicalAddressID]) REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Patient_PostalAddress] FOREIGN KEY ([PostalAddressID]) REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Patient_Reason] FOREIGN KEY ([ReasonID]) REFERENCES [dbo].[InActiveReason] ([ReasonID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_PatientRelationship_Patient] FOREIGN KEY ([RelationShipID]) REFERENCES [dbo].[PatientRelationship] ([RelationshipID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Race_Patient] FOREIGN KEY ([RaceID]) REFERENCES [dbo].[Race] ([RaceID])
GO
ALTER TABLE [dbo].[Patient] ADD CONSTRAINT [FK_Title_Patient] FOREIGN KEY ([TitleID]) REFERENCES [dbo].[Title] ([TitleID])
GO
