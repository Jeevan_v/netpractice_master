CREATE TABLE [dbo].[DiagnosisTypeLink]
(
[DiagnosisTypeLinkID] [int] NOT NULL,
[DiagnosisID] [int] NOT NULL,
[DiagnosisTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisTypeLink] ADD CONSTRAINT [PK_DiagnosisTypeLink11] PRIMARY KEY CLUSTERED  ([DiagnosisTypeLinkID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_DIAGNOSISTYPE_LINK_DIAGNOSISID] ON [dbo].[DiagnosisTypeLink] ([DiagnosisTypeID]) INCLUDE ([DiagnosisID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisTypeLink] WITH NOCHECK ADD CONSTRAINT [FK_DiagnosisTypeLink_Diagnosis2] FOREIGN KEY ([DiagnosisID]) REFERENCES [dbo].[Diagnosis] ([DiagnosisID])
GO
ALTER TABLE [dbo].[DiagnosisTypeLink] WITH NOCHECK ADD CONSTRAINT [FK_DiagnosisTypeLink_DiagnosisType2] FOREIGN KEY ([DiagnosisTypeID]) REFERENCES [dbo].[DiagnosisType] ([DiagnosisTypeID])
GO
