CREATE TABLE [dbo].[Practice]
(
[PracticeID] [int] NOT NULL IDENTITY(1, 1),
[PracticeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PracticeNo] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[VatNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PracticeTypeID] [int] NULL,
[AddVat] [bit] NULL,
[IsActive] [bit] NULL,
[ExtraInfo] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PackageID] [int] NULL,
[IsAurum] [bit] NULL,
[HasAurumNurse] [bit] NULL,
[ReasonID] [int] NULL,
[BankID] [int] NULL,
[BankNameOther] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[BranchCode] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[BranchName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[AccountNumber] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[PaymentDurationID] [int] NULL,
[RegistrationDate] [datetime] NULL CONSTRAINT [DF_Practice_RegistrationDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Practice] ADD CONSTRAINT [PK_Practice] PRIMARY KEY CLUSTERED  ([PracticeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Practice] ADD CONSTRAINT [FK_BankID_Bank] FOREIGN KEY ([BankID]) REFERENCES [dbo].[Bank] ([BankID])
GO
ALTER TABLE [dbo].[Practice] ADD CONSTRAINT [FK_Practice_InActiveReason] FOREIGN KEY ([ReasonID]) REFERENCES [dbo].[InActiveReason] ([ReasonID])
GO
ALTER TABLE [dbo].[Practice] ADD CONSTRAINT [FK_Practice_Practice] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
GO
