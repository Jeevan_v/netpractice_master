CREATE TABLE [dbo].[JobTitle]
(
[JobTitleID] [int] NOT NULL IDENTITY(1, 1),
[JobTitleName] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JobTitle] ADD CONSTRAINT [PK_JobTitle] PRIMARY KEY CLUSTERED  ([JobTitleID]) ON [PRIMARY]
GO
