CREATE TABLE [dbo].[StockCode]
(
[StockCodeID] [int] NOT NULL IDENTITY(1, 1),
[StockItemID] [int] NULL,
[StockCodeTypeID] [int] NULL,
[Code] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[PackSize] [decimal] (9, 2) NULL,
[PackPrice] [decimal] (9, 3) NULL,
[UnitPrice] [decimal] (9, 3) NULL,
[Discontinued] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StockCode] ADD CONSTRAINT [PK_StockCode] PRIMARY KEY CLUSTERED  ([StockCodeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCK_CODE_ID] ON [dbo].[StockCode] ([Code]) INCLUDE ([StockCodeID], [Discontinued]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKCODE_DISCONTINUE] ON [dbo].[StockCode] ([Discontinued]) INCLUDE ([StockItemID], [StockCodeTypeID], [Code], [PackPrice], [UnitPrice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKCODE_DISCOUNT_CODE] ON [dbo].[StockCode] ([Discontinued], [Code]) INCLUDE ([StockItemID], [StockCodeTypeID], [PackPrice], [UnitPrice]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKCODE_STOCKITEMID] ON [dbo].[StockCode] ([StockCodeTypeID], [Code]) INCLUDE ([StockItemID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKCODE_PACKSIZE_CODE] ON [dbo].[StockCode] ([StockCodeTypeID], [PackSize]) INCLUDE ([Code]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKCODE_STOCKCODEID] ON [dbo].[StockCode] ([StockCodeTypeID], [PackSize], [Discontinued]) INCLUDE ([StockCodeID], [Code]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKCODE_CODEID_PACKSIZE_DIS] ON [dbo].[StockCode] ([StockCodeTypeID], [PackSize], [Discontinued]) INCLUDE ([StockCodeID], [StockItemID], [Code]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKCODE_PACKPRICE_UNITPRIC] ON [dbo].[StockCode] ([StockItemID], [Discontinued]) INCLUDE ([StockCodeTypeID], [Code], [PackPrice], [UnitPrice]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StockCode] ADD CONSTRAINT [FK_StockCodeType_StockCode] FOREIGN KEY ([StockCodeTypeID]) REFERENCES [dbo].[StockCodeType] ([StockCodeTypeID])
GO
ALTER TABLE [dbo].[StockCode] ADD CONSTRAINT [FK_StockItem_StockCode] FOREIGN KEY ([StockItemID]) REFERENCES [dbo].[StockItem] ([StockItemID])
GO
