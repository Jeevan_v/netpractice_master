CREATE TABLE [dbo].[ClaimDetail]
(
[ClaimDetailID] [int] NOT NULL IDENTITY(1, 1),
[ClaimID] [int] NOT NULL,
[VisitLineID] [int] NULL,
[NappiCode] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[Status] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[AmountPaid] [decimal] (36, 2) NULL,
[AmountOverCharged] [decimal] (18, 2) NULL,
[Auth_HNET] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimDetail] ADD CONSTRAINT [PK_ClaimDetail] PRIMARY KEY CLUSTERED  ([ClaimDetailID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_CLAIMDETAIL_CLAIMID_STATUS] ON [dbo].[ClaimDetail] ([ClaimID], [Status]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [CLAIMDETAIL_VISITLINE] ON [dbo].[ClaimDetail] ([VisitLineID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimDetail] ADD CONSTRAINT [FK_Claim_ClaimDetail] FOREIGN KEY ([ClaimID]) REFERENCES [dbo].[Claim] ([ClaimID])
GO
ALTER TABLE [dbo].[ClaimDetail] ADD CONSTRAINT [FK_VisitLine_ClaimDetail] FOREIGN KEY ([VisitLineID]) REFERENCES [dbo].[VisitLine] ([VisitLineID])
GO
