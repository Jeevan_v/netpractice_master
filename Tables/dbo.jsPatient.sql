CREATE TABLE [dbo].[jsPatient]
(
[patientid] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ dob] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ idnumber] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ surname] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ fullnames] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ initials] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ title] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ gender] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ language] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
