CREATE TABLE [dbo].[DiagnosisTypeLink_bk]
(
[DiagnosisTypeLinkID] [int] NOT NULL IDENTITY(1, 1),
[DiagnosisID] [int] NOT NULL,
[DiagnosisTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisTypeLink_bk] ADD CONSTRAINT [PK_DiagnosisTypeLink] PRIMARY KEY CLUSTERED  ([DiagnosisTypeLinkID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisTypeLink_bk] ADD CONSTRAINT [FK_DiagnosisTypeLink_Diagnosis] FOREIGN KEY ([DiagnosisID]) REFERENCES [dbo].[Diagnosis_BK] ([DiagnosisID])
GO
ALTER TABLE [dbo].[DiagnosisTypeLink_bk] ADD CONSTRAINT [FK_DiagnosisTypeLink_DiagnosisType] FOREIGN KEY ([DiagnosisTypeID]) REFERENCES [dbo].[DiagnosisType] ([DiagnosisTypeID])
GO
