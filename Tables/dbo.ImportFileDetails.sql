CREATE TABLE [dbo].[ImportFileDetails]
(
[FileId] [int] NOT NULL IDENTITY(1, 1),
[FileName] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[ProcessDate] [datetime] NULL,
[Status] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[FileType] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[TotalRows] [int] NULL,
[TotalSheets] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImportFileDetails] ADD CONSTRAINT [PK_ImportFileDetails] PRIMARY KEY CLUSTERED  ([FileId]) ON [PRIMARY]
GO
