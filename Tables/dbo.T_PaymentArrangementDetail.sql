CREATE TABLE [dbo].[T_PaymentArrangementDetail]
(
[PaymentArrangementDetail_ID] [int] NOT NULL IDENTITY(0, 1),
[PaymentArrangement_ID] [int] NULL,
[LocationType_ID] [int] NULL,
[DetailCode] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[LongDescription] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[MastermedLink] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MedadLink] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DateValidFrom] [datetime] NOT NULL,
[DateValidTo] [datetime] NOT NULL,
[IsStandardExcluded] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_PaymentArrangementDetail] ADD CONSTRAINT [PK_MEPaymentArrangementDetail] PRIMARY KEY CLUSTERED  ([PaymentArrangementDetail_ID]) ON [PRIMARY]
GO
