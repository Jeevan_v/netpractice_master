CREATE TABLE [dbo].[Account]
(
[AccountID] [int] NOT NULL IDENTITY(1, 1),
[AccountNumber] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[PracticeID] [int] NOT NULL,
[PatientID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED  ([AccountID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_Account_Practice] ON [dbo].[Account] ([PracticeID]) INCLUDE ([AccountID], [PatientID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Account] WITH NOCHECK ADD CONSTRAINT [FK_Patient_Account] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[Patient] ([PatientID])
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [FK_Practice_Account] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
GO
ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT [FK_Patient_Account]
GO
