CREATE TABLE [dbo].[TempTableNappiCode]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[NappiCodeWithSuffix] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[PackSize] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[PackPrice] [varchar] (800) COLLATE Latin1_General_CI_AS NULL,
[Schedule] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[NappiCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_TMP_NAPPI_CODE] ON [dbo].[TempTableNappiCode] ([NappiCode]) INCLUDE ([NappiCodeWithSuffix], [Description], [PackSize], [PackPrice], [Schedule]) ON [PRIMARY]
GO
