CREATE TABLE [dbo].[CountryProvinceRelationship]
(
[CountryID] [int] NOT NULL,
[ProvinceID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CountryProvinceRelationship] ADD CONSTRAINT [PK_CountryProvinceRelationship] PRIMARY KEY CLUSTERED  ([CountryID], [ProvinceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CountryProvinceRelationship] ADD CONSTRAINT [FK_CountryProvinceRelationship_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[CountryProvinceRelationship] ADD CONSTRAINT [FK_CountryProvinceRelationship_Province] FOREIGN KEY ([ProvinceID]) REFERENCES [dbo].[Province] ([ProvinceID])
GO
