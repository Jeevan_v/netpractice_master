CREATE TABLE [dbo].[Country]
(
[CountryID] [int] NOT NULL IDENTITY(1, 1),
[CountryCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[CountryName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED  ([CountryID]) ON [PRIMARY]
GO
