CREATE TABLE [dbo].[T_ChargeCode]
(
[ChargeCode_ID] [int] NOT NULL IDENTITY(89273, 1),
[PaymentArrangementGroup_ID] [int] NOT NULL,
[PractitionerGroup_ID] [int] NOT NULL,
[ChargeCode] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[CodeType] [nvarchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[CodeGroup_ID] [int] NOT NULL,
[CodeGroupDetail_ID] [int] NOT NULL,
[ShortDescription] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[LongDescription] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NOT NULL,
[Section] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[DateValidFrom] [datetime] NOT NULL,
[DateValidTo] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_ChargeCode] ADD CONSTRAINT [PK_MEChargeCode] PRIMARY KEY CLUSTERED  ([ChargeCode_ID]) ON [PRIMARY]
GO
