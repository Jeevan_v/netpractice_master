CREATE TABLE [dbo].[ChargeGroup]
(
[ChargeGroupID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NOT NULL,
[GroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ChargeGroupDefiinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LongDescription] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeGroup] ADD CONSTRAINT [PK_ChargeGroup] PRIMARY KEY CLUSTERED  ([ChargeGroupID]) ON [PRIMARY]
GO
