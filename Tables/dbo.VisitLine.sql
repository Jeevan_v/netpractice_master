CREATE TABLE [dbo].[VisitLine]
(
[VisitLineID] [int] NOT NULL IDENTITY(1, 1),
[VisitID] [int] NOT NULL,
[Quantity] [int] NULL,
[Amount] [decimal] (36, 2) NULL,
[LineDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DispFee] [decimal] (18, 2) NULL,
[NoDays] [int] NULL,
[IsChronic] [bit] NULL,
[Code] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Diagnosis] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (8000) COLLATE Latin1_General_CI_AS NULL,
[DosageID] [int] NULL,
[DailyDosage] [numeric] (6, 2) NULL,
[Starttime] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Endtime] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[TaxAmount] [decimal] (36, 2) NULL,
[IsModifier] [bit] NULL,
[AssistantPracticeNo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AssistantMpNo] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[ModifierAppliedCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisitLine] ADD CONSTRAINT [PK_VisitLine] PRIMARY KEY CLUSTERED  ([VisitLineID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_VISITLINE_AMOUNT] ON [dbo].[VisitLine] ([VisitID]) INCLUDE ([Amount]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisitLine] ADD CONSTRAINT [FK_Visit_VisitLine] FOREIGN KEY ([VisitID]) REFERENCES [dbo].[Visit] ([VisitID])
GO
