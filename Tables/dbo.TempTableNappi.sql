CREATE TABLE [dbo].[TempTableNappi]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Nappi] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[IsMedMat] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ProductStrength] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DosageFormCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
