CREATE TABLE [dbo].[MFCodeType]
(
[MFCodeTypeID] [int] NOT NULL IDENTITY(1, 1),
[AccountGroupID] [int] NULL,
[CodeTypeDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[MFProviderCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Submit] [bit] NULL,
[LastDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MFCodeType] ADD CONSTRAINT [PK_MFCodeType] PRIMARY KEY CLUSTERED  ([MFCodeTypeID]) ON [PRIMARY]
GO
