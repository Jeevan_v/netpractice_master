CREATE TABLE [dbo].[ChargeCode]
(
[ChargeCodeID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NULL,
[ChargeGroupID] [int] NULL,
[TypeID] [int] NOT NULL,
[CodeGroupID] [int] NOT NULL,
[ChargeCode] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Type] [char] (2) COLLATE Latin1_General_CI_AS NULL,
[ShortDescription] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[LongDescription] [nvarchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[Variation] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PaymentArrangementGroup_ID] [int] NULL,
[CodeType] [nvarchar] (5) COLLATE Latin1_General_CI_AS NULL,
[CodeGroupDetail_ID] [int] NULL,
[Section] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[DateValidFrom] [datetime] NULL,
[DateValidTo] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeCode] ADD CONSTRAINT [PK_ChargeCode] PRIMARY KEY CLUSTERED  ([ChargeCodeID]) ON [PRIMARY]
GO
