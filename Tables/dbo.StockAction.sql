CREATE TABLE [dbo].[StockAction]
(
[StockActionID] [int] NOT NULL IDENTITY(1, 1),
[Definition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StockAction] ADD CONSTRAINT [PK_StockAction] PRIMARY KEY CLUSTERED  ([StockActionID]) ON [PRIMARY]
GO
