CREATE TABLE [dbo].[Log_Automail]
(
[Autokey] [int] NOT NULL IDENTITY(1, 1),
[Mail_Date] [datetime] NULL,
[IsSuccess] [int] NULL,
[Log_Comments] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
