CREATE TABLE [guest].[StoredProcedureRunDetails]
(
[name] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[timestamp] [datetimeoffset] NULL,
[timestamp (UTC)] [datetimeoffset] NULL,
[cpu_time] [decimal] (20, 0) NULL,
[duration] [decimal] (20, 0) NULL,
[physical_reads] [decimal] (20, 0) NULL,
[logical_reads] [decimal] (20, 0) NULL,
[writes] [decimal] (20, 0) NULL,
[result] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[row_count] [decimal] (20, 0) NULL,
[connection_reset_option] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[object_name] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[statement] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[data_stream] [varbinary] (max) NULL,
[output_parameters] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
