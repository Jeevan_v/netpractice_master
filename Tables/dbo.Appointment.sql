CREATE TABLE [dbo].[Appointment]
(
[AppointmentID] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Date] [date] NOT NULL,
[StartTime] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EndTime] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CreatedBy] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CreatorID] [int] NOT NULL,
[AppointmentStatus] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IsBooked] [bit] NOT NULL,
[CreatedTime] [datetime] NOT NULL,
[VenueID] [int] NOT NULL,
[DoctorID] [int] NOT NULL,
[PatientID] [int] NOT NULL,
[RescheduleUserID] [int] NULL,
[RescheduledTime] [datetime] NULL,
[ReschedulePatientID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [PK_Appointment] PRIMARY KEY CLUSTERED  ([AppointmentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [FK_Appointment_PatientByPatientID] FOREIGN KEY ([PatientID]) REFERENCES [dbo].[Patient] ([PatientID])
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [FK_Appointment_PatientByReschedulePatientID] FOREIGN KEY ([ReschedulePatientID]) REFERENCES [dbo].[Patient] ([PatientID])
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [FK_Appointment_UserByDoctorID] FOREIGN KEY ([DoctorID]) REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [FK_Appointment_UserByReschedulerID] FOREIGN KEY ([RescheduleUserID]) REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [FK_Appointment_Venue] FOREIGN KEY ([VenueID]) REFERENCES [dbo].[Venue] ([VenueID])
GO
