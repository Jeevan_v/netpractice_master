CREATE TABLE [dbo].[Mail]
(
[ToEmailId] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[FromEmailID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Subject] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Body] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EmailType] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DateLogged] [datetime] NULL,
[IsSent] [bit] NULL,
[id] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
