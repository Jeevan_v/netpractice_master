CREATE TABLE [dbo].[PaymentDuration]
(
[PaymentDurationID] [int] NOT NULL IDENTITY(1, 1),
[PaymentDuration] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PaymentDuration] ADD CONSTRAINT [PK_PaymentDuration] PRIMARY KEY CLUSTERED  ([PaymentDurationID]) ON [PRIMARY]
GO
