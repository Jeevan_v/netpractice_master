CREATE TABLE [dbo].[UserActivityLog]
(
[UserActivityLogID] [int] NOT NULL IDENTITY(1, 1),
[MembershipID] [uniqueidentifier] NOT NULL,
[LoginTime] [datetime] NULL,
[IPAddress] [nchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Activity] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserActivityLog] ADD CONSTRAINT [PK_UserActivityLog] PRIMARY KEY CLUSTERED  ([UserActivityLogID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserActivityLog] ADD CONSTRAINT [FK_aspnetUsers_UserActivityLog] FOREIGN KEY ([MembershipID]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
