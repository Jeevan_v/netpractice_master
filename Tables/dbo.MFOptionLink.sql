CREATE TABLE [dbo].[MFOptionLink]
(
[MFOptionLinkID] [int] NOT NULL IDENTITY(1, 1),
[MFOptionID] [int] NOT NULL,
[PlanID] [int] NOT NULL,
[MFOptionLinkDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Prefix] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Suffix] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OptionNo] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[OptionLimit] [decimal] (13, 4) NULL,
[POPostal] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POPostalNo] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[POPostOffice] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POCity] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POProvince] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POCountry] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POPostalCode] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PrefSufMask] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[PreAuth] [bit] NULL,
[Note] [ntext] COLLATE Latin1_General_CI_AS NULL,
[Discontinued] [bit] NULL,
[Custom] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MFOptionLink] ADD CONSTRAINT [PK_MFOptionLink] PRIMARY KEY CLUSTERED  ([MFOptionLinkID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MFOptionLink] ADD CONSTRAINT [FK_MFOption_MFOptionLink] FOREIGN KEY ([MFOptionID]) REFERENCES [dbo].[MFOption] ([MFOptionID])
GO
