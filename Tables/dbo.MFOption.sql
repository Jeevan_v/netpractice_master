CREATE TABLE [dbo].[MFOption]
(
[MFOptionID] [int] NOT NULL IDENTITY(1, 1),
[MFOptionName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MFOptionDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MFOption] ADD CONSTRAINT [PK_MFOption] PRIMARY KEY CLUSTERED  ([MFOptionID]) ON [PRIMARY]
GO
