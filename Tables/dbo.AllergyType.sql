CREATE TABLE [dbo].[AllergyType]
(
[AllergyTypeID] [int] NOT NULL IDENTITY(1, 1),
[AllergyType] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllergyType] ADD CONSTRAINT [PK_AllergyType] PRIMARY KEY CLUSTERED  ([AllergyTypeID]) ON [PRIMARY]
GO
