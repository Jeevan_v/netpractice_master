CREATE TABLE [dbo].[PostalCode]
(
[PostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[PostalCode] [int] NULL,
[StreetCode] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostalCode] ADD CONSTRAINT [PK_PostalCode] PRIMARY KEY CLUSTERED  ([PostalCodeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostalCode] ADD CONSTRAINT [IX_PostalCode_StreetCode] UNIQUE NONCLUSTERED  ([PostalCode], [StreetCode]) ON [PRIMARY]
GO
