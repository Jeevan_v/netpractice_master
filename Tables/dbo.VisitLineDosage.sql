CREATE TABLE [dbo].[VisitLineDosage]
(
[VisitLineDosageID] [int] NOT NULL IDENTITY(1, 1),
[Size] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Dose] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DoseForm] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DoseFrequency] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Route/Location] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OtherInstruction] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Time/Period] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VisitLineDosage] ADD CONSTRAINT [PK_VisitLineDosage] PRIMARY KEY CLUSTERED  ([VisitLineDosageID]) ON [PRIMARY]
GO
