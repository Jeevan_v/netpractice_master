CREATE TABLE [dbo].[CSVTail]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[CSVHeader_ID] [int] NOT NULL,
[RecordType] [varchar] (40) COLLATE Latin1_General_CI_AS NULL,
[NumerOfEntries] [int] NULL,
[TotalPaidToServiceProvider] [decimal] (36, 2) NULL
) ON [PRIMARY]
GO
