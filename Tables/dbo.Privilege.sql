CREATE TABLE [dbo].[Privilege]
(
[PrivilegeID] [int] NOT NULL IDENTITY(1, 1),
[Privilege] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PrivilegeLabel] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Privilege] ADD CONSTRAINT [PK_Privilege] PRIMARY KEY CLUSTERED  ([PrivilegeID]) ON [PRIMARY]
GO
