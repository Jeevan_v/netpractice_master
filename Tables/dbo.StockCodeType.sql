CREATE TABLE [dbo].[StockCodeType]
(
[StockCodeTypeID] [int] NOT NULL IDENTITY(1, 1),
[StockCodeType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[StockTypeDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DefinitionTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StockCodeType] ADD CONSTRAINT [PK_StockCodeType] PRIMARY KEY CLUSTERED  ([StockCodeTypeID]) ON [PRIMARY]
GO
