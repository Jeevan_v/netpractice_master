CREATE TABLE [dbo].[T_PaymentArrangement]
(
[PaymentArrangement_ID] [int] NOT NULL IDENTITY(593, 1),
[PaymentArrangementGroup_ID] [int] NULL,
[DetailCode] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[LongDescription] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[DateValidFrom] [datetime] NOT NULL,
[DateValidTo] [datetime] NOT NULL,
[ElixirRateID] [int] NULL,
[CCTGroupID] [int] NULL,
[Iscontract] [nchar] (1) COLLATE Latin1_General_CI_AS NULL,
[Isdefault] [nchar] (1) COLLATE Latin1_General_CI_AS NULL,
[MEDetailCode] [nvarchar] (15) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_PaymentArrangement] ADD CONSTRAINT [PK_MEPaymentArrangement] PRIMARY KEY CLUSTERED  ([PaymentArrangement_ID]) ON [PRIMARY]
GO
