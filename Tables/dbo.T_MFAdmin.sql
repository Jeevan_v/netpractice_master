CREATE TABLE [dbo].[T_MFAdmin]
(
[MFAdmin_ID] [int] NOT NULL IDENTITY(1, 1),
[MFAdminName] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[MFAdminDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PO_Postal] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[PO_PostalNo] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[PO_PostOffice] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[PO_City] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[PO_Province] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[PO_Country] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[PO_PostalCode] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[FaxNo] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[TelNo] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[Prefix] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Custom] [tinyint] NULL,
[Discontinued] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_MFAdmin] ADD CONSTRAINT [PK_T_MFAdmin] PRIMARY KEY CLUSTERED  ([MFAdmin_ID]) ON [PRIMARY]
GO
