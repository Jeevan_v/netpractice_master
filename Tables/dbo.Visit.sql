CREATE TABLE [dbo].[Visit]
(
[VisitID] [int] NOT NULL IDENTITY(1, 1),
[AccountID] [int] NOT NULL,
[VenueID] [int] NOT NULL,
[ChargeTypeID] [int] NOT NULL,
[ChargeCategoryID] [int] NOT NULL,
[SchemeID] [int] NOT NULL,
[PlanID] [int] NOT NULL,
[MembershipNo] [nchar] (15) COLLATE Latin1_General_CI_AS NULL,
[PlaceOfService] [int] NULL,
[AuthorisationCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Attachment] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[DateLogged] [datetime] NULL,
[VisitDate] [datetime] NULL,
[DoctorID] [int] NOT NULL,
[InvoiceNo] [int] NOT NULL,
[ReferringDoctorID] [int] NULL,
[RefPracticeNo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BillTo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Visit] ADD CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED  ([VisitID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_ACCOUNTID_VISITID] ON [dbo].[Visit] ([AccountID]) INCLUDE ([VisitID], [VisitDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_VISIT_DATELOGGED] ON [dbo].[Visit] ([DateLogged]) INCLUDE ([VisitID], [AccountID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_VISIT_VENUE_SCHEME] ON [dbo].[Visit] ([VenueID], [DoctorID]) INCLUDE ([VisitID], [SchemeID], [VisitDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Visit] ADD CONSTRAINT [FK_Visit_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[Visit] ADD CONSTRAINT [FK_Visit_ChargeCategory] FOREIGN KEY ([ChargeCategoryID]) REFERENCES [dbo].[ChargeCategory] ([ChargeCategoryID])
GO
ALTER TABLE [dbo].[Visit] ADD CONSTRAINT [FK_Visit_Venue] FOREIGN KEY ([VenueID]) REFERENCES [dbo].[Venue] ([VenueID])
GO
