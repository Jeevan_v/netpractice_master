CREATE TABLE [dbo].[ModifierCondition]
(
[ModifierConditionId] [int] NOT NULL IDENTITY(1, 1),
[TimedIntervalStart] [int] NULL,
[TimedIntervalEnd] [int] NULL,
[TimedFrequency] [int] NULL,
[TimedUnits] [int] NULL,
[TimedPrice] [decimal] (18, 2) NULL,
[ChargeCodeID] [int] NULL,
[ModifierRuleID] [int] NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_ModifierCondition_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModifierCondition] ADD CONSTRAINT [PK_ModifierCondition] PRIMARY KEY CLUSTERED  ([ModifierConditionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModifierCondition] ADD CONSTRAINT [FK_ModifierCondition_ChargeCode] FOREIGN KEY ([ChargeCodeID]) REFERENCES [dbo].[ChargeCode] ([ChargeCodeID])
GO
ALTER TABLE [dbo].[ModifierCondition] ADD CONSTRAINT [FK_ModifierCondition_ModifierRules] FOREIGN KEY ([ModifierRuleID]) REFERENCES [dbo].[ModifierRules] ([ModifierRuleID])
GO
