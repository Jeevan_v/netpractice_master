CREATE TABLE [dbo].[Module]
(
[ModuleID] [int] NOT NULL IDENTITY(1, 1),
[DisplayName] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[Name] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Version] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Module] ADD CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED  ([ModuleID]) ON [PRIMARY]
GO
