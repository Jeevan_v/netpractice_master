CREATE TABLE [dbo].[CSVHeader]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[RecordType] [varchar] (40) COLLATE Latin1_General_CI_AS NULL,
[BISourceID] [varchar] (40) COLLATE Latin1_General_CI_AS NULL,
[BICode] [varchar] (40) COLLATE Latin1_General_CI_AS NULL,
[BatchNo] [varchar] (40) COLLATE Latin1_General_CI_AS NULL,
[BatchDate] [date] NULL,
[RemittanceNo] [varchar] (40) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
