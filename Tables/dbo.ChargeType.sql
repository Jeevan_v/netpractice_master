CREATE TABLE [dbo].[ChargeType]
(
[ChargeTypeID] [int] NOT NULL IDENTITY(1, 1),
[CategoryID] [int] NOT NULL,
[ChargeType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[StraightCharge] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeType] ADD CONSTRAINT [PK_ChargeType] PRIMARY KEY CLUSTERED  ([ChargeTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeType] ADD CONSTRAINT [FK_Category_ChargeType] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Category] ([CategoryID])
GO
