CREATE TABLE [dbo].[ModifierType]
(
[ModifierTypeID] [int] NOT NULL IDENTITY(1, 1),
[ModifierType] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModifierType] ADD CONSTRAINT [PK_ModifierType] PRIMARY KEY CLUSTERED  ([ModifierTypeID]) ON [PRIMARY]
GO
