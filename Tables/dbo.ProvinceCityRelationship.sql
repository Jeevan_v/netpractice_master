CREATE TABLE [dbo].[ProvinceCityRelationship]
(
[ProvinceID] [int] NOT NULL,
[CityID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProvinceCityRelationship] ADD CONSTRAINT [PK_ProvinceCityRelationship] PRIMARY KEY CLUSTERED  ([ProvinceID], [CityID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProvinceCityRelationship] ADD CONSTRAINT [FK_Province_ProvinceCityRelationship] FOREIGN KEY ([ProvinceID]) REFERENCES [dbo].[Province] ([ProvinceID])
GO
