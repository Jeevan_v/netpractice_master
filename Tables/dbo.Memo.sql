CREATE TABLE [dbo].[Memo]
(
[MemoID] [int] NOT NULL IDENTITY(1, 1),
[VisitLineID] [int] NOT NULL,
[Memo] [text] COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [PK_Memo] PRIMARY KEY CLUSTERED  ([MemoID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Memo] ADD CONSTRAINT [FK_VisitLine_Memo] FOREIGN KEY ([VisitLineID]) REFERENCES [dbo].[VisitLine] ([VisitLineID])
GO
