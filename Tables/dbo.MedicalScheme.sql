CREATE TABLE [dbo].[MedicalScheme]
(
[MedicalSchemeID] [int] NOT NULL IDENTITY(1, 1),
[MFAdminID] [int] NULL,
[SchemeName] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[POPostal] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POPostalNo] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[POPostOffice] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POCity] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POProvince] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POCountry] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[POPostalCode] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[FaxNo] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[TelNo] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[Prefix] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[URL] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PrefProvider] [smallint] NULL,
[MFSchemeDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[StatementLayoutID] [int] NULL,
[InvoiceLayoutID] [int] NULL,
[Discontinued] [bit] NULL,
[Custom] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicalScheme] ADD CONSTRAINT [PK_MedicalScheme_1] PRIMARY KEY CLUSTERED  ([MedicalSchemeID]) ON [PRIMARY]
GO
