CREATE TABLE [dbo].[SubUrbCityPostalCodeRelationship]
(
[SubUrbID] [int] NOT NULL,
[CityID] [int] NOT NULL,
[PostalCodeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SubUrbCityPostalCodeRelationship] ADD CONSTRAINT [PK_SubUbrCityPostalCodeRelationship] PRIMARY KEY CLUSTERED  ([SubUrbID], [CityID], [PostalCodeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SubUrbCityPostalCodeRelationship] ADD CONSTRAINT [FK_SubUbrCityPostalCodeRelationship_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [dbo].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [dbo].[SubUrbCityPostalCodeRelationship] ADD CONSTRAINT [FK_SubUrbCityPostalCodeRelationship_City] FOREIGN KEY ([CityID]) REFERENCES [dbo].[City] ([CityID])
GO
ALTER TABLE [dbo].[SubUrbCityPostalCodeRelationship] ADD CONSTRAINT [FK_SubUrbCityPostalCodeRelationship_SubUrb] FOREIGN KEY ([SubUrbID]) REFERENCES [dbo].[SubUrb] ([SubUrbID])
GO
