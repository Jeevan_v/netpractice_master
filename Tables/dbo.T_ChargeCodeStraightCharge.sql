CREATE TABLE [dbo].[T_ChargeCodeStraightCharge]
(
[ChargeCodeStraightCharge_ID] [int] NOT NULL IDENTITY(4408428, 1),
[ChargeCode_ID] [int] NOT NULL,
[PractitionerType_ID] [int] NOT NULL,
[PaymentArrangementDetail_ID] [int] NOT NULL,
[ValueExclUnrounded] [numeric] (18, 4) NOT NULL,
[ValueExclRounded] [numeric] (18, 4) NOT NULL,
[VatPerc_ID] [int] NOT NULL,
[ValueInclUnrounded] [numeric] (18, 4) NOT NULL,
[ValueInclRounded] [numeric] (18, 4) NOT NULL,
[DateValidFrom] [datetime] NOT NULL,
[DateValidTo] [datetime] NOT NULL,
[RoundingRule_ID] [int] NOT NULL,
[LocationType_ID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[T_ChargeCodeStraightCharge] ADD CONSTRAINT [PK_MEChargeCodeStraightCharge] PRIMARY KEY CLUSTERED  ([ChargeCodeStraightCharge_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_T_CHARGECODESTRAIGHTCHARGE_DATEVALID_TO_FROM] ON [dbo].[T_ChargeCodeStraightCharge] ([ChargeCode_ID], [PractitionerType_ID], [PaymentArrangementDetail_ID], [DateValidFrom], [DateValidTo]) ON [PRIMARY]
GO
