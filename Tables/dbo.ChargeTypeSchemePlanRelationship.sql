CREATE TABLE [dbo].[ChargeTypeSchemePlanRelationship]
(
[MedicalSchemeID] [int] NOT NULL,
[MedicalPlanID] [int] NOT NULL,
[ChargeTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChargeTypeSchemePlanRelationship] WITH NOCHECK ADD CONSTRAINT [FK_ChargeType_ChargeTypeSchemePlanRelationship] FOREIGN KEY ([ChargeTypeID]) REFERENCES [dbo].[ChargeType] ([ChargeTypeID])
GO
ALTER TABLE [dbo].[ChargeTypeSchemePlanRelationship] WITH NOCHECK ADD CONSTRAINT [FK_MedicalPlan_ChargeTypeSchemePlanRelationship] FOREIGN KEY ([MedicalPlanID]) REFERENCES [dbo].[MedicalPlan] ([MedicalPlanID])
GO
ALTER TABLE [dbo].[ChargeTypeSchemePlanRelationship] WITH NOCHECK ADD CONSTRAINT [FK_MedicalScheme_ChargeTypeSchemePlanRelationship] FOREIGN KEY ([MedicalSchemeID]) REFERENCES [dbo].[MedicalScheme] ([MedicalSchemeID])
GO
