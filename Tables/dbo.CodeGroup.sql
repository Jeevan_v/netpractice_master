CREATE TABLE [dbo].[CodeGroup]
(
[CodeGroupID] [int] NOT NULL,
[MFOptionID] [int] NULL,
[StockCodeTypeID] [int] NULL,
[StockPriceTypeID] [int] NULL,
[OwnStockCodeTypeID] [int] NULL,
[CodeGroupDescr] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CodeGroupDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CodeType] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[ChargeVAT] [smallint] NULL,
[OwnPriceMarkup] [numeric] (5, 2) NULL,
[Scriptable] [int] NULL,
[PastelGroup] [int] NULL,
[DefaultChargeCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CodeGroup] ADD CONSTRAINT [PK_CodeGroup] PRIMARY KEY CLUSTERED  ([CodeGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CodeGroup] ADD CONSTRAINT [FK_MFOption_CodeGroup] FOREIGN KEY ([MFOptionID]) REFERENCES [dbo].[MFOption] ([MFOptionID])
GO
ALTER TABLE [dbo].[CodeGroup] ADD CONSTRAINT [FK_StockCodeType_CodeGroup] FOREIGN KEY ([StockCodeTypeID]) REFERENCES [dbo].[StockCodeType] ([StockCodeTypeID])
GO
