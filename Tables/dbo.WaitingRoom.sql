CREATE TABLE [dbo].[WaitingRoom]
(
[WaitingRoomID] [int] NOT NULL IDENTITY(1, 1),
[AccountID] [int] NOT NULL,
[VenueID] [int] NOT NULL,
[DateLogged] [datetime] NOT NULL,
[Time] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[IsWalkIn] [bit] NOT NULL,
[WaitStartTime] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[DoctorID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WaitingRoom] ADD CONSTRAINT [PK_WaitingRoom'] PRIMARY KEY CLUSTERED  ([WaitingRoomID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WaitingRoom] ADD CONSTRAINT [FK__WaitingRo__Docto__24134F1B] FOREIGN KEY ([DoctorID]) REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[WaitingRoom] ADD CONSTRAINT [FK_WaitingRoom_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[WaitingRoom] ADD CONSTRAINT [FK_WaitingRoom_Venue] FOREIGN KEY ([VenueID]) REFERENCES [dbo].[Venue] ([VenueID])
GO
