CREATE TABLE [dbo].[jsClient Data]
(
[accountid] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ accname] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ addrline1] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ addrline2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ city] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ postcode] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ email] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ tel] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ cell] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ fax] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ fileref] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ vatregno] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
