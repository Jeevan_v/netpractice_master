CREATE TABLE [dbo].[StockItem]
(
[StockItemID] [int] NOT NULL IDENTITY(1, 1),
[ManufacturerID] [int] NULL,
[CodeGroupID] [int] NULL,
[StockGroupID] [int] NULL,
[Schedule] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[StockDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Status] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[PrefStockLevel] [int] NULL,
[MinStockLevel] [int] NULL,
[PackSize] [decimal] (9, 2) NULL,
[DefaultDosageID] [int] NULL,
[ATCL5] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ATCL4] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ExactGeneric] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[MarkupPercentage] [decimal] (13, 4) NULL,
[ZeroMarkupPrice] [decimal] (13, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[StockItem] ADD CONSTRAINT [PK_StockItem] PRIMARY KEY CLUSTERED  ([StockItemID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_STOCKITEM_DESCRIPTION] ON [dbo].[StockItem] ([Description]) INCLUDE ([StockItemID], [CodeGroupID], [PackSize]) ON [PRIMARY]
GO
