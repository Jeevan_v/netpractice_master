CREATE TABLE [dbo].[User]
(
[UserID] [int] NOT NULL IDENTITY(1, 1),
[MembershipID] [uniqueidentifier] NOT NULL,
[TitleID] [int] NULL,
[Initials] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[FirstName] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[IDNo] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[MPNo] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[DocPracticeNo] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[JobTitleID] [int] NULL,
[ReasonID] [int] NULL,
[DispenseLicenseNo] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[PracticeTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED  ([UserID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_aspnetUsers_User] FOREIGN KEY ([MembershipID]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_JobTitle_User] FOREIGN KEY ([JobTitleID]) REFERENCES [dbo].[JobTitle] ([JobTitleID])
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_Title_User] FOREIGN KEY ([TitleID]) REFERENCES [dbo].[Title] ([TitleID])
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_User_InActiveReason] FOREIGN KEY ([ReasonID]) REFERENCES [dbo].[InActiveReason] ([ReasonID])
GO
