CREATE TABLE [dbo].[CategoryType]
(
[CategoryTypeID] [int] NOT NULL IDENTITY(1, 1),
[CategoryTypeDesc] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CategoryType] ADD CONSTRAINT [PK_CategoryType] PRIMARY KEY CLUSTERED  ([CategoryTypeID]) ON [PRIMARY]
GO
