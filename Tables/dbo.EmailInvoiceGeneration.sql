CREATE TABLE [dbo].[EmailInvoiceGeneration]
(
[EmailInvoiceGenerationId] [int] NOT NULL IDENTITY(1, 1),
[InvoiceNo] [int] NULL,
[ClaimId] [int] NULL,
[VisitDate] [datetime] NULL,
[DateLogged] [datetime] NULL,
[SchemeName] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[MedicalAidEmail] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[FromEmail] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[IsResend] [bit] NULL,
[GeneratedDate] [datetime] NULL,
[Status] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[IsSuccess] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailInvoiceGeneration] ADD CONSTRAINT [PK_EmailInvoiceGeneration] PRIMARY KEY CLUSTERED  ([EmailInvoiceGenerationId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_EMAILINVOICEGEN_CLAIMID] ON [dbo].[EmailInvoiceGeneration] ([ClaimId]) INCLUDE ([DateLogged], [SchemeName], [MedicalAidEmail]) ON [PRIMARY]
GO
