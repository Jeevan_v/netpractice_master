CREATE TABLE [dbo].[Menu]
(
[MenuID] [int] NOT NULL IDENTITY(1, 1),
[DisplayName] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[NavigateURL] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ParentID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Menu] ADD CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED  ([MenuID]) ON [PRIMARY]
GO
