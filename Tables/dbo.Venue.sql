CREATE TABLE [dbo].[Venue]
(
[VenueID] [int] NOT NULL IDENTITY(1, 1),
[PracticeID] [int] NOT NULL,
[VenueName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Telephone] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[Fax] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_Venue_IsActive] DEFAULT ((1)),
[PhysicalAddressID] [int] NULL,
[PostalAddressID] [int] NULL,
[ReasonID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Venue] ADD CONSTRAINT [PK_Venue] PRIMARY KEY CLUSTERED  ([VenueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Venue] ADD CONSTRAINT [FK_Practice_Venue] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
GO
ALTER TABLE [dbo].[Venue] ADD CONSTRAINT [FK_Venue_Address_Physical] FOREIGN KEY ([PhysicalAddressID]) REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Venue] ADD CONSTRAINT [FK_Venue_Address_Postal] FOREIGN KEY ([PostalAddressID]) REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Venue] ADD CONSTRAINT [FK_Venue_IsActiveReasonRelationships] FOREIGN KEY ([ReasonID]) REFERENCES [dbo].[InActiveReason] ([ReasonID])
GO
ALTER TABLE [dbo].[Venue] ADD CONSTRAINT [FK_Venue_Venue] FOREIGN KEY ([VenueID]) REFERENCES [dbo].[Venue] ([VenueID])
GO
