CREATE TABLE [dbo].[Package]
(
[PackageID] [int] NOT NULL IDENTITY(1, 1),
[Package] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Package] ADD CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED  ([PackageID]) ON [PRIMARY]
GO
