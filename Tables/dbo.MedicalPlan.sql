CREATE TABLE [dbo].[MedicalPlan]
(
[MedicalPlanID] [int] NOT NULL IDENTITY(1, 1),
[MedicalSchemeID] [int] NULL,
[AccountGroupID] [int] NULL,
[TypeID] [int] NULL,
[CategoryID] [int] NULL,
[DiagnosisCategoryID] [int] NULL,
[ChargeTypeID] [int] NULL,
[PlanName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Prefix] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MFPlanDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[NetPartner] [tinyint] NULL,
[Discontinued] [bit] NULL,
[Custom] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicalPlan] ADD CONSTRAINT [PK_MedicalPlan] PRIMARY KEY CLUSTERED  ([MedicalPlanID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicalPlan] ADD CONSTRAINT [FK_Category_MedicalPlan] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Category] ([CategoryID])
GO
