CREATE TABLE [dbo].[DiagnosisType]
(
[DiagnosisTypeID] [int] NOT NULL,
[TypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TypeDefinition] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DiagnosisType] ADD CONSTRAINT [PK_DiagnosisType] PRIMARY KEY CLUSTERED  ([DiagnosisTypeID]) ON [PRIMARY]
GO
