CREATE TABLE [dbo].[UnitCharge]
(
[UnitChargeID] [int] NOT NULL IDENTITY(1, 1),
[ChargeGroupID] [int] NOT NULL,
[ChargeTypeID] [int] NOT NULL,
[CategoryID] [int] NOT NULL,
[UnitCharge] [decimal] (13, 4) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UnitCharge] ADD CONSTRAINT [PK_UnitCharge] PRIMARY KEY CLUSTERED  ([UnitChargeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UnitCharge] ADD CONSTRAINT [FK_ChargeGroup_UnitCharge] FOREIGN KEY ([ChargeGroupID]) REFERENCES [dbo].[ChargeGroup] ([ChargeGroupID])
GO
